#!/usr/bin/env python
# encoding: utf-8
import subprocess
import time
from filecmp import dircmp
from pathlib import Path

from might.common.miscellaneous import quit_with_error


def ramdisk_handler(logger, mount=False, unmount=False, ramdisk_directory=None, directory_to_load=None):
    """
    RAMDISK. RAMDISK.

    All hail the SPPPEEEEEEEED.

    This method will attempt to mount or unmount a ramdisk for use in speeding up various aspects of the the selected
    analyses.

    :param mount: if mount is True, we will attempt to mount the ramdisk
    :param unmount: if unmount is True, we will attempt to unmount the ramdisk
    :param ramdisk_directory: if a path is provided, we will verify it exists and use it
    :param directory_to_load:  if a path is provided, we will verify it exists and use it
    :return: True if the requested action was accomplished (mount or unmount), False if not
    """

    if (mount and (not ramdisk_directory or not directory_to_load)) or (unmount and not ramdisk_directory):
        return False

    logger.log('Checking the current status of the ramdisk\n')

    # verify that either mount or unmount are true (otherwise return False for impossible request)
    if (mount and unmount) or (not mount and not unmount):
        quit_with_error(logger, 'An illegal request was submitted to the ramdisk handler: mount = {} and unmount = {}'.format(mount, unmount))

    # verify that the ramdisk directory exists
    if ramdisk_directory:
        ramdisk_directory = Path(ramdisk_directory).expanduser().resolve()
        if not ramdisk_directory.is_dir():
           quit_with_error(logger, 'Unable to locate the ramdisk directory at the specified path')

    # verify that the directory_to_load exists
    if directory_to_load:
        directory_to_load = Path(directory_to_load).expanduser().resolve()
        if not directory_to_load.is_dir():
            quit_with_error(logger, 'Unable to locate the directory for the ramdisk at the specified path')
        else:
            logger.log('The directory to be loaded was located at: {}\n'.format(str(directory_to_load)))

    # verify whether or not the ramdisk is mounted currently. This is accomplished by running
    # 'df -h | grep [ramisk_directory]' and capturing the stdout from the terminal. If the ramdisk is mounted the length
    # of the stdout and by extension mount_status will be non-zero.
    df_proc = str(subprocess.run(['df', '-h'], stdout=subprocess.PIPE).stdout)
    if str(ramdisk_directory) in df_proc:
        logger.log('The ramdisk is currently mounted\n')
        mount_status = True
    else:
        mount_status = False

    # if mount was requested, handle the mounting and loading of the ramdisk as necessary
    if mount:

        # mount the ramdisk is it is not currently mounted
        if not mount_status:
            logger.log('The ramdisk is not currently mounted. Mounting now...')
            # mount the ramdisk
            subprocess.run(['mount', str(ramdisk_directory)])
            # use the mount_status method above to verify successful mounting
            df_proc = str(subprocess.run(['df', '-h'], stdout=subprocess.PIPE).stdout)
            if str(ramdisk_directory) in df_proc:
                logger.log('The ramdisk has been successfully mounted!\n')
            else:
                quit_with_error(logger, 'Unable to mount the ramdisk')

        # check to see if the database directory is currently loaded
        logger.log('Checking to see if the database directory has already been loaded on to the ramdisk...')
        mounted_directory = ramdisk_directory / directory_to_load.name
        if mounted_directory.exists():
            # There are two possible scenarios where MIGHT might find that the database is already present on the
            # ramdisk. Possibility #1: Another application has already loaded the required files onto the ramdisk
            # completely. In this case we can just use it. Possibility #2: Another application IS IN THE PROCESS of
            # loading the required files. In this case we will sleep for 30s and then check again to see if the files
            # are fully loaded
            waiting_to_load = True
            while waiting_to_load:
                dcmp = dircmp(str(mounted_directory), str(directory_to_load))
                # Possibility #1: Another application has already loaded the required files onto the ramdisk
                if len(dcmp.right_only) == 0 and len(dcmp.left_only) == 0 and len(dcmp.diff_files) == 0:
                    logger.log('The ramdisk is mounted, the database is loaded, time to boogie!\n')
                    return True
                # Possibility  # 2: Another application IS IN THE PROCESS of loading the required files. In this case
                # we will sleep for 30s and then check again to see if the files are fully loaded
                else:
                    logger.log('The ramdisk is currently being loaded by another application. Waiting...!\n')
                    time.sleep(30)

        # load the database directory on to the ramdisk
        logger.log('Database directory has NOT been loaded to the ramdisk.\n')
        logger.log('Copying the database directory contents to the ramdisk...')
        subprocess.run(['cp', '-r', str(directory_to_load), str(ramdisk_directory)])

        time.sleep(1)

        # verify that the directory was loaded successfully
        dcmp = dircmp(str(mounted_directory), str(directory_to_load))
        if len(dcmp.right_only) > 0:
            quit_with_error(logger, 'The ramdisk failed to load the full complement of files from the database directory')
        if len(dcmp.diff_files) > 0:
            quit_with_error(logger, 'Discrepancies exist between the files in the database directory and the ramdisk')

        logger.log('Database directory successfully loaded to ramdisk. Ready for use!\n')

        return True

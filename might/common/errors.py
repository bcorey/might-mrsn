#!/usr/bin/env python
# encoding: utf-8

import re


class BadPathError(Exception):
    """Raise this if the path provided by the user is no good"""
    
    def __init__(self, path, path_description, context=None):

        self.path = path  # the bad path that caused the exception
        self.path_description = path_description  # the description of the expected path
        self.context = context  # additional context describing the path and/or requirements
        self.header = 'Error: the following user provided file path is invalid'
    
    def __str__(self):
        if self.context:
            return bold_red('\n{}\npath: {}\npath description: {}\ncontext: {}\n'.format(self.header, str(self.path), self.path_description, self.context))
        else:
            return bold_red('\n{}\npath: {}\npath description: {}\n'.format(self.header, str(self.path), self.path_description))


class MissingPathError(Exception):
    """Raise this if a required path is missing"""
    
    def __init__(self, path_description, context=None):

        self.path_description = path_description  # the description of the expected path
        self.context = context  # additional context describing the path and/or requirements
        self.header = 'Error: the following file was not located'
    
    def __str__(self):
        if self.context:
            return bold_red('\n{}\npath description: {}\ncontext: {}\n'.format(self.header, self.path_description, self.context))
        else:
            return bold_red('\n{}\npath description: {}\n'.format(self.header, self.path_description))


class MissingPrerequisiteError(Exception):
    """Raise this if a required analysis step hasn't been completed """
    
    def __init__(self, prerequisite, context=None):

        self.prerequisite = prerequisite  # the description of the unfufilled prerequisite
        self.context = context  # additional context describing the path and/or requirements
        self.header = 'Error: the following prerequisite of the requested analysis has not been met'
    
    def __str__(self):
        if self.context:
            return bold_red('\n{}\nprerequisite: {}\ncontext: {}\n'.format(self.header, self.prerequisite, self.context))
        else:
            return bold_red('\n{}\nprerequisite: {}\n'.format(self.header, self.prerequisite))


class CheckpointFileInconsistentError(Exception):
    """Raise this if a stage in a checkpoint file does not agree with the expected output files"""
    
    def __init__(self, checkpoint_file, stage, files, context=None):

        self.checkpoint_file = str(checkpoint_file)  # the path to the checkpoint file in question
        self.stage = stage  # the checkpoint file stage that is problematic
        self.files = '\n'.join(files)
        self.header = 'Error: the following checkpoint file indicates that the listed stage is complete, however the output file(s) listed are missing'
    
    def __str__(self):
        return bold_red('\n{}\ncheckpoint file: {}\nstage: {}\nfile(s): {}'.format(self.header, self.checkpoint_file, self.stage, self.files))


class NothingToDoError(Exception):
    """Raise this if there is no new input to process"""
    
    def __init__(self, message, context=''):

        self.message = message  # the path to the checkpoint file in question
        self.context = context
    
    def __str__(self):
        return bold_red('\nThere is nothing to process in: {}\n{}'.format(self.message, self.context))


class InvalidInputError(Exception):
    """
    Error is thrown when one or more of the input DataFrames is deemed invalid
    """

    def __init__(self, dataframes, message):

        self.dataframes = ', '.join(dataframes)
        self.message = message
    
    def __str__(self):
        return '\nThe following dataframe is invalid for analysis: {}\n{}'.format(self.dataframes, self.message)


class SampleNotFoundError(Exception):
    """
    Error is thrown when a sample is not located in the provided dataframe
    """

    def __init__(self, sample, dataframe, message):

        self.sample = sample
        self.dataframe = dataframe
        self.message = message
    
    def __str__(self):
        return '\nSample {} was not located in {} \n{}'.format(self.sample, self.dataframe, self.message)


class DuplicateEntriesFoundError(Exception):
    """
    Error is thrown when a sample that should have only one corresponding entry in the dataframe has multiple
    """

    def __init__(self, sample, dataframe, message):

        self.sample = sample
        self.dataframe = dataframe
        self.message = message
    
    def __str__(self):
        return '\nDuplicate entries for sample {} were located in {} \n{}'.format(self.sample, self.dataframe, self.message)


class IsolateMetadataInvalidError(Exception):
    """Exception raised when there is an issue with the user-designated isolate metadata file"""

    def __init__(self, specific_error):

        column_list = ['Sample ID', 'ST', 'Project Code', 'Shipment', 'Patient ID', 'CHCS #', 'Collection Date', 
                   'Isolation Source', 'Organism ID', 'Sequencing ID', 'Submitter ID', 'Submitting Facility',
                   'Ward']

        self.specific_error = specific_error
        self.helper_message = 'Reminder: the required columns for the isolate metadata sheet are:\n{}'.format('\n'.join(column_list))
    
    def __str__(self):
        return '\n{}\n\n{}'.format(bold_red(self.specific_error), self.helper_message)


class ClusterTrackingNotPossibleError(Exception):
    """Exception raised when the user has requested that historical cluster tracking be performed but doing so isn't feasible"""

    def __init__(self, specific_error):
        self.specific_error = specific_error
        self.helper_message = 'In order to enable cluster tracking, this tool requires that the output directories and file names be formatted in particular ways.\n\n'\
            'First, the name of the output directory MUST have the same name as the first part of the distance matrix file. So for a distance matrix file'\
            '\n\n  Abaumannii.distance_matrix.xlsx\n\nthe output directory structure must be:\n\n  <output directory specified at the command line>/Abaumannii/'\
            '\n\nWithin the Abaumannii directory you will have one or more time stamped directories of the form:\n\n  <time stamp>.cluster.might\n\nWithin which'\
            ' there MUST be a file formatted as:\n\n  <time stamp>.cluster_analysis.might\n\nThis is the file that will be used to perform cluster tracking'\
            ' operations.\n\nMIGHT will automatically select the most recent directory that contains a suitably named cluster_analysis file.'\
    
    def __str__(self):
        return '\n{}\n\n{}'.format(bold_red(self.specific_error), self.helper_message)


class ClusterAnalysisInvalid(Exception):
    """Exception raised when the user has specified an analysis that is invalid (components are missing)"""

    def __init__(self, specific_error):
        self.specific_error = specific_error
    
    def __str__(self):
        return '\n{}'.format(bold_red(self.specific_error))



END_FORMATTING = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'
YELLOW = '\033[93m'
DIM = '\033[2m'
RED = '\033[31m'
GREEN = '\033[32m'


def green(text):
    return GREEN + text + END_FORMATTING


def underline(text):
    return UNDERLINE + text + END_FORMATTING


def bold_green(text):
    return GREEN + BOLD + text + END_FORMATTING


def yellow(text):
    return YELLOW + text + END_FORMATTING


def bold_yellow(text):
    return YELLOW + BOLD + text + END_FORMATTING


def red(text):
    return RED + text + END_FORMATTING


def bold_red(text):
    return RED + BOLD + text + END_FORMATTING


def bold(text):
    return BOLD + text + END_FORMATTING


def bold_yellow_underline(text):
    return YELLOW + BOLD + UNDERLINE + text + END_FORMATTING


def dim(text):
    return DIM + text + END_FORMATTING


def remove_formatting(text):
    return re.sub('\033.*?m', '', text)


def remove_dim_formatting(text):
    return re.sub('\033\[2m', '', text)
    
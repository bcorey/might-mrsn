#!/usr/bin/env python
# encoding: utf-8

import argparse

import might
from might.common import log
from might.common.miscellaneous import ascii_art

def parse_arguments(__author__, __version__):
    """
    capture user instructions from CLI. What options are available is defined by the passed kwarg entrypoint
    """

    ascii_art(logo='might', author=__author__, version=__version__)

    ####################################################################################################################
    #   Begin parser definition
    ####################################################################################################################

    parser = argparse.ArgumentParser(prog='might',
                                     usage='might <command> [options] <input>',
                                     description=log.bold_yellow('MRSN Integrated Genome Handling Tool\n'),
                                     )

    subparsers = parser.add_subparsers(title='Available commands', help='', metavar='')

    ####################################################################################################################
    #   bcl2fastq
    ####################################################################################################################
    subparser_bcl2fastq = subparsers.add_parser('bcl2fastq',
                                              help='Use bcl2fastq to generate sample read files',
                                              usage='might bcl2fastq [options] <output_directory> <run_directory> <sample_sheet> <run ID>',
                                              description=log.bold_yellow(
                                                  'command "bcl2fastq" uses bcl2fastq to generate sample read files. It '
                                                  'will report the resultant reads with their sizes to the terminal'))

    subparser_bcl2fastq.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_bcl2fastq.add_argument('run_directory', type=str, help='path to illumina run output directory')
    subparser_bcl2fastq.add_argument('sample_sheet', type=str, help='path to the SampleSheet.csv used for sample demultiplexing')
    subparser_bcl2fastq.add_argument('run', type=str, help='the run name associated with these samples (i.e. MISEQ-RH123)')

    optional_bcl2fastq_args = subparser_bcl2fastq.add_argument_group('Optional Arguments')
    optional_bcl2fastq_args.add_argument('--cores', type=int, default=1,help='number of cores to use during sample processing')
    optional_bcl2fastq_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_bcl2fastq_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')
    optional_bcl2fastq_args.add_argument('--pbs', action='store_true', help='run the analysis as a pbs batch job')

    subparser_bcl2fastq.set_defaults(func=might.run.bcl2fastq)
    subparser_bcl2fastq.set_defaults(entry_point='bcl2fastq')

    ####################################################################################################################
    #   kraken2
    ####################################################################################################################
    subparser_kraken2 = subparsers.add_parser('kraken2',
                                              help='Perform kraken2 analysis for sample read files',
                                              usage='might kraken2 [options] <output_directory> <reads_directory> <kraken2_database_directory',
                                              description=log.bold_yellow('command "kraken2" performs kraken2 analysis for samples from read files'))

    subparser_kraken2.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_kraken2.add_argument('reads', type=str, help='path to the directory where the reads files are stored')
    subparser_kraken2.add_argument('kraken2_database', type=str, help='path to the directory housing the kraken2 database files')

    optional_kraken2_args = subparser_kraken2.add_argument_group('Optional Arguments')
    optional_kraken2_args.add_argument('--ramdisk', type=str, help='path to the ramdisk where the kraken2 ramdisk is/will be mounted')
    optional_kraken2_args.add_argument('--sample-list', type=str, help='path to the file containing the list of samples to be analyzed [autodetect]')
    optional_kraken2_args.add_argument('--cores', type=int, default=1, help='number of cores to use during sample processing')
    optional_kraken2_args.add_argument('--cores-per-sample', type=int, default=1, help='number of cores to allocate to each analysis during sample processing')
    optional_kraken2_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_kraken2_args.add_argument('--no-clean', action='store_true', help='omit the compression of read files after analysis.')
    optional_kraken2_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')
    optional_kraken2_args.add_argument('--pbs', type=int, choices=[2, 3, 4, 5, 6, 7, 8, 9], help='run the analysis as a pbs batch job ON THE DESIGNATED NODE')

    subparser_kraken2.set_defaults(func=might.run.kraken2)
    subparser_kraken2.set_defaults(entry_point='kraken2')

    ####################################################################################################################
    #   call-id
    ####################################################################################################################
    subparser_species_caller = subparsers.add_parser('call-id',
                                              help='Make species/contamination calls from kraken reports',
                                              usage='might call-id [options] <output_directory>',
                                              description=log.bold_yellow('command "call-id" performs interactive species calling/contamination detection analysis for samples using the results from kraken2 analysis'))

    subparser_species_caller.add_argument('output', type=str, help='path to the directory where output is/will be stored')

    optional_species_caller_args = subparser_species_caller.add_argument_group('Optional Arguments')
    optional_species_caller_args.add_argument('--sample-list', type=str, help='path to the file containing the list of samples to be analyzed [autodetect]')
    optional_species_caller_args.add_argument('--auto', action='store_true', help='auto accept clean genus species hits without displaying to the terminal')
    optional_species_caller_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_species_caller_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2, 3], help='the level of reporting done to the terminal window [1]')

    subparser_species_caller.set_defaults(func=might.run.speciesCaller)
    subparser_species_caller.set_defaults(entry_point='species-caller')

    ####################################################################################################################
    #   assembly (with shovill as of v1.3.0)
    ####################################################################################################################
    subparser_assembly = subparsers.add_parser('assembly',
                                              help='de novo assembly from illumina paired-end read files',
                                              usage='might assembly [options] <output_directory> <reads_directory> <run ID>',
                                              description=log.bold_yellow('command "assembly" performs read trimming and assembly using bbduk and shovill'))

    subparser_assembly.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_assembly.add_argument('reads', type=str, help='path to the directory where the reads files are stored')
    subparser_assembly.add_argument('run', type=str, help='the run name associated with these samples (i.e. MISEQ-RH123)')

    optional_assembly_args = subparser_assembly.add_argument_group('Optional Arguments')
    optional_assembly_args.add_argument('--sample-list', type=str, default=None, help='path to the file containing the list of samples to be analyzed [autodetect]')
    optional_assembly_args.add_argument('--cores', type=int, default=1, help='number of cores to use during sample processing')
    optional_assembly_args.add_argument('--cores-per-sample', type=int, default=1, help='number of cores to allocate to each analysis during sample processing')
    optional_assembly_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_assembly_args.add_argument('--no-clean', action='store_true', help='omit the compression of read files after analysis.')
    optional_assembly_args.add_argument('--keep', type=int, choices=[1, 2], default=2, help='Level of intermediate files to be retained after the amr completes, 0 is the least and 2 is the most [2]')
    optional_assembly_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2, 3], help='the level of reporting done to the terminal window [1]')
    optional_assembly_args.add_argument('--pbs', action='store_true', help='run the analysis as a pbs batch job')
    optional_assembly_args.add_argument('--length-filter', type=int, default=200, help='the minimum contig length to keep')
    optional_assembly_args.add_argument('--coverage-filter', type=int, default=10, help='the default average coverage floor for contigs')
    optional_assembly_args.add_argument('--timeout', type=int, default=8, help='the maximum number of hours to attempt assembly for')

    subparser_assembly.set_defaults(func=might.run.assembly)
    subparser_assembly.set_defaults(entry_point='assembly')

    ####################################################################################################################
    #   metadata
    ####################################################################################################################
    subparser_metadata = subparsers.add_parser('metadata',
                                               help='Import metadata for samples',
                                               usage='might metadata [options] <output_directory> <metadata_file>',
                                               description=log.bold_yellow('command "metadata" imports metadata for samples'))

    subparser_metadata.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_metadata.add_argument('metadata_file', type=str, help='path to the metadata file')

    optional_metadata_args = subparser_metadata.add_argument_group('Optional Arguments')
    optional_metadata_args.add_argument('--sample-list-file', type=str, help='path to the file containing the list of samples to be analyzed [autodetect]')
    optional_metadata_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_metadata_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')

    subparser_metadata.set_defaults(func=might.run.metadata)
    subparser_metadata.set_defaults(entry_point='metadata')

    ####################################################################################################################
    #   amr
    ####################################################################################################################
    subparser_amr = subparsers.add_parser('amr',
                                          help='Perform amr analysis for samples using reads/assemblies/both',
                                          usage='might amr [options] <output_directory> <run ID>',
                                          description=log.bold_yellow(
                                              'command "amr" performs amr analysis for samples from reads/assemblies/both'))

    subparser_amr.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_amr.add_argument('run', type=str, help='the run name associated with these samples (i.e. MISEQ-RH123)')

    optional_amr_args = subparser_amr.add_argument_group('Optional Arguments')
    optional_amr_args.add_argument('--reads', type=str, help='path to the directory where the read files are stored')
    optional_amr_args.add_argument('--assembly', type=str, help='path to the directory where the assembly files are stored')
    optional_amr_args.add_argument('--analysis-type', type=str, choices=['reads', 'assembly', 'combination'], default='combination', help='requested analysis type [combination]')
    optional_amr_args.add_argument('--sample-list-file', type=str, default=None, help='path to the file containing the list of samples to be analyzed [autodetect]')
    optional_amr_args.add_argument('--cores', type=int, default=1, help='number of cores to use during sample processing')
    optional_amr_args.add_argument('--cores-per-sample', type=int, default=1, help='number of cores to allocate to each analysis during sample processing')
    optional_amr_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_amr_args.add_argument('--no-clean', action='store_true', help='omit the compression of read files after analysis.')
    optional_amr_args.add_argument('--keep', type=int, choices=[1, 2], default=2, help='Level of intermediate files to be retained after the amr completes, 0 is the least and 2 is the most [2]')
    optional_amr_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2, 3], help='the level of reporting done to the terminal window [1]')
    optional_amr_args.add_argument('--pbs', action='store_true', help='run the analysis as a pbs batch job')
    optional_amr_args.add_argument('--summary-only', action='store_true', help='summarize amr reports for the output directory samples')

    subparser_amr.set_defaults(func=might.run.amr)
    subparser_amr.set_defaults(entry_point='amr')

    ####################################################################################################################
    #   mlst
    ####################################################################################################################
    subparser_mlst = subparsers.add_parser('mlst',
                                          help='Perform mlst analysis for samples using assembly files',
                                          usage='might mlst [options] <output_directory> <assembly_directory',
                                          description=log.bold_yellow(
                                              'command "mlst" performs mlst analysis for samples from assembly files'))

    subparser_mlst.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_mlst.add_argument('assembly', type=str, help='path to the directory where sample assembly files are stored')

    optional_mlst_args = subparser_mlst.add_argument_group('Optional Arguments')
    optional_mlst_args.add_argument('--sample-list', type=str, help='path to the file containing the list of samples to be analyzed [autodetect]')
    optional_mlst_args.add_argument('--cores', type=int, default=1, help='number of cores to use during sample processing')
    optional_mlst_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_mlst_args.add_argument('--keep', type=int, choices=[1, 2], default=2, help='Level of intermediate files to be retained after the mlst completes, 0 is the least and 2 is the most [2]')
    optional_mlst_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')
    optional_mlst_args.add_argument('--pbs', action='store_true', help='run the analysis as a pbs batch job')

    subparser_mlst.set_defaults(func=might.run.mlst)
    subparser_mlst.set_defaults(entry_point='mlst')

    ####################################################################################################################
    #   QC
    ####################################################################################################################
    subparser_qc = subparsers.add_parser('qc',
                                           help='Summarize the available QC data',
                                           usage='might qc [options] <output_directory>',
                                           description=log.bold_yellow(
                                               'command "qc" summarizes the available qc information'))

    subparser_qc.add_argument('output', type=str, help='path to the directory where output is/will be stored')

    optional_qc_args = subparser_qc.add_argument_group('Optional Arguments')
    optional_qc_args.add_argument('--sample-list', type=str, help='path to the file containing the list of samples to be analyzed [autodetect]')
    optional_qc_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')

    subparser_qc.set_defaults(func=might.run.qc)
    subparser_qc.set_defaults(entry_point='qc')

    ####################################################################################################################
    #   cluster
    ####################################################################################################################
    subparser_cluster = subparsers.add_parser('cluster',
                                              help='Perform automated cluster detection and generate potential transmission iTOL dataset',
                                              usage='cobra-kai cluster [options] <output_directory> <distance_matrix_directory>',
                                              description=log.bold_yellow(
                                                  'Converts the distance matrix output of a cgMLST '
                                                  'comparison table into the potential '
                                                  'transmission connections iTOL dataset. Also '
                                                  'performs potential transmission/outbreak '
                                                  'cluster detection/reporting'))

    subparser_cluster.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_cluster.add_argument('input', type=str, help='path to the input directory where the distance matrix(s) are stored')

    user_info_args = subparser_cluster.add_argument_group('User Info')
    user_info_args.add_argument('-u', '--username', type=str, required=True, help='Your patient db username')
    user_info_args.add_argument('-p', '--password', type=str, required=True, help='Your patient db password')

    optional_cluster_args = subparser_cluster.add_argument_group('Optional Arguments')
    optional_cluster_args.add_argument('--auth', type=str, default='bi-db', help='The patient DB auth source to use, if not using the default')
    optional_cluster_args.add_argument('--db-uri', type=str, default='10.8.0.5:27017/bi-db', help='The patient DB URI string to use, if not using the default')
    optional_cluster_args.add_argument('--threshold', type=float, default=15, help='maximum allelic distance between adjacent isolates that constitutes a potential transmission event [15]')
    optional_cluster_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_cluster_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2, 3], help='the level of reporting done to the terminal window [1]')

    subparser_cluster.set_defaults(func=might.run.cluster)
    subparser_cluster.set_defaults(entry_point='cluster')

    ####################################################################################################################
    #   update
    ####################################################################################################################
    subparser_update = subparsers.add_parser('update',
                                          help='Update the AMR and MLST databases',
                                          usage='might update [options]',
                                          description=log.bold_yellow(
                                              'command "update" updates the the AMR and MLST databases'))

    optional_update_args = subparser_update.add_argument_group('Optional Arguments')
    optional_update_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2],
                                   help='the level of reporting done to the terminal window [1]')

    subparser_update.set_defaults(func=might.run.update)
    subparser_update.set_defaults(entry_point='update')

    ####################################################################################################################
    #   version
    ####################################################################################################################
    subparser_version = subparsers.add_parser('version',
                                              help='Get versions and exit',
                                              usage='might version',
                                              description='returns the version of the installed might package')

    subparser_version.set_defaults(func=might.common.miscellaneous.get_version)
    subparser_version.set_defaults(version=__version__)

    ####################################################################################################################
    #
    # Collect args from parser
    #
    ####################################################################################################################

    args = parser.parse_args()

    # turn on debug messaging if verbosity == 3
    if hasattr(args, 'verbosity'):
        if getattr(args, 'verbosity') == 3:
            setattr(args, 'debug', True)
        else:
            setattr(args, 'debug', False)

    # launch analysis
    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.print_help()

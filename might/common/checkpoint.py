#!/usr/bin/env python3
# encoding: utf-8
import json
import subprocess

from might.common.miscellaneous import file_check, quit_with_error


class CheckPoint:

    def __init__(self, name, analysis_type, logger, checkpoint_directory, force=False):
        self.name = str(name)
        self.logger = logger

        # type defines the nature of what is being tracked. It can be a sample or a reference. The content of the
        # checkpoint object and the associated checkpoint JSON file are governed by the type
        try:
            assert analysis_type in ['kraken2', 'amr', 'mlst', 'assembly']
            self.analysis_type = str(analysis_type)
        except AssertionError:
            quit_with_error(logger, 'Error: the value for type passed to CheckPoint must be either kraken2, amr, or mlst, not {}'.format(str(analysis_type)))

        # checkpoint directory stores the path to the directory where the checkpoint file is stored
        self.checkpoint_directory = checkpoint_directory

        # using initialize_checkpoint_file() check to see if the checkpoint file already exists or if a new one needs
        # to be created
        self.checkpoint_file = self.initialize_checkpoint_file(force=force)

        # perform an initial read of the checkpoint_file to set the self.checkpoint_dict
        self.checkpoint_dict = None
        self.read_checkpoint_file()

    def initialize_checkpoint_file(self, force=False):

        checkpoint_file = self.checkpoint_directory / ('{}.{}.checkpoint.JSON'.format(self.name, self.analysis_type))

        # check to see if the checkpoint file exists
        try:
            assert not file_check(file_path=checkpoint_file, file_or_directory='file')
        except AssertionError:
            if not force:
                return checkpoint_file
            else:
                subprocess.run(['rm', str(checkpoint_file)])

        # make the checkpoint directory if it doesn't already exist
        if not self.checkpoint_directory.is_dir():
            subprocess.run(['mkdir', '-p', str(self.checkpoint_directory)])

        if self.analysis_type == 'kraken2':

            # create the skeleton JSON content for this isolate
            checkpoint_dict = {
                'kraken report generated': None,
                'kraken report parsed': None,
            }

        if self.analysis_type == 'assembly':

            # create the skeleton JSON content for this isolate
            checkpoint_dict = {
                'reads preprocessed': None,
                'read headers adjusted': None,
                'assembly': None,
                'assembly postprocessing': None,
            }

        if self.analysis_type == 'amr':
            # create the skeleton JSON content for this isolate
            checkpoint_dict = {
                'ariba status': None,
                'amrfinder for reads': None,
                'amrfinder for assembly': None,
                'filtered summary': None
            }

        if self.analysis_type == 'mlst':
            # create the skeleton JSON content for this isolate
            checkpoint_dict = {
                'mlst analysis': None
            }

        # write the skeleton JSON to the checkpoint file
        with open(checkpoint_file, 'w') as chkpnt_file:
            json.dump(checkpoint_dict, chkpnt_file)

        # verify that the JSON file was successfully created
        try:
            assert file_check(file_path=checkpoint_file, file_or_directory='file')
            return checkpoint_file
        except AssertionError:
            quit_with_error(self.logger, 'failed to create the json checkpoint file for ' + self.name)

    def read_checkpoint_file(self):
        # check to see if the checkpoint file exists
        try:
            assert file_check(file_path=self.checkpoint_file, file_or_directory='file')
        except AssertionError:
            quit_with_error(self.logger, 'Checkpoint file for ' + self.name + ' cannot be located!')
            raise

        # attempt to read in the checkpoint file JSON
        try:
            with open(self.checkpoint_file, 'r') as chkpnt_file:
                checkpoint_data = json.load(chkpnt_file)
        except json.JSONDecodeError:
            self.logger.log('There was a problem reading the checkpoint file. In the interest of fidelity this analysis'
                            ' will be reinitialized with a new checkpoint file', verbosity=3)
            self.initialize_checkpoint_file(force=True)

        # parse checkpoint_data to update the checkpoint statuses
        setattr(self, 'checkpoint_dict', checkpoint_data)

    def update_checkpoint_file(self, analysis_step=None, new_status='not provided'):
        # Validate passed kwargs
        try:
            assert analysis_step is not None
        except AssertionError:
            quit_with_error(self.logger, 'update_checkpoint_file() requires an analysis step be specified')

        try:
            assert analysis_step in list(self.checkpoint_dict.keys())
        except AssertionError:
            quit_with_error(self.logger,
                            'update_checkpoint_file() was passed an invalid analysis step: ' + analysis_step)

        try:
            assert new_status in [
                None,
                'complete',
                'failed'
            ]
        except AssertionError:
            quit_with_error(self.logger,
                            'update_checkpoint_file() was passed an invalid new status: ' + new_status)

        try:
            assert file_check(file_path=self.checkpoint_file, file_or_directory='file')
        except AssertionError:
            quit_with_error(self.logger, 'file for ' + self.name + ' cannot be located!')

        # update the sample object status
        checkpoint_dict = getattr(self, 'checkpoint_dict')
        checkpoint_dict[analysis_step] = new_status
        setattr(self, 'checkpoint_dict', checkpoint_dict)

        # attempt to read in the checkpoint file JSON
        with open(self.checkpoint_file, 'r') as chkpnt_file:
            checkpoint_data = json.load(chkpnt_file)

        # update the JSON entry for analysis step by setting it to new_status
        checkpoint_data[analysis_step] = new_status

        # write the new JSON information to the file
        with open(self.checkpoint_file, 'w') as chkpnt_file:
            json.dump(checkpoint_data, chkpnt_file)

        return

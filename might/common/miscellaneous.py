#!/usr/bin/env python
# encoding: utf-8
import distutils
import subprocess
import sys
from pathlib import Path
from shutil import which

from might.common import log


def path_clean(file_path):
    """
    sometimes users are careless. This method will use the pathLib module to convert user input into full posix path
    objects to avoid confusion downstream
    """

    return Path(str(file_path)).expanduser().resolve()


def file_extension_check(file_path=None, file_extension=None):
    """
    verify that the file provided has the correct extension
    """
    for extension in file_extension:
        if extension in file_path.suffixes:
            return True
    return False


def file_check(file_path=None, file_or_directory=None):
    """
    checks to see if the file associated with the individual_sequencing_sample object exists in the file system at the
    specified PATH. Returns True if yes and False if no. Also return False if the attribute does not exist
    """
    if file_or_directory == 'file':
        if file_path:
            if file_path.exists():
                return True
            else:
                return False
        else:
            return False

    if file_or_directory == "directory":
        if file_path:
            if file_path.is_dir():
                return True
            else:
                return False
        else:
            return False


def check_for_dependency(program):
    """
    check_for_dependency() will use distutils.spawn to determine if the input dependency is available on the users PATH
    returns True if the dependency is located, False otherwise
    """
    if which(str(program)):
        return True
    else:
        return False


def quit_with_error(logger, message):
    """
    Displays the given message and ends the program's execution.
    """
    logger.log(log.bold_red('Error: ') + message, 0, stderr=True)
    sys.exit(1)


def query_yes_no(question, default=None):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def round_down(num, divisor):
    return num - (num % divisor)


def ascii_art(logo=None, author=None, version=None, logo_only=False):
    """
    Print out the ascii art logo requested by the user
    :param logo: the name of the logo to be printed
    """

    if logo == 'might':

        ascii_logo = log.bold_green("""

            .___  ___.  __    _______  __    __  .__________.
            |   \/   | |  |  /  _____||  |  |  | |          |
            |  \  /  | |  | |  |  __  |  |__|  | `---|  |---`
            |  |\/|  | |  | |  | |_ | |   __   |     |  |     
            |  |  |  | |  | |  |__| | |  |  |  |     |  |     
            |__|  |__| |__|  \______| |__|  |__|     |__|     
     """)

        if logo_only:
            return ascii_logo

        print(ascii_logo)
        print(log.bold("\t\tMRSN Intergrated Genome Handling Tool"))
        print(log.bold("\t\t\t" + str(author)))
        print(log.bold("\t\t\t\tv" + str(version)))
        print('\n')


def get_version(args):
    print(log.bold('Installed version: ' + args.version))


def compression_handler(target_file, logger, compress=False, decompress=False, compression_type=None):
    # verify that exactly one action was specified
    try:
        assert (compress and not decompress) or (not compress and decompress)
    except AssertionError:
        quit_with_error(logger, 'Either compress OR decompress must be specified for compression_handler()!')
        raise

    # verify that the compression_type passed is one that is acceptable
    try:
        assert compression_type == 'gzip' or compression_type == 'bzip2' or compression_type == None
    except AssertionError:
        quit_with_error(logger, 'Either gzip or bzip2 must be specified for compression_handler()!')
        raise

    # if the file extension is '.tmp', it means we made this file during the run and it doesn't need to be decompressed
    if target_file.suffix == '.tmp':
        return target_file

    # if the compression type wasn't specified, try and guess based on the extension
    if compression_type is None:
        if target_file.suffix == '.gz':
            compression_type = 'gzip'
        elif target_file.suffix == '.bz2':
            compression_type = 'bzip2'
        else:
            if compress:
                logger.log(
                    'compression_handler() was asked to compress {} but was NOT told how, so it will use bzip2'.format(
                        str(target_file)))
                compression_type = 'bzip2'
            elif decompress:
                if target_file.suffix not in ['.fastq', '.fna', '.fasta', '.fa']:
                    quit_with_error(logger,
                                    'compression_handler() was asked to decompress {}, but is too derpy to figure out how! (Only supports .gz and .bz2)'.format(
                                        str(target_file)))
                else:
                    compression_type = 'bzip2'

    # set the actions based on the compression_type and compress vs decompress
    if compression_type == 'gzip':
        if compress:
            action = 'gzip'
        if decompress:
            action = 'gunzip'
    if compression_type == 'bzip2':
        if compress:
            action = 'bzip2'
        if decompress:
            action = 'bunzip2'

    # determine if the action requested is required (don't compress a compressed file, etc)
    if action == 'gzip':
        if target_file.suffix == '.gz':
            return target_file
        else:
            output_path = target_file.parent / (target_file.name + '.gz')
    if action == 'gunzip':
        if target_file.suffix != '.gz':
            return target_file
        else:
            output_path = target_file.parent / (target_file.name[:-3])
    if action == 'bzip2':
        if target_file.suffix == '.bz2':
            return target_file
        else:
            output_path = target_file.parent / (target_file.name + '.bz2')
    if action == 'bunzip2':
        if target_file.suffix != '.bz2':
            return target_file
        else:
            output_path = target_file.parent / (target_file.name[:-4])

    # perform the action using subprocess, then return the output_path
    compression_process = subprocess.run(
        [
            action,
            str(target_file)
        ],
        stderr=subprocess.PIPE
    )

    # check the return code
    try:
        assert compression_process.returncode == 0
    except AssertionError:
        quit_with_error(logger,
                        'compresssion_handler() returned a non-zero returncode when running {} on {}'.format(action,
                                                                                                             str(
                                                                                                                 target_file)))

    # validate that the output file exists
    try:
        assert file_check(file_path=output_path, file_or_directory='file')
    except AssertionError:
        quit_with_error(logger, 'compression_handler() failed to create the output file {}'.format(str(output_path)))

    return output_path


def parallel_compression_handler(target_files, compress=False, decompress=False, compression_type=None, logger=None,
                                 cores=1):
    """
    Use GNU parallel to handle the simultaneous compression/decompression of all of the files in a list of files.
    """
    # verify that exactly one action was specified
    try:
        assert (compress and not decompress) or (not compress and decompress)
    except AssertionError:
        quit_with_error(logger, 'Either compress OR decompress must be specified for compression_handler()!')
        raise

    # verify that the compression_type passed is one that is acceptable
    try:
        assert compression_type == 'gzip' or compression_type == 'bzip2'
    except AssertionError:
        quit_with_error(logger, 'Either gzip or bzip2 must be specified for compression_handler()!')
        raise

    # set the actions based on the compression_type and compress vs decompress
    if compression_type == 'gzip':
        if compress:
            action = 'gzip'
        if decompress:
            action = 'gunzip'
    if compression_type == 'bzip2':
        if compress:
            action = 'bzip2'
        if decompress:
            action = 'bunzip2'

    # Based on the action, see if the action is required (i.e. can't gunzip a non-.gz file). To track this, we will
    # create a new list (action_required) which will just store True for action required and False for no action
    # required. We will also create a third list of the expected output file path post action or the current file path
    # if no action is required. Finally we will zip these two lists to the target_files list
    action_required = []
    output_paths = []
    for target_file in target_files:
        if action == 'gzip':
            if target_file.suffix == '.gz':
                action_required.append(False)
                output_paths.append(target_file)
            else:
                action_required.append(True)
                output_path = target_file.parent / (target_file.name + '.gz')
                output_paths.append(output_path)
        if action == 'gunzip':
            if target_file.suffix != '.gz':
                action_required.append(False)
                output_paths.append(target_file)
            else:
                action_required.append(True)
                output_path = target_file.parent / (target_file.name[:-3])
                output_paths.append(output_path)
        if action == 'bzip2':
            if target_file.suffix == '.bz2':
                action_required.append(False)
                output_paths.append(target_file)
            else:
                action_required.append(True)
                output_path = target_file.parent / (target_file.name + '.bz')
                output_paths.append(output_path)
        if action == 'bunzip2':
            if target_file.suffix != '.bz2':
                action_required.append(False)
                output_paths.append(target_file)
            else:
                action_required.append(True)
                output_path = target_file.parent / (target_file.name[:-4])
                output_paths.append(output_path)

    # validate the target files exist if an action is required. If one is missing and we are decompressing, this is
    # fatal as we won't be able to produce some form of output. If compress this gets a warning as it just means we
    # are doing a bad job cleaning
    validated_target_files = []
    for target_file in target_files:
        try:
            assert file_check(file_path=target_file, file_or_directory='file')
            validated_target_files.append(True)
        except AssertionError:
            if decompress:
                quit_with_error(logger, 'Failed to locate target file at specified path: ' + str(target_file))
            if compress:
                logger.log(log.bold_red(
                    'WARNING: Unable to locate the target file: {}, so compression can not be performed'.format(
                        str(target_file))))
                validated_target_files.append(False)

    # zipping all of these lists together should give us tuples that we can use for for a) calling parallel compresssion
    # b) verifying output and c) returning the new paths in a list with the same order as target_files
    target_file_tuple = list(zip(target_files, action_required, output_paths, validated_target_files))

    # run the action on all files where action_required is true and validated_target_files is true using parallel
    files_for_processing = [str(target_file[0]) for target_file in target_file_tuple if
                            (target_file[1] == True and target_file[3] == True)]

    if len(files_for_processing) > 0:

        files_as_string = ' '.join(files_for_processing)

        if cores > len(files_for_processing):
            cores = len(files_for_processing)

        parallel_process = subprocess.run(
            [
                'parallel -j ' + str(cores) + ' ' + action + ' {} ::: ' + files_as_string
            ],
            shell=True,
            stderr=subprocess.PIPE
        )

        # check the return code
        try:
            assert parallel_process.returncode == 0
        except AssertionError:
            quit_with_error(logger,
                            'parallel compression/decompression returned a non-zero returncode: {} when handling {}'.format(str(parallel_process.returncode), files_as_string))

    return [target_file[2] for target_file in target_file_tuple]


def pick_from_a_list(prompt, choices, logger):
    """
    present the user with a list of choices and ask them to choose one

    input is a list of choices and a prompt. Returns one of the choices as selected by the user
    """

    logger.log("\nUser intervention required!")

    enumerated_choices = [(str(choice[0]),choice[1]) for choice in enumerate(choices, start=1)]
    enumerated_choices.append(("n", None))

    user_choice = None
    while user_choice not in [choice[0] for choice in enumerated_choices]:
        logger.log(prompt + " or select n to select none of these")
        for option in enumerated_choices:
            if option[0] == 'n':
                logger.log("n - None of these documents match")
            else:
                logger.log("{} - Select Document #{}".format(str(option[0]), str(option[0])))
                
        
        user_choice = input().lower()

        if user_choice not in [choice[0] for choice in enumerated_choices]:
            logger.log("Not an acceptable answer, try again!")
    
    if user_choice != "n":  # user made a valid choice from the list that was offered
        user_choice = int(user_choice)
        return enumerated_choices[user_choice - 1][1]
    else:
        return None


def log_a_list(a_list, a_logger):
    """
    log out a list
    """
    for element in a_list:
        try:
            a_logger.log(str(element))
        except TypeError:
            print(str(element))
            raise
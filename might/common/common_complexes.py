common_complexes_dict = {
    '1': 'Acinetobacter baumannii complex',
    '2': 'Enterobacter cloacae complex',
    '3': 'Klebsiella pneumoniae complex',
    '4': 'Burkholderia cepacia complex',
    '5': 'Aeromonas hydrophila complex',
    '6': 'Citrobacter freundii complex',
}
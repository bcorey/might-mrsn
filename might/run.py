#!/usr/bin/env python3
# encoding: utf-8

import subprocess
import sys
from datetime import datetime
from pathlib import Path

from pymongo import errors as pyerr

from might.analysis.bcl2fastq import Bcl2fastq
from might.analysis.assembly import Assembly
from might.analysis.clustering.cluster_manager import ClusterManager
from might.analysis.database_methods import database_check
from might.analysis.metadata import Metadata
from might.analysis.mlst import Mlst
from might.analysis.qc import Qc
from might.analysis.amr import Amr
from might.analysis.kraken2 import Kraken2, SpeciesCaller
from might.common import log
from might.common.input_parsing import parse_arguments
from might.common.miscellaneous import file_check
from might.common.errors import BadPathError, NothingToDoError

__author__ = 'Brendan Corey'
__version__ = '1.3.0'


def might():
    parse_arguments(__author__, __version__)


def bcl2fastq(args):
    """
    run the sample sheet validation and bcl2fastq pipeline to generate demulitplexed read files
    """

    # initialize the Bcl2fastq analysis instance
    bcl2fastq = Bcl2fastq(
        args.output,
        args.run_directory,
        args.sample_sheet,
        args.run,
        cores=args.cores,
        force=args.force,
        verbosity=args.verbosity
    )

    # validate essential paths are set and valid
    bcl2fastq.check_paths()

    # initialize the logger
    bcl2fastq.initialize_logger()

    # perform dependency checks to ensure analysis is possible
    bcl2fastq.dependency_check()

    # validate the sample sheet (as best we can)
    bcl2fastq.validate_sample_sheet()

    # if the pbs flag was invoked, run bcl2fastq.run_pbs(), which will run command as a pbs batch job
    if args.pbs:
        bcl2fastq.run_pbs()

    # run bcl2fastq analysis and summary
    bcl2fastq.run_bcl2fastq()

    # summarize the results of the run
    bcl2fastq.evaluate_bcl2fastq_run_results()

    bcl2fastq.logger.log_section_header('bcl2fastq analysis is now complete!')


def kraken2(args):
    """
    Use class Kraken2 from kraken2.py to run 1+ kraken2 species identification/contamination detection analyses
    """
    # initialize the Kraken2 analysis instance
    kraken2 = Kraken2(
        args.sample_list,
        args.output,
        args.reads,
        args.kraken2_database,
        ramdisk=args.ramdisk,
        keep=2,
        force=args.force,
        cores=args.cores,
        cores_per_sample=args.cores_per_sample,
        verbosity=args.verbosity,
        no_clean=args.no_clean,
        pbs=args.pbs
    )

    # validate essential paths are set and valid
    kraken2.check_paths()

    # initialize the kraken2 logger
    kraken2.initialize_logger()

    # perform dependency checks to ensure analysis is possible
    kraken2.dependency_check()

    # parse a provided sample list file OR scan directories to establish the sample list
    kraken2.generate_sample_list()

    # create the individual kraken2 sample objects (Kraken2Sample)
    kraken2.generate_kraken2_sample_objects()

    # if 1+ samples were identified that require kraken2 analysis AND the ramdisk was requested set it up now
    if args.ramdisk:
        kraken2.setup_ramdisk()

    # if the pbs flag was invoked, run kraken2.run_pbs(), which will run command as a pbs batch job
    if args.pbs:
        kraken2.run_pbs()

    # run the threaded kraken2 analyses
    kraken2.kraken2_threaded_analyses()

    kraken2.logger.log_section_header('Kraken2 analysis is now complete!')
    kraken2.logger.log_explanation('Feel free to use "MIGHT species-caller" to perform interactive species/contamination calling of the kraken2 reports!')


def speciesCaller(args):
    """
    Use Class SpeciesCaller to perform MIGHT-guided interactive species/contamination calling
    """
    # initialize the SpeciesCaller analysis instance
    species_caller = SpeciesCaller(
        args.sample_list,
        args.output,
        auto=args.auto,
        force=args.force,
        verbosity=args.verbosity
    )

    # validate essential paths are set and valid
    species_caller.check_paths()

    # initialize the species_caller logger
    species_caller.initialize_logger()

    # parse a provided sample list file OR scan directories to establish the sample list
    species_caller.generate_sample_list()

    # create the individual kraken2 sample objects (Kraken2Sample)
    species_caller.generate_kraken2_sample_objects()

    # perform interactive species calling
    species_caller.perform_species_calling()

    # summarize species call results
    species_caller.summarize_species_calling_results()

    species_caller.logger.log_section_header('Species calling is now complete!')
    species_caller.logger.log_explanation('Please review the results in the output directory (species_calls_summary.csv)'
                                          '. You may also wish to review the contaminated_samples.txt and the '
                                          'species_calls_more_info_requested_samples.txt files. The full kraken reports'
                                          ' for the latter should have been copied to the output directory for a more '
                                          'thorough review!')


def assembly(args):
    """
    run the assembly pipeline for one or more samples. Use the Assembly class from assembly.py to coordinate dependency and path checks, generation of a sample list
    if one is not explicitly provided, validating individual sample availablity and necessity of assembly steps via checkpointing, and threaded assembly workflows
    """
    # initialize the assembly analysis instance
    assembly = Assembly(
        args.output,
        args.run,
        args.reads,
        args.sample_list,
        keep=args.keep,
        force=args.force,
        no_clean=args.no_clean,
        cores=args.cores,
        cores_per_sample=args.cores_per_sample,
        verbosity=args.verbosity,
        timeout=args.timeout, 
        length_filter=args.length_filter, 
        coverage_filter=args.coverage_filter
    )
    
    # validate essential paths are set and valid
    assembly.check_paths()

    # initialize the assembly logger
    assembly.initialize_logger()

    # perform dependency checks to ensure analysis is possible
    assembly.dependency_check()

    # parse a provided sample list file OR scan directories to establish the sample list
    assembly.generate_sample_list()

    # create the individual assembly sample objects (AssemblySample)
    assembly.generate_assembly_sample_objects()

    # log out a preview of the individual assemblies that are to be performed
    assembly.preview_of_slated_analyses()

    # if the pbs flag was invoked, run assembly.run_pbs(), which will run command as a pbs batch job
    if args.pbs:
        assembly.run_pbs()

    # run the threaded assemblies
    assembly.threaded_assemblies()

    assembly.logger.log('Assembly is now complete!')


def amr(args):
    """
    Use class Amr from amr.py to run 1+ AMR predictions
    """
    # initialize the AMR analysis instance
    amr = Amr(
        args.output,
        args.run,
        sample_list_file=args.sample_list_file,
        analysis_type=args.analysis_type,
        reads_directory=args.reads,
        assembly_directory=args.assembly,
        keep=args.keep,
        force=args.force,
        cores=args.cores,
        cores_per_sample=args.cores_per_sample,
        verbosity=args.verbosity,
        no_clean=args.no_clean,
        summary_only=args.summary_only
    )

    # validate essential paths are set and valid
    amr.check_paths()

    # initialize the amr logger
    amr.initialize_logger()

    if not amr.summary_only:
        # perform dependency checks to ensure analysis is possible
        amr.dependency_check()

        # make sure that the amr databases are setup and the tiers and families file is around
        amr.validate_amr_resources()

    # parse a provided sample list file OR scan directories to establish the sample list
    amr.generate_sample_list()

    # create the individual amr sample objects (AmrSample)
    amr.generate_amr_sample_objects()

    # if only a summary has been requested, generate this and exit
    if amr.summary_only:
        amr.summarize_all_amr()
        sys.exit()
    
    # print a summary of the analyses to be performed
    amr.preview_of_slated_analyses()

    # if the pbs flag was invoked, run amr.run_pbs(), which will run command as a pbs batch job
    if args.pbs:
        amr.run_pbs()

    # run the threaded analyses
    amr.threaded_amr_analyses()

    amr.logger.log('AMR analysis is now complete!')


def mlst(args):
    """
    Use the Mlst class from mlst.py to run 1+ individual MLST assignments
    """
    # initialize the MLST analysis instance
    mlst = Mlst(
        args.output,
        args.assembly,
        args.sample_list,
        force=args.force,
        cores=args.cores,
        verbosity=args.verbosity
    )

    # validate essential paths are set and valid
    mlst.check_paths()

    # initialize the mlst logger
    mlst.initialize_logger()

    # perform dependency checks to ensure analysis is possible
    mlst.dependency_check()

    # parse a provided sample list file OR scan directories to establish the sample list
    mlst.generate_sample_list()

    # create the individual mlst sample objects (MlstSample)
    mlst.generate_mlst_sample_objects()

    if args.pbs:
        mlst.run_pbs()

    # run the threaded analyses
    mlst.threaded_mlst()

    # summarize all of the mlst results
    mlst.summarize_mlst_results()

    mlst.logger.log('MLST analysis is now complete')


def qc(args):
    """
    Use class Qc to collect and summarize all available QC data in the designated output directory
    """
    # initialize the MLST analysis instance
    qc = Qc(
        args.sample_list,
        args.output,
        verbosity=args.verbosity
    )

    # validate essential paths are set and valid
    qc.check_paths()

    # initialize the qc logger
    qc.initialize_logger()

    # parse a provided sample list file OR scan directories to establish the sample list
    qc.generate_sample_list()

    # create the individual qc sample objects (QcSample)
    qc.generate_qc_sample_objects()

    # summarize all of the qc output
    qc.summarize_qc()

    qc.logger.log('QC analysis is now complete')


def metadata(args):
    """
    Use class Metadata from metadata.py to manage importing metadata info from the Molecular Lab's Run Sheet
    """

    # initialize the metadata importer instance
    metadata = Metadata(
        args.output,
        args.metadata_file,
        sample_list_file=args.sample_list_file,
        force=args.force,
        verbosity=args.verbosity
    )

    # validate essential paths are set and valid
    metadata.check_paths()

    # initialize the metadata logger
    metadata.initialize_logger()

    # read in the metadata file
    metadata.metadata_file_to_dataframe()

    # parse a provided sample list file OR scan directories to establish the sample list
    metadata.generate_sample_list()

    # create the individual metadata sample objects (MetaDataSample)
    metadata.generate_metadata_sample_objects()

    # perform all of the individual metadata imports
    metadata.attempt_individual_metadata_import()

    # summarize all of the metadata reports
    metadata.summarize_metadata()

    metadata.logger.log('Metadata import complete!')


def cluster(args):
    """
    Use class ClusterManager from clustering.py to manage the identification of potential transmission clusters
    from a user-provided path containing nxn distance matrix file(s)
    """

    cluster_manager = ClusterManager(
        args.input, 
        args.output, 
        args.username, 
        args.password, 
        args.db_uri,
        args.auth,
        verbosity=args.verbosity
        )
    
    # ensure that all of the essential user provided paths are valid
    try:
        cluster_manager.validate_input_paths()
    except BadPathError as err:
        print(str(err))
        print("Aborting run!")
        sys.exit(1)

    # initialize the cluster manager logger
    cluster_manager.initialize_logger()

    # test that the bi-db URI and credentials are valid
    try:
        cluster_manager.validate_connection_to_bi_database()
    except pyerr.ServerSelectionTimeoutError as err:
        cluster_manager.logger.log(log.bold_red("FATAL ERROR: unable to contact the bi-db. Please ensure that you are connected to the BI VPN"))
        cluster_manager.logger.log(str(err))
        sys.exit(1)

    # check the input directory for distance matrix file(s)
    try:
        cluster_manager.scan_input_directory_for_distance_matrix_files()
    except NothingToDoError as err:
        cluster_manager.logger.log(str(err))
        sys.exit(1)

    # out of an overabundance of caution, run double_check_force to make sure the user really wants to overwrite
    cluster_manager.double_check_force()

    # time to run the individual cluster analyses
    cluster_manager.run_individial_cluster_analyses()

    # generate the summary report for all analyses
    cluster_manager.summarize_all_genus_cluster_reports()


def update(args):
    # initialize an update log file in the amrfinderplus directory
    proc = subprocess.Popen(['which', 'amrfinder'], stdout=subprocess.PIPE)
    amrfinder_path = Path(str(proc.stdout.read().strip())[2:-1])
    amrfinder_path = amrfinder_path.parent / 'data'
    amrfinder_path = Path(amrfinder_path).resolve()

    if not file_check(file_path=amrfinder_path, file_or_directory='directory'):
        subprocess.run(['mkdir', str(amrfinder_path)])
    logger = log.Log(
        log_filename=amrfinder_path / (
            '{}.update.log'.format(datetime.now().strftime("%Y-%m-%d_%H-%M"))),
        stdout_verbosity_level=args.verbosity
    )

    database_check(logger, update=True)

    logger.log_section_header('Database updating is complete!')


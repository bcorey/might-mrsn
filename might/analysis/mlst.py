#!/usr/bin/env python
# encoding: utf-8
import math
import os
import random
import shutil
import subprocess
import sys
import time
import psutil
from datetime import datetime
from pathlib import Path
import multiprocessing as mp
from fnmatch import fnmatch

import pandas as pd

from might.analysis.sample import Sample
from might.common.errors import MissingPrerequisiteError, MissingPathError, BadPathError, CheckpointFileInconsistentError
from might.common import log
from might.common.miscellaneous import file_check, check_for_dependency, quit_with_error, parallel_compression_handler, \
    compression_handler


class Mlst:

    def __init__(self, output_directory, assembly_directory, sample_list_file = None, force=False,
                 cores=1, verbosity=2):

        self.output_directory = output_directory
        self.sample_list_file = sample_list_file
        self.sample_list = []
        self.individual_sample_analyses = None
        self.log_directory = None
        self.assembly_directory = assembly_directory
        self.force = force
        self.cores = cores
        self.verbosity = verbosity

    def check_paths(self):
        """
        validate the required paths exist
        """
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            print(log.bold_yellow('WARNING: The output directory path ({}) is new, so we will make it now'.format(str(self.output_directory))))
            subprocess.run(
                [
                    'mkdir', '-p', str(self.output_directory)
                ]
            )

        # ensure that the output directory at least has an individual_sample_analyses directory for all but bcl2fastq and cluster
        self.individual_sample_analyses = self.output_directory / 'individual_sample_analyses'
        if not file_check(file_path=self.individual_sample_analyses, file_or_directory='directory'):
            subprocess.run(
                [
                    'mkdir', '-p', str(self.individual_sample_analyses)
                ]
            )
        
        # clean and validate the assembly directory
        if self.assembly_directory:
            self.assembly_directory = Path(self.assembly_directory).expanduser().resolve()
            if not file_check(file_path=self.assembly_directory, file_or_directory='directory'):
                raise BadPathError(self.assembly_directory, 'assembly directory')

        # if a sample_list_file was passed, validate it
        if self.sample_list_file:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list file')
            
        # prepare the log directory
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])
  
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        try:
            self.logger = log.Log(log_filename=self.log_directory / ('{}.mlst.might.log'.format(datetime.now().strftime("%Y-%m-%d_%H-%M"))), stdout_verbosity_level=self.verbosity)
        except: # need to figure out the logger error codes
            pass

    def dependency_check(self):
        """
        Ensure that the primary dependencies for this analysis are available on the path
        """
        try:
            assert check_for_dependency('mlst')
        except AssertionError:
            quit_with_error(self.logger, 'mlst could NOT be located')

    def generate_sample_list(self):
        """
        generate the list of samples to assemble, UNLESS the list was passed by the user
        """
        # clean, validate, and read in the sample list IF IT WAS PASSED
        if self.sample_list_file is not None:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list')

            with open(self.sample_list_file, 'r') as f:
                self.sample_list = [line.strip() for line in f.readlines()]
        else:
            
            for entry in os.scandir(self.individual_sample_analyses):
                if file_check(file_path=Path(entry.path).expanduser().resolve(), file_or_directory='directory'):
                    if entry.name == 'individual_sample_analyses':
                        continue
                    sample_name = entry.name
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)

            if self.assembly_directory:
                for entry in os.scandir(self.assembly_directory):
                    if fnmatch(entry.name, '*_*.fasta*') or fnmatch(entry.name, '*_*.fna*'):
                        sample_name = entry.name.split('_')[0]
                        if sample_name not in self.sample_list:
                            self.sample_list.append(sample_name)
                    elif fnmatch(entry.name, '*.*fasta*') or fnmatch(entry.name, '*.*fna*'):
                        sample_name = entry.name.split('.')[0]
                        if sample_name not in self.sample_list:
                            self.sample_list.append(sample_name)

    def generate_mlst_sample_objects(self):
        """
        generate sample objects for all of the samples in sample list and store it in self.sample_objects
        """
        self.sample_objects = []
        for sample in self.sample_list:
            if sample != 'Undetermined':  # No point in assembling the recycling bin of read files
                self.sample_objects.append(
                    MlstSample(
                        sample, 
                        self.output_directory,
                        force = self.force, 
                        assembly_directory=self.assembly_directory, 
                        verbosity=self.verbosity
                        )
                    )

        # iterate throught the sample objects in self.sample_objects. Partition samples into one of two lists:
        # analysis_required and analysis_complete based on the return value of <sample>.configure_amr_analysis
        self.analysis_required = []
        self.analysis_complete = []
        self.analysis_impossible = []

        for sample in self.sample_objects:

            sample.check_mlst_status()
            if sample.mlst_status == 'complete':
                self.analysis_complete.append(sample)
                continue

            try:
                sample.validate_mlst_possible()  # the sample is reporting that it is capable of undergoing assembly
                self.analysis_required.append(sample)
            except MissingPathError as err:
                self.logger.log(str(err))
                self.analysis_impossible.append(sample)
            except BadPathError as err:
                self.logger.log(str(err))
                self.analysis_impossible.append(sample)

    def preview_of_slated_analyses(self):
        """
        log a summary of the analyses to be performed
        """
        self.logger.log_section_header('Summary of MLST assignments to be performed')
        self.logger.log('\n {} samples were detected for analysis'.format(str(len(self.sample_objects))))
        self.logger.log('\n {} samples have already undergone MLST assignment'.format(str(len(self.analysis_complete))))
        self.logger.log('\n The following {} samples will undergo MLST assignment:'.format(str(len(self.analysis_required))))
        for sample in self.analysis_required:
            self.logger.log(str(sample.name))
        self.logger.log('\n The following {} samples were flagged for missing a requisite file for MLST assignment:'.format(str(len(self.analysis_impossible))))
        for sample in self.analysis_impossible:
            self.logger.log(str(sample.name))

    def threaded_mlst(self):
        """
        run threaded_mlst() for each sample in self.analysis_required using multiprocessing
        """
        self.logger.log_section_header('Now starting threaded MLST assignment')
        self.logger.log_explanation("Running threaded MLST assigntment using python multiprocessing")

        if len(self.analysis_required) == 0:
            self.logger.log('\nMLST threaded analyses are now complete!')
            return

        pool_size = math.floor(self.cores)
        self.logger.log("Running {} processes ({} cores at {} cores per sample)".format(str(pool_size), str(self.cores), str(1)))

        p = mp.Pool(pool_size)
        p.map(MlstSample.threaded_mlst_process, self.analysis_required)
        p.close()
        p.join()

        self.logger.log('\nMLST assignment threaded analyses are now complete!')

    def run_pbs(self):
        """
        create and submit a pbs batch script that will perform this analysis. This gets around the network stability
        issues that can cause long runnning jobs to be terminated
        """

        self.logger.log_section_header('Submitting MLST job(s) to pbs server')
        self.logger.log_explanation('Since the -pbs flag was invoked, we will submit the job(s) to the pbs batch server for'
                                    'processing. Please note that the current script will terminate once the job has '
                                    'been submitted, so you will need to monitor qstat to evaluate when the job has '
                                    'completed\n')

        self.logger.log('Generating the pbs batch script(s)\n')

        for sample in self.analysis_required:

            # set the pbs scipt file path
            sample.pbs_script_file = sample.mlst_directory / (sample.name + '.mlst.pbs')

            sample.logger.log('Batch script file at: {}\n'.format(str(sample.pbs_script_file)))

            # verify that the pbs file does NOT already exist. If it does, delete it
            if file_check(file_path=sample.pbs_script_file, file_or_directory='file'):
                sample.logger.log('An existing pbs batch script file was located and will be overwritten\n')
                subprocess.run(['rm', str(sample.pbs_script_file)])

            # generate a tmp file that will act as a sample-list. This will allow us to run kraken2 for a single sample
            sample.sample_list = sample.mlst_directory / (sample.name + '.sample_list.tmp')

            # verify that the sample list tmp file does NOT already exist. If it does, delete it
            if file_check(file_path=sample.sample_list, file_or_directory='file'):
                sample.logger.log('An existing sample list tmp file was located and will be overwritten\n')
                subprocess.run(['rm', str(sample.sample_list)])

            # write the sample name to the tmp file
            with open(str(sample.sample_list), 'w') as sample_list:
                sample_list.write(sample.name)

            # prepare the pbs file
            with open(str(sample.pbs_script_file), 'w') as pbs:

                # add the shebang line
                pbs.write('#!/bin/bash\n')

                # write the nodes, cores, and walltime parameters
                pbs.write('#PBS -l nodes=1:ppn=1\n')
                pbs.write('#PBS -l walltime=00:10:00\n')

                output_path = sample.mlst_directory / (sample.name + '.mlst.stdout')
                error_path = sample.mlst_directory / (sample.name + '.mlst.stderr')

                # write the stdout and stderr paths
                pbs.write('#PBS -o {}\n'.format(str(output_path)))
                pbs.write('#PBS -e {}\n'.format(str(error_path)))

                # teach pbs server about conda
                pbs.write('source /data/MRSN_Research/mrsn/miniconda/etc/profile.d/conda.sh\n')

                # activate might
                pbs.write('conda activate might\n')

                # write the actual might command to be run as a batch job
                pbs.write('might mlst {} {} --sample-list {}'.format(sample.output_directory, sample.assembly_directory, sample.sample_list))

            # submit the pbs job to the batch server
            sample.logger.log('Submitting the batch script file to the pbs batch server for sample: {}'.format(sample.name))

            pbs_job_submission_process = subprocess.run(
                [
                    'qsub',
                    '-q',
                    'batch',
                    str(sample.pbs_script_file)
                ],
                stdout=subprocess.PIPE
            )

            sample.logger.log(str(pbs_job_submission_process.stdout))

            time.sleep(1)

        self.logger.log('The job(s) have been submitted. Please monitor qstat for job completion!')
        sys.exit()

    def summarize_mlst_results(self):
        """
        prepare a summary file of all available MLST assignment results
        """
        self.logger.log_section_header('Summarizing MLST analyses')
        self.logger.log_explanation('Summary files will be generated for MLST output for all samples in the current output/mlst directory')

        self.summary_file = self.output_directory / 'mlst_summary.csv'

        # remove existing summary files is they are present
        if self.summary_file.exists():
            subprocess.run(['rm', str(self.summary_file)])
        
        self.logger.log_section_header('Collecting the individual MLST files for summary')
        files_for_summary = []
        missing_files = []
        for sample in self.sample_objects:
            if sample.mlst_report.exists():
                files_for_summary.append([sample.name, sample.mlst_report])
            else:
                missing_files.append(sample)

        self.logger.log("Located following MLST report files:")
        for entry in files_for_summary:
            self.logger.log(str(entry[1]))
        self.logger.log("\nThe following samples do not have a MLST report file:")
        for entry in missing_files:
            self.logger.log(entry.name)

        self.logger.log('\n\nGenerating summary file: {}'.format(log.bold(str(self.summary_file))))

        column_list = ['sample', 'source', 'scheme', 'mlst', 'gene 1', 'gene 2', 'gene 3', 'gene 4', 'gene 5', 'gene 6',
                    'gene 7', 'gene 8']
        column_list_out = ['sample', 'scheme', 'mlst', 'gene 1', 'gene 2', 'gene 3', 'gene 4', 'gene 5', 'gene 6', 'gene 7',
                        'gene 8', 'source']

        summary_dataframe = pd.DataFrame(columns=column_list)

        for entry in files_for_summary:
            # Read in the input file to the entry_dataframe
            entry_dataframe = pd.read_csv(str(entry[1]), sep=',', header=None, names=column_list[1:])
            entry_dataframe['sample'] = str(entry[0])

            # add the current entry_dataframe to the appropriate summary dataframe
            summary_dataframe = pd.concat([summary_dataframe, entry_dataframe], ignore_index=True, sort=False)

        # write the summary dataframe to summary file
        summary_dataframe.to_csv(self.summary_file, sep=',', index=False, columns=column_list_out, mode='a')

        # verify that both summary files were generated. If so, return True, else return false
        if self.summary_file.exists():
            self.logger.log('mlst_summary() finished successfully')
        else:
            self.logger.log('mlst_summary() failed to generate the requested mlst summary files')

class MlstSample(Sample):
    """
    Class MlstSample extends the Sample class, adding the configuration required for taking a single assembly file through MLST assignment
    """

    def __init__(self, name, output_directory, force=False, assembly_directory=None, verbosity=2):

        super().__init__(name, output_directory, assembly_directory=assembly_directory, verbosity=verbosity)

        # set the key mlst input/output paths
        self.kraken2_directory = self.individual_analysis_directory / 'kraken2'
        self.species_call_file = self.kraken2_directory / (self.name + '_species_call.csv')
        self.species = None
        self.mslt_scheme = None
        self.mlst_directory = self.individual_analysis_directory / 'mlst'
        self.mlst_report = self.mlst_directory / (self.name + '_mlst_report.csv')
        
        self.force = force
        self.log_filename=self.log_directory / ('{}.mlst.might.log'.format(self.name))

        self.mlst_status = None 

        # initialize the logger for this sample
        self.initialize_logger(self.log_filename)

        # initialize the CheckPoint for this sample
        self.initialize_sample_checkpoint_file('mlst', self.force)

        # if force is being invoked, remove any output that may exist
        if self.force:
            if file_check(file_path=self.mlst_directory, file_or_directory='directory'):
                subprocess.run(['rm', '-r', str(self.mlst_directory)])

    def prepare_mlst_directory(self):
        """

        """
        if not self.mlst_directory.exists():
            subprocess.run(["mkdir","-p",str(self.mlst_directory)])

    def threaded_mlst_process(self):
        """
        governing process for a threaded assembly analysis
        """
        self.prepare_mlst_directory()
        self.preview_individual_analysis()

        # process read files using read_file_preprocessing()
        try:
            self.run_mlst()
        except AssertionError:
            return
        
        self.logger.log_section_header('Individual sample MLST assignment complete!', verbosity=3)
        return

    def check_mlst_status(self):
        """
        return the current status of the mlst for this sample ('new', 'incomplete', 'complete')
        """
        # if the mlst analysis has already been completed according to the checkpoint file, verify that the output
        # files exist. If it does NOT exist, revert the checkpoint file to indicate that analysis is required. Log a
        # warning that the checkpoint file was not in agreement with the output
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['mlst analysis'] == 'complete':
            if file_check(file_path=self.mlst_report, file_or_directory='file'):
                self.logger.log('Sample {} is marked complete in its mlst checkpoint file, and the key output was located'
                           ', so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                self.mlst_status = 'complete'
            else:
                raise CheckpointFileInconsistentError(self.checkpoint.checkpoint_file, 'mlst analysis', [str(self.mlst_report)])
    
    def validate_mlst_possible(self):
        """
        ensure that the requirements for assembly are met. returns 0 if valid. If invalid, log the error message and returns 1
        """
        try:
            self.scan_for_assembly(self.assembly_directory)
        except MissingPathError as err:
            self.logger.log(str(err))
            raise
        except BadPathError as err:
            self.logger.log(str(err))
            raise

    def preview_individual_analysis(self):
        """
        log a preview of the individual analysis
        """
        self.logger.log_section_header("Preview of MLST Plan", verbosity=3)
        self.logger.log_explanation("Summary of key MLST parameters", verbosity=3)
        self.logger.log("The name of this MLST sample is {}".format(log.bold_green(self.name)), verbosity=3)
        self.logger.log("The assembly file for this assembly project is {}".format(log.bold_green(str(self.assembly))), verbosity=3)
        
    def lookup_species_call(self):
        """
        attempt to get the species call to enforce a scheme for ESKAPEs based on Kraken results
        """
        species_to_enforce_dict = {
        'coli': 'Escherichia_coli_1',
        'pneumoniae': 'Klebsiella_pneumoniae',
        'baumannii': 'Acinetobacter_baumannii_2',
        'aeruginosa': 'Pseudomonas_aeruginosa',
        'aureus': 'Staphylococcus_aureus',
        'faecium': 'Enterococcus_faecium',
        'faecalis': 'Enterococcus_faecalis',
        'cloacae': 'Enterobacter_cloacae',
        'gonorrhoeae': 'Neisseria_spp.',
        'difficile': 'Clostridioides_difficile',
        'oxytoca': 'Klebsiella_oxytoca',
        'aerogenes': 'Klebsiella_aerogenes',
        'maltophilia': 'Stenotrophomonas_maltophilia',
        }
        
        species_column_list = [
            'Genus',
            'Species',
            'WGS ID',
            'top hits',
            'WGS ID - Method',
            'DB Build',
            'Contaminated?',
            'Manually entered fields?'
        ]
        if file_check(file_path=self.species_call_file, file_or_directory='file'):
            try:
                species_dataframe = pd.read_csv(self.species_call_file, usecols=species_column_list)
                self.species = species_dataframe.loc[0, 'Species'].lower()
            except pd.errors.EmptyDataError:  # Case of empty csv
                self.logger.log('The individual species call file for sample {} is empty: {}!'.format(self.name, str(self.species_call_file)), verbosity=3)
            except ValueError:  # Incorrectly formatted csv
                self.logger.log('The species call file for sample {} is incorrectly formatted: {}!'.format(self.name, str(self.species_call_file)), verbosity=3)
            except:
                self.logger.log('An unexpected error occured while trying to read the species output for this sample. Proceeding using the mlst autodetect utility')   
        
        if self.species:
            for key in species_to_enforce_dict.keys():
                if self.species.startswith(key):
                    self.mlst_scheme = species_to_enforce_dict[key]
    
    def run_mlst(self):
        """
        run TSeemann's mlst program to assign MLST for this sample
        """
        self.logger.log_section_header('Now performing MLST assignment with Torsten Tseemanns mlst', verbosity=3)

        # check to see if this step is marked completed by the checkpoint file
        if self.mlst_status == 'complete':
            self.logger.log("This step is already vaerified complete, skipping!")

        # If the isolate has reached this step, analysis will need to be performed. First we will purge any existing output
        self.logger.log('Cleaning up any partially completed analysis output', verbosity=3)
        if file_check(file_path=self.mlst_report, file_or_directory='file'):
            subprocess.run(['rm', str(self.mlst_report)])

        # Attempt to get a scheme to enforce with lookup_species_call()
        self.lookup_species_call()

        if self.mlst_scheme:

            self.logger.log('[{}]\tSpecies was detected as {}, so using MLST scheme {}'.format(self.name, self.species, self.mlst_scheme))

            # run mlst
            mlst_cmd = 'mlst --scheme {} --quiet {} >> {}'.format(str(self.mlst_scheme), str(self.assembly), str(self.mlst_report))
            self.logger.log('Command: ' + log.bold(mlst_cmd) + '\n', verbosity=3)
            self.logger.log('Now running MLST', verbosity=3)

            mlst_process = subprocess.run(
                [
                    'mlst',
                    '--scheme', self.mlst_scheme,
                    '--quiet',
                    str(self.assembly)],
                stdout=subprocess.PIPE,
                universal_newlines=True)
        
        else:

            # run mlst
            mlst_cmd = 'mlst --exclude Escherichia_coli_2,Acinetobacter_baumannii_1 --quiet {} >> {}'.format(str(self.assembly), str(self.mlst_report))
            self.logger.log('Command: ' + log.bold(mlst_cmd) + '\n', verbosity=3)
            self.logger.log('Now running MLST', verbosity=3)

            mlst_process = subprocess.run(
                [
                    'mlst',
                    '--quiet',
                    '--exclude', 'Escherichia_coli_2,Acinetobacter_baumannii_1',
                    str(self.assembly)],
                stdout=subprocess.PIPE,
                universal_newlines=True)

        # check the final return code from mlst
        try:
            assert mlst_process.returncode == 0
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='mlst analysis', new_status='failed')
            quit_with_error(self.logger, 'mlst returned a non-zero returncode for sample {}: {}'.format(self.name, str(mlst_process.returncode)))

        with open(self.mlst_report, 'w') as output:
            for line in mlst_process.stdout:
                line = ' | '.join([i for i in line.split(',')])
                line = ','.join([i for i in line.split('\t')])
                output.write(line)

        # validate that the output file was generated where we were expecting it
        try:
            assert file_check(file_path=self.mlst_report, file_or_directory='file')
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='mlst analysis', new_status='failed')
            quit_with_error(self.logger,
                            'something went wrong during MLST assignment. mlst failed to generate the MLST report file for'
                            ' {} at the expected path: {}'.format(self.name, str(self.mlst_report)))

        # update the checkpoint to indicate successful completion of this step
        self.checkpoint.update_checkpoint_file(analysis_step='mlst analysis', new_status='complete')

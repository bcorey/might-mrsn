#!/usr/bin/env python
# encoding: utf-8
import csv
import math
import os
import subprocess
import sys
from pathlib import Path
from datetime import datetime
from fnmatch import fnmatch
import shutil

from might.common import log
from might.common.miscellaneous import file_check, quit_with_error, check_for_dependency, file_extension_check
from might.common.errors import BadPathError


class Bcl2fastq:

    def __init__(self, output_directory, run_directory, sample_sheet, run_name, cores=1, force=False, verbosity=2):

        self.output_directory = output_directory
        self.run_directory = run_directory
        self.sample_sheet = sample_sheet
        self.run_name = run_name.upper()
        self.cores = cores
        self.force = force
        self.verbosity = verbosity

        self.sample_list = None
    
    def check_paths(self):
        """
        validate the required paths exist
        """
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            print(log.bold_yellow('WARNING: The output directory path ({}) is new, so we will make it now'.format(str(self.output_directory))))
            subprocess.run(
                [
                    'mkdir', '-p', str(self.output_directory)
                ]
            )
        
        self.run_directory = Path(self.run_directory).expanduser().resolve()
        if not file_check(file_path=self.run_directory, file_or_directory='directory'):
            raise BadPathError(self.run_directory, "run directory")
        
        self.sample_sheet = Path(self.sample_sheet).expanduser().resolve()
        if not file_check(file_path=self.sample_sheet, file_or_directory='file'):
            raise BadPathError(self.sample_sheet, "sample sheet")
   
        # prepare the log directory
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])

        # prepare the raw_reads directory
        self.reads_directory = self.output_directory / 'reads' / 'raw_reads'
  
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        try:
            self.logger = log.Log(
                log_filename=self.log_directory / '{}.bcl2fastq.might.log'.format(datetime.now().strftime("%Y-%m-%d_%H-%M")),
                stdout_verbosity_level=self.verbosity
            )
        except: # need to figure out the logger error codes
            pass

    def dependency_check(self):
        """
        Ensure that the primary dependencies for this analysis are available on the path
        """
        try:
            assert check_for_dependency('bcl2fastq')
        except AssertionError:
            quit_with_error(self.logger, 'bcl2fastq could NOT be located')
    
    def validate_sample_sheet(self):
        """
         This method will make sure that the sample sheet is free of duplicate sample names and/or barcode combinations
         """
        self.logger.log_section_header('Validating the sample sheet file')
        # verify that the sample sheet meets the no duplicates criteria for samples and barcode sets that will cause it to
        # fail silently
        sample_sheet_data = {}
        sample_list = []

        if not file_extension_check(file_path=self.sample_sheet, file_extension=['.csv']):
            quit_with_error(self.logger, 'sample sheet must be a csv!')
        
        # To avoid windows-to-unix carriage return issues, run dos2unix on the sample sheet
        subprocess.run(['dos2unix', str(self.sample_sheet)])

        with open(str(self.sample_sheet), 'r') as sample_sheet:
            sample_sheet_reader = csv.DictReader(sample_sheet,
                                                 fieldnames=['Sample_ID', '', 'index', '', 'index2'],
                                                 delimiter=',')
            sample_number = 0
            bad_sheet = False  # tracks whether a sample sheet error was found
            header = True  # tracks whether or not we have made it to the significant data yet
            for row in sample_sheet_reader:
                if row['Sample_ID'] == 'Sample_ID':  # we have reached the start of the relevant data
                    header = False
                    continue

                if header:
                    continue

                # make sure that there are no spaces in the sample_id
                if ' ' in row['Sample_ID']:
                    self.logger.log('Sample name detected with illegal character " ": {}'.format(row['Sample_ID']))
                    bad_sheet = True

                if len(row['index']) == len(row['index2']) and len(row['index']) == 8:
                    concatenated_indices = row['index'] + row['index2']
                    sample_number += 1
                    if row['Sample_ID'] in sample_sheet_data.keys():
                        self.logger.log('Duplicate sample names detected in the provided sample sheet: {}'.format(row[
                                            'Sample_ID']))
                        bad_sheet = True
                    elif concatenated_indices in sample_sheet_data.values():
                        self.logger.log('Duplicate barcode combinations detected in the provided sample sheet: I7= {} I5= {}'.format(row['index'], row['index2']))
                        bad_sheet = True
                    else:
                        sample_sheet_data[row['Sample_ID']] = concatenated_indices
                        sample_list.append(row['Sample_ID'])
                else:
                    self.logger.log('Sample sheet has unnecessary columns!')
                    bad_sheet = True
                    break

        if bad_sheet == True:
            quit_with_error(self.logger, 'One or more sample sheet errors were detected. Please correct and try again!')

        self.sample_list = sample_list

        self.logger.log('\nSample sheet appears to be valid!')
    
    def validate_reads_directory(self):
        """
        Make sure that the reads directory exists. If it does, ensure that the read designations from the sample sheet are not already present. If it
        doesn't exist, make it now
        """
        # make sure that self.reads_directory exists
        if not file_check(file_path=self.reads_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', '-p', str(self.reads_directory)])

        # use the sample list generated during validate_sample_sheet() to verify that the read files haven't already
        # been generated in the reads directory. If they have, overwrite if force is true, otherwise quit with error
        for file in os.scandir(str(self.reads_directory)):
            if file.name.split('_')[0] in self.sample_list:
                if self.force:
                    self.logger.log('a read file for sample {} was detected in the reads directory: {}. Since force was'
                                    ' specified, it will be deleted'.format(file.name.split('_')[0], file.name))
                    subprocess.run(['rm', str(file)])
                else:
                    quit_with_error(self.logger,
                                    'a read file for sample {} was detected in the reads directory: {}. Since force was'
                                    ' not specified the run is aborting!'.format(file.name.split('_')[0], file.name))

    def run_bcl2fastq(self):
        """
        handles the running of bcl2fastq
        """
        self.logger.log_section_header('Now running bcl2fastq')
        # Determine the appropriate thread allocations for the run
        # --loading-threads: default is 4. Only change if the number of allotted processors is <4
        if int(self.cores) < 4:
            loading_threads = str(self.cores)
        else:
            loading_threads = '4'
        # As many as you want
        processing_threads = str(self.cores)
        # --writing threads: default is 4. Only change if the number of allotted processors is <4
        if int(self.cores) < 4:
            writing_threads = str(self.cores)
        else:
            writing_threads = '4'

        # prepare the bcl2fastq2 command as a string for logging purposes
        bcl2fastq2_cmd = 'bcl2fastq --runfolder-dir {} --loading-threads {} --processing-threads {} --writing-threads {}' \
                         ' --barcode-mismatches=1 --no-lane-splitting --sample-sheet {} -o {}'.format(
            str(self.run_directory), loading_threads, processing_threads, writing_threads, str(self.sample_sheet),
            str(self.reads_directory))

        self.logger.log('Command: ' + log.bold(bcl2fastq2_cmd) + '\n')

        bcl2fastq_process = subprocess.run(
            [
                'bcl2fastq',
                '--runfolder-dir', str(self.run_directory),
                '--loading-threads', loading_threads,
                '--processing-threads', processing_threads,
                '--writing-threads', writing_threads,
                '--barcode-mismatches=1',
                '--no-lane-splitting',
                '--sample-sheet', str(self.sample_sheet),
                '-o', str(self.reads_directory)
            ],
            stderr=subprocess.DEVNULL)

        # check the final return code from amrfinder
        try:
            assert bcl2fastq_process.returncode == 0
        except AssertionError:
            quit_with_error(self.logger,
                            'bcl2fastq returned a non-zero returncode: {}'.format(str(bcl2fastq_process.returncode)))

        self.logger.log('bcl2fastq run complete!')

    def evaluate_bcl2fastq_run_results(self):
        """
        Go through the reads directory to determine if all of the expected read files were generated. Determine whether
        read files fall significantly above/below the median for the run
        """

        # log the results for each sample in self.sample_list
        self.logger.log_section_header('Evaluating bcl2fastq output')
        self.logger.log_explanation('Checking the output of bcl2fastq. If the read file for a sample is average in size,'
                                    ' it will be presented in green. If it is considerably larger or smaller than the '
                                    'average for the run it will show up in yellow. If it is under 20MB this is '
                                    'considered a critical issue (unlikely to meet coverage threshold) and will be shown'
                                    ' in red')

        # compute the average read file size
        file_size_list = [float(os.path.getsize(file))/1000000 for file in os.scandir(str(self.reads_directory))]
        average_file_size = math.floor(sum(file_size_list)/len(file_size_list))

        self.logger.log('\nThe average file size for this run was: {} MB\n'.format(log.bold(str(average_file_size))))

        # report read file size for each sample
        for sample in self.sample_list:
            self.logger.log('Read files for Sample {}'.format(log.bold(str(sample))))

            r1_read = None
            r2_read = None

            # find the read files for the sample
            for file in os.scandir(str(self.reads_directory)):
                if (file.name.startswith(sample + "_") or file.name.startswith(
                        sample + ".")) and fnmatch(file, '*R1*fastq*'):
                    r1_read = Path(file.path)
                    r1_size = math.floor((float(os.path.getsize(file) / 1000000)))
                elif (file.name.startswith(sample + "_") or file.name.startswith(
                        sample + ".")) and fnmatch(file, '*R2*fastq*'):
                    r2_read = Path(file.path)
                    r2_size = math.floor((float(os.path.getsize(file) / 1000000)))

            # move the read file from name.fastq.gz to name.runid.fastq.gz
            r1_name_original = r1_read.name.split(".")
            r1_name_modified = r1_read.parent / (".".join([r1_name_original[0],self.run_name,r1_name_original[1], r1_name_original[2]]))
            shutil.move(str(r1_read), str(r1_name_modified))
            r1_read = r1_name_modified

            r2_name_original = r2_read.name.split(".")
            r2_name_modified = r2_read.parent / (".".join([r2_name_original[0],self.run_name,r2_name_original[1], r2_name_original[2]]))
            shutil.move(str(r2_read), str(r2_name_modified))
            r2_read = r2_name_modified
            
            # report the findings to the user
            if r1_read:
                if r1_size < 20:
                    self.logger.log(log.red('R1 file path: {}\t{} MB\tCRITICAL'.format(str(r1_read), str(r1_size))))
                elif r1_size < average_file_size * 0.5 or r1_size > average_file_size * 2:
                    self.logger.log(log.yellow('R1 file path: {}\t{} MB\tWarning'.format(str(r1_read), str(r1_size))))
                else:
                    self.logger.log(log.green('R1 file path: {}\t{} MB'.format(str(r1_read), str(r1_size))))
            else:
                self.logger.log(log.bold_red('NO R1 READ FILE DETECTED!\tCRITICAL'))

            if r2_read:
                if r2_size < 20:
                    self.logger.log(log.red('R2 file path: {}\t{} MB\tCRITICAL\n'.format(str(r1_read), str(r1_size))))
                elif r2_size < average_file_size * 0.5 or r2_size > average_file_size * 2:
                    self.logger.log(log.yellow('R2 file path: {}\t{} MB\tWarning\n'.format(str(r1_read), str(r1_size))))
                else:
                    self.logger.log(log.green('R2 file path: {}\t{} MB\n'.format(str(r1_read), str(r1_size))))
            else:
                self.logger.log(log.bold_red('NO r2 READ FILE DETECTED!\tCRITICAL\n'))

    def run_pbs(self):
        """
        create and submit a pbs batch script that will perform this analysis. This gets around the network stability
        issues that can cause long runnning jobs to be terminated
        """

        self.logger.log_section_header('Submitting bcl2fastq job to pbs server')
        self.logger.log_explanation('Since the -pbs flag was invoked, we will submit the job to the pbs batch server for'
                                    'processing. Please note that the current script will terminate once the job has '
                                    'been submitted, so you will need to monitor qstat to evaluate when the job has '
                                    'completed\n')

        self.logger.log('Generating the pbs batch script\n')

        # set the pbs scipt file path
        self.pbs_script_file = self.output_directory / 'bcl2fastq.pbs'

        self.logger.log('Batch script file at: {}\n'.format(str(self.pbs_script_file)))

        # verify that the pbs file does NOT already exist. If it does, delete it
        if file_check(file_path=self.pbs_script_file, file_or_directory='file'):
            self.logger.log('An existing pbs batch script file was located and will be overwritten\n')
            subprocess.run(['rm', str(self.pbs_script_file)])

        # prepare the pbs file
        with open(str(self.pbs_script_file), 'w') as pbs:

            # add the shebang line
            pbs.write('#!/bin/bash\n')

            # add the nodes, cores, and wall-time parameters. We will reset the cores requirement to a max of 30 if a
            # greater value was provided by the user to make it fit on any available node
            self.cores = str(min(int(self.cores), 30))
            pbs.write('#PBS -l nodes=1:ppn={}\n'.format(self.cores))
            pbs.write('#PBS -l walltime=24:00:00\n')

            output_path = self.output_directory / 'bcl2fastq.stdout'
            error_path = self.output_directory / 'bcl2fastq.stderr'

            # write the stdout and stderr paths
            pbs.write('#PBS -o {}\n'.format(str(output_path)))
            pbs.write('#PBS -e {}\n'.format(str(error_path)))

            # teach pbs server about conda
            pbs.write('source /data/MRSN_Research/mrsn/miniconda/etc/profile.d/conda.sh\n')

            # activate might
            pbs.write('conda activate might\n')

            # write the actual might command to be run as a batch job
            pbs.write('might bcl2fastq {} {} {} --cores {}'.format(self.output_directory, self.run_directory, self.sample_sheet, self.cores))

        # submit the pbs job to the batch server
        self.logger.log('Submitting the batch script file to the pbs batch server\n')

        subprocess.run(
            [
                'qsub',
                '-q',
                'batch',
                str(self.pbs_script_file)
            ]
        )

        self.logger.log('The job has been submitted. Please monitor qstat for job completion!')
        sys.exit()

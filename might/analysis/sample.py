#!/usr/bin/env python
# encoding: utf-8
from fnmatch import fnmatch
import os
import subprocess
from pathlib import Path

from might.common import log
from might.common.checkpoint import CheckPoint
from might.common.miscellaneous import file_check, path_clean, quit_with_error
from might.common.errors import BadPathError, MissingPathError, MissingPrerequisiteError, CheckpointFileInconsistentError


class Sample:
    """
    Parent class sample collects all of the basic information for a sequencing sample
    """

    def __init__(self, name, output_directory, reads_directory=None, assembly_directory=None, verbosity=2):

        self.name = name
        self.output_directory = output_directory
        self.individual_sample_analyses = self.output_directory / 'individual_sample_analyses'
        self.individual_analysis_directory = self.individual_sample_analyses / self.name
        self.checkpoint_directory = self.individual_analysis_directory / 'checkpoints'
        self.log_directory = self.individual_analysis_directory / 'logs'
        self.reads_directory = reads_directory
        self.assembly_directory = assembly_directory
        self.verbosity = verbosity
        self.logger = None

        self.r1_fastq = None
        self.r2_fastq = None
        self.assembly = None

        # initialize the individual analysis directory if necessary
        if not file_check(file_path=self.individual_analysis_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', '-p', str(self.individual_analysis_directory)])

    def scan_for_fastq(self, directory=None):
        """
        scans the specified directory for fastq files that match the name of the sample, then assigns their POSIX paths
        to the self.r1_fastq and self.r2_fastq object attributes.
        """

        for file in os.scandir(directory):
            if (file.name.startswith(self.name + "_") or file.name.startswith(
                    self.name + ".")) and fnmatch(file, '*R1*fastq*'):
                self.r1_fastq = path_clean(file_path=file.path)
            elif (file.name.startswith(self.name + "_") or file.name.startswith(
                    self.name + ".")) and fnmatch(file, '*R2*fastq*'):
                self.r2_fastq = path_clean(file_path=file.path)
        
        if not self.r1_fastq:
            raise MissingPathError('R1 read file')
        if not file_check(file_path=self.r1_fastq, file_or_directory='file'):
            raise BadPathError(str(self.r1_fastq), 'R1 read file', 'Sample {} can not be processed as the forward read is missing!'.format(self.name))
        if not self.r2_fastq:
            raise MissingPathError('R2 read file')
        if not file_check(file_path=self.r2_fastq, file_or_directory='file'):
            raise BadPathError(str(self.r2_fastq), 'R2 read file', 'Sample {} can not be processed as the reverse read is missing!'.format(self.name))
        
    def scan_for_assembly(self, directory=None):
        """
        scans the specified directory for fasta file that match the name of the sample, then assigns the POSIX path to
        the self.assembly object attributes
        """

        # search the directory for files that start with the sample name and end with/include a common fasta extension
        for file in os.scandir(directory):
            if (file.name.startswith(self.name + '_') or file.name.startswith(
                    self.name + '.')) and (
                    fnmatch(file, '*.fasta*') or fnmatch(file, '*.fna*')):
                self.assembly = path_clean(file_path=file.path)
        
        if not self.assembly:
            raise MissingPathError('Assembly file')
        if not file_check(file_path=self.assembly, file_or_directory='file'):
            raise BadPathError(str(self.assembly), 'Assembly file', 'Sample {} can not be processed as the assembly file path is invalid!'.format(self.name))

    def post_analysis_cleanup(self, keep=2):
        # method will perform post analysis cleanup for samples THAT COMPLETED ALL ANALYSIS STEPS based on the --keep
        # level submitted by the user. Samples that failed to complete will retain their intermediate files for
        # troubleshooting and restarting the analysis at a viable checkpoint

        self.logger.log(
            'Performing post analysis cleanup for sample {} based on a keep value of {}'.format(self.name, str(keep)),
            verbosity=3)

        # check to see if the analysis was completed, if not we will skip
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['individual_analysis'] != 'complete':
            self.logger.log(
                'Sample did NOT make it all the way through analysis, so intermediate files will kept for troubleshooting and reanalyzing')
            return []

        # based on the keep value, we will begin performing cleanup operations
        # we will pass back a list of files that need to be compressed to run that process in parallel
        files_for_compression = []

        # all keep conditions will involve compressing the raw read files
        files_for_compression.append(self.r1_fastq)
        files_for_compression.append(self.r2_fastq)

        if keep == 2:
            # compress the trimmed read files
            for attribute in ['r1_trimmed', 'r2_trimmed', 'r1_fastq_sorted', 'r2_fastq_sorted']:
                if hasattr(self, attribute):
                    file = getattr(self, attribute)
                    if file_check(file_path=file, file_or_directory='file'):
                        files_for_compression.append(file)

        #if keep == 1 or keep == 0:
            # we will keep all but the largest files (processed reads, bam)
            #subprocess.run(['rm', '-r', str(self.processed_reads_directory)])

        if keep == 0:
            # delete all intermediate files that are not essential to future group analysis
            pass

        return files_for_compression

    def initialize_sample_checkpoint_file(self, analysis_type, force):
        """
        initialize a sample checkpoint file based on the passed parameters
        """

        try:
            self.checkpoint = CheckPoint(
            name=self.name,
            analysis_type=analysis_type,
            logger=self.logger,
            checkpoint_directory=self.checkpoint_directory,
            force=force
            )
        except:  # need to figure out the potential error codes from CheckPoint
            pass
    
    def initialize_logger(self, log_filename):
        """
        initialize the logger for this sample
        """

        try:
            if not self.log_directory.is_dir():
                subprocess.run(['mkdir', str(self.log_directory)])
            self.logger = log.Log(
                log_filename=log_filename,
                stdout_verbosity_level=self.verbosity
            )
        except: # need to figure out the logger error codes
            pass


class AniSample(Sample):
    """
    Class AniSample extends the Sample class, adding the configuration required for taking a single sequencing sample through the species assignment process using ANI
    """

    def __init__(self, name, output_directory, force=False, reads_directory=None, verbosity=2):

        super().__init__(name, output_directory, reads_directory=reads_directory, verbosity=verbosity)

        # paths for trimmed reads
        self.processed_reads_directory = self.output_directory / 'reads' / 'processed_reads'
        self.r1_trimmed = self.processed_reads_directory / (self.name + '.R1.trimmed.fastq')
        self.r2_trimmed = self.processed_reads_directory / (self.name + '.R2.trimmed.fastq')

        # path to the species call file (may or may not exist). We will use the genus for read file subsampling if that info is available
        self.kraken2_directory = self.individual_analysis_directory / 'kraken2'
        self.species_call_file = self.kraken2_directory / (self.name + '_species_call.csv')

        # set the key assembly output paths
        self.assembly_directory = self.individual_analysis_directory / 'assembly'

        
        self.force = force
        self.log_filename=self.log_directory / ('{}.assembly.might.log'.format(self.name))

        self.assembly_status = None 

        # initialize the logger for this sample
        self.initialize_logger(self.log_filename)

        # initialize the CheckPoint for this sample
        self.initialize_sample_checkpoint_file('assembly', self.force)

        # if force is being invoked, remove any output that may exist
        if self.force:
            if file_check(file_path=self.assembly_directory, file_or_directory='directory'):
                subprocess.run(['rm', '-r', str(self.assembly_directory)])
            if file_check(file_path=self.r1_trimmed, file_or_directory='file'):
                subprocess.run(['rm', str(self.r1_trimmed)])
            if file_check(file_path=self.r2_trimmed, file_or_directory='file'):
                subprocess.run(['rm', str(self.r2_trimmed)])
        
    def check_assembly_status(self):
        """
        return the current status of the assembly for this sample ('new', 'incomplete', 'complete')
        """
        # if the assembly analysis has already been completed according to the checkpoint file, verify that the output
        # files exist. If it does NOT exist, revert the checkpoint file to indicate that analysis is required. Log a
        # warning that the checkpoint file was not in agreement with the output
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['assembly postprocessing'] == 'complete':
            if file_check(file_path=self.assembly_QC_report, file_or_directory='file') and file_check(file_path=self.assembly_file_trimmed, file_or_directory='file'):
                logger.log('Sample {} is marked complete in its assembly checkpoint file, and the key output was located'
                           ', so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                self.assembly_status = 'complete'
            else:
                raise CheckpointFileInconsistentError(self.checkpoint.checkpoint_file, 'assembly postprocessing', [str(self.assembly_QC_report), str(self.assembly_file_trimmed)])
    
    def validate_assembly(self):
        """
        ensure that the requirements for a kraken2 analysis are met. returns 0 if valid. If invalid, log the error message and returns 1
        """
        try:
            self.scan_for_fastq(self.reads_directory)
            return 0
        except MissingPathError as err:
            self.logger.log(str(err))
            return 1
        except BadPathError as err:
            self.logger.log(str(err))
            return 1

    def configure_assembly(self):
        """
        perform any setup operations for this analysis
        """
        if not file_check(file_path=self.assembly_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', '-p', str(self.assembly_directory)])
    

class KleborateSample(Sample):
    """
    Class KleborateSample extends the Sample class, adding the configuration required for taking a single sequencing sample through analysis with Kleborate
    """

    def __init__(self, name, output_directory, force=False, reads_directory=None, verbosity=2):

        super().__init__(name, output_directory, reads_directory=reads_directory, verbosity=verbosity)

        # paths for trimmed reads
        self.processed_reads_directory = self.output_directory / 'reads' / 'processed_reads'
        self.r1_trimmed = self.processed_reads_directory / (self.name + '.R1.trimmed.fastq')
        self.r2_trimmed = self.processed_reads_directory / (self.name + '.R2.trimmed.fastq')

        # path to the species call file (may or may not exist). We will use the genus for read file subsampling if that info is available
        self.kraken2_directory = self.individual_analysis_directory / 'kraken2'
        self.species_call_file = self.kraken2_directory / (self.name + '_species_call.csv')

        # set the key assembly output paths
        self.assembly_directory = self.individual_analysis_directory / 'assembly'

        
        self.force = force
        self.log_filename=self.log_directory / ('{}.assembly.might.log'.format(self.name))

        self.assembly_status = None 

        # initialize the logger for this sample
        self.initialize_logger(self.log_filename)

        # initialize the CheckPoint for this sample
        self.initialize_sample_checkpoint_file('assembly', self.force)

        # if force is being invoked, remove any output that may exist
        if self.force:
            if file_check(file_path=self.assembly_directory, file_or_directory='directory'):
                subprocess.run(['rm', '-r', str(self.assembly_directory)])
            if file_check(file_path=self.r1_trimmed, file_or_directory='file'):
                subprocess.run(['rm', str(self.r1_trimmed)])
            if file_check(file_path=self.r2_trimmed, file_or_directory='file'):
                subprocess.run(['rm', str(self.r2_trimmed)])
        
    def check_assembly_status(self):
        """
        return the current status of the assembly for this sample ('new', 'incomplete', 'complete')
        """
        # if the assembly analysis has already been completed according to the checkpoint file, verify that the output
        # files exist. If it does NOT exist, revert the checkpoint file to indicate that analysis is required. Log a
        # warning that the checkpoint file was not in agreement with the output
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['assembly postprocessing'] == 'complete':
            if file_check(file_path=self.assembly_QC_report, file_or_directory='file') and file_check(file_path=self.assembly_file_trimmed, file_or_directory='file'):
                logger.log('Sample {} is marked complete in its assembly checkpoint file, and the key output was located'
                           ', so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                self.assembly_status = 'complete'
            else:
                raise CheckpointFileInconsistentError(self.checkpoint.checkpoint_file, 'assembly postprocessing', [str(self.assembly_QC_report), str(self.assembly_file_trimmed)])
    
    def validate_assembly(self):
        """
        ensure that the requirements for a kraken2 analysis are met. returns 0 if valid. If invalid, log the error message and returns 1
        """
        try:
            self.scan_for_fastq(self.reads_directory)
            return 0
        except MissingPathError as err:
            self.logger.log(str(err))
            return 1
        except BadPathError as err:
            self.logger.log(str(err))
            return 1

    def configure_assembly(self):
        """
        perform any setup operations for this analysis
        """
        if not file_check(file_path=self.assembly_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', '-p', str(self.assembly_directory)])
    



    def configure_metadata_import(self, logger, force=False):
        """
        configure the sample object for importing metadata
        """
        # set the key output paths for this sample for metadata import
        self.metadata_directory = self.individual_analysis_directory / 'metadata'
        self.metadata_file = self.metadata_directory / (self.name + '_metadata.csv')

        if force:
            if file_check(file_path=self.metadata_directory, file_or_directory='directory'):
                subprocess.run(['rm', '-r', str(self.metadata_directory)])

        # make/remake the metadata directory if it does not exist
        if not self.metadata_directory.exists():
            subprocess.run(['mkdir', str(self.metadata_directory)])

        # if the metadata file already exists, we will mark this sample as 'imported'. Otherwise we will mark it as
        # 'analysis'
        if file_check(file_path=self.metadata_file, file_or_directory='file'):
            logger.log(
                'Metadata file for sample {} was located, so skipping this import'.format(self.name),
                verbosity=3)
            return 'imported'

        # initialize a metadata log file for this sample
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])
        self.logger = log.Log(
            log_filename=self.log_directory / ('{}.metadata_import.might.log'.format(self.name)),
            stdout_verbosity_level=self.verbosity
        )

        return 'analysis'

    def configure_mlst_analysis(self, logger, force=False):
        """
        configure the sample object for mlst analysis
        """
        # check for a mlst_analysis checkpoint file to see if the analysis has already been completed
        # Initialize a CheckPoint object that will handle all of the checkpoint read/write actions for this sample
        self.checkpoint = CheckPoint(
            name=self.name,
            analysis_type='mlst',
            logger=logger,
            checkpoint_directory=self.checkpoint_directory,
            force=force
        )

        # set the key output paths for this sample for mlst analysis
        # path to the species call file (may or may not exist)
        self.kraken2_directory = self.individual_analysis_directory / 'kraken2'
        self.species_call_file = self.kraken2_directory / (self.name + '_species_call.csv')
        self.individual_mlst_directory = self.individual_analysis_directory / 'mlst'
        if not self.individual_mlst_directory.exists():
            subprocess.run(['mkdir', str(self.individual_mlst_directory)])
        self.mlst_report = self.individual_mlst_directory / (self.name + '_mlst_report.csv')

        if force:
            self.checkpoint.update_checkpoint_file(analysis_step='mlst analysis', new_status=None)
            if file_check(file_path=self.mlst_report, file_or_directory='file'):
                subprocess.run(['rm', str(self.mlst_report)])

        # if the mlst analysis has already been completed according to the checkpoint file, verify that the output
        # file exists. If it does NOT exist, revert the checkpoint file to indicate that analysis is required. Log a
        # warning that the checkpoint file was not in agreement with the output
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['mlst analysis'] == 'complete':
            if file_check(file_path=self.mlst_report, file_or_directory='file'):
                logger.log('Sample {} is marked complete in its mlst checkpoint file, and the mlst report file was'
                           ' located, so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                return 'complete'
            else:
                logger.log(
                    'WARNING: Sample {} is marked complete in its mlst checkpoint file, BUT the mlst report '
                    'could NOT be located. Individual analysis will be performed'.format(self.name))
                self.checkpoint.update_checkpoint_file(analysis_step='mlst analysis', new_status=None)

        # initialize a mlst log file for this sample
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])
        self.logger = log.Log(
            log_filename=self.log_directory / ('{}.mlst.might.log'.format(self.name)),
            stdout_verbosity_level=self.verbosity
        )

        # ensure that the assembly file required for analyzing this sample is visible
        if not file_check(file_path=self.assembly, file_or_directory='file'):
            self.logger.log('Sample {} can not be processed as the assembly file is missing!'.format(self.name))
            return 'missing assembly'

        return 'analysis'

    def configure_QC_analysis(self, logger, force=False):
        """
        configure the sample object for performing QC analysis
        """
        # set the key output paths for this sample for metadata import
        self.qc_directory = self.individual_analysis_directory / 'QC'
        self.qc_file = self.qc_directory / (self.name + '_QC.csv')

        # key output files from other analysis steps
        self.kraken2_directory = self.individual_analysis_directory / 'kraken2'
        self.species_call_file = self.kraken2_directory / (self.name + '_species_call.csv')

        self.assembly_directory = self.individual_analysis_directory / 'assembly'
        self.assembly_QC_report = self.assembly_directory / (self.name + '.assembly_QC.csv')

        self.metadata_directory = self.individual_analysis_directory / 'metadata'
        self.metadata_file = self.metadata_directory / (self.name + '_metadata.csv')

        self.individual_mlst_directory = self.individual_analysis_directory / 'mlst'
        self.mlst_report = self.individual_mlst_directory / (self.name + '_mlst_report.csv')

        # make/remake the metadata directory if it does not exist
        if not self.qc_directory.exists():
            subprocess.run(['mkdir', str(self.qc_directory)])

        # every run will recreate the sample QC sheet, so remove the existing version if there is one
        if file_check(file_path=self.qc_file, file_or_directory='file'):
            subprocess.run(['rm', str(self.qc_file)])

        # initialize a qc log file for this sample
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])
        self.logger = log.Log(
            log_filename=self.log_directory / ('{}.QC.might.log'.format(self.name)),
            stdout_verbosity_level=self.verbosity
        )



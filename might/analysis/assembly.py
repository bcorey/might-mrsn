#!/usr/bin/env python
# encoding: utf-8
import math
import os
import random
import shutil
import subprocess
import sys
import time
import psutil
from datetime import datetime
from pathlib import Path
import multiprocessing as mp
from fnmatch import fnmatch

import pandas as pd
from Bio import SeqIO

from might.analysis.sample import Sample
from might.common.errors import MissingPrerequisiteError, MissingPathError, BadPathError, CheckpointFileInconsistentError
from might.common import log
from might.common.miscellaneous import file_check, check_for_dependency, quit_with_error, parallel_compression_handler, \
    compression_handler, round_down


class Assembly:

    def __init__(self, output_directory, run_name, reads_directory=None, sample_list_file = None, keep=2, force=False, no_clean=False,
                 cores=1, cores_per_sample=1, verbosity=2, timeout=12, length_filter=200, coverage_filter=10):

        self.output_directory = output_directory
        self.run_name = run_name
        self.sample_list_file = sample_list_file
        self.sample_list = []
        self.individual_sample_analyses = None
        self.log_directory = None
        self.reads_directory = reads_directory
        self.keep = keep
        self.force = force
        self.no_clean = no_clean
        self.cores = cores
        self.cores_per_sample = cores_per_sample
        self.verbosity = verbosity
        self.max_time = timeout
        
        # filter_length and coverage_filter are used to filter the raw assembly coming out of shovill
        self.length_filter = length_filter
        self.coverage_filter = coverage_filter

    def check_paths(self):
        """
        validate the required paths exist
        """
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            print(log.bold_yellow('WARNING: The output directory path ({}) is new, so we will make it now'.format(str(self.output_directory))))
            subprocess.run(
                [
                    'mkdir', '-p', str(self.output_directory)
                ]
            )

        # ensure that the output directory at least has an individual_sample_analyses directory for all but bcl2fastq and cluster
        self.individual_sample_analyses = self.output_directory / 'individual_sample_analyses'
        if not file_check(file_path=self.individual_sample_analyses, file_or_directory='directory'):
            subprocess.run(
                [
                    'mkdir', '-p', str(self.individual_sample_analyses)
                ]
            )
        
        # clean and validate the reads and assembly directories IF THEY WERE PASSED
        if self.reads_directory:
            self.reads_directory = Path(self.reads_directory).expanduser().resolve()
            if not file_check(file_path=self.reads_directory, file_or_directory='directory'):
                raise BadPathError(self.reads_directory, 'raw reads directory')

        # if a sample_list_file was passed, validate it
        if self.sample_list_file:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list file')
            
        # prepare the log directory
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])
  
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        try:
            self.logger = log.Log(
                log_filename=self.log_directory / 'assembly.shovill.might.log',
                stdout_verbosity_level=self.verbosity
            )
        except: # need to figure out the logger error codes
            pass

    def dependency_check(self):
        """
        Ensure that the primary dependencies for this analysis are available on the path
        """
        try:
            assert check_for_dependency('bbduk.sh')
        except AssertionError:
            quit_with_error(self.logger, 'bbduk.sh could NOT be located')

        try:
            assert check_for_dependency('shovill')
        except AssertionError:
            quit_with_error(self.logger, 'Shovill could NOT be located')

    def generate_sample_list(self):
        """
        generate the list of samples to assemble, UNLESS the list was passed by the user
        """
        # clean, validate, and read in the sample list IF IT WAS PASSED
        if self.sample_list_file is not None:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list')

            with open(self.sample_list_file, 'r') as f:
                self.sample_list = [line.strip() for line in f.readlines()]
        else:
            
            for entry in os.scandir(self.individual_sample_analyses):
                if file_check(file_path=Path(entry.path).expanduser().resolve(), file_or_directory='directory'):
                    if entry.name == 'individual_sample_analyses':
                        continue
                    sample_name = entry.name
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)

            for entry in os.scandir(self.reads_directory):
                if fnmatch(entry.name, '*_R*.fastq*'):
                    sample_name = entry.name.split('_')[0]
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)
                if fnmatch(entry.name, '*.R*.fastq*'):
                    sample_name = entry.name.split('.')[0]
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)

    def generate_assembly_sample_objects(self):
        """
        generate sample objects for all of the samples in sample list and store it in self.sample_objects
        """
        self.sample_objects = []
        for sample in self.sample_list:
            if sample != 'Undetermined':  # No point in assembling the recycling bin of read files
                self.sample_objects.append(
                    AssemblySample(
                        sample, 
                        self.output_directory,
                        self.run_name,
                        force = self.force, 
                        reads_directory=self.reads_directory, 
                        verbosity=self.verbosity,
                        timeout = self.max_time, 
                        length_filter=self.length_filter, 
                        coverage_filter=self.coverage_filter,
                        cores=self.cores_per_sample,
                        no_clean = self.no_clean
                        )
                    )

        # iterate throught the sample objects in self.sample_objects. Partition samples into one of two lists:
        # analysis_required and analysis_complete based on the return value of <sample>.configure_amr_analysis
        self.analysis_required = []
        self.analysis_complete = []
        self.analysis_impossible = []

        for sample in self.sample_objects:

            sample.check_assembly_status()
            if sample.assembly_status == 'complete':
                self.analysis_complete.append(sample)
                continue

            try:
                sample.validate_assembly_possible()  # the sample is reporting that it is capable of undergoing assembly
                self.analysis_required.append(sample)
            except MissingPathError as err:
                self.logger.log(str(err))
                self.analysis_impossible.append(sample)
            except BadPathError as err:
                self.logger.log(str(err))
                self.analysis_impossible.append(sample)

    def preview_of_slated_analyses(self):
        """
        log a summary of the analyses to be performed
        """
        self.logger.log_section_header('Summary of assembly to be performed')
        self.logger.log('\n {} samples were detected for analysis'.format(str(len(self.sample_objects))))
        self.logger.log('\n {} samples have already undergone assembly'.format(str(len(self.analysis_complete))))
        self.logger.log('\n The following {} samples will undergo assembly:'.format(str(len(self.analysis_required))))
        for sample in self.analysis_required:
            self.logger.log(str(sample.name))
        self.logger.log('\n The following {} samples were flagged for missing a requisite file for assembly:'.format(str(len(self.analysis_impossible))))
        for sample in self.analysis_impossible:
            self.logger.log(str(sample.name))

    def threaded_assemblies(self):
        """
        run threaded_assembly() for each sample in self.analysis_required using mutliprocessing
        """
        self.logger.log_section_header('Now starting threaded assemblies')
        self.logger.log_explanation("Running threaded assemblies using python multiprocessing")

        pool_size = math.floor(self.cores / self.cores_per_sample)
        self.logger.log("Running {} processes ({} cores at {} cores per sample)".format(str(pool_size), str(self.cores), str(self.cores_per_sample)))

        p = mp.Pool(pool_size)
        p.map(AssemblySample.threaded_assembly_process, self.analysis_required)
        p.close()
        p.join()

        self.logger.log('\nAssembly threaded analyses are now complete!')

    def run_pbs(self):
        """
        create and submit a pbs batch script that will perform this analysis. This gets around the network stability
        issues that can cause long runnning jobs to be terminated
        """

        self.logger.log_section_header('Submitting assembly job(s) to pbs server')
        self.logger.log_explanation('Since the -pbs flag was invoked, we will submit the job(s) to the pbs batch server for'
                                    'processing. Please note that the current script will terminate once the job has '
                                    'been submitted, so you will need to monitor qstat to evaluate when the job has '
                                    'completed\n')

        self.logger.log('Generating the pbs batch script(s)\n')

        for sample in self.analysis_required:

            # set the pbs scipt file path
            sample.pbs_script_file = sample.assembly_directory / (sample.name + '.assembly.pbs')

            sample.logger.log('Batch script file at: {}\n'.format(str(sample.pbs_script_file)))

            # verify that the pbs file does NOT already exist. If it does, delete it
            if file_check(file_path=sample.pbs_script_file, file_or_directory='file'):
                sample.logger.log('An existing pbs batch script file was located and will be overwritten\n')
                subprocess.run(['rm', str(sample.pbs_script_file)])

            # generate a tmp file that will act as a sample-list. This will allow us to run kraken2 for a single sample
            sample.sample_list = sample.assembly_directory / (sample.name + '.sample_list.tmp')

            # verify that the sample list tmp file does NOT already exist. If it does, delete it
            if file_check(file_path=sample.sample_list, file_or_directory='file'):
                sample.logger.log('An existing sample list tmp file was located and will be overwritten\n')
                subprocess.run(['rm', str(sample.sample_list)])

            # write the sample name to the tmp file
            with open(str(sample.sample_list), 'w') as sample_list:
                sample_list.write(sample.name)

            # prepare the pbs file
            with open(str(sample.pbs_script_file), 'w') as pbs:

                # add the shebang line
                pbs.write('#!/bin/bash\n')

                # write the nodes, cores, and walltime parameters
                pbs.write('#PBS -l nodes=1:ppn=1\n')
                pbs.write('#PBS -l walltime=36:00:00\n')

                output_path = sample.assembly_directory / (sample.name + '.assembly.stdout')
                error_path = sample.assembly_directory / (sample.name + '.assembly.stderr')

                # write the stdout and stderr paths
                pbs.write('#PBS -o {}\n'.format(str(output_path)))
                pbs.write('#PBS -e {}\n'.format(str(error_path)))

                # teach pbs server about conda
                pbs.write('source /data/MRSN_Research/mrsn/miniconda/etc/profile.d/conda.sh\n')

                # activate might
                pbs.write('conda activate might\n')

                # write the actual might command to be run as a batch job
                if self.no_clean:
                    pbs.write('might assembly {} {} --sample-list {} --cores {} --cores-per-sample {} --no-clean'.format(self.output_directory, self.reads_directory, sample.sample_list, self.cores_per_sample, self.cores_per_sample))
                else:
                    pbs.write('might assembly {} {} --sample-list {} --cores {} --cores-per-sample {}'.format(self.output_directory, self.reads_directory, sample.sample_list, self.cores_per_sample, self.cores_per_sample))

            # submit the pbs job to the batch server
            sample.logger.log('Submitting the batch script file to the pbs batch server for sample: {}'.format(sample.name))

            pbs_job_submission_process = subprocess.run(
                [
                    'qsub',
                    '-q',
                    'batch',
                    str(sample.pbs_script_file)
                ],
                stdout=subprocess.PIPE
            )

            sample.logger.log(str(pbs_job_submission_process.stdout))

            time.sleep(1)

        self.logger.log('The job(s) have been submitted. Please monitor qstat for job completion!')
        sys.exit()


class AssemblySample(Sample):
    """
    Class AssemblySample extends the Sample class, adding the configuration required for taking a single sequencing sample through assembly with Shovill
    """

    def __init__(self, name, output_directory, run_name, force=False, reads_directory=None, verbosity=2, timeout=12, length_filter=200, coverage_filter=10, cores=1, no_clean=False):

        super().__init__(name, output_directory, reads_directory=reads_directory, verbosity=verbosity)

        self.run_name = run_name
        
        # filter_length and coverage_filter are used to filter the raw assembly coming out of shovill
        self.length_filter = length_filter
        self.coverage_filter = coverage_filter

        # timeout is an optional command line argument (default is 12hrs)
        self.max_time = int(timeout) * 3600

        # cores represents the number of cores that should be assigned to this assembly process (default is 1)
        self.cores = cores

        # whether files should be compressed at the end of the run
        self.no_clean = no_clean

        # paths for trimmed reads
        self.processed_reads_directory = self.output_directory / 'reads' / 'processed_reads'
        self.r1_trimmed = self.processed_reads_directory / ('{}.{}.R1.trimmed.fastq'.format(self.name, self.run_name))
        self.r2_trimmed = self.processed_reads_directory / ('{}.{}.R2.trimmed.fastq'.format(self.name, self.run_name))

        # path to the species call file (may or may not exist). We will use the genus for read file subsampling if that info is available
        self.kraken2_directory = self.individual_analysis_directory / 'kraken2'
        self.species_call_file = self.kraken2_directory / (self.name + '_species_call.csv')
        self.genus = None
        self.estimated_genome_size = None
        self.target_depth_of_coverage = 150

        # set the key assembly output paths
        self.assembly_directory = self.individual_analysis_directory / 'assembly'
        self.assembly_file_raw = self.assembly_directory / 'contigs.fa'
        self.top_level_assembly_directory = self.output_directory / 'assembly'
        self.assembly_file_final = self.assembly_directory / '{}.{}.shovill.fasta'.format(self.name, self.run_name)
        self.assembly_file_final_copied = self.top_level_assembly_directory / self.assembly_file_final.name

        # key QC metrics/stats and associated files
        self.assembly_QC_report = self.assembly_directory / (self.name + '.assembly_QC.csv')
        self.bbmap_directory = self.assembly_directory / 'ref'
        self.insert_histogram = self.assembly_directory / (self.name + '.insert_histogram.txt')
        self.median_insert_size = None
        self.raw_total_contig_length = 0  # used for calculating total length, average read depth, and N50
        self.raw_total_read_depth = 0  # used for calculating the average read depth
        self.raw_contig_count = 0
        self.average_read_depth = None
        self.read_depth_filter = 10  # the default is 10X
        self.final_contig_count = None
        self.final_assembly_length = None
        self.final_assembly_average_read_depth = None
        self.final_assembly_n50 = None

        self.force = force
        self.log_filename=self.log_directory / ('{}.assembly.might.log'.format(self.name))

        self.assembly_status = None 

        # initialize the logger for this sample
        self.initialize_logger(self.log_filename)

        # initialize the CheckPoint for this sample
        self.initialize_sample_checkpoint_file('assembly', self.force)

        # if force is being invoked, remove any output that may exist
        if self.force:
            if file_check(file_path=self.assembly_directory, file_or_directory='directory'):
                subprocess.run(['rm', '-r', str(self.assembly_directory)])
            if file_check(file_path=self.r1_trimmed, file_or_directory='file'):
                subprocess.run(['rm', str(self.r1_trimmed)])
            if file_check(file_path=self.r2_trimmed, file_or_directory='file'):
                subprocess.run(['rm', str(self.r2_trimmed)])

    def threaded_assembly_process(self):
        """
        governing process for a threaded assembly analysis
        """

        self.preview_individual_analysis()

        # process read files using read_file_preprocessing()
        try:
            self.read_file_preprocessing()
        except AssertionError:
            self.logger.log("[{}] Something went awry during read file preprocessing. Returning this process to the pool")
            return
        
        # estimate the genome size if possible with estimate_genome_size_by_genus()
        try:
            self.estimate_genome_size_by_genus()
        except AssertionError:
            self.logger.log("[{}] Something went awry during genome size estimation. Returning this process to the pool")
            return
        
        # run the shovill assembly with run_shovill_assembly()
        try:
            self.run_shovill_assembly()
        except AssertionError:
            self.logger.log("[{}] Something went awry during assembly with shovill. Returning this process to the pool")
            return
        
        # perform assembly postprocessing and generate the QC report
        try:
            self.assembly_postprocessing()
        except AssertionError:
            self.logger.log("[{}] Something went awry during the assembly post-processing step. Returning this process to the pool")
            return
        
        # compress intermediate files
        try:
            self.assembly_cleanup()
        except AssertionError:
            return
        
        self.logger.log_section_header('Individual sample assembly process complete for {}!'.format(self.name))
        return

    def check_assembly_status(self):
        """
        return the current status of the assembly for this sample ('new', 'incomplete', 'complete')
        """
        # if the assembly analysis has already been completed according to the checkpoint file, verify that the output
        # files exist. If it does NOT exist, revert the checkpoint file to indicate that analysis is required. Log a
        # warning that the checkpoint file was not in agreement with the output
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['assembly postprocessing'] == 'complete':
            if file_check(file_path=self.assembly_QC_report, file_or_directory='file') and file_check(file_path=self.assembly_file_final, file_or_directory='file'):
                self.logger.log('Sample {} is marked complete in its assembly checkpoint file, and the key output was located'
                           ', so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                self.assembly_status = 'complete'
            else:
                raise CheckpointFileInconsistentError(self.checkpoint.checkpoint_file, 'assembly postprocessing', [str(self.assembly_QC_report), str(self.assembly_file_final)])
    
    def validate_assembly_possible(self):
        """
        ensure that the requirements for assembly are met. returns 0 if valid. If invalid, log the error message and returns 1
        """
        try:
            self.scan_for_fastq(self.reads_directory)
        except MissingPathError as err:
            self.logger.log(str(err))
            raise
        except BadPathError as err:
            self.logger.log(str(err))
            raise

    def preview_individual_analysis(self):
        """
        log a preview of the individual analysis
        """
        self.logger.log_section_header("Preview of Assembly Plan", verbosity=3)
        self.logger.log_explanation("Summary of key assembly parameters", verbosity=3)
        self.logger.log("The name of this assembly project is {}".format(log.bold_green(self.name)), verbosity=3)
        self.logger.log("The forward read for this assembly project is {}".format(log.bold_green(str(self.r1_fastq))), verbosity=3)
        self.logger.log("The reverse read for this assembly project is {}".format(log.bold_green(str(self.r2_fastq))), verbosity=3)
        self.logger.log(log.underline("\nAssembly parameters:"), verbosity=3)
        self.logger.log("The assembler: {}".format(str(log.bold_green('shovill v1.0.4'))), verbosity=3)
        self.logger.log("The length filter for this assembly project is {}".format(log.bold_green(str(self.length_filter))), verbosity=3)
        self.logger.log("The default coverage filter for this assembly project is {}".format(log.bold_green(str(self.coverage_filter))), verbosity=3)
        self.logger.log("\nThe core allocation for this assembly project is {}".format(log.bold_green(str(self.cores))), verbosity=3)

    def read_file_preprocessing(self):
        """
        read_file_preprocessing()
            check to see if the processed, unsorted reads are present
            check to see if the raw reads are both present
            run bbduk to generate the processed, unsorted reads for analysis
        """
        self.logger.log_section_header('Adapter/Quality Trimming', verbosity=3)
        self.logger.log_explanation('Perform adapter (and light quality) trimming of Illumina read files using bbduk.', verbosity=3)

        # check for trimmed fastq files in the processed reads directory
        if file_check(file_path=self.r1_trimmed, file_or_directory='file') and file_check(file_path=self.r2_trimmed,
                                                                                        file_or_directory='file'):
            self.logger.log('Located trimmed read files for ' + log.bold_green(str(self.name)), verbosity=3)

            # verify that the file can be trusted using the checkpoint object
            self.checkpoint.read_checkpoint_file()
            if self.checkpoint.checkpoint_dict['reads preprocessed'] == 'complete':
                self.logger.log('checkpoint file indicates that this step was completed. Skipping!', verbosity=3)
                return self.r1_trimmed, self.r2_trimmed
            else:
                self.logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                        'will be trashed the analysis redone', verbosity=3)
                if file_check(file_path=self.r1_trimmed, file_or_directory='file'):
                    subprocess.run(['rm', str(self.r1_trimmed)])
                if file_check(file_path=self.r2_trimmed, file_or_directory='file'):
                    subprocess.run(['rm', str(self.r2_trimmed)])

        # check to make sure that the output_directory exists. If not, make it
        try:
            assert file_check(file_path=self.output_directory, file_or_directory='directory')
        except AssertionError:
            subprocess.run(['mkdir', str(self.output_directory)], stderr=subprocess.DEVNULL)

        # verify that we can see the adapter file that should come packaged with the distribution
        adapter_file = Path(__file__).parent.parent / 'resources' / 'adapters.fa'
        try:
            assert file_check(file_path=adapter_file, file_or_directory='file')
        except AssertionError:
            self.logger.log('read_file_preprocessing() unable to locate the adapter file at' + str(adapter_file))
            raise

        # make sure that the self.r1_fastq and self.r2_fastq files are decompressed
        self.r1_fastq = compression_handler(self.r1_fastq, self.logger, decompress=True)
        self.r2_fastq = compression_handler(self.r2_fastq, self.logger, decompress=True)

        # prepare the adapter trimming cmd as a string
        adapter_trimming_cmd = \
            'bbduk.sh in1={} in2={} out1={} out2={} ref={} ktrim=r k=23 mink=11 hdist=1 tpe tbo qtrim=r trimq=15 mlf=50 t={}'.format(
                str(self.r1_fastq), str(self.r2_fastq), str(self.r1_trimmed), str(self.r2_trimmed), str(adapter_file), str(self.cores)
            )

        self.logger.log('\nbbduk command: {}'.format(adapter_trimming_cmd), verbosity=3)

        # run the adapter trimming command
        bbduk_process = subprocess.run(
            ['bbduk.sh',
            '-Xmx6G',
            'in1=' + str(self.r1_fastq),
            'in2=' + str(self.r2_fastq),
            'out1=' + str(self.r1_trimmed),
            'out2=' + str(self.r2_trimmed),
            'ref=' + str(adapter_file),
            'ktrim=r',
            'k=23',
            'mink=11',
            'hdist=1',
            'tpe',
            'tbo',
            'qtrim=r',
            'trimq=15',
            'minlen=100',
            '-t=' + str(self.cores)],
            stderr=subprocess.PIPE,
            universal_newlines=True)

        # check the return code from bbduk
        try:
            assert bbduk_process.returncode == 0
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='reads preprocessed', new_status='failed')
            self.logger.log('bbduk returned a non-zero returncode for {}: {}\n{}'.format(self.name, str(bbduk_process.returncode), str(bbduk_process.stderr)))
            raise

        # log the bbduk stderr to the sample's log file
        bbduk_stderr = bbduk_process.stderr.split('Version')[1].splitlines()
        self.logger.log('Version ' + bbduk_stderr[0], verbosity=3)
        for line in bbduk_stderr[1:]:
            self.logger.log(line, verbosity=3)

        # validate that the output file was generated where we were expecting it
        try:
            assert file_check(file_path=self.r1_trimmed, file_or_directory='file') and file_check(
                file_path=self.r2_trimmed, file_or_directory='file')
            self.checkpoint.update_checkpoint_file(analysis_step='reads preprocessed', new_status='complete')
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='reads preprocessed', new_status='failed')
            self.logger.log('something went wrong during trimming. bbduk failed to generate at least one of the trimmed read files for ' + self.name)
            raise
    
    def estimate_genome_size_by_genus(self):
        """
        shovill includes read file subsampling based on anticipated genome size. Use the results of the kraken2-based species-call (if available) to determine
        a likely estimated genome size
        """
        self.logger.log_section_header("Estimated genome size based on genus assignment", verbosity=3)
        self.logger.log_explanation("Shovill includes the option to subsample read files based on the estimated genome size. To establish this estimate MIGHT will attempt to use the kraken2-based species-call (if available)", verbosity=3)

        # attempt to get the genus from the species caller output
        species_column_list = [
            'Genus',
            'Species',
            'WGS ID',
            'top hits',
            'WGS ID - Method',
            'DB Build',
            'Contaminated?',
            'Manually entered fields?'
        ]
        if file_check(file_path=self.species_call_file, file_or_directory='file'):
            try:
                species_dataframe = pd.read_csv(str(self.species_call_file), usecols=species_column_list)
                self.genus = species_dataframe.loc[0, 'Genus'].lower()
            except pd.errors.EmptyDataError:  # Case of empty csv
                self.logger.log('The individual species call file for sample {} is empty!'.format(self.name), verbosity=3)
            except ValueError:  # Incorrectly formatted csv
                self.logger.log('The species call file for sample {} is incorrectly formatted!'.format(self.name), verbosity=3)
            except:
                self.logger.log('An unexpected error occured while trying to read the species output for this sample. Proceeding with the conservative subsampling metrics', verbosity=3)
            
        if self.genus:
            genus_size_dict = {
                'acinetobacter': '4.2M',
                'escherichia': '5.2M',
                'klebsiella': '5.6M',
                'staphylococcus': '3.2M',
                'pseudomonas': '7.2M',
                'enterobacter': '5.2M',
                'enterococcus': '3.5M',
                'citrobacter': '5.2M',
                'morganella': '4.2M',
                'proteus': '4.5M',
                'shigella': '5.2M',
                'serratia': '6.0M',
                'streptococcus': '2.5M',
                'stenotrophomonas': '5.5M',
                'neisseria': '2.5M',
                'candida': '16M',
                'phage/virus': '0.25M',
            }

            if self.genus in genus_size_dict.keys():
                self.estimated_genome_size = genus_size_dict[self.genus]
                self.logger.log('Sample {} was assigned a genus of {} and will be evaluated for subsampling using an estimated genome size of {}Mb\n'.format(
                        self.name, self.genus, str(self.estimated_genome_size)), verbosity=3)
            else:
                self.estimated_genome_size = None
                self.logger.log(
                    'Sample {} was assigned a genus of {}, which is not in the dictionary, and will be evaluated for subsampling by shovill\n'.format(
                        self.name, self.genus, verbosity=3))
        else:
            self.estimated_genome_size = None
            self.logger.log(
                'Sample {} was not assigned a genus, and will be evaluated for subsampling by shovill\n'.format(self.name), verbosity=3)

    def read_file_subsample(self, target_depth_of_coverage=100):
        self.logger.log_section_header('Read file subsampling', verbosity=3)
        self.logger.log_explanation('Determining if the read files are of excessive size. If they are we will attempt to '
                            'downsample to a read count more likely to complete a successful assembly.', verbosity=3)

        self.logger.log('The current target depth of coverage is {}x\n'.format(str(target_depth_of_coverage)), verbosity=3)

        # create tmp file paths for the r1 and r2 read files
        r1_tmp = self.r1_trimmed.parent / (self.name + '.r1.fastq.tmp')
        r2_tmp = self.r2_trimmed.parent / (self.name + '.r2.fastq.tmp')

        # Delete the existing files (old ones) if they exist
        if file_check(file_path=r1_tmp, file_or_directory='file'):
            subprocess.run(['rm', str(r1_tmp)])
        if file_check(file_path=r2_tmp, file_or_directory='file'):
            subprocess.run(['rm', str(r2_tmp)])

        # read the r1 read into memory to get an estimate of the read length
        r1_reads = SeqIO.parse(str(self.r1_trimmed), 'fastq')
        read_count = len(list(r1_reads))

        # if the number of reads is less than 25K, we won't perform any subsampling. I would love to just fail the assembly
        # outright, but other people ThInK ThAt ItS BeTtEr To JuSt LeT iT FaIl
        if read_count <= 25000:
            self.logger.log('The read count is laughably small for this sample ({} reads), so no subsampling will be performed'.format(str(read_count)), verbosity=3)
            del r1_reads  # garbage collection
            return self.r1_trimmed, self.r2_trimmed

        r1_reads = SeqIO.parse(str(self.r1_trimmed), 'fastq')
        num_reads_sampled = 25000
        reads_for_length_estimate = set(random.sample(range(read_count+1), num_reads_sampled))
        record_number = 0
        total_read_length = 0
        for read in r1_reads:
            if record_number in reads_for_length_estimate:
                total_read_length += len(read.seq)
            record_number += 1
        read_length = int(math.floor((total_read_length / num_reads_sampled)))

        self.logger.log('Read length sampling suggests that the average read length post-trimming is {}bp\n'.format(str(read_length)), verbosity=3)

        # calculate the number of reads to preserve based on the estimated genome size and read depth
        if self.estimated_genome_size:
            number_of_reads_to_keep = int(math.ceil((int(self.estimated_genome_size[:1]) * 1000000 * target_depth_of_coverage) / (read_length * 2)))  # times two to account for paired end reads
        else:  # if shovill was estimating the genome size, just default to 7 here
            number_of_reads_to_keep = int(math.ceil((7 * 1000000 * target_depth_of_coverage) / (read_length * 2)))  # times two to account for paired end reads

        # if the number_of_reads_to_keep is less than or equal to the read_count, we will return the reads with no modification
        if number_of_reads_to_keep >= read_count and target_depth_of_coverage == 100:
            self.logger.log('Sample {} has {} reads in the r1 file, which is less than the cutoff value of {}, so no subsampling will be performed'.format(self.name, str(read_count), str(number_of_reads_to_keep)), verbosity=3)
            return self.r1_trimmed, self.r2_trimmed
        elif number_of_reads_to_keep >= read_count and target_depth_of_coverage < 100:  # case where the current depth of coverage would require more read depth than is available. Raise the error to tell the calling method to try to subsample more stringently or fail the assembly
            raise AssertionError

        self.logger.log('Sample {} has {} reads in the r1 file, which is greater than the cutoff value of {}, so subsampling will be performed'.format(self.name, str(read_count), str(number_of_reads_to_keep)), verbosity=3)

        # use the bbtools reformat.sh utility to generate a subset of the reads
        reformat_proc = subprocess.run(
            [
                'reformat.sh',
                'in1={}'.format(str(self.r1_trimmed)),
                'in2={}'.format(str(self.r2_trimmed)),
                'out1={}'.format(str(r1_tmp)),
                'out2={}'.format(str(r2_tmp)),
                'samplereadstarget={}'.format(str(number_of_reads_to_keep))

            ],
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

        # check the return code from bbduk
        try:
            assert reformat_proc.returncode == 0
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='assembly postprocessing', new_status='failed')
            self.logger.log('bbtools returned a non-zero returncode for {}: {}\n{}'.format(self.name, str(reformat_proc.returncode), str(reformat_proc.stderr)))
            raise
        
        # validate that the output files were generated where we were expecting it
        try:
            assert file_check(file_path=r1_tmp, file_or_directory='file') and file_check(file_path=r2_tmp, file_or_directory='file')
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='assembly postprocessing', new_status='failed')
            self.logger.log('something went wrong during read file subsampling. bbtools failed to generate at least one of the subsampled read files for ' + self.name)
            raise

        return r1_tmp, r2_tmp

    def run_shovill_assembly(self):
        """
        run the shovill assembler using the processed read files and the estimated genome size
        """
        self.logger.log_section_header("Shovill Assembly", verbosity=3)
        self.logger.log_explanation("Running the shovill assembly program (if necessary)", verbosity=3)

        # check the checkpoint file for this step
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['assembly'] == 'complete':
            if file_check(file_path=self.assembly_file_raw, file_or_directory='file'):
                self.logger.log('checkpoint file indicates that this step was completed. Skipping!', verbosity=3)
                return
        else:
            self.logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                    'will be trashed and the analysis redone', verbosity=3)
            if file_check(file_path=self.assembly_directory, file_or_directory='directory'):
                subprocess.run(['rm', '-r', str(self.assembly_directory)])

        # check for trimmed fastq files in the output directory
        if file_check(file_path=self.r1_trimmed, file_or_directory='file') and file_check(file_path=self.r2_trimmed, file_or_directory='file'):
            self.logger.log('Located trimmed read files for ' + log.bold_green(str(self.name)), verbosity=3)
        else:
            self.checkpoint.update_checkpoint_file(analysis_step='reads preprocessed', new_status='failed')
            self.logger.log('could not locate the trimmed fastq files in the output directory!')
            raise AssertionError

        # update the checkpoint file (replace "failed" or None with None)
        self.checkpoint.update_checkpoint_file(analysis_step='assembly', new_status=None)

        # check to make sure that the output_directory does NOT exist. Newbler needs to do it itself
        try:
            assert not file_check(file_path=self.assembly_directory, file_or_directory='directory')
        except AssertionError:
            subprocess.run(['rm', '-r', str(self.assembly_directory)])

        # make sure that the r1_trimmed and r2_trimmed files are decompressed
        self.r1_trimmed = compression_handler(self.r1_trimmed, self.logger, decompress=True)
        self.r2_trimmed = compression_handler(self.r2_trimmed, self.logger, decompress=True)

        # run the assembly command
        if self.genus:  # if the genus was determined during estimate_genome_size_by_genus(), we supply the estimated genome size to the --gsize flag

            # prepare the assembly command as a string
            assembly_cmd = 'shovill --outdir {} --R1 {} --R2 {} --depth {} --mminlen {} --mincov {} --gsize {} --cpus {}\n'.format(str(self.assembly_directory), str(self.r1_trimmed), str(self.r2_trimmed), str(self.target_depth_of_coverage), str(self.length_filter), str(self.coverage_filter), str(self.estimated_genome_size), str(self.cores))
            self.logger.log('\nassembly command: {}'.format(assembly_cmd), verbosity=3)

            try:
                assembly_process = subprocess.Popen(
                    [
                        'shovill',
                        '--outdir', str(self.assembly_directory),
                        '--R1',str(self.r1_trimmed),
                        '--R2',str(self.r2_trimmed),
                        '--depth', str(self.target_depth_of_coverage),
                        '--minlen', str(self.length_filter),
                        '--mincov', str(self.coverage_filter),
                        '--gsize', str(self.estimated_genome_size),
                        '--cpus', str(self.cores)
                    ],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.PIPE
                )
                outs, errs = assembly_process.communicate(timeout=self.max_time)
            except subprocess.TimeoutExpired:
                self.logger.log('Shovill failed to complete in the allotted time ({} hrs), so it will be terminated!'.format(str(self.max_time/3600)), verbosity=3)
                assembly_process = psutil.Process(assembly_process.pid)
                self.logger.log('Assembly process to kill: {}'.format(str(assembly_process.pid)), verbosity=3)
                for child in assembly_process.children(recursive=True):
                    self.logger.log('Assembly child process to kill {}'.format(str(child.pid)), verbosity=3)
                    child.kill()
                assembly_process.kill()
                raise
        
        else:

            # prepare the assembly command as a string
            assembly_cmd = 'shovill --outdir {} --R1 {} --R2 {} --depth {} --mminlen {} --mincov {} --cpus {}\n'.format(str(self.assembly_directory), str(self.r1_trimmed), str(self.r2_trimmed), str(self.target_depth_of_coverage), str(self.length_filter), str(self.coverage_filter), str(self.cores))
            self.logger.log('\nassembly command: {}'.format(assembly_cmd), verbosity=3)

            try:
                assembly_process = subprocess.Popen(
                    [
                        'shovill',
                        '--outdir', str(self.assembly_directory),
                        '--R1',str(self.r1_trimmed),
                        '--R2',str(self.r2_trimmed),
                        '--depth', str(self.target_depth_of_coverage),
                        '--minlen', str(self.length_filter),
                        '--mincov', str(self.coverage_filter),
                        '--cpus', str(self.cores)
                    ],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.PIPE
                )
                outs, errs = assembly_process.communicate(timeout=self.max_time)
            except subprocess.TimeoutExpired:
                self.logger.log('Shovill failed to complete in the allotted time ({} hrs), so it will be terminated!'.format(str(self.max_time/3600)), verbosity=3)
                assembly_process = psutil.Process(assembly_process.pid)
                self.logger.log('Assembly process to kill: {}'.format(str(assembly_process.pid)), verbosity=3)
                for child in assembly_process.children(recursive=True):
                    self.logger.log('Assembly child process to kill {}'.format(str(child.pid)), verbosity=3)
                    child.kill()
                assembly_process.kill()
                raise

        # check the return code from shovill
        try:
            assert assembly_process.returncode == 0
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='assembly', new_status='failed')
            self.logger.log('Shovill returned a non-zero returncode for {}: {}\n'.format(self.name, str(assembly_process.returncode)))
            for line in errs.decode('utf8').splitlines():
                self.logger.log(line, verbosity=3)
            raise

        # validate that the output file was generated where we were expecting it
        try:
            assert file_check(file_path=self.assembly_file_raw, file_or_directory='file')
            self.checkpoint.update_checkpoint_file(analysis_step='assembly', new_status='complete')
            for line in errs.decode('utf8').splitlines():
                self.logger.log(line, verbosity=3)
            self.logger.log('Shovill completed successfully for sample: {}!\n'.format(self.name))
            return
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='assembly', new_status='failed')
            self.logger.log('something went wrong during assembly. Shovill failed to generate at least one of the key ouptut files for {}'.format(self.name))
            raise
    
    def assembly_postprocessing(self):
        """
        Use bbmap to estimate the average insert size/generate the insert size histogram
        clean the draft assembly and perform QC checks
        """
        self.logger.log_section_header('Assembly Postprocessing and QC', verbosity=3)
        self.logger.log_explanation('Perform QC checks and filtering actions on the draft assembly produced by Newbler', verbosity=3)

        # check the checkpoint file for this step
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['assembly postprocessing'] == 'complete':
            if file_check(file_path=self.assembly_file_final, file_or_directory='file') and file_check(file_path=self.assembly_QC_report, file_or_directory='file'):
                self.logger.log('checkpoint file indicates that this step was completed. Skipping!', verbosity=3)
                return
        else:
            self.logger.log('checkpoint file does not recognize this step as complete, so the existing output of this step '
                    'will be trashed and the analysis redone', verbosity=3)
            if file_check(file_path=self.assembly_file_final, file_or_directory='file'):
                subprocess.run(['rm', str(self.assembly_file_final)])
            if file_check(file_path=self.insert_histogram, file_or_directory='file'):
                subprocess.run(['rm', str(self.insert_histogram)])
            if file_check(file_path=self.assembly_QC_report, file_or_directory='file'):
                subprocess.run(['rm', str(self.assembly_QC_report)])

        # check for the key assembly output files in the assembly directory
        if file_check(file_path=self.assembly_file_raw, file_or_directory='file'):
            self.logger.log('Located assembly output files for ' + log.bold_green(str(self.name)), verbosity=3)

            # make sure that the top level assembly directory is present
            if not file_check(file_path=self.top_level_assembly_directory, file_or_directory='directory'):
                subprocess.run(['mkdir', str(self.top_level_assembly_directory)])
        else:
            if self.checkpoint.checkpoint_dict['assembly'] == 'failed':  # case of an assembly that failed to complete
                self.logger.log('The assembly for this sample failed, so we will log a blank assembly QC report!', verbosity=3)
            else:
                self.checkpoint.update_checkpoint_file(analysis_step='assembly', new_status='failed')  # the assembly wasn't marked as a fail, but can't find the contigs.fa file
                self.logger.log('could not locate the key assembly output files in the output directory!', verbosity=3)

            # prepare a failed assembly QC report for this sample
            self.log_failed_assembly()
            return

        # handle the case where an assembly file is produced but it is completely empty
        if os.stat(self.assembly_file_raw).st_size == 0:
            self.logger.log('The assembly file for this sample is empty, so we will log a blank assembly QC report!', verbosity=3)
            self.log_failed_assembly()
            return

        # calculate the key assembly QC metrics and generate the filtered assembly file
        self.parse_assembly_stats_from_shovill_assembly()

        # estimate the median insert size
        try:
            self.estimate_insert_size_with_bbmap()
        except AssertionError:
            self.logger.log("No insert size estimate was generated")

        # collect all of the assembly QC metrics into a dataframe
        self.logger.log('Generating the assembly QC report for this sample\n', verbosity=3)
        qc_data_dict = {
            'Sample': [self.name],
            'Number of Contigs - Raw': [self.raw_contig_count],
            'Number of Contigs Filtered for Length': [0],
            'Total Length of Contigs Filtered for Length': [0],
            'Number of Contigs Filtered for Low Coverage': [self.raw_contig_count - self.final_contig_count],
            'Total Length of Contigs Filtered for Low Coverage': [self.raw_total_contig_length - self.final_assembly_length],
            'Number of Good Contigs': [self.final_contig_count],
            'Average Read Depth of Good Contigs': [self.final_assembly_average_read_depth],
            'Total Length of Good Contigs': [self.final_assembly_length],
            'N50': [self.final_assembly_n50],
            'Median Insert Size': [self.median_insert_size],
            'Assembler': ['Shovill v1.1.0']
        }

        qc_dataframe = pd.DataFrame.from_dict(qc_data_dict)

        # write the information in the QC_dataframe to the assembly_QC_report file
        qc_dataframe.to_csv(str(self.assembly_QC_report), index=False)

        # copy the cleaned assembly file to the assembly directory
        self.logger.log('Copying the filtered assembly file to: {}'.format(str(self.assembly_file_final_copied)))
        if file_check(file_path=self.assembly_file_final_copied, file_or_directory='file'):
            subprocess.run(['rm', str(self.assembly_file_final_copied)])
        shutil.copy(str(self.assembly_file_final), str(self.assembly_file_final_copied))

        self.checkpoint.update_checkpoint_file(analysis_step='assembly postprocessing', new_status='complete')

        self.logger.log('Assembly postprocessing and QC complete!', verbosity=3)

    def log_failed_assembly(self):
        """
        prepare a blanked out individual assembly QC report for samples that failed to produce a meaningful assembly
        """
        qc_data_dict = {
            'Sample': [self.name],
            'Number of Contigs - Raw': ['N/A'],
            'Number of Contigs Filtered for Length': ['N/A'],
            'Total Length of Contigs Filtered for Length': ['N/A'],
            'Number of Contigs Filtered for Low Coverage': ['N/A'],
            'Total Length of Contigs Filtered for Low Coverage': ['N/A'],
            'Number of Good Contigs': ['N/A'],
            'Average Read Depth of Good Contigs': ['N/A'],
            'Total Length of Good Contigs': ['N/A'],
            'N50': ['N/A'],
            'Median Insert Size': ['N/A'],
            'Assembler': ['Shovill 1.1.0']
        }

        qc_dataframe = pd.DataFrame.from_dict(qc_data_dict)

        # write the information in the QC_dataframe to the assembly_QC_report file
        qc_dataframe.to_csv(str(self.assembly_QC_report), index=False)

        self.checkpoint.update_checkpoint_file(analysis_step='assembly postprocessing', new_status='complete')
    
    def parse_assembly_stats_from_shovill_assembly(self):
        """
        use biopython's SeqIO module to parse the meaningful information from the shovill assembly file
        """
        # read in the contig statistics (length and average read depth) from the contig graph file into a dictionary
        self.logger.log('Parsing the Shovill output file: {}\n'.format(str(self.assembly_file_raw)), verbosity=3)
        
        assembly_stat_dict = {}  # used for storing contig lengths, read depths, and correction counts

        for seq in SeqIO.parse(self.assembly_file_raw, "fasta"):
            
            seq_description_list = seq.description.split()
            
            contig_length = int(seq_description_list[1].split('=')[1])
            contig_read_depth = float(seq_description_list[2].split('=')[1])
            contig_correction_count = int(seq_description_list[3].split('=')[1])
            
            assembly_stat_dict[seq.id] = {
                'contig length': contig_length,
                'contig read depth': contig_read_depth,
                'contig correction count': contig_correction_count,
            }
            self.raw_contig_count +=1
            self.raw_total_contig_length += contig_length
            self.raw_total_read_depth += (contig_length * contig_read_depth)

        self.average_read_depth = self.raw_total_read_depth/self.raw_total_contig_length
        self.logger.log('The average read depth of contigs in this assembly is: {}'.format(str(self.average_read_depth)), verbosity=3)

        self.read_depth_filter = max(self.average_read_depth * 0.1, self.read_depth_filter)  # the read depth filter is set to 10x or 10% of the average read depth, whichever is greater
        self.logger.log('The read depth filter is either 10% of the average read depth OR 10x, whichever is greater', verbosity=3)
        self.logger.log('For this assembly, the read depth filter is: {}'.format(str(self.read_depth_filter)), verbosity=3)

        # iterate through the assembly_stat_dict. When a contig is identified that has a contig read depth that is less than
        # the read depth filter, log it and add the key to the list of contigs to remove from the final assembly
        contigs_to_remove = []
        for k,v in assembly_stat_dict.items():
            
            if v['contig read depth'] < self.read_depth_filter:
                contigs_to_remove.append(k)
                self.logger.log('Contig {} has a coverage of {}, which is under the threshold {} so it will be tossed'.format(k, str(v['contig read depth']), str(self.read_depth_filter)), verbosity=3)
            
        # remove the filtered contigs from assembly_stat_dict to calculate final values
        for contig_id in contigs_to_remove:
            assembly_stat_dict.pop(contig_id)

        # Calculate the total length, average read depth, and N50 of the filtered assembly
        self.final_contig_count = self.raw_contig_count - len(contigs_to_remove)
        self.final_assembly_length = sum([contig['contig length'] for contig in assembly_stat_dict.values()])
        self.final_assembly_average_read_depth = round(sum([contig['contig length'] * contig['contig read depth'] for contig in assembly_stat_dict.values()]) / self.final_assembly_length, 2)
        contig_length_list = sorted([contig['contig length'] for contig in assembly_stat_dict.values()], reverse=True)
        count = 0
        running_contig_length = 0
        for contig in contig_length_list:
            running_contig_length += contig
            count += 1
            
            if running_contig_length >= self.final_assembly_length / 2:
                self.final_assembly_n50 = count
                break
           
        # use SeqIO to go through the assembly one more time. This time only retaining contigs that have passed the read depth filter
        contigs_for_final_assembly = []
        for seq in SeqIO.parse(self.assembly_file_raw, "fasta"):
            
            if seq.name not in contigs_to_remove:
                contigs_for_final_assembly.append(seq)
            
        # write the final assembly file to disk
        with open(self.assembly_file_final, 'w') as fout:
            SeqIO.write(contigs_for_final_assembly, fout, 'fasta')
        
    def estimate_insert_size_with_bbmap(self):
        """
        use bbmap and approximately 5X coverage worth of reads to estimate the median insert size
        """
        # calculate the median insert size using bbmap
        self.logger.log_section_header("Estimating Insert Size with bbmap", verbosity=3)
        self.logger.log_explanation("Using bbmap and approximately 5X worth of subsampled reads, estimate the median insert size of the input libraries", verbosity=3)
        
        self.logger.log('subsetting reads to calculate the insert size histogram\n', verbosity=3)
        try:
            r1_tmp, r2_tmp = self.read_file_subsample(target_depth_of_coverage=5)
        except AssertionError:  # failed to generate the subset read files so we won't do any read mapping
            raise

        self.logger.log('Now running bbmap to calculate the insert size histogram\n', verbosity=3)
        bbmap_cmd = 'bbmap.sh in1={} in2={} ref={} path={} ihist={} t=1'.format(str(r1_tmp), str(r2_tmp), str(self.assembly_file_final), str(self.assembly_directory), str(self.insert_histogram))
        self.logger.log('\nbbmap command: {}\n'.format(bbmap_cmd), verbosity=3)

        # run the bbmap command
        bbmap_process = subprocess.run(
            [
                'bbmap.sh',
                '-Xmx5g',
                'in1=' + str(r1_tmp),
                'in2=' + str(r2_tmp),
                'ref=' + str(self.assembly_file_final),
                'path=' + str(self.assembly_directory),
                'ihist=' + str(self.insert_histogram),
                't=' + str(self.cores)
            ],
            stderr=subprocess.PIPE,
            universal_newlines=True)

        # if subsampled reads were generated for the process, delete them now
        if r1_tmp != self.r1_trimmed:
            subprocess.run(['rm', str(r1_tmp)])
        if r2_tmp != self.r2_trimmed:
            subprocess.run(['rm', str(r2_tmp)])

        # check the return code from bbmap
        try:
            assert bbmap_process.returncode == 0
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='assembly postprocessing', new_status='failed')
            self.logger.log('bbmap returned a non-zero returncode for {}: {}\n{}'.format(self.name, str(bbmap_process.returncode), str(bbmap_process.stderr)), verbosity=3)
            raise

        # validate that the output file was generated where we were expecting it
        try:
            assert file_check(file_path=self.insert_histogram, file_or_directory='file')
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='assembly postprocessing', new_status='failed')
            self.logger.log('something went wrong during read mapping. bbmap failed to generate at least one of the key ouptut files for ' + self.name, verbosity=3)
            raise

        subprocess.run(['rm', '-r', str(self.bbmap_directory)])

        # parse the insert size histogram to get the avg insert size
        self.logger.log('Calculating the average insert size based on the bbmap results', verbosity=3)
        with open(str(self.insert_histogram), 'r') as ihist:
            for line in ihist.readlines():
                if line.split('\t')[0] == '#Median':
                    self.median_insert_size = line.split('\t')[1].strip()
                    break

        self.logger.log('The average insert size for this sample was: {} bp\n'.format(str(self.median_insert_size)), verbosity=3)

    def assembly_cleanup(self):
        """
        clean up intermediate files, compress large files, etc
        """
        self.logger.log_section_header("Post Assembly Clean-up", verbosity=3)
        if self.no_clean:
            self.logger.log("User specified --no-clean at the command line, so skipping clean up steps")
            return

        read_files = []
        self.scan_for_fastq()
        read_files.append(self.r1_fastq)
        read_files.append(self.r2_fastq)
        read_files.append(self.r1_trimmed)
        read_files.append(self.r2_trimmed)

        parallel_compression_handler(
            read_files,
            compress=True,
            compression_type='bzip2',
            logger=self.logger,
            cores=self.cores
            )
        
        self.logger.log('Cleanup complete!')
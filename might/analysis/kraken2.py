#!/usr/bin/env python
# encoding: utf-8
import multiprocessing as mp
import os
import shutil
import subprocess
import sys
import time
import math
from datetime import datetime
from pathlib import Path
from fnmatch import fnmatch

import pandas as pd

from might.common import log
from might.analysis.sample import Sample
from might.common.miscellaneous import file_check, quit_with_error, check_for_dependency, compression_handler, \
    parallel_compression_handler, round_down
from might.common.ramdisk_manager import ramdisk_handler
from might.common.errors import BadPathError, MissingPrerequisiteError, MissingPathError


class Kraken2:
    """
    Class Kraken2 is used to manage running kraken2 analysis for 1+ samples
    """
    def __init__(self, sample_list_file, output_directory, reads_directory, kraken2_database, ramdisk=None, keep=2,
                 force=False, cores=1, cores_per_sample=1, verbosity=2, no_clean=False, pbs=False):

        self.sample_list_file = sample_list_file
        self.output_directory = output_directory
        self.reads_directory = reads_directory
        self.kraken2_database = kraken2_database
        self.ramdisk = ramdisk
        self.keep = keep
        self.force = force
        self.cores = cores
        self.cores_per_sample = cores_per_sample
        self.verbosity = verbosity
        self.no_clean = no_clean
        self.pbs = pbs

        self.sample_list = []

    def check_paths(self):
        """
        validate the required paths exist
        """
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            print(log.bold_yellow('WARNING: The output directory path ({}) is new, so we will make it now'.format(str(self.output_directory))))
            subprocess.run(
                [
                    'mkdir', '-p', str(self.output_directory)
                ]
            )
        
        self.kraken2_database = Path(self.kraken2_database).expanduser().resolve()
        if not file_check(file_path=self.kraken2_database, file_or_directory='directory'):
            raise BadPathError(self.kraken2_database, "kraken2 database")
        
        # validate that the minimal required files are present in the kraken2 database directory
        self.hash_file = self.kraken2_database / 'hash.k2d'
        self.opts_file = self.kraken2_database / 'opts.k2d'
        self.seqid_file = self.kraken2_database / 'seqid2taxid.map'
        self.taxo_file = self.kraken2_database / 'taxo.k2d'
        minimum_file_list = [self.hash_file, self.opts_file, self.seqid_file, self.taxo_file]
        for file in minimum_file_list:
            if not file_check(file_path=file, file_or_directory='file'):
                raise BadPathError(file, "kraken2 database file")

        if self.ramdisk:
            self.ramdisk = Path(self.ramdisk).expanduser().resolve()
            if not file_check(file_path=self.ramdisk, file_or_directory='directory'):
                raise BadPathError(self.ramdisk, "ramdisk")
        
        # clean and validate the reads and assembly directories IF THEY WERE PASSED
        if self.reads_directory:
            self.reads_directory = Path(self.reads_directory).expanduser().resolve()
            if not file_check(file_path=self.reads_directory, file_or_directory='directory'):
                raise BadPathError(self.reads_directory, 'raw reads directory')
        
        # ensure that the output directory at least has an individual_sample_analyses directory for all but bcl2fastq and cluster
        self.individual_sample_analyses = self.output_directory / 'individual_sample_analyses'
        if not file_check(file_path=self.individual_sample_analyses, file_or_directory='directory'):
            subprocess.run(
                [
                    'mkdir', '-p', str(self.individual_sample_analyses)
                ]
            )

        # prepare the log directory
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])
  
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        try:
            self.logger = log.Log(
                log_filename=self.log_directory / '{}.kraken2.might.log'.format(datetime.now().strftime("%Y-%m-%d_%H-%M")),
                stdout_verbosity_level=self.verbosity
            )
        except: # need to figure out the logger error codes
            pass

    def dependency_check(self):
        """
        Ensure that the primary dependencies for this analysis are available on the path
        """
        try:
            assert check_for_dependency('kraken2')
        except AssertionError:
            quit_with_error(self.logger, 'kraken2 could NOT be located')
    
    def generate_sample_list(self):
        """
        generate the list of samples to assemble, UNLESS the list was passed by the user
        """
        # clean, validate, and read in the sample list IF IT WAS PASSED
        if self.sample_list_file is not None:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list')

            with open(self.sample_list_file, 'r') as f:
                self.sample_list = [line.strip() for line in f.readlines()]
        else:
            for entry in os.scandir(self.individual_sample_analyses):
                if file_check(file_path=Path(entry.path).expanduser().resolve(), file_or_directory='directory'):
                    if entry.name == 'individual_sample_analyses':
                        continue
                    sample_name = entry.name
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)

            for entry in os.scandir(self.reads_directory):
                if fnmatch(entry.name, '*_R*.fastq*'):
                    sample_name = entry.name.split('_')[0]
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)
                if fnmatch(entry.name, '*.R*.fastq*'):
                    sample_name = entry.name.split('.')[0]
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)

    def generate_kraken2_sample_objects(self):
        """
        generate sample objects for all of the samples in sample list and store it in self.sample_objects
        """

        if self.ramdisk:
            kraken2_db = self.ramdisk / self.kraken2_database.name
            memory_mapping = True
        else:
            kraken2_db = self.kraken2_database
            memory_mapping = False

        self.sample_objects = []
        for sample in self.sample_list:
            if sample != 'Undetermined':  # No point in analyzing the recycling bin of read files
                self.sample_objects.append(
                    Kraken2Sample(
                        sample, 
                        self.output_directory,
                        kraken2_database=kraken2_db,
                        reads_directory=self.reads_directory,
                        memory_mapping = memory_mapping,
                        force = self.force, 
                        cores=self.cores_per_sample,
                        no_clean = self.no_clean,
                        verbosity=self.verbosity
                        )
                    )

        # iterate through the sample objects in self.sample_objects. Partition samples into one of two lists:
        # analysis_required and analysis_complete based on the return value of <sample>.configure_amr_analysis
        self.analysis_required = []
        self.analysis_complete = []
        self.analysis_impossible = []

        for sample in self.sample_objects:

            sample.check_kraken2_status()
            if sample.kraken2_status == 'complete':
                self.analysis_complete.append(sample)
                continue

            try:
                sample.validate_kraken2_possible()  # the sample is reporting that it is capable of undergoing kraken2 analysis
                self.analysis_required.append(sample)
            except MissingPathError as err:
                self.logger.log(str(err))
                self.analysis_impossible.append(sample)
            except BadPathError as err:
                self.logger.log(str(err))
                self.analysis_impossible.append(sample)

    def setup_ramdisk(self):
        """
        if 1+ samples are to be analyzed AND the ramdisk option was specified this method will manage setting up the
        ramdisk for use by kraken2
        """
        try:
            assert ramdisk_handler(
                self.logger,
                mount=True,
                ramdisk_directory=self.ramdisk,
                directory_to_load=self.kraken2_database
            )
        except AssertionError:
            quit_with_error(self.logger, 'Unable to load the ramdisk!')

    def kraken2_threaded_analyses(self):
        """
        run kraken2_threaded_analysis() for each sample in self.analysis_required using mutliprocessing
        """
        self.logger.log_section_header('Now starting threaded Kraken2 analyses')
        self.logger.log_explanation("Running threaded kraken2 analyses using python multiprocessing")

        if len(self.analysis_required) == 0:
            self.logger.log('\nKraken2 threaded analyses are now complete!')
            return

        pool_size = math.floor(self.cores / self.cores_per_sample)
        self.logger.log("Running {} processes ({} cores at {} cores per sample)".format(str(pool_size), str(self.cores), str(self.cores_per_sample)))

        p = mp.Pool(pool_size)
        p.map(Kraken2Sample.threaded_kraken2_process, self.analysis_required)
        p.close()
        p.join()

        self.logger.log('\nKraken2 threaded analyses are now complete!')

    def run_pbs(self):
        """
        create and submit a pbs batch script that will perform this analysis. This gets around the network stability
        issues that can cause long runnning jobs to be terminated
        """

        self.logger.log_section_header('Submitting kraken2 job(s) to pbs server')
        self.logger.log_explanation(
            'Since the -pbs flag was invoked, we will submit the job(s) to the pbs batch server for'
            'processing. Please note that the current script will terminate once the job has '
            'been submitted, so you will need to monitor qstat to evaluate when the job has '
            'completed\n')

        self.logger.log('Generating the pbs batch script(s)\n')

        node_dict = {
            '2': ['amedpbswrair002.amed.ds.army.mil', 31],
            '3': ['amedpbswrair003.amed.ds.army.mil', 31],
            '4': ['amedpbswrair004.amed.ds.army.mil', 31],
            '5': ['amedpbswrair005.amed.ds.army.mil', 39],
            '6': ['amedpbswrair006.amed.ds.army.mil', 39],
            '7': ['amedpbswrair007.amed.ds.army.mil', 39],
            '8': ['amedpbswrair008.amed.ds.army.mil', 39],
            '9': ['amedpbswrair009.amed.ds.army.mil', 31]
        }

        self.node = node_dict[str(self.pbs)][0]
        self.cores = min(self.cores, node_dict[str(self.pbs)][1])  # allow the user to restrict the number of cores used, but not exceed the limit of the selected node

        # set the pbs scipt file path
        self.pbs_script_file = self.output_directory / 'kraken2.pbs'

        self.logger.log('Batch script file at: {}\n'.format(str(self.pbs_script_file)))

        # verify that the pbs file does NOT already exist. If it does, delete it
        if file_check(file_path=self.pbs_script_file, file_or_directory='file'):
            self.logger.log('An existing pbs batch script file was located and will be overwritten\n')
            subprocess.run(['rm', str(self.pbs_script_file)])

        # prepare the pbs file
        with open(str(self.pbs_script_file), 'w') as pbs:

            # add the shebang line
            pbs.write('#!/bin/bash\n')

            # write the nodes, cores, and walltime parameters
            pbs.write('#PBS -l nodes={}:ppn={}\n'.format(self.node, self.cores))
            pbs.write('#PBS -l walltime=04:00:00\n')

            output_path = self.output_directory / 'kraken2.stdout'
            error_path = self.output_directory / 'kraken2.stderr'

            # write the stdout and stderr paths
            pbs.write('#PBS -o {}\n'.format(str(output_path)))
            pbs.write('#PBS -e {}\n'.format(str(error_path)))

            # teach pbs server about conda
            pbs.write('source /data/MRSN_Research/mrsn/miniconda/etc/profile.d/conda.sh\n')

            # activate might
            pbs.write('conda activate might\n')

            # write the actual might command to be run as a batch job
            if self.ramdisk:
                if self.no_clean:
                    pbs.write('might kraken2 {} {} {} --ramdisk {} --cores {} --no-clean'.format(self.output_directory,
                                                                                                 self.reads_directory,
                                                                                                 self.kraken2_database,
                                                                                                 self.ramdisk,
                                                                                                 self.cores))
                else:
                    pbs.write('might kraken2 {} {} {} --ramdisk {} --cores {}'.format(self.output_directory,
                                                                                      self.reads_directory,
                                                                                      self.kraken2_database,
                                                                                      self.ramdisk, self.cores))
            else:
                if self.no_clean:
                    pbs.write('might kraken2 {} {} {} --cores {} --no-clean'.format(self.output_directory,
                                                                                    self.reads_directory,
                                                                                    self.kraken2_database, self.cores))
                else:
                    pbs.write('might kraken2 {} {} {} --cores {}'.format(self.output_directory, self.reads_directory,
                                                                         self.kraken2_database, self.cores))

        # submit the pbs job to the batch server
        self.logger.log('Submitting the batch script file to the pbs batch server\n')

        pbs_job_submission_process = subprocess.run(
            [
                'qsub',
                '-q',
                'batch',
                str(self.pbs_script_file)
            ]
        )

        time.sleep(1)

        self.logger.log('The kraken2 job has been submitted. Please monitor qstat for job completion!')
        sys.exit()


class SpeciesCaller:
    """
    Class SpeciesCaller is used to manage the MIGHT guided interactive species calling analysis for 1+ samples
    """
    def __init__(self, sample_list_file, output_directory, auto=False, force=False, verbosity=2):
        self.sample_list_file = sample_list_file
        self.output_directory = output_directory
        self.auto = auto
        self.force = force
        self.verbosity = verbosity

        self.sample_list = []

    def check_paths(self):
        """
        validate the required paths exist
        """
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            print(log.bold_yellow('WARNING: The output directory path ({}) is new, so we will make it now'.format(str(self.output_directory))))
            subprocess.run(
                [
                    'mkdir', '-p', str(self.output_directory)
                ]
            )
        
        # ensure that the output directory at least has an individual_sample_analyses directory for all but bcl2fastq and cluster
        self.individual_sample_analyses = self.output_directory / 'individual_sample_analyses'
        if not file_check(file_path=self.individual_sample_analyses, file_or_directory='directory'):
            subprocess.run(
                [
                    'mkdir', '-p', str(self.individual_sample_analyses)
                ]
            )

        # prepare the log directory
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])

        self.species_call_summary = self.output_directory / 'species_calls_summary.csv'
  
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        try:
            self.logger = log.Log(
                log_filename=self.log_directory / '{}.species-caller.might.log'.format(datetime.now().strftime("%Y-%m-%d_%H-%M")),
                stdout_verbosity_level=self.verbosity
            )
        except: # need to figure out the logger error codes
            pass
    
    def generate_sample_list(self):
        """
        generate the list of samples to assemble, UNLESS the list was passed by the user
        """
        # clean, validate, and read in the sample list IF IT WAS PASSED
        if self.sample_list_file is not None:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list')

            with open(self.sample_list_file, 'r') as f:
                self.sample_list = [line.strip() for line in f.readlines()]
        else:
            for entry in os.scandir(self.individual_sample_analyses):
                if file_check(file_path=Path(entry.path).expanduser().resolve(), file_or_directory='directory'):
                    if entry.name == 'individual_sample_analyses':
                        continue
                    sample_name = entry.name
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)

    def generate_kraken2_sample_objects(self):
        """
        generate sample objects for all of the samples in sample list and store it in self.sample_objects
        """

        self.sample_objects = []
        for sample in self.sample_list:
            if sample != 'Undetermined':  # No point in analyzing the recycling bin of read files
                self.sample_objects.append(
                    Kraken2Sample(
                        sample, 
                        self.output_directory,
                        verbosity=self.verbosity,
                        auto=self.auto
                        )
                    )

        # iterate through the sample objects in self.sample_objects. Partition samples into one of two lists:
        # analysis_required and analysis_complete based on the return value of <sample>.configure_amr_analysis
        self.analysis_required = []
        self.analysis_complete = []
        self.analysis_impossible = []

        for sample in self.sample_objects:

            try:
                sample.check_species_calling_status(force=self.force)
                if sample.kraken2_status == 'complete':
                    self.analysis_complete.append(sample)
                    continue
                if sample.kraken2_status == "incomplete":
                    self.analysis_required.append(sample)
            except MissingPrerequisiteError as err:
                self.logger.log(str(err))
                self.analysis_impossible.append(sample)

    def perform_species_calling(self):
        """
        For each Kraken2Sample object, perform the species calling workflow
        """
        self.logger.log_section_header("Performing Interactive Kraken2 Results Adjudication")
        self.logger.log_explanation("MIGHT will walk the user through the significant kraken2 results and help them adjudicate the findings")
        
        for sample in self.analysis_required:

            try:
                sample.parse_kraken2_report()
            except:
                self.logger.log("Something went wrong trying to parse the kraken2 report for {}!".format(sample.name))
            try:
                sample.construct_taxon_rank_code_dict()
            except:
                self.logger.log("Something went wrong trying to construct the taxon rank code dictionary for {}!".format(sample.name))
            #try:
            sample.user_involved_genus_species_contamination_caller()
            #except:
                #self.logger.log("Something went wrong trying to perform species calling for {}!".format(sample.name))

    def summarize_species_calling_results(self):
        """
        Generate a species call summary file based on the 
        """
        self.logger.log_section_header('Summarizing species calls')
        self.logger.log_explanation("Creating a summary report of all available species calling reports at {}".format(str(self.species_call_summary)))

        column_list = ['Sample', 'Genus', 'Species', 'WGS ID', 'top hits', 'WGS ID - Method', 'DB Build', 'Contaminated?', 'Manually entered fields?']

        summary_dataframe = pd.DataFrame(columns=column_list)

        sample_list = self.analysis_required + self.analysis_complete

        for sample in sample_list:
            sample.checkpoint.read_checkpoint_file()
            if sample.checkpoint.checkpoint_dict['kraken report parsed'] == 'complete':
                if file_check(file_path=sample.species_call_file, file_or_directory='file'):
                    sample_dataframe = pd.read_csv(str(sample.species_call_file), sep=',', usecols=column_list)
                    summary_dataframe = pd.concat([summary_dataframe, sample_dataframe], ignore_index=True, sort=False)

        # write the summary dataframe to summary file
        summary_dataframe.to_csv(str(self.species_call_summary), sep=',', index=False, columns=column_list, mode='w')

class Kraken2Sample(Sample):
    """
    Class Kraken2Sample extends the Sample class, adding the configuration required for taking a single sequencing sample through kraken2 analysis
    """
    def __init__(self, name, output_directory, kraken2_database=None, reads_directory=None, memory_mapping=False, force=False, cores=1, no_clean=True, verbosity=2, auto=False):

        super().__init__(name, output_directory, reads_directory=reads_directory, verbosity=verbosity)
        self.kraken2_database = kraken2_database  # whether the base kraken2 database install location OR the ramdisk path if that option was invoked
        self.kraken2_directory = self.individual_analysis_directory / 'kraken2'
        self.kraken2_database_info_file = self.kraken2_directory / 'database_build.info'
        self.memory_mapping = memory_mapping
        self.kraken2_report = self.kraken2_directory / (self.name + '_kraken2_report.txt')
        self.force = force
        self.cores = cores
        self.no_clean = no_clean

        # specific to call-id
        self.auto = auto

        self.log_filename=self.log_directory / ('{}.kraken2.might.log'.format(self.name))

        self.kraken2_status = None

        # parsed/intermediate attributes
        self.kraken2_report_dataframe = None
        self.filtered_kraken2_report_dataframe = None

        # summary fields and resources
        self.taxon_rank_code_dict = {'D': ['Domain'], 'P': ['Phylum'], 'C': ['Class'], 'O': ['Order'], 'F': ['Family'],
                                'G': ['Genus'], 'S': ['Species'], 'S1': ['SubSpecies'], 'U': ['Unclassified']}
        self.taxon_rank_code_list = ['U', 'D', 'P', 'C', 'O', 'F', 'G', 'S', 'S1']

        # species/genus/contamination calling resources
        self.species_call_dict = {
            'best_hits': [],  # best hits will contain a list of tuples, where each tuple will be of form (genus, species, % classified reads for this species). Tuples will be in descending order of percentages
            'unclassified_reads_over_10_percent': None,
            'best_species_hit_under_40%': None,
            'number of classified reads': None,
            'Domain': None,
            'Phylum': None,
            'Class': None,
            'Order': None,
            'Family': None,
            'Genus': None,
            'Species': None,
        }
        self.genus = None
        self.formatted_genus = None
        self.species = None

        self.species_call_dataframe = None
        self.species_call_file = self.kraken2_directory / (self.name + '_species_call.csv')

        # initialize the logger for this sample
        self.initialize_logger(self.log_filename)

        # initialize the CheckPoint for this sample
        self.initialize_sample_checkpoint_file('kraken2', self.force)

        # if force is being invoked, remove any output that may exist
        if self.force:
            if file_check(file_path=self.kraken2_report, file_or_directory='file'):
                    subprocess.run(['rm', str(self.kraken2_report)])
    
    def create_kraken2_analysis_directory(self):
        """

        """
        if not self.kraken2_directory.exists():
            subprocess.run(['mkdir', '-p', str(self.kraken2_directory)])
        
    def check_kraken2_status(self):
        """
        return the current status of the kraken2 analysis for this sample ('new', 'incomplete', 'complete')
        """
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['kraken report generated'] == 'complete':
            if file_check(file_path=self.kraken2_report, file_or_directory='file'):
                self.logger.log('Sample {} is marked complete in its kraken2 checkpoint file, and the kraken report file was'
                           ' located, so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                self.kraken2_status = 'complete'
            else:
                self.logger.log(
                    'WARNING: Sample {} is marked complete in its kraken2 checkpoint file, BUT the kraken report '
                    'could NOT be located. Individual analysis will be performed'.format(self.name))
                self.checkpoint.update_checkpoint_file(analysis_step='kraken report generated', new_status=None)
                self.kraken2_status = 'incomplete'
        else:
            self.kraken2_status = 'incomplete'
    
    def validate_kraken2_possible(self):
        """
        ensure that the requirements for assembly are met. returns 0 if valid. If invalid, log the error message and returns 1
        """
        try:
            self.scan_for_fastq(self.reads_directory)
        except MissingPathError as err:
            self.logger.log(str(err))
            raise
        except BadPathError as err:
            self.logger.log(str(err))
            raise
        
    def configure_kraken2_analysis(self):
        """
        perform any setup operations for this analysis
        """
        if not file_check(file_path=self.kraken2_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', '-p', str(self.kraken2_directory)])
    
    def threaded_kraken2_process(self):
        """
        governing process for a threaded assembly analysis
        """
        # prepare the kraken2 analysis directory
        self.create_kraken2_analysis_directory()

        # run kraken2 analysis for this sample
        try:
            self.run_kraken2()
        except AssertionError:
            return

        # make a copy of the database_build.info file in the kraken2 output directory if available
        if file_check(file_path=self.kraken2_database_info_file, file_or_directory='file'):
            local_copy = self.kraken2_directory / self.kraken2_database_info_file.name
            shutil.copy(str(self.kraken2_database_info_file), str(local_copy))
        
        # compress read files (if requested)
        try:
            self.kraken2_cleanup()
        except AssertionError:
            return
        
        # parse the kraken2 report
        try:
            self.parse_kraken2_report()
        except AssertionError:
            return
        
        # populate the taxon rank code dictionary using the DataFrame generated by parse_kraken2_report()
        try:
            self.construct_taxon_rank_code_dict()
        except AssertionError:
            pass
        
        # perform summarizing activity
        try:
            self.summarize_kraken2_results()
        except AssertionError:
            return
        
        self.logger.log_section_header('Individual sample assembly complete!', verbosity=3)
        return

    def run_kraken2(self):
        """
        run kraken2 for this sample
        """
        self.logger.log_section_header('Kraken2 - species/contamination prediction from read files', verbosity=3)
        self.logger.log_explanation('Kraken2 is used to 1) assign genus/species and 2) detect contamination from sequencing read files', verbosity=3)

        # If the isolate has reached this step, analysis will need to be performed. First we will purge any existing output
        self.logger.log('Cleaning up any partially completed analysis output', verbosity=3)
        if file_check(file_path=self.kraken2_report, file_or_directory='file'):
            subprocess.run(['rm', str(self.kraken2_report)])

        # decompress the R1 and R2 reads (if compressed)
        self.r1_fastq = compression_handler(self.r1_fastq, self.logger, decompress=True)
        self.r2_fastq = compression_handler(self.r2_fastq, self.logger, decompress=True)

        # if memory_mapping, we assume that the kraken2_database path passed is for a ramdisk version of the database
        if self.memory_mapping:
            
            # log the kraken2 command
            kraken2_cmd = 'kraken2 --db {} --memory-mapping --use-names --report {} --threads {} --paired {} {}'.format(str(self.kraken2_database), str(self.kraken2_report), str(self.cores), str(self.r1_fastq), str(self.r2_fastq))
            self.logger.log('Command: {}'.format(log.bold(kraken2_cmd)), verbosity=3)
            self.logger.log('Now running kraken2...\n', verbosity=3)

            # run kraken2 command with ramdisk utility
            kraken2_process = subprocess.run(['kraken2',
                                            '--db', str(self.kraken2_database),
                                            '--memory-mapping',
                                            '--use-names',
                                            '--report', str(self.kraken2_report),
                                            '--threads', str(self.cores),
                                            '--paired', str(self.r1_fastq), str(self.r2_fastq)],
                                            stdout=subprocess.DEVNULL,
                                            stderr=subprocess.PIPE,
                                            universal_newlines=True)

        else:
            # log the kraken2 command
            kraken2_cmd = 'kraken2 --db {} --use-names --report {} --threads {} --paired {} {}'.format(str(self.kraken2_database), str(self.kraken2_report), str(self.cores), str(self.r1_fastq), str(self.r2_fastq))
            self.logger.log('Command: ' + log.bold(kraken2_cmd), verbosity=3)
            self.logger.log('Now running kraken2...\n', verbosity=3)

            # run kraken2 command without ramdisk utility
            kraken2_process = subprocess.run(['kraken2',
                                            '--db', str(self.kraken2_database),
                                            '--use-names',
                                            '--report', str(self.kraken2_report),
                                            '--threads', str(self.cores),
                                            '--paired', str(self.r1_fastq), str(self.r2_fastq)],
                                            stdout=subprocess.DEVNULL,
                                            stderr=subprocess.PIPE,
                                            universal_newlines=True)
        
        # check the final return code from kraken2
        try:
            assert kraken2_process.returncode == 0
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='kraken report generated', new_status='failed')
            self.logger.log('kraken2 returned a non-zero returncode for {}: {}\n{}'.format(self.name, str(kraken2_process.returncode), str(kraken2_process.stderr)))
            raise

        # validate that the output file was generated where we were expecting it
        try:
            assert file_check(file_path=self.kraken2_report, file_or_directory='file')
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='kraken report generated', new_status='failed')
            self.logger.log('something went wrong during kraken2 analysis. kraken2 failed to generate the report file for'
                            ' {} at the expected path: {}'.format(self.name, str(self.kraken2_report)))
            return
        
        # validate that the output file has some content
        try:
            assert os.stat(str(self.kraken2_report)).st_size > 1000
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='kraken report generated', new_status='failed')
            self.logger.log('WARNING: kraken2 generated a report file for {} at the expected path: {}, but the file is less than 1kb in size. Please check read file sizes!'.format(self.name, str(self.kraken2_report)))
            return

        # update the checkpoint to indicate successful completion of this step
        self.checkpoint.update_checkpoint_file(analysis_step='kraken report generated', new_status='complete')

        return

    def parse_kraken2_report(self):
        """
        The parser will read the entire report file into a dataframe (kraken_output_raw). It will then filter this dataframe
        to remove all entries where the percentage is <1% (with the exception of unclassified reads, always kept). It will
        provide a summary of this filtered data to the terminal as well as the log file (if run from MIGHT) of the form:

        (Taxon Rank Code) - Taxon Rank full length name
            Percentage for entry #1  Scientific Name for entry #1
            Percentage for entry #2  Scientific Name for entry #2

        Where entry #1 is the hit with the highest percentage for that taxon rank code, entry #2 is the second highest, etc.

        NOTE: JUST BECAUSE THE PARSER RETURNS PROBABLE CONTAMINATION DOES NOT MEAN THAT THE SAMPLE IS DEFINITELY
        CONTAMINATED AND VICE VERSA. THIS METHOD IS INTENDED TO AUTOMATE SPECIES CALLS USING FAIRLY CONSERVATIVE VALUES
        THAT REFLECT OUR EXPERIENCE WITH BACTERIAL CLINICAL ISOLATES. IT IS ALWAYS RECOMMENDED THAT YOU DOUBLE CHECK THE
        SUMMARY REPORT IN THE LOG FILE AND WHEN IN DOUBT REFER TO THE FULL KRAKEN2 REPORT FILE!
        """
        self.logger.log_section_header('Parsing the kraken2 report file', verbosity=3)
        self.logger.log_explanation('Reading in the kraken2 report file and filtering out insignificant entries', verbosity=3)

        # read the kraken2_report_file into a pandas DataFrame
        columns = ['Percentage', 'Reads_Covered', 'Reads_Assigned', 'Taxon_Rank_Code', 'TaxID', 'Scientific_Name']
        self.kraken2_report_dataframe = pd.read_csv(self.kraken2_report, sep='\t', header=None, names=columns)

        # for each value in Taxon_Rank_Code, preserve all values over 1%. Also preserve the Unclassified value regardless
        # of size
        kraken_by_rank_code = (self.kraken2_report_dataframe[(self.kraken2_report_dataframe.Percentage >= 1.00) | (self.kraken2_report_dataframe.Taxon_Rank_Code == 'U')]).groupby('Taxon_Rank_Code')

        # generate a new dataframe containing only the filtered values
        self.filtered_kraken2_report_dataframe = pd.DataFrame(columns=columns)
        for name, group in kraken_by_rank_code:

            self.filtered_kraken2_report_dataframe = pd.concat([self.filtered_kraken2_report_dataframe, group])

        self.filtered_kraken2_report_dataframe.sort_index(inplace=True)
    
    def construct_taxon_rank_code_dict(self):
        """
        convert the information in self.filtered_kraken2_report_dataframe into the format of the taxon_rank_code_dict:
        rank:   [   full key name, [percentages], [scientific names], number of scientific names, [fragments covered by the clade]]
        """
        self.logger.log_section_header('Constructing the taxon rank code dictionary', verbosity=3)
        self.logger.log_explanation('Reconstructing the information in the filtered kraken2 report output into a more usable format', verbosity=3)

        for rank in self.taxon_rank_code_list:

            # expand the values in taxon_rank_code_dict s.t. for a given key {key: [full key name, list of all percentages,
            # list of all scientific names, number of scientific names, list of all fragments covered by that clade]
            self.taxon_rank_code_dict[rank].append(self.filtered_kraken2_report_dataframe[self.filtered_kraken2_report_dataframe.Taxon_Rank_Code == rank].Percentage.values)
            self.taxon_rank_code_dict[rank].append(self.filtered_kraken2_report_dataframe[self.filtered_kraken2_report_dataframe.Taxon_Rank_Code == rank].Scientific_Name.values)
            self.taxon_rank_code_dict[rank].append(len(self.filtered_kraken2_report_dataframe[self.filtered_kraken2_report_dataframe.Taxon_Rank_Code == rank].Percentage.values))
            self.taxon_rank_code_dict[rank].append(self.filtered_kraken2_report_dataframe[self.filtered_kraken2_report_dataframe.Taxon_Rank_Code == rank].Reads_Covered.values)

    def summarize_kraken2_results(self):
        """
        Log a summary of the significant results in the filtered kraken2 report DataFrame
        """
        self.logger.log_section_header('Summarizing the kraken2 report file for sample {}'.format(self.name), verbosity=1)
        self.logger.log_explanation('Preparing a summary of the kraken2 report findings', verbosity=1)

        for rank in self.taxon_rank_code_list:
            self.logger.log('(' + rank + ') - ' + log.underline(self.taxon_rank_code_dict[rank][0]), verbosity=1)

            # if there are no entries at this rank_code, log it and move on to the next rank_code
            if len(self.taxon_rank_code_dict[rank][1]) == 0:
                self.logger.log(log.red('\tNo hits >1.00% found at this taxon rank'), verbosity=1)
                continue

            # reformat the scientific names to remove all of the unnecessary left indentation
            self.taxon_rank_code_dict[rank][2] = [entry.lstrip() for entry in self.taxon_rank_code_dict[rank][2]]

            # sort the percentage and scientific_name lists according to the percentage list in descending order i.e. the
            # highest percentage and corresponding scientific name will be first in both lists
            self.taxon_rank_code_dict[rank][1], self.taxon_rank_code_dict[rank][2], self.taxon_rank_code_dict[rank][4] = (list(t) for t in zip(
                *sorted(zip(self.taxon_rank_code_dict[rank][1], self.taxon_rank_code_dict[rank][2], self.taxon_rank_code_dict[rank][4]),
                        reverse=True)))

            # log all of the entries for each taxon rank code, color coded to indicate if one or more than one entry is
            # present at a given taxon rank code
            for entry in range(self.taxon_rank_code_dict[rank][3]):
                if self.taxon_rank_code_dict[rank][3] == 1:
                    self.logger.log('\t{}\t{}\t{}'.format(
                        log.green(str(self.taxon_rank_code_dict[rank][1][entry])),
                        log.green(str(self.taxon_rank_code_dict[rank][4][entry])),
                        log.green(str(self.taxon_rank_code_dict[rank][2][entry]))
                    ),
                        verbosity=1
                    )
                else:
                    self.logger.log('\t{}\t{}\t{}'.format(
                        log.red(str(self.taxon_rank_code_dict[rank][1][entry])),
                        log.red(str(self.taxon_rank_code_dict[rank][4][entry])),
                        log.red(str(self.taxon_rank_code_dict[rank][2][entry]))
                    ),
                        verbosity=1
                    )

    def kraken2_cleanup(self):
        """
        clean up intermediate files, compress large files, etc
        """
        self.logger.log_section_header("Post Kraken2 Clean-up", verbosity=3)
        if self.no_clean:
            self.logger.log("User specified --no-clean at the command line, so skipping clean up steps")
            return

        read_files = []
        self.scan_for_fastq()
        read_files.append(self.r1_fastq)
        read_files.append(self.r2_fastq)

        parallel_compression_handler(
            read_files,
            compress=True,
            compression_type='bzip2',
            logger=self.logger,
            cores=self.cores
            )
        
        self.logger.log('Cleanup complete!', verbosity=3)

    def check_species_calling_status(self, force=False):
        """
        return the current status of the genus-call analysis for this sample ('new', 'incomplete', 'complete')
        """
        # Since we are piggybacking on the kraken2 checkpoint file, --force means we only overwrite the last step
        if force:
            self.checkpoint.update_checkpoint_file(analysis_step="kraken report parsed", new_status=None)
            self.kraken2_status = "incomplete"

        # if species calling analysis has already been completed according to the checkpoint file, verify that the output
        # file exists. If it does NOT exist, revert the checkpoint file to indicate that analysis is required. Log a
        # warning that the checkpoint file was not in agreement with the output for that sample
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['kraken report parsed'] == 'complete':
            if file_check(file_path=self.species_call_file, file_or_directory='file'):
                self.logger.log(
                    'Sample {} is marked complete in its kraken2 checkpoint file, and the species call file was'
                    ' located, so individual analysis of this sample may be skipped'.format(self.name),
                    verbosity=3)
                self.kraken2_status = 'complete'

        # if the kraken2 analysis has already been completed according to the checkpoint file, verify that the output
        # file exists. If it does NOT exist, revert the checkpoint file to indicate that analysis is required. Log a
        # warning that the checkpoint file was not in agreement with the output for that sample, and then return
        # "missing" to the calling method to indicate that this sample needs to have kraken2 rerun.
        else:
            if self.checkpoint.checkpoint_dict['kraken report generated'] == 'complete':
                if file_check(file_path=self.kraken2_report, file_or_directory='file'):
                    self.logger.log('Sample {} is marked complete in its kraken2 checkpoint file, and the kraken report file was'
                            ' located, so individual analysis of this ssmple will be queued'.format(self.name), verbosity=3)
                    self.kraken2_status = 'incomplete'
                else:
                    self.logger.log(
                        'WARNING: Sample {} is marked complete in its kraken2 checkpoint file, BUT the kraken report '
                        'could NOT be located. Species call analysis can NOT be performed on this sample until kraken2 '
                        'analysis has been rerun!'.format(self.name))
                    self.checkpoint.update_checkpoint_file(analysis_step='kraken report generated', new_status=None)
                    raise MissingPrerequisiteError('kraken2 report file', 'Can not parse a report that doesnt exist. Please run the kraken2 command first!')
            elif self.checkpoint.checkpoint_dict['kraken report generated'] == 'failed':
                self.logger.log(
                    'WARNING: Sample {} failed kraken2 analysis. Species call analysis can NOT be performed on'
                    ' this sample until kraken2 analysis has been run SUCCESSFULLY!'.format(self.name))
                raise MissingPrerequisiteError('kraken2 report file', 'The checkpoint file indicates that kraken2 failed for this sample. Please rerun that step before attempting this analysis!')
            else:
                self.logger.log(
                    'WARNING: Sample {} has not undergone kraken2 analysis. Species call analysis can NOT be performed on'
                    ' this sample until kraken2 analysis has been run!'.format(self.name))
                raise MissingPrerequisiteError('kraken2 report file', 'Can not parse a report that doesnt exist. Please run the kraken2 command first!')

    def prepare_kraken2_predictions(self):
        """
        For use in conjuction with the genus/species calling method of MIGHT, this method assembles the information required to
        adjudicate genus/species calls, contamination prediction, etc
        """
        self.logger.log_section_header('Preparing WGS ID predictions from the kraken2 results', verbosity=3)
        self.logger.log_explanation("Applying rules to generate species calling recommendations and warnings based on the parsed kraken2 report", verbosity=3)

        try:
            if self.taxon_rank_code_dict['U'][1][0] > 10:  # check to see if >10% of reads were deemed 'unclassified'
                self.species_call_dict['unclassified_reads_over_10_percent'] = "WARNING: High percentage of unclassified reads. The percentage of unclassified reads exceeds 10%!"
        except IndexError:
            pass

        try:
            if self.taxon_rank_code_dict['S'][1][0] <= 40:  # the best species specific hit is less than 40%
                self.species_call_dict['best_species_hit_under_40%'] = "WARNING: Low percentage of species specific reads. The percentage of reads specific to the highest species hit is less than 40%"
        except IndexError:
            pass

        try:
            if self.taxon_rank_code_dict['D'][4][0] <= 50000:  # the number of reads that were assigned to at least the domain is less than or equal to 50,000
                self.species_call_dict['number of classified reads'] = "WARNING: Low number of classified reads. The total number of reads that were classified is less than 50,000"
        except IndexError:
            pass

        # iterate through the D/P/C/O/F/G/S keys in the taxon_rank_code_dict and add a warning if multiple hits >5% are present
        for i in self.taxon_rank_code_list[1:-1]:
            if self.taxon_rank_code_dict[i][3] != 1:
                if not all(j <= 5.00 for j in self.taxon_rank_code_dict[i][1][1:]):
                    self.species_call_dict[self.taxon_rank_code_dict[i][0]] = "WARNING: Multiple entries at the {} level have specific hits >5%!".format(i[0])
                else:
                    self.species_call_dict[self.taxon_rank_code_dict[i][0]] = None 
            else:
                self.species_call_dict[self.taxon_rank_code_dict[i][0]] = None
        
        # create the list of tuples to populate the value for species_call_dict key "best hits"
        for i in range(0, len(self.taxon_rank_code_dict['S'][1])):  # for every entry at the species level
            self.genus = self.taxon_rank_code_dict['S'][2][i].split()[0]  # the genus should be the first space separated value in the scientific name field
            self.formatted_genus = self.genus[0].upper() + self.genus[1:].lower()
            self.species = ' '.join(self.taxon_rank_code_dict['S'][2][i].split()[1:]).lower()  # the species should be the all but the first space separated value in the scientific name field
            self.species_call_dict['best_hits'].append((self.formatted_genus, self.species, self.taxon_rank_code_dict['S'][1][i]))  # append the tuple of form (genus, species, percentage)

    def generate_might_recommendations(self):
        """
        prepare and log mights 'best guess' predictions for genus and species calls based on the kraken2_species_dict
        """      
        # unpack the species_call_recommendation to see what we (think) we are dealing with. We will use the lists of keys to sort
        # warnings from content
        warning_keys = list(self.species_call_dict.keys())[1:]

        # if any of the warning keys have a value other than None, we disable the --auto attribute and log
        # the warning to the terminal/log file
        for key in warning_keys:
            if self.species_call_dict[key]:
                self.auto = False  # disable the auto functionality (below) since at least one warning was thrown
                self.logger.log(log.bold_red(self.species_call_dict[key]))  # log the warning
        self.logger.log('')

        # pull out top genus, top species, and a summary of all of the hits from the best_hits key tuple
        try:
            self.top_genus = self.species_call_dict['best_hits'][0][0]  # top genus is genus from first tuple in best_hits
            self.top_species = self.species_call_dict['best_hits'][0][1]  # top species is species from first tuple in best_hits
            self.all_hits = ['{} {} ({}%)'.format(i[0], i[1], str(i[2])) for i in self.species_call_dict['best_hits'][:3]]
        except IndexError:  # in the kraken parser we only capture 'S' level hits that are >=1%. If none of these exist, then we will get an IndexError here that we deal with below
            self.auto = False
            self.top_genus = log.bold_red('no hits over 1%')
            self.top_species = log.bold_red('no hits over 1%')
            self.all_hits = ['']
            self.logger.log(log.bold_red('WARNING: Best hit is <1%!'))

        # if --auto flag was invoked and is still valid, we will automatically accept a clean hit
        if self.auto:
            self.species_call_dataframe.loc[0, 'Sample'] = self.name
            self.species_call_dataframe.loc[0, 'Genus'] = self.top_genus
            self.species_call_dataframe.loc[0, 'Species'] = self.top_species
            self.species_call_dataframe.loc[0, 'WGS ID'] = '{} {}'.format(self.top_genus, self.top_species)
            self.species_call_dataframe.loc[0, 'top hits'] = ' | '.join(self.all_hits)
            self.species_call_dataframe.to_csv(str(self.species_call_file), index=False)
            self.checkpoint.update_checkpoint_file(analysis_step='kraken report parsed', new_status='complete')

            return
        
        # MIGHT recommendations for genus and species are provided to the user as separate fields. For each (genus and species)
        # the top hit from the best_hits key is presented, color coded based on the presence or absence of non-None type
        # values for the warning keys
        self.genus_recommendation = log.bold_green(self.top_genus)  # default is bold green unless contamination is predicted
        self.species_recommendation = log.bold_green(self.top_species)  # default is bold green unless contamination is predicted
        self.possible_contamination = False

        if any(self.species_call_dict[key] for key in warning_keys[:-2]):  # set color for genus and species to red if any higher level warnings are present
            self.genus_recommendation = log.bold_red(self.top_genus)
            self.species_recommendation = log.bold_red(self.top_species)
        
        if self.species_call_dict['Genus']:  # set the genus color to red if >1 genus is present at >5=%
            self.genus_recommendation = log.bold_red(self.top_genus)
            self.possible_contamination = True

        if self.species_call_dict['Species']:  # set the species color to red if >1 species is present at >5=%
            self.species_recommendation = log.bold_red(self.top_species)
            self.possible_contamination = True

    def log_might_recommendations(self):
        """
        log the current might recommendations to the terminal
        """
        # rerun the summarize_kraken2_results() method to display the summary to the user
        self.summarize_kraken2_results()
        
        # log the best genus hit, best species hit, and the might recommendation
        self.logger.log(log.underline('MIGHT Recommendations:'))
        self.logger.log('MIGHT {} Best Hit: {}'.format(log.bold('Genus'), self.genus_recommendation))
        self.logger.log('MIGHT {} Best Hit: {}'.format(log.bold('Species'), self.species_recommendation))

        # choose the might recommendation based on the combination of warnings and information available
        if self.species_call_dict['number of classified reads']:  # the case where less than 50,000 reads were classified
            if 'blank' in self.name.lower():  # the case where a blank sample contains less than 50,000 classified reads (GOOD)
                self.logger.log('MIGHT recommended call: {}\n'.format(log.bold_red('[5]  Mark sample as Blank')))
            else:  # the case where a normal sample has less than 50,000 classifid reads (BAD)
                self.logger.log('MIGHT recommended call: {}\n'.format(log.bold_red('[6]  No coverage. Sample has insufficient read depth to make a genus/species prediction')))

        elif self.possible_contamination:  # the case where contamination is indicated by >1 hit in D/P/C/O/F/G/S has a value >5%
            if any(self.species_call_dict[key] for key in ['Domain', 'Phylum', 'Class', 'Order', 'Family', 'Genus']):
                self.logger.log('MIGHT recommended call: {}\n'.format(log.bold_red('[3]  Declare this sample contaminated')))
            else:
                self.logger.log('MIGHT recommended call: {}\n'.format(log.bold_red('[7]  Common complex | [3]  Declare this sample contaminated')))

        elif 'blank' in self.name.lower():  # the case where the sample is blank BUT it registered enough reads to be considered a sample
            self.logger.log('MIGHT recommended call: {}\n'.format(log.bold_red('[5]  Mark sample as Blank')))
        
        else:
            self.logger.log('MIGHT recommended call: {} {} ([1]  Accept the MIGHT recommendations for genus and species)\n'.format(self.genus_recommendation, self.species_recommendation))
        
        self.logger.log(log.underline('Recommendation Color Code Key:'))
        self.logger.log(log.bold_green('Green means no warnings are associated with this recommendation'))
        self.logger.log(log.bold_red('Red means one or more warnings are associated with this recommendation'))
        self.logger.log(log.bold_yellow('Yellow means that the recommended value has been modified by the user\n'))

    def user_involved_genus_species_contamination_caller(self):
        """
        FOR CALL-ID: method will be responsible for guiding the user through the species call process. The program
        will print the parsed kraken report to the terminal window. It will present the user with a recommendation based on
        the content of the parsed report. It will then prompt the user to make a call:
            1) accept the recommendation as is
            2) modify the genus and/or species call
            3) reject the sample as contaminated
            4) request "more info", in which case the isolate will be skipped and the full kraken report will be copied to
            the top level directory for further analysis
        At the end of the process, species calls will be logged to a species call .csv file. Contaminated isolates will be
        returned and added to a list of contaminated isolates for removal and resequencing. Isolates with "more info"
        requested will NOT recieve a species call (no .csv generated), but will be returned and added to a list of isolates
        requiring more attention
        """
        self.logger.log_section_header('Now making species call for {}'.format(self.name), verbosity=0)

        # attempt to get the database build information from the build info file
        database_build_date = None
        if self.kraken2_database_info_file:
            if file_check(file_path=self.kraken2_database_info_file, file_or_directory='file'):
                with open(self.kraken2_database_info_file, 'r') as db_info:
                    database_build_date = db_info.readline().strip()           

        # call prepare_kraken2_predictions() to populate self.species_call_dict
        self.prepare_kraken2_predictions()

        # prepare the species_call_dataframe to accept the results of the species call
        column_list = [
            'Sample', 
            'Genus', 
            'Species', 
            'WGS ID', 
            'top hits', 
            'WGS ID - Method', 
            'DB Build', 
            'Contaminated?', 
            'Manually entered fields?'
        ]
        self.species_call_dataframe = pd.DataFrame(columns=column_list)
        self.species_call_dataframe.loc[0, 'WGS ID - Method'] = 'kraken2'
        self.species_call_dataframe.loc[0, 'DB Build'] = database_build_date
        self.species_call_dataframe.loc[0, 'Contaminated?'] = 'no'
        self.species_call_dataframe.loc[0, 'Manually entered fields?'] = 'no'

        # generate the MIGHT recommendations
        self.generate_might_recommendations()

        # if self.auto is still True at this point, an automated species call was made, so we can skip the user input
        if self.auto:
            return

        # loop until the user commits to a species call
        species_call_complete = False  # track whether an acceptable answer has been provided (default)
        while not species_call_complete:

            # log the current MIGHT recommendations
            self.log_might_recommendations()

            # content for question/choices for top level question #1
            question = log.bold('Please make a selection for sample {}:'.format(self.name))  
            q1_valid_choices = ['1', '2', '3', '4', '5', '6', '7', '8']
            option_1 = '[1]  Accept the MIGHT recommendations for genus and species'
            option_2 = '[2]  Manually enter the genus and/or species value'
            option_3 = '[3]  Declare this sample contaminated.'
            option_4 = '[4]  Defer (make no call at this time)'
            option_5 = '[5]  Mark sample as Blank'
            option_6 = '[6]  No coverage. Sample has insufficient read depth to make a genus/species prediction'
            option_7 = '[7]  Common complex'
            option_8 = '[8]  Phage/Viral genome'

            # content for question/choices for optional question #2 (invoked only if option 2 selected from question #1)
            question_2 = log.bold('What field(s) would you like to edit?')
            q2_valid_choices = ['0', '1', '2', '3']
            option_return = '[0]  Return to the previous menu'
            option_2a = '[1]  I wish to manually enter the genus'
            option_2b = '[2]  I wish to manually enter the species'
            option_2c = '[3]  I wish to manually enter the genus AND species'

            # content for question/choices for optional question #3 (invoked only if option 7 selected from question #1)
            question_3 = log.bold('Please select a common complex from the list. Enter 0 to go back')
            q3_valid_choices = ['0']
            q3_options = ['[0]  Return to the previous menu']
            from might.common.common_complexes import common_complexes_dict
            for key,value in common_complexes_dict.items():
                q3_valid_choices.append(key)
                q3_options.append('[{}]  {}'.format(key, value))

            # prompts for user input used in conjunction with options from question #2
            prompt_1 = 'Please enter the genus'
            prompt_2 = 'Please enter the species'

            # present the user with question #1
            self.logger.log(question)
            self.logger.log(option_1)
            self.logger.log(option_2)
            self.logger.log(option_3)
            self.logger.log(option_4)
            self.logger.log(option_5)
            self.logger.log(option_6)
            self.logger.log(option_7)
            self.logger.log(option_8)
            selection = input().lower()

            # validate and interpret the user response to question #1
            if selection in q1_valid_choices:
                
                # handle a response of 1 (user accepts the MIGHT recommendation)
                if selection == '1':
                    self.species_call_dataframe.loc[0, 'Sample'] = self.name
                    self.species_call_dataframe.loc[0, 'Genus'] = log.remove_formatting(self.genus_recommendation)
                    self.species_call_dataframe.loc[0, 'Species'] = log.remove_formatting(self.species_recommendation)
                    self.species_call_dataframe.loc[0, 'WGS ID'] = '{} {}'.format(log.remove_formatting(self.genus_recommendation), log.remove_formatting(self.species_recommendation))
                    self.species_call_dataframe.loc[0, 'top hits'] = ' | '.join(self.all_hits)
                    
                    species_call_complete = True
                
                # handle a response of 2 (user requests to manually input genus, species, or both)
                if selection == '2':

                    option_2_complete = False
                    while not option_2_complete:

                        # present the user with question #2
                        self.logger.log(question_2)
                        self.logger.log(option_return)
                        self.logger.log(option_2a)
                        self.logger.log(option_2b)
                        self.logger.log(option_2c)
                        option_2_selection = input().lower()

                        # validate and interpret the user response to question #2
                        if option_2_selection in q2_valid_choices:

                            # handle a response of 0 (user wishes to return to the previous screen)
                            if option_2_selection == '0':
                                option_2_complete = True
                            
                            # handle a response of 1 (user wishes to update ONLY the genus field)
                            if option_2_selection == '1':
                                self.logger.log(prompt_1)
                                self.genus_recommendation = log.bold_yellow(input().lower())
                                option_2_complete = True
                            
                            # handle a response of 2 (user wishes to update ONLY the species field)
                            if option_2_selection == '2':
                                self.logger.log(prompt_2)
                                self.species_recommendation = log.bold_yellow(input().lower())
                                option_2_complete = True
                            
                            # handle a response of 3 (user wishes to update BOTH the genus and species fields)
                            if option_2_selection == '3':
                                self.logger.log(prompt_1)
                                self.genus_recommendation = log.bold_yellow(input().lower())
                                self.logger.log(prompt_2)
                                self.species_recommendation = log.bold_yellow(input().lower())
                                option_2_complete = True
                            self.logger.log('Values updated')
                            #self.species_call_dict[0] = 'clean'
                            self.species_call_dataframe.loc[0, 'Manually entered fields?'] = 'yes'
                        else:
                            self.logger.log('Please answer with 1, 2, or 3')
                
                # handle a response of 3 (declaring the sample to be contaminated)
                if selection == '3':
                    self.species_call_dataframe.loc[0, 'Sample'] = self.name
                    self.species_call_dataframe.loc[0, 'Contaminated?'] = 'yes'
                    self.species_call_dataframe.loc[0, 'top hits'] = '|'.join(self.all_hits)
                    species_call_complete = True
                
                # handle a response of 4 (user requests more information on this sample and will make a call in a later run)
                if selection == '4':
                    return 'more info requested'
                
                # handle a response of 5 (user declares that this sample is an intentional blank)
                if selection == '5':
                    return 'omit'
                
                # handle a response of 6 (user declares that there is insufficent read depth to make a determination)
                if selection == '6':
                    self.species_call_dataframe.loc[0, 'Sample'] = self.name
                    self.species_call_dataframe.loc[0, 'Genus'] = 'N/A'
                    self.species_call_dataframe.loc[0, 'Species'] = 'N/A'
                    self.species_call_dataframe.loc[0, 'WGS ID'] = 'N/A'
                    self.species_call_dataframe.loc[0, 'top hits'] = 'No Coverage'
                    species_call_complete = True
                
                # handle a response of 7 (user wants to browse the common complex list)
                if selection == '7':

                    option_7_complete = False
                    while not option_7_complete:

                        self.logger.log(question_3)
                        for option in q3_options:
                            self.logger.log(option)
                        option_7_selection = input().lower()    

                        # validate and interpret the user response to question #7
                        if option_7_selection in q3_valid_choices:

                            # handle a response of 0 (user wishes to return to the previous screen)
                            if option_7_selection == '0':
                                option_7_complete = True
                            
                            # handle another response of 2 (user wishes to update ONLY the species field)
                            if option_7_selection != '0':
                                common_complex_selection = common_complexes_dict[option_7_selection].split()
                                self.genus_recommendation = log.bold_yellow(common_complex_selection[0])
                                self.species_recommendation = log.bold_yellow(' '.join(common_complex_selection[1:]))
                                self.species_call_dataframe.loc[0, 'Manually entered fields?'] = 'yes'
                                option_7_complete = True
                        
                        else:
                            self.logger.log('Please answer with one of the following: {}'.format(', '.join(q3_valid_choices)))
                
                # handle a response of 8 (user wants to classify the sample as a phage/viral genome)
                if selection == '8':

                    self.genus_recommendation = 'phage/virus'
                    self.species_recommendation = ''
                    self.species_call_dataframe.loc[0, 'Manually entered fields?'] = 'yes'


            
            # handle the case where the user response is not in the list of valid choices
            else:
                self.logger.log('Please answer with 1, 2, 3, 4, 5, 6, 7, or 8')

        # if species call was not set to None then we will write the species call DataFrame to the species_call_file
        if self.species_call_dataframe is not None:
            self.species_call_dataframe.to_csv(str(self.species_call_file), index=False, columns=column_list)

        # if the value of contaminated is yes, we will return 'contaminated' to the calling method
        if self.species_call_dataframe.loc[0, 'Contaminated?'] == 'yes':
            self.checkpoint.update_checkpoint_file(analysis_step='kraken report parsed', new_status='complete')
            return 'contaminated'

        self.checkpoint.update_checkpoint_file(analysis_step='kraken report parsed', new_status='complete')
        return 'clean'

#!/usr/bin/env python
# encoding: utf-8

import os
import subprocess
import sys
from pathlib import Path

import pandas as pd
from might.analysis.clustering.cluster_analysis import (
    ClusterAnalysis, IndividualClusterAnalysisFailedError)
from might.common import log
from might.common.errors import BadPathError, NothingToDoError
from might.common.miscellaneous import log_a_list, query_yes_no
from pymongo import MongoClient
from pymongo import errors as pyerr


#################################################################################################################################################
#
#  Class ClusterManager
#
#   Class ClusterManager is responsible for initiating and running all of the individual clusters (one per input distance matrix file). It
#   is also responsible for the validation of user provided file paths, credentials, and availability of the bi-db
#
#################################################################################################################################################
class ClusterManager:
    """
    Class ClusterManager is responsible for instantiating and managing GenusClusters based on the distance matrix files present in the input
    directory.
    """

    def __init__(self, input_directory, output_directory, username, password, db_uri, auth_source, threshold=10, force=False, verbosity=2):

        self.input_directory = input_directory
        self.output_directory = output_directory

        self.username = username
        self.password = password
        self.db_uri = db_uri
        self.auth_source = auth_source

        self.uri_string = self.uri_string = "mongodb://{}:{}@{}?authSource={}".format(self.username, self.password, self.db_uri, self.auth_source)
        self.client = MongoClient(self.uri_string)
        self.db = self.client['bi-db']

        self.threshold = threshold

        self.force = force
        self.verbosity = verbosity
        if self.verbosity == 3:
            self.debug = True
        else:
            self.debug = False

        self.log_directory = None

        self.distance_matrix_file_paths = []
        self.genus_cluster_objects = []

    def validate_input_paths(self):
        """
        ensure that the input and output directory paths are valid
        """
        self.input_directory = Path(self.input_directory).expanduser().resolve()
        if not self.input_directory.is_dir():
            raise BadPathError(self.input_directory, 'input directory', 'The input directory path is invalid!')
        
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not self.output_directory.exists():
            subprocess.run(['mkdir', '-p', str(self.output_directory)])
    
    def validate_connection_to_bi_database(self):
        """
        ensure that the passed bi-db parameters are valid
        """
        self.logger.log_section_header("Establishing connection to the bi-db")
        self.logger.log_explanation("Ensuring that the bi-db is available to MIGHT")
        try:
            self.client.server_info()
        except pyerr.ServerSelectionTimeoutError:
            raise

    def scan_input_directory_for_distance_matrix_files(self):
        """
        scan the input directory for files that match the formatting requirements to be used as input for GenusCluster analysis
        """
        acceptable_file_ending = 'distance_matrix.xlsx'

        self.logger.log_section_header("Scanning for Distance Matrix file(s)")
        self.logger.log_explanation("Scanning the provided input directory ({}) for distance matrix files (files ending with '{}'".format(str(self.input_directory), acceptable_file_ending))

        for entry in os.scandir(self.input_directory):
            if entry.name.endswith(acceptable_file_ending):
                if entry not in self.distance_matrix_file_paths:
                    self.distance_matrix_file_paths.append(Path(entry.path).expanduser().resolve())
        
        if len(self.distance_matrix_file_paths) == 0:
            raise NothingToDoError(str(self.input_directory), 'No properly formatted distance matrix files were detected (ending in distance_matrix.xlsx)')

        self.logger.log("Detected the following distance matrix files:")
        log_a_list(self.distance_matrix_file_paths, self.logger)
            
    def double_check_force(self):
        """
        if the user has requested force, give them a chance to rethink that
        """
        if self.force == True:
            if not query_yes_no('You have indicated that you want to force overwrite the content at {}. Are you sure about this?'.format(str(self.output_directory))):
                print('Good call. Pick a new output directory and try again!')
                sys.exit()
        
        # testing option
        if self.force == 'auto':
            subprocess.run(['rm', '-r', str(self.output_directory)])
    
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', '-p', str(self.log_directory)])
        try:
            self.logger = log.Log(
                log_filename=self.log_directory / 'cluster.might.log',
                stdout_verbosity_level=self.verbosity
            )
        except: # need to figure out the logger error codes
            pass

    def run_individial_cluster_analyses(self):
        """
        for each file that was added to distance_matrix_file_paths, initialize and run through the GenusCluster workflow
        """
        self.logger.log_section_header('Now starting individual file cluster analyses')
        self.logger.log_explanation('For each file that was identified as a potential distance matrix based on the format of the name in the input directory we will perform cluster analysis')
        
        for distance_matrix_file in self.distance_matrix_file_paths:

            current_cluster = ClusterAnalysis(
                self.output_directory,
                distance_matrix_file,
                self.logger,
                self.db,
                threshold=self.threshold,
                verbosity=self.verbosity
            )

            # perform individual cluster analysis
            try:
                current_cluster.analyze_cluster()
            except IndividualClusterAnalysisFailedError as err:
                self.logger.log(str(err))
                continue
            
            # append the current cluster object to clusters
            self.genus_cluster_objects.append(current_cluster)

    def summarize_all_genus_cluster_reports(self):
        """
        generate a summary file that contains all of the information contained in the individual GenusCluster report files
        """
        self.logger.log_section_header("Summarizing individual report files")
        self.logger.log_explanation("Concatenating all of the information present in each of the individual cluster analysis reports")

        self.summary_report_file = self.output_directory / "summary.clusters.might.xlsx"

        self.context_dataframe_new_only = pd.DataFrame()
        self.context_dataframe_multipatient = pd.DataFrame()
        self.context_dataframe = pd.DataFrame()
        self.qc_dataframe = pd.DataFrame()
        self.filtered_amr_dataframe = pd.DataFrame()
        self.patient_dataframe = pd.DataFrame()
        self.ast_dataframe = pd.DataFrame()

        for genus_cluster in self.genus_cluster_objects:
            self.context_dataframe_new_only = pd.concat([self.context_dataframe_new_only, genus_cluster.context_dataframe_new_only])
            self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, genus_cluster.context_dataframe_multipatient])
            self.context_dataframe = pd.concat([self.context_dataframe, genus_cluster.context_dataframe])
            self.qc_dataframe = pd.concat([self.qc_dataframe, genus_cluster.qc_dataframe])
            self.filtered_amr_dataframe = pd.concat([self.filtered_amr_dataframe, genus_cluster.filtered_amr_dataframe])
            self.patient_dataframe = pd.concat([self.patient_dataframe, genus_cluster.patient_dataframe])
            self.ast_dataframe = pd.concat([self.ast_dataframe, genus_cluster.ast_dataframe])

        with pd.ExcelWriter(self.summary_report_file, mode='w', engine='openpyxl') as writer: # pylint: disable=abstract-class-instantiated
            self.context_dataframe_new_only.to_excel(writer, sheet_name="Clusters - New Isolates", index=False)
            self.context_dataframe_multipatient.to_excel(writer, sheet_name="Clusters - Multiple Patients", index=False)
            self.context_dataframe.to_excel(writer, sheet_name="Clusters - All", index=False)
            self.qc_dataframe.to_excel(writer, sheet_name="QC Summary", index=False)
            self.filtered_amr_dataframe.to_excel(writer, sheet_name="AMR Summary", index=False)
            self.patient_dataframe.to_excel(writer, sheet_name="Patient Summary", index=False)
            self.ast_dataframe.to_excel(writer, sheet_name="AST Summary", index=False)


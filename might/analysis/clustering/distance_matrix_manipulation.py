#!/usr/bin/env python
# encoding: utf-8
import csv
import os
import shutil
import subprocess
import sys
from pathlib import Path

import numpy as np
import pandas as pd
from coolname import generate_slug

from might.common import log
from might.common.errors import (BadPathError, InvalidInputError,
                                 NothingToDoError, SampleNotFoundError)
from might.common.miscellaneous import (file_check, file_extension_check,
                                        path_clean, query_yes_no,
                                        quit_with_error)

#################################################################################################################################################
#
#  Class DistanceMatrixManipulation
#
#   Class DistanceMatrixManipulation is responsible for any and all tasks related to importing and manipulating the distance matrix that informs
#   cluster detection
#
#################################################################################################################################################
class DistanceMatrixManipulation:

    def __init__(self, output_directory, distance_matrix_file, clustering_threshold, logger, verbosity=2):

        self.output_directory = output_directory
        self.distance_matrix_file = distance_matrix_file
        self.clustering_threshold = clustering_threshold
        self.logger = logger
        self.verbosity = verbosity
        self.debug = False
        if self.verbosity == 3:
            self.debug = True

        self.distance_matrix_dataframe = None  # used to store the distance matrix as a pandas DataFrame
        self.sorted_distance_matrix_dataframe = None
        self.pairwise_series = None  # reorganization of the distance matrix into a multi-index pairwise pandas Series object
        self.filtered_pairwise_series = None  # multi-index pairwise pandas Series object with only values <= the threshold retained
        self.series_as_dict = None  # information from the filtered_pairwise_series as a dict
        self.multi_index_dict = None # reorganization of the filtered_pairwise_series Object into a multi-index python dict

        self.potential_transmission_pairs_file = self.output_directory / 'potential_transmission_pairs.csv'  # path to the potential transmission pairs .csv file
        self.potential_transmission_itol_dataset_txt_file = self.output_directory / 'potential_transmission_iTOL_dataset.txt'  # path to the potential transmission pairs iTOL dataset file

    def distance_matrix_to_dataframe(self):
        """
        read the distance matrix file into a pandas DataFrame. Map the index and column names to strings, sort on both axes, and ensure that it is square
        """
        self.logger.log_section_header('Converting distance matrix in {} to pandas Dataframe object'.format(str(self.distance_matrix_file)))

        try:
            self.distance_matrix_dataframe = pd.read_excel(self.distance_matrix_file, index_col=0, header=0, engine='openpyxl')
        except ValueError:
            raise InvalidInputError(['distance matrix'], 'The distance matrix file is misformatted')

        if self.debug:
            print(self.distance_matrix_dataframe.head(20))

        self.distance_matrix_dataframe.index = self.distance_matrix_dataframe.index.map(str)
        self.distance_matrix_dataframe.columns = self.distance_matrix_dataframe.columns.map(str)
        self.sorted_distance_matrix_dataframe = self.distance_matrix_dataframe.sort_index(axis=0).sort_index(axis=1)
        
        if not self.sorted_distance_matrix_dataframe.shape[0] == self.sorted_distance_matrix_dataframe.shape[1]:
            raise InvalidInputError(['distance matrix dataframe'], 'The distance matrix must be square!')

        self.logger.log("The distance matrix has been imported and is {}x{}".format(str(self.sorted_distance_matrix_dataframe.shape[0]), str(self.sorted_distance_matrix_dataframe.shape[0])))

        if self.debug:
            print(self.sorted_distance_matrix_dataframe.head(20))

    def dataframe_to_pairwise_distance(self):
        """
        convert the dataframe to a pairwise distance multi-index Series object
        """
        self.logger.log_section_header('Converting Dataframe to multi-index Series object')
        self.logger.log(
            log.dim('   a) Create an upper triangle matrix from the distance matrix to remove redundant information'))
        self.logger.log(log.dim('   b) Generate multi-index Series object from the upper triangle matrix using stack()\n'))

        seqsphere_upper_tri_matrix = self.sorted_distance_matrix_dataframe.where(np.triu(np.ones(self.sorted_distance_matrix_dataframe.shape), 1).astype(bool))
        self.seqsphere_pairwise = seqsphere_upper_tri_matrix.stack()

        if self.debug:
            print(self.seqsphere_pairwise)

    def threshold_filter(self):
        """
        apply the threshold filter
        """
        self.logger.log_section_header('Applying threshold filter')
        self.logger.log(
            log.dim('   a) Filter and drop all series entries where the pairwise distance is greater than the threshold\n'))

        starting_len = self.seqsphere_pairwise.shape[0]

        self.filtered_pairwise_series = self.seqsphere_pairwise.mask(self.seqsphere_pairwise >= self.clustering_threshold).dropna()

        ending_len = self.filtered_pairwise_series.shape[0]

        if self.debug:
            print(self.filtered_pairwise_series)

        self.logger.log("Retained {} of {} pairwise distances that were below the threshold of {}".format(str(ending_len), str(starting_len), str(self.clustering_threshold)))

    def multi_index_series_to_dict(self):
        """
        Convert the multi-index series into a dictionary
        """
        self.logger.log_section_header('Converting multi-index Series object to dictionary')
        self.logger.log(log.dim('   a) Convert the Series object to a dictionary of form {(isolate 1, isolate2): distance}\n'))

        self.series_as_dict = self.filtered_pairwise_series.to_dict()

        if self.debug:
            self.logger.log('Printing result of multi_index_series_to_dict() for debugging')
            self.logger.log('Isolate 1\tIsolate 2\tDistance')
            for isolate_pair, distance in self.series_as_dict.items():
                self.logger.log(str(isolate_pair[0]) + '\t' + str(isolate_pair[1]) + '\t' + str(distance))

    def generate_potential_transmission_itol_dataset(self):
        self.logger.log_section_header('Generating potential transmission dataset for iTOL')
        self.logger.log(
            log.dim('   a) Write output to potential_transmission_pairs.csv as "isolate 1, isolate 2, allelic distance"'))
        self.logger.log(log.dim('   b) Check for the iTOL template file'))
        self.logger.log(log.dim('   c) Write potential transmission data to potential_transmission_iTOL_dataset.txt'))

        self.logger.log('\nNow generating potential transmission pair file: ' + log.green(str(self.potential_transmission_pairs_file)))

        with open(self.potential_transmission_pairs_file, 'w') as output_csv:
            csv_writer = csv.writer(output_csv, delimiter=',')
            for isolate_pair, allelic_distance in self.series_as_dict.items():
                csv_writer.writerow([isolate_pair[0], isolate_pair[1], allelic_distance])

        if self.debug:
            with open(self.potential_transmission_pairs_file, 'r') as output_csv:
                csv_reader = csv.reader(output_csv, delimiter=',')
                for line in csv_reader:
                    self.logger.log(line)

        itol_template_file = Path(__file__).parent.parent / 'resources' / 'itol_connection_template.txt'

        try:
            assert itol_template_file.exists()
        except AssertionError:
            raise BadPathError(itol_template_file, "itol template file")

        self.logger.log('\nThe itol connection template file is: ' + log.green(str(itol_template_file)))
        shutil.copyfile(itol_template_file, self.potential_transmission_itol_dataset_txt_file)

        try:
            assert self.potential_transmission_itol_dataset_txt_file.exists()
        except AssertionError:
            raise BadPathError(self.potential_transmission_itol_dataset_txt_file, "potential transmission itol dataset file")

        self.logger.log('Now generating itol connection dataset: ' + log.green(str(self.potential_transmission_itol_dataset_txt_file)))

        with open(self.potential_transmission_itol_dataset_txt_file, 'a') as dataset_file:
            connecting_line_features = '5\trgba(255, 15, 15, 0.5)\tnormal\n'
            for isolate_pair, allelic_distance in self.series_as_dict.items():
                dataset_file.write(
                    str(isolate_pair[0]) + '\t' + str(isolate_pair[1]) + '\t' + connecting_line_features)

        if self.debug:  # print the dataset file to the terminal for inspection
            with open(self.potential_transmission_itol_dataset_txt_file, 'r') as debug_file:
                for line in debug_file.readlines():
                    self.logger.log(line.strip())

    def generate_subset_distance_matrix_dataframe(self, cluster_name, context_dataframe):
        """
        return a subset of self.distance_matrix_dataframe that only contains the entries specific to cluster_name
        """
        self.logger.log_section_header("Creating the subset distance matrix DataFrame for cluster {}".format(cluster_name))
        self.logger.log_explanation("Filtering out all non-cluster implicated samples from the input distance matrix DataFrame")

        # get sample_list, the list of mrsnId implicated in the cluster cluster_name
        context_subset = context_dataframe.loc[context_dataframe["clusterName"] == cluster_name]
        sample_list = context_subset["mrsnId"].to_list()

        # create a subset distance matrix dataframe that only includes samples in the current cluster
        subset_distance_dataframe = self.sorted_distance_matrix_dataframe.drop(columns=[col for col in self.sorted_distance_matrix_dataframe.columns.to_list() if col not in sample_list])
        subset_distance_dataframe = subset_distance_dataframe.drop(index=[col for col in subset_distance_dataframe.index.to_list() if col not in sample_list])

        return subset_distance_dataframe


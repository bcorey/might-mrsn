#!/usr/bin/env python
# encoding: utf-8
import datetime
import subprocess
import sys

import numpy as np
import pandas as pd
from might.analysis.clustering.bi_db_interaction.patients_collection_queries import \
    PatientAstQuery
from might.analysis.clustering.bi_db_interaction.sequenced_isolates_collection_queries import (
    MultipleMrsnIdsError, QcAmrQuery)
from might.analysis.clustering.bi_db_interaction.surveillance_clusters_queries import \
    SurveillanceClusters
from might.analysis.clustering.distance_matrix_manipulation import \
    DistanceMatrixManipulation
from might.analysis.clustering.pickle_jar import PickleJar
from might.common import log
from might.common.errors import BadPathError, InvalidInputError
from might.common.miscellaneous import file_check, log_a_list, query_yes_no
from pandas.errors import MergeError
from sklearn.cluster import DBSCAN


#################################################################################################################################################
#
#  Class ClusterAnalysis
#
#   Objects of class ClusterAnalysis represent the core cluster analyses processes originating from a single distance matrix dataframe. It 
#   contains the cluster detection and filtering methods, as well as housekeeping tasks like preparing the individual output directory, 
#   recovering progress from the picklejar, and writing the individual cluster analysis summary file (xlsx)
#
#################################################################################################################################################
class ClusterAnalysis:
    """
    Handles the cluster analysis tasks for a single Genus (i.e. based on a single cgMLST distance matrix file)
    """

    def __init__(self, output_directory, distance_matrix_file, logger, db, threshold=10, force=False, verbosity=2):

        # input files and parameters
        self.output_directory = output_directory  # directory where the output will be stored
        self.distance_matrix_file = distance_matrix_file  # path to the distance matrix file
        self.output_subdirectory = self.output_directory / (self.distance_matrix_file.name.split(".distance_matrix.xlsx")[0])
        self.logger = logger
        self.db = db
        self.clustering_threshold = threshold  # the threshold allelic distance for inclusion in outbreak cluster detection
        self.verbosity = verbosity  # determines the amount of information that will be logged to the terminal
        self.debug = False  # if True, all debugging messages and table previews will be logged to the terminal
        if self.verbosity == 3:
            self.debug = True

        # instances of non-class objects. Initialized in the methods under "External-class methods"
        self.distance_matrix_manipulation = None
        self.qc_amr_query = None
        self.surveillance_clusters = None
        self.patient_ast_query = None

        # distance matrix attributes
        self.sorted_distance_matrix_dataframe = None

        # outbreak clustering DataFrames
        self.outbreak_cluster_dataframe = None  # results of DBSCAN cluster analysis as a pandas DataFrame
        self.metadata_dataframe = None  # isolate metadata stored in a DataFrame
        self.decorated_outbreak_cluster_dataframe = None  # result of merging outbreak_cluster_dataframe with metadata
        self.existing_clusters_dataframe = None  # result of importing previous analysis results as a pandas DataFrame
        self.filtered_outbreak_dataframe = None  # result of filtering out same patient isolates from outbreak_cluster_dataframe
        self.removed_isolates = None  # list of isolates that were removed during same patient deduplication

        # database query results
        self.qc_dataframe = None
        self.amr_dataframe = None
        self.patient_dataframe = None
        self.ast_dataframe = None

        # reformatted DataFrames
        self.reformatted_amr_dataframe = None
        self.reformatted_ast_dataframe = None

        # merged DataFrames
        self.context_dataframe = None
        self.context_dataframe_multipatient = pd.DataFrame()
        self.context_dataframe_new_only = pd.DataFrame()

        # output file paths
        self.potential_transmission_pairs_file = self.output_subdirectory / 'potential_transmission_pairs.csv'  # path to the potential transmission pairs .csv file
        self.potential_transmission_itol_dataset_txt_file = self.output_subdirectory / 'potential_transmission_iTOL_dataset.txt'  # path to the potential transmission pairs iTOL dataset file
        self.outbreak_cluster_file = self.output_subdirectory / 'cluster_analysis.xlsx'  # path to the outbreak cluster summary file

        # the output directory should not exist at this point. We will generate it (as well as the parent directory) now
        if not self.output_directory.is_dir():
            subprocess.run(['mkdir', '-p', str(self.output_directory)])
        
        # initialize the picklejar for this run
        self.pickle_jar = PickleJar(self.output_subdirectory)

    def analyze_cluster(self):
        """
        run the full analysis for this cluster
        """
        self.logger.log_section_header("Now running the individual analysis for {}".format(str(self.distance_matrix_file.name)))
        self.logger.log_explanation("The distance matrix file will be imported and clustered. Context will be collected from the bi-db and used to decorate and filter the results")

        # prepare the current clusters output subdirectory
        self.prepare_output_subdirectory()

        # import and manipulate the distance matrix
        self.distance_matrix_import()
        
        # PERFORM OUTBREAK CLUSTER DETECTION
        self.outbreak_cluster_detection()

        # query for, filter, and reformat the qc and amr data
        try:
            self.query_qc_and_amr()
        except InvalidInputError as err:
            self.logger.log(str(err))
            raise IndividualClusterAnalysisFailedError("QC/AMR import")
        except MultipleMrsnIdsError as err:
            self.logger.log(str(err))
            raise IndividualClusterAnalysisFailedError("QC/AMR import")
            
        # merge the outbreak_cluster_dataframe and 1) the qc dataframe and 2) the reformatted_amr_dataframe
        try:
            self.merge_qc_amr()
        except IndividualClusterAnalysisFailedError:
            raise

        if self.debug:
            print(self.context_dataframe)

        # query for existing outbreak cluster information from the surveillance_clusters collection
        self.query_existing_surveillance_clusters()
        
        # merge the existing clusters information into the context dataframe
        try:
            self.merge_existing_clusters()
        except IndividualClusterAnalysisFailedError:
            raise

        # add in any patientsCollectionIds from previous attempts from the picklejar
        self.add_patient_collection_ids_from_pickle_jar()

        if self.debug:
            print(self.context_dataframe)

        # query for, perform interactive filtering of patients collection records, and filter/reformat ast results
        self.query_patient_and_ast()
        # merge the patient metadata and ast results into the context dataframe
        try:
            self.merge_patient_ast()
        except IndividualClusterAnalysisFailedError:
            raise

        self.clean_up_dates()

        # establish whether samples are newly identified or previously described in clusters
        self.establish_new_vs_old_sample()

        if self.debug:
            print(self.context_dataframe.loc[:,["mrsnId", "isNew", "patientsCollectionId"]])

        # update the surveillance_clusters collection to reflect the most recent data
        self.update_surveillance_clusters()

        # Apply first filter: Remove clusters that involve only a single patient
        self.filter_same_patient_clusters()

        # Apply second filter: Remove clusters that don't include any newly identified isolates
        self.filter_old_clusters()

        # Write out the report
        self.generate_report_file()

    def prepare_output_subdirectory(self):
        if not self.output_subdirectory.is_dir():
            subprocess.run(['mkdir', '-p', str(self.output_subdirectory)])

    #################################################################################################################################################
    #
    #   External-class data manipulations
    #
    #       Methods that involve using other classes (DistanceMatrixManipulation, QcAmrQuery, PatientAstQuery, SurveillanceClusters) to gather and
    #       manipulate datasets 
    #
    #################################################################################################################################################
    def distance_matrix_import(self):
        """
        Initialize an object of class DistanceMatrixManipulation to manage import of distance matrix from input file
        """
        self.distance_matrix_manipulator = DistanceMatrixManipulation(self.output_subdirectory, self.distance_matrix_file, self.clustering_threshold, self.logger)
        try:
            self.distance_matrix_manipulator.distance_matrix_to_dataframe()
            self.sorted_distance_matrix_dataframe = self.distance_matrix_manipulator.sorted_distance_matrix_dataframe.copy()
            self.distance_matrix_manipulator.dataframe_to_pairwise_distance()
            self.distance_matrix_manipulator.threshold_filter()
            self.distance_matrix_manipulator.multi_index_series_to_dict()
            self.distance_matrix_manipulator.generate_potential_transmission_itol_dataset()
        except InvalidInputError as err:
            self.logger.log(str(err))
            self.logger.log("Can not continue analysis of this cluster, aborting and moving on")
        except BadPathError as err:
            self.logger.log(str(err))
            self.logger.log("WARNING: Due to the missing file, the iTOL transmission file will not be generated for this distance matrix")
    
    def query_qc_and_amr(self):
        """
        Initialize an object of class QcAmrQuery to collect and filter the QC and AMR data for isolates implicated in outbreak clusters
        """
        self.qc_amr_query = QcAmrQuery(self.db, self.outbreak_cluster_dataframe, self.logger, self.pickle_jar, verbosity=3)
        try:
            self.qc_amr_query.query_for_qc_and_amr()
        except InvalidInputError:
            raise
            
        self.qc_dataframe = self.qc_amr_query.qc_dataframe.copy()
        self.amr_dataframe = self.qc_amr_query.amr_dataframe.copy()
        self.filtered_amr_dataframe = self.qc_amr_query.filtered_amr_dataframe.copy()
        self.reformatted_amr_dataframe = self.qc_amr_query.reformatted_amr_dataframe.copy()
    
    def merge_qc_amr(self):
        """
        merge the results of query_qc_amr()) into the context dataframe
        """
        self.logger.log_section_header("Merging QC and AMR results")
        self.logger.log_explanation("Combining the outbreak cluster information with the QC, MLST, and reformatted AMR data collected from the bi-db")

        self.context_dataframe = self.outbreak_cluster_dataframe.copy()

        # first merge the QC results into the context dataframe. Validate 1:1 (we should have filtered for a single QC entry in qc_amr_query() with filter_qc_and_amr_dataframes_for_most_recent())
        try:
            self.context_dataframe = self.context_dataframe.merge(self.qc_dataframe, how='left', on='mrsnId', validate="1:1")
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: QC results were not 1:1 with the context dataframe"))
            raise IndividualClusterAnalysisFailedError("Merging QC results")

        # second, merge the reformatted amr results into the context dataframe. Validate 1:1 (otherwise this indicates that the AMR reformatting step didn't collapse AMR alleles correctly)
        try:
            self.context_dataframe = self.context_dataframe.merge(self.reformatted_amr_dataframe, how='left', left_on='mrsnId', right_on=self.reformatted_amr_dataframe.index, validate="1:1")
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: AMR results were not 1:1 with the context dataframe"))
            raise IndividualClusterAnalysisFailedError("Merging Reformatted AMR results")
        
    def query_existing_surveillance_clusters(self):
        """
        Initialize an object of class SurveillanceClusters to collect information about previously identified outbreak clusters
        """
        self.surveillance_clusters = SurveillanceClusters(self.db, self.logger, verbosity=self.verbosity)
        self.surveillance_clusters.gather_surveillance_cluster_data(self.context_dataframe)

    def merge_existing_clusters(self):
        """
        merge the results of query_existing_surveillance_clusters() into the context dataframe
        """
        self.logger.log_section_header("Merging existing outbreak cluster information")
        self.logger.log_explanation("Combining the outbreak cluster information with previously observed cluster information from the bi-db")

        try:
            self.context_dataframe = self.context_dataframe.merge(self.surveillance_clusters.surveillance_clusters_dataframe, how='left', on='mrsnId', validate="1:1")
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: Existing cluster results were not 1:1 with the context dataframe. Potentially duplicate entries in the BI-DB"))
            log_a_list(self.surveillance_clusters.surveillance_clusters_dataframe['mrsnId'].loc[self.surveillance_clusters.surveillance_clusters_dataframe['mrsnId'].duplicated(keep=False)].to_list(), self.logger)
            raise IndividualClusterAnalysisFailedError("Merging existing outbreak cluster results")
        
    def query_patient_and_ast(self):
        """
        Initialize an object of class PatientAstQuery to collect and filter patient metadata for isolates implicated in outbreak clusters
        """
        self.patient_ast_query = PatientAstQuery(self.db, self.context_dataframe, self.logger, self.pickle_jar, verbosity=3)
        try:
            self.patient_ast_query.validate_context_dataframe()
            self.patient_ast_query.query_patients_collection()
            self.patient_ast_query.filter_and_reformat_ast()
            self.context_dataframe = self.patient_ast_query.context_dataframe.copy()
            self.patient_dataframe = self.patient_ast_query.patient_dataframe.copy()
            self.ast_dataframe = self.patient_ast_query.ast_dataframe.copy()
            self.reformatted_ast_dataframe = self.patient_ast_query.reformatted_ast_dataframe.copy()
        except InvalidInputError as err:
            self.logger.log(str(err))
            self.logger.log("Due to the uncaught error, no patient or AST metadata will be collected for this run")
    
    def merge_patient_ast(self):
        """
        merge the results of query_patient_and_ast() into the context dataframe
        """
        self.logger.log_section_header("Merging patient metadata and AST results")
        self.logger.log_explanation("Combining the outbreak cluster information with the patient metadata and AST results data collected from the bi-db")

        # first merge the patient metadata. Since a given patientCollectionId can correspond to more than one mrsnId <shudder> this validation is many-to-1
        try:
            self.context_dataframe = self.context_dataframe.merge(self.patient_dataframe, how='left', on='patientsCollectionId', validate="m:1")
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: Patient metadata results were not 1:m with the context dataframe"))
            raise IndividualClusterAnalysisFailedError("Merging Patient metadata results")
        
        # second merge in the reformatted ast results. Like with the metadata this will also require a many-to-1 validation scheme
        try:
            self.context_dataframe = self.context_dataframe.merge(self.reformatted_ast_dataframe, how='left', left_on='patientsCollectionId', right_on=self.reformatted_ast_dataframe.index, validate='m:1')
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: Reformatted AST results were not 1:m with the context dataframe"))
            raise IndividualClusterAnalysisFailedError("Merging reformatted AST results")
    
    def update_surveillance_clusters(self):
        """
        Use the instance of SurveillanceClusters initialized in existing_surveillance_clusters_query() to update the collection to reflect
        any new information from this run
        """
        self.surveillance_clusters.update_surveillance_clusters_collection(self.context_dataframe, self.distance_matrix_manipulator)


    #################################################################################################################################################
    #
    #   Outbreak Cluster Detection and Filtering
    #
    #################################################################################################################################################
    def outbreak_cluster_detection(self):
        """
        Perform potential outbreak cluster detection using DBSCAN
        """
        self.logger.log_section_header('Performing potential outbreak cluster detection')
        self.logger.log(log.dim('   a) Use DBSCAN to perform cluster assignments for all isolates'))
        self.logger.log(log.dim('   b) Import metadata (if available)'))
        self.logger.log(log.dim('      i) Remove sets containing ONLY isolates from a single patient'))
        self.logger.log(log.dim('   c) Remove sets containing ONLY isolates from a single patient'))
        self.logger.log(log.dim('   d) Reconstruct distance matrices for detected clusters'))

        self.logger.log('\nRunning DBSCAN with {} and {}'.format(log.bold_green('eps={}'.format(self.clustering_threshold)),log.bold_green('min_samples=2\n')))

        # a) Use DBSCAN to perform cluster assignments for all isolates
        db = DBSCAN(eps=self.clustering_threshold, min_samples=2, metric='precomputed').fit_predict(self.sorted_distance_matrix_dataframe)
        clusters = []

        if self.debug:
            print(db)

        for cluster in set(db):
            if cluster != -1:
                clusters.append(sorted([i[1] for i in set(zip(db, self.sorted_distance_matrix_dataframe.index)) if i[0] == cluster]))

        self.logger.log('\nNumber of clusters detected before filtering: ' + str(len(clusters)))
        for cluster in range(0, len(clusters)):
            self.logger.log('\n' + log.underline('Cluster ' + str(cluster + 1) + ' (n=' + str(
                len(clusters[cluster])) + ')' + ' contains the following isolates:'))
            for isolate in clusters[cluster]:
                self.logger.log('   ' + str(isolate))

        # Generate a dataframe of clustered isolates with metadata
        flattened_cluster_dict = {'mrsnId': [], 'cluster': []}
        for cluster in range(0, len(clusters)):
            for isolate in clusters[cluster]:
                flattened_cluster_dict['mrsnId'].append("MRSN" + isolate)
                flattened_cluster_dict['cluster'].append(cluster)

        if self.debug:
            print(flattened_cluster_dict)

        column_list = ['mrsnId', 'cluster']

        self.outbreak_cluster_dataframe = pd.DataFrame(flattened_cluster_dict, columns=column_list).astype({'mrsnId': 'str'})

        if self.debug:
            print(self.outbreak_cluster_dataframe)

    def clean_up_dates(self):
        """
        clean up date fields in context DataFrame
        """
        for index in self.context_dataframe.index:
            if not isinstance(self.context_dataframe.loc[index, "dateOfCulture"], datetime.date) and self.context_dataframe.loc[index, "dateOfCulture"] is not np.nan:
                try:
                    self.context_dataframe.loc[index, "dateOfCulture"] = datetime.datetime.strptime(self.context_dataframe.at[index, "dateOfCulture"], '%d-%m-%Y')
                except TypeError:
                    print("experienced a typeerror")
                    print(self.context_dataframe.at[index, "dateOfCulture"])

    def establish_new_vs_old_sample(self):
        """
        If a sample already belongs to a named outbreak cluster, the sample is old aka previously observed. Otherwise the sample is considered
        new aka not previously implicated in an outbreak cluster. Record this information as a boolean in the column "isNew"
        """
        self.logger.log_section_header("Annotating Previously Observed Isolates")
        self.logger.log_explanation("Adding a column to the context DataFrame, 'isNew', that is True if an isolate has not previsouly been implicated in a potential transmission cluster")

        print(self.context_dataframe.columns)

        self.context_dataframe['isNew'] = False
        mask = pd.isnull(self.context_dataframe['clusterName'])
        self.context_dataframe.loc[mask,'isNew'] = True

    def filter_same_patient_clusters(self):
        """
        Create a new subset of the context dataframe, context_dataframe_multipatient, that contains only information on clusters that contain at least 
        two distinct patients
        """
        self.logger.log_section_header("Removing Same Patient Clusters")
        self.logger.log_explanation("Removing potential outbreak clusters that contain isolates from only one patient (i.e. serial isolates)")

        grouped_by_cluster = self.context_dataframe.groupby("clusterName")

        for name, group in grouped_by_cluster:

            # add back the clusterName field
            group.loc[:,"clusterName"] = name

            self.logger.log(log.bold("\nNow analyzing cluster: {}".format(name)))
            
            if self.debug:
                print(group[["mrsnId", "patientId", "armordPatientId"]])

            # if all isolates have a patientId value, we can use this to assess the number of patients involved
            if group["patientId"].notna().all():  # returns True if all isolates have a patientId in the context dataframe
                if len(set(group["patientId"].to_list())) == 1:
                    self.logger.log("Cluster {} only contains isolates from one patient ({}), so it will be filtered".format(name, group["patientId"].to_list()[0]))
                    continue
                else:
                    self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                    continue

            # if all isolates have a armordPatientId value, we can use this to assess the number of patients involved
            if group["armordPatientId"].notna().all():  # returns True if all isolates have a armordPatientId in the context dataframe
                if len(set(group["armordPatientId"].to_list())) == 1 and len(set(group["patientId"].loc[group["patientId"].notna()])) <= 1:
                    self.logger.log("Cluster {} only contains isolates from one armordPatientId ({}), so it will be filtered".format(name, group["patientId"].to_list()[0]))
                    continue
                else:
                    self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                    continue

            # in all other cases we will have insufficient patient information to confidently filter out a cluster programmatically, so the remaining code only
            # determines the logging method prior to retaining the cluster

            # if some isolates have a patientId and the remainder have an M#, attempt to reconcile these values and see if multiple patients are involved
            if group["patientId"].notna().any() and group["armordPatientId"].notna().any():  # returns True if any isolates have a patientId or an M# in the context dataframe
                
                # first see if the isolates with patientId values represent 2+ patients, which will automatically qualify the cluster for inclusion
                if len(set(group["patientId"].loc[group["patientId"].notna()])) > 1:
                    self.logger.log("Cluster {} contains some isolates without patientIds, but 2+ patientIds are present, so it will be retained".format(name))
                    self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                    continue

                # if only one patientId is present, see if there are more than one M# present, which also qualifies the cluster to move forward
                if len(set(group["armordPatientId"].loc[group["armordPatientId"].notna()])) > 1:
                    self.logger.log("Cluster {} contains some isolates without patientIds, but 2+ armordPatientIds are present, so it will be retained".format(name))
                    self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                    continue

            # if any isolates don't have a patientId OR an M#, keep the cluster and log a warning that same patient filtering couldn't be performed reliably
            double_null = False
            for index in group.index.tolist():
                if pd.isna(group.loc[index, "patientId"]) and pd.isna(group.loc[index, "armordPatientId"]):
                    self.logger.log("WARNING: Isolate {} has neither a patientId or an M#. Out of an abundance of caution we will pass this cluster ({}) through".format(group.at[index, "mrsnId"], name))
                    double_null = True
            if double_null:
                self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                continue
            
            # if we got this far, it appears that there are patientIds and M#'s for all samples but no overlap, so we keep the cluster
            self.logger.log("There is insufficient information to determine the number of patients in this cluster. Out of an abundance of caution we will pass this cluster ({}) through".format(name))
            self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
            continue

    def filter_old_clusters(self):
        """
        Create a new subset of the context dataframe, context_dataframe_new_only, from context_dataframe_multipatient, that contains only information
        on clusters that contain at least one sample that was identified as "new"
        """
        self.logger.log_section_header("Removing clusters containing only previously reported on isolates")
        self.logger.log_explanation("Removing potential outbreak clusters that contain isolates that are all located in the surveillance clusters collection")

        grouped_by_cluster = self.context_dataframe_multipatient.groupby("clusterName")

        for name, group in grouped_by_cluster:

            if (group["isNew"] == True).any():  # returns True if any isolate in the cluster is new
                self.context_dataframe_new_only = pd.concat([self.context_dataframe_new_only, group])
            else:
                self.logger.log("Cluster {} contained no new isolates, so it will be filtered".format(name))

    #################################################################################################################################################
    #
    #   Checkpointing: temporary storage of mrsnId to patientCollectionId relationships that were determined during the run. Allows the user
    #   to stop/abort/error out of the current run without losing the progress they already made
    #
    #   For more info, see class PickleJar below
    #
    ################################################################################################################################################# 
    def add_patient_collection_ids_from_pickle_jar(self):
        """
        use the contents of the pickle jar to catch up any progress that was made on assigning patientCollectionId's from the last run
        """
        # add in any patient_collection_ids that were found in the picklejar
        self.pickle_jar.read_a_pickle()
        
        if self.pickle_jar.patient_collection_ids:
            for k,v in self.pickle_jar.patient_collection_ids.items():
                
                self.context_dataframe.loc[self.context_dataframe["mrsnId"] == k, "patientsCollectionId"] = v
    
    #################################################################################################################################################
    #
    #  Output/Report Generation
    #
    #################################################################################################################################################
    def generate_report_file(self):
        """
        prepare the final report document, writing out the filtered and raw files to an Excel Spreadsheet
        """
        self.logger.log_section_header("Generating the report file(s) for this run")
        self.logger.log_explanation("Generating an Excel spreadsheet that contains detected clusters under various filtering conditions as well as raw data")
    
        with pd.ExcelWriter(self.outbreak_cluster_file, mode='w', engine='openpyxl') as writer:  # pylint: disable=abstract-class-instantiated
            self.context_dataframe_new_only.to_excel(writer, sheet_name="Clusters - New Isolates", index=False)
            self.context_dataframe_multipatient.to_excel(writer, sheet_name="Clusters - Multiple Patients", index=False)
            self.context_dataframe.to_excel(writer, sheet_name="Clusters - All", index=False)
            self.qc_dataframe.to_excel(writer, sheet_name="QC Summary", index=False)
            self.filtered_amr_dataframe.to_excel(writer, sheet_name="AMR Summary", index=False)
            self.patient_dataframe.to_excel(writer, sheet_name="Patient Summary", index=False)
            self.ast_dataframe.to_excel(writer, sheet_name="AST Summary", index=False)


class IndividualClusterAnalysisFailedError(Exception):
    """Raise this if the current cluster analysis fails for any reason to let the cluster manager know not to expect results"""
    
    def __init__(self, analysis_stage):

        self.header = "FATAL ERROR: The current cluster analysis failed!"
        self.analysis_stage = analysis_stage
    
    def __str__(self):

        return log.bold_red('\n{}\nThe analysis failed during: {}\n'.format(self.header, self.analysis_stage))

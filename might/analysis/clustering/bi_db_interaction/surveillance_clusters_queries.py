#!/usr/bin/env python
# encoding: utf-8
import datetime

import numpy as np
import pandas as pd
from coolname import generate_slug
from might.common import log
from might.common.miscellaneous import query_yes_no, quit_with_error
from pandas.errors import MergeError
from pymongo import MongoClient
from pymongo import errors as pyerr


#################################################################################################################################################
#
#  Class SurveillanceClusters
#
#   Class SurveillanceClusters is responsible for querying the surveillance_clusters collection of the bi-db and parsing the results
#   to provide context regarding existing clusters and take advantage of MRSN-to-patient linking done previously. It is also used to 
#   create, update, and merge surveillance cluster documents based on clustering performed during the current run
#
#################################################################################################################################################
class SurveillanceClusters:

    def __init__(self, db, logger, verbosity=2):
        
        self.db = db
        self.logger = logger
        self.verbosity = verbosity  # allows for the logging in this section to be selectively printed to the terminal
        self.debug=False
        if self.verbosity == 3:
            self.debug = True

        self.surveillance_clusters_dataframe = None
    
    def gather_surveillance_cluster_data(self, context_dataframe):
        """
        Query the surveillance_clusters collection of the bi-db to get relevant information
        """
        self.context_dataframe = context_dataframe

        self.logger.log_section_header("Gathering Potential Transmission Cluster Data from BI-DB")
        self.logger.log_explanation("Collecting available information on previously identified potential transmission clusters from the BI-DB's surveillance_clusters collection")

        samples = self.context_dataframe["mrsnId"].to_list()
        
        query = self.db.surveillance_clusters.find( { "isolates.mrsnId": { "$in": samples } } )  # gather all active clusters that have a sample in samples

        column_list = ["mrsnId", "clusterName", "clusterNickName", "patientsCollectionId", "dateAdded", "priorClusters"]

        clusters_query_list = []
        for doc in query:

            if not doc["isActive"]:
                continue

            cluster_name = doc["_id"]
            cluster_nick_name = doc["clusterNickName"]
            
            # unpack the isolates embedded object list
            for isolate in doc['isolates']:
                isolate["clusterName"] = cluster_name
                isolate["clusterNickName"] = cluster_nick_name
                clusters_query_list.append(isolate)
        
        self.surveillance_clusters_dataframe = pd.DataFrame(clusters_query_list, columns=column_list)

        if self.debug:
            print(self.surveillance_clusters_dataframe)  

    def update_surveillance_clusters_collection(self, context_dataframe, distance_matrix_manipulation_object):
        """
        main method of SurveillanceClusters. Manages updating the surveillance_clusters collection of the bi-db to reflect new samples/data
        """
        self.context_dataframe = context_dataframe
        self.distance_matrix_manipulation_object = distance_matrix_manipulation_object

        self.logger.log_section_header("Updating the surveillance clusters collection in the bi-db")
        self.logger.log_explanation("Creating, modifying, or merging detected potential transmission clusters as necessary based on the latest data")

        # group the context dataframe by cluster
        context_dataframe_by_cluster = self.context_dataframe.groupby(["cluster"])
        for name, group in context_dataframe_by_cluster:

            self.logger.log("Processing cluster: {}".format(log.bold(str(name))))
            self.logger.log("This cluster contains {} entries".format(log.bold(str(len(group.index.to_list())))))

            # sort by date of culture since this will be the basis of naming/renaming clusters
            group = group.sort_values('dateOfCulture')

            cluster_names = group["clusterName"].loc[group["clusterName"].notna()].to_list()
            cluster_has_new = any(group["isNew"] == True)

            self.logger.log("This cluster contains {} entries with existing cluster names".format(log.bold(str(len(cluster_names)))))
            for cluster_name in cluster_names:
                self.logger.log("\t{}".format(cluster_name))
            
            # case 1: all samples are previously described and have the same clusterName (no action required)
            if len(cluster_names) == 1 and not cluster_has_new:
                self.logger.log("No changes detected for cluster {}".format(cluster_names[0]), verbosity=3)
                continue

            # case 2: 1+ new samples, 1+ samples have a clusterName, only one clusterName (update surveillance document)
            if len(cluster_names) == 1 and cluster_has_new:
                cluster_name = cluster_names[0]
                self.update_surveillance_cluster_document(cluster_name, group)
                continue

            # case 3: 1+ new samples, 1+ samples have a clusterName, 2+ clusterNames (merge surveillance documents)
            if len(cluster_names) > 1 and cluster_has_new:
                self.merge_surveillance_cluster_documents(group)
                continue

            # case 4: ALL new samples (create surveillance document)
            if len(cluster_names) == 0 and cluster_has_new:
                self.create_surveillance_cluster_document(name, group)
                continue
            
            # case 5: all samples are previously described, 2+ clusterNames (WARNING + merge surveillance documents)
            if len(cluster_names) > 1 and not cluster_has_new:
                pass

    def summarize_surveillance_cluster_document(self, doc_id):
        """
        log a summary of the surveillance cluster document with _id doc_id
        """
        query = self.db.surveillance_clusters.find_one({"_id": doc_id})

        if query["isActive"]:
            cluster_status = "Active"
        else:
            cluster_status = "Deactivated"

        # collect all values for fields of interest into a dictionary
        isolate_info_dict = {"verifiedFacilityName": [], "dateOfCulture": [], "specificPatientLocation": [], "cultureType": []}
        patients_collection_ids = [isolate["patientsCollectionId"] for isolate in query["isolates"]]
        patients_collection_query = self.db.patients.find({"_id": { "$in": patients_collection_ids } }, projection=isolate_info_dict.keys())
        for doc in patients_collection_query:
            for key in isolate_info_dict.keys():
                isolate_info_dict[key].append(doc[key])

        list_of_facilities = sorted(list(set(isolate_info_dict["verifiedFacilityName"])))
        list_of_culture_types = sorted(list(set(isolate_info_dict["cultureType"])))
        

        # log out the significant information
        self.logger.log("Cluster Name: {}".format(log.bold(query["_id"])))
        self.logger.log("Cluster Nick Name: {}".format(log.bold(query["clusterNickName"])))
        self.logger.log("Cluster Status: {}".format(log.bold(cluster_status)))

    def create_surveillance_cluster_document(self, cluster, subset_dataframe):
        """
        For entirely NEW clusters, create a new document in the bi-db's surveillance_clusters collection.
        """
        self.logger.log_section_header("Creating a new entry for cluster {} in BI-DB's surveillance_clusters collection".format(cluster))
        self.logger.log_explanation("Documents in the surveillance_clusters collection track isolates implicated in potential transmission clusters through time")

        # get the subset of the context dataframe that pertains to the cluster_name
        cluster_subset_dataframe = self.distance_matrix_manipulation_object.generate_subset_distance_matrix_dataframe(cluster, self.context_dataframe)

        # get all of the _ids (aka cluster names) in the surveillance_clusters collection
        current_cluster_names = self.db.surveillance_clusters.distinct("_id")

        print(subset_dataframe)

        # cluster names are of the form verified_facility-wgsId-ST-#. If there are matches for the first three, we need to index the # field by one
        try:
            cluster_facility = str(subset_dataframe["verifiedFacilityName"].loc[subset_dataframe["verifiedFacilityName"].first_valid_index()])
        except KeyError:  # no patient data was available for this cluster, so we will need to get the facility name from somewhere else
            self.logger.log("None of the isolates in this cluster contain a valid verifiedFacilityName field value")
            if query_yes_no("Would you like to add one manually?"):
                cluster_facility = input("Please enter the facility name to use: ").upper()
            else:
                cluster_facility = "unknown"
        cluster_wgs_id = str(subset_dataframe["wgsId"].loc[subset_dataframe["wgsId"].first_valid_index()])

        # for cluster st, we want to replace "-" with "unknown|novel for clarity"
        cluster_st = str(subset_dataframe["mlst"].loc[subset_dataframe["mlst"].first_valid_index()])
        if cluster_st == "-":
            cluster_st = "UNKNOWN|NOVEL"

        # contruct the cluster name string prefix
        cluster_name_prefix = "-".join([cluster_facility, cluster_wgs_id, cluster_st]).replace(" ", "_")
        self.logger.log("The cluster name prefix for this cluster: {}".format(log.bold(cluster_name_prefix)))

        # compare the cluster name prefix to the current cluster names. If it appears, we set the # to the len of the existing list +1
        matching_current_cluster_names = [cluster_name for cluster_name in current_cluster_names if cluster_name_prefix in cluster_name]
        cluster_name = "-".join([cluster_name_prefix, str(len(matching_current_cluster_names)+1)])
        
        # pick a cluster nickname
        cluster_nick_name = None
        while not cluster_nick_name:
            potential_nick_name = generate_slug(3)
            while potential_nick_name in self.db.surveillance_clusters.distinct("clusterNickNames"):
                potential_nick_name = generate_slug(3)
            cluster_nick_name = potential_nick_name
            #if query_yes_no("Accept {} as the cluster nick name?".format(log.bold(potential_nick_name)), default="yes"):
             #   cluster_nick_name = potential_nick_name

        # create the new surveillance_clusters collection document
        date_added = datetime.datetime.now()
        isolates = []
        isolate_entries = subset_dataframe.loc[:,["mrsnId", "patientsCollectionId"]].to_dict(orient="index")
        for v in isolate_entries.values():
            v["dateAdded"] = date_added
            isolates.append(v)
        
        new_surveillance_cluster = {
            "_id": cluster_name,
            "clusterNickName": cluster_nick_name,
            "isActive": True,
            "dateCreated": date_added,
            "isolates": isolates,
            "cgmlstDistanceMatrix": cluster_subset_dataframe.to_dict(orient="records")
        }

        self.db.surveillance_clusters.insert_one(new_surveillance_cluster)

        # update the context dataframe with the new values of cluster_name and cluster_nick_name
        for sample in subset_dataframe["mrsnId"].to_list():
            self.context_dataframe["clusterName"].loc[self.context_dataframe["mrsnId"] == sample] = cluster_name
            self.context_dataframe["clusterNickName"].loc[self.context_dataframe["mrsnId"] == sample] = cluster_nick_name

        # TODO log a summary of date range, facilities, and previous reports for the new cluster

    def update_surveillance_cluster_document(self, cluster_name, subset_dataframe):
        """
        For clusters with an existing document in the bi-db's surveillance_clusters collection update the isolates list and cgmlstDistanceMatrix fields
        """
        self.logger.log_section_header("Updating the entry for {} in BI-DB's surveillance_clusters collection".format(cluster_name))
        self.logger.log_explanation("Documents in the seqeuenced_isolates collection will be updated to reflect the current outbreakClusterName value")

        # get the subset of the context dataframe that pertains to the cluster_name
        cluster_subset_dataframe = self.distance_matrix_manipulation_object.generate_subset_distance_matrix_dataframe(cluster_name, self.context_dataframe)

        # add isolate information for each isolate in the cluster that is not already present
        new_isolate_dataframe = subset_dataframe.loc[subset_dataframe["isNew"] == True, ["mrsnId", "patientsCollectionId"]]
        date_added = datetime.datetime.now()

        for index in new_isolate_dataframe.index:
            new_isolate = {
                "mrsnId": new_isolate_dataframe.at[index, "mrsnId"],
                "patientsCollectionId": new_isolate_dataframe.at[index, "patientsCollectionId"],
                "dateAdded": date_added,
            }
            self.db.surveillance_clusters.update_one({"_id": cluster_name}, {"$addToSet": {"isolates": new_isolate}})

            # update the value for clusterName/clusterNickName for this isolate in the context dataframe
            cluster_nick_name = str(subset_dataframe["clusterNickName"].loc[subset_dataframe["clusterNickName"].first_valid_index()])

            self.context_dataframe.at[index, "clusterName"] = cluster_name
            self.context_dataframe.at[index, "clusterNickName"] = cluster_nick_name
        
        # set the cgmlstDistanceMatrix field in the surveillance_clusters collection document to the new subset dataframe
        self.db.surveillance_clusters.update_one({"_id": cluster_name}, {"$set": {"cgmlstDistanceMatrix": cluster_subset_dataframe.to_dict(orient="records")}})
        
        # TODO log a summary of date range, facilities, and previous reports for the updated cluster

    def merge_surveillance_cluster_documents(self, subset_dataframe):
        """
        When a cgMLST cluster is associated with 2+ entries in the bi-db's surveillance_clusters collection this method is used to merge the relevant fields from the cluster
        being retired into the one that is being expanded. It then sets the isActive field in the retired cluster to False
        """
        self.logger.log_section_header("Merging entries in BI-DB's surveillance_clusters collection")
        self.logger.log_explanation("MIGHT cluster has detected that a cluster contains isolates that were previously being tracked by 2+ documents in the surveillance_clusters collection. The isolates will be merged into the earliest cluster and the other clusters will be set to inactive")

        # determine the earliest cluster based on the collection date of the earliest isolate in the current cluster
        earliest_index = subset_dataframe.loc[subset_dataframe["mrsnId"].first_valid_index()]
        earliest_mrsnid = str(earliest_index["mrsnId"])
        earliest_cluster = str(earliest_index["clusterName"])
        earliest_cluster_nick_name = str(earliest_index["clusterNickName"])

        self.logger.log("Isolate {} from cluster {} has the earliest collection date, so all isolates will be merged into this cluster".format(earliest_mrsnid, earliest_cluster))
        
        clusters_to_deactivate = [cluster for cluster in subset_dataframe["clusterName"].to_list() if cluster != earliest_cluster]
        self.logger.log("The following clusters will be merged. The associated bi-db surveillance_clusters documents will be set to inactive")
        for cluster in clusters_to_deactivate:
            self.logger.log("\t{}".format(cluster))

        # gather all of the information from the isolates embedded documents field for each cluster in clusters_to_deactivate
        for cluster in clusters_to_deactivate:

            # get the info from the isolates field, and add the cluster to be inactivated to the priorClusters field
            deactivated_isolates = subset_dataframe.loc[subset_dataframe["clusterName"] == cluster].to_dict(orient="index")
            for v in deactivated_isolates.values():
                if v["priorClusters"] != np.nan:
                    v["priorClusters"] = "|".join([v["priorClusters"], cluster])
                else:
                    v["priorClusters"] = cluster

                #insert the entry into the active clusters isolates list
                self.db.surveillance_clusters.update_one({"_id": earliest_cluster}, {"$addToSet": {"isolates": v}})

                # update the clusterName, clusterNickname, and priorClusters fields for this sample in the context dataframe
                self.context_dataframe["clusterName"].loc[self.context_dataframe["mrsnId"] == v["mrsnId"]] = earliest_cluster
                self.context_dataframe["clusterNickName"].loc[self.context_dataframe["mrsnId"] == v["mrsnId"]] = earliest_cluster_nick_name
                self.context_dataframe["priorClusters"].loc[self.context_dataframe["mrsnId"] == v["mrsnId"]] = v["priorClusters"]

            # set the clusters isActive field to False
            self.db.surveillance_clusters.update_one({"_id": cluster}, {"$set": {"isActive": False}})

        # use the update_surveillance_cluster_document() method to handle any new isolates
        self.update_surveillance_cluster_document(earliest_cluster, subset_dataframe)


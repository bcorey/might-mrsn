#!/usr/bin/env python
# encoding: utf-8
import pandas as pd
from pandas.errors import MergeError

from pymongo import MongoClient
from pymongo import errors as pyerr

from might.common import log
from might.common.miscellaneous import path_clean, file_check, file_extension_check, quit_with_error, query_yes_no
from might.common.errors import InvalidInputError

#################################################################################################################################################
#
#  Class QcAmrQuery
#
#   Class QcAmrQuery is responsible for querying the sequenced_isolates collection of the bi-db and parsing the results
#   to populate the qc_dataframe and amr_dataframe. It includes methods to performing filtering operations for cases where
#   multiple good run data is available for a sample. It can also filter AMR and reformat it to one line per sample
#
#################################################################################################################################################
class QcAmrQuery:

    def __init__(self, db, outbreak_cluster_dataframe, logger, pickle_jar, verbosity=2):
        
        self.db = db
        self.outbreak_cluster_dataframe = outbreak_cluster_dataframe
        self.logger = logger
        self.verbosity = verbosity  # allows for the logging in this section to be selectively printed to the terminal

        # primary output
        self.qc_dataframe = pd.DataFrame()
        self.amr_dataframe = pd.DataFrame()

        self.filtered_amr_dataframe = None

        # the reformatted ast dataframe has been filtered and converted to one line per sample
        self.reformatted_amr_dataframe = None

    def query_for_qc_and_amr(self):
        """
        Query the bi-db for qc and amr records. Filter for the most recent valid record, filter AMR, and reformat the AMR results into a one line per sample format
        """
        self.logger.log_section_header("Querying the bi-db for QC and AMR information")
        self.logger.log_explanation("Collecting the most recent valid QC and AMR results from the bi-db's sequenced_isolates collection")

        try:
            self.validate_outbreak_cluster_dataframe()
            self.gather_isolate_metadata()
            self.filter_qc_and_amr_dataframes_for_most_recent()
            self.filter_amr_dataframe()
            self.reformat_amr_dataframe()
        except InvalidInputError:
            raise
        except MultipleMrsnIdsError:
            raise
        
    def validate_outbreak_cluster_dataframe(self):
        """
        ensure that the context dataframe passed during init contains the required columns
        """
        req_cols = [
            'mrsnId', 
        ]

        if not all(col in self.outbreak_cluster_dataframe.columns for col in req_cols):
            raise InvalidInputError(['outbreak cluster dataframe'], 'Missing at least one required column!')

    def gather_isolate_metadata(self):
        """
        collect the isolate metadata from the bi-db's sequenced_isolates collection
        """
        self.logger.log_section_header("Gathering Isolate Data from BI-DB")
        self.logger.log_explanation("Collecting available species ID, MLST, and AMR information from the BI-DB")

        qc_dataframe_fields = [
            'mrsnId',
            'qcCallFinal',
            'chcsAccession',
            'genus',
            'wgsId',
            'sequencingRun',
            'sequencingRunDate',
            'mlst',
            'projects',
            'amr',
            'armordPatientId',
            'armordFacility',
            'armordCultureDate',
            'armordCultureType'
        ]

        acceptable_qc = [
            "PASS",
            "PASS - ID",
            "pass",
            "pass - ID"
        ]

        #samples = [''.join(["MRSN",entry]) for entry in self.outbreak_cluster_dataframe['mrsnId'].to_list()]
        samples = [entry for entry in self.outbreak_cluster_dataframe['mrsnId'].to_list()]

        query = self.db.sequenced_isolates.find({'mrsnId': { '$in': samples}, 'qcCallFinal': {'$in': acceptable_qc}}, projection=qc_dataframe_fields)

        amr_query_list = []
        qc_query_list = []  

        for document in query:

            mrsn_id = document['mrsnId']
            sequencing_run = document['sequencingRun']

            # convert the array of projects to a | separated string of projectCodes
            project_codes = []
            try:
                for project in document['projects']:
                    project_codes.append(project['projectCode'])
                project_codes_as_string = ' | '.join(project_codes)
            except TypeError:  # handle case where no projects are present
                project_codes_as_string = ""

            # replace the projects array entry with the string representation
            document['projects'] = project_codes_as_string

            query_entry = document

            # add the mrsn_id to each amr entry as mrsnId
            if 'amr' in document.keys():
                for amr in document['amr']:
                    amr['mrsnId'] = mrsn_id
                    amr['sequencingRun'] = sequencing_run
                    amr_query_list.append(amr)

                # save the qc results (document sans amr) to self.qc_query_list
                query_entry.pop('amr', None)

            qc_query_list.append(query_entry)

        # convert the qc_query_list and amr_query_list to pandas DataFrames
        self.qc_dataframe = pd.DataFrame(qc_query_list)
        self.amr_dataframe = pd.DataFrame(amr_query_list)

        # move the mrsnId column in amr_dataframe to first position
        last_col = self.amr_dataframe.pop('mrsnId')
        self.amr_dataframe.insert(0, 'mrsnId', last_col)
        last_col = self.amr_dataframe.pop('sequencingRun')
        self.amr_dataframe.insert(0, 'sequencingRun', last_col)

    def filter_qc_and_amr_dataframes_for_most_recent(self):
        """
        Some isolates have multiple records corresponding to sequencing runs that they passed QC on. In these cases we will
        default to the most recent run on which the isolate passed.
        """
        self.logger.log_section_header("Filtering Isolate QC Results for the Most Recent Passed Entry")
        self.logger.log_explanation("Some isolates have multiple records corresponding to sequencing runs that they passed QC on. In these cases we will default to the most recent run on which the isolate passed.")

        # Use pandas groupby() on mrsnId. If more than one entry is present, sort by sequencingRunDate and drop all but the most recent
        grouped_by_mrsn_id = self.qc_dataframe.groupby(['mrsnId'])
        for name, group in grouped_by_mrsn_id:
            if len(group.index) == 1:
                continue
            
            self.logger.log("{} has more than one PASS or PASS - ID qcCallFinal status in the sequenced_isolates collection".format(name), verbosity=3)
            group = group.sort_values('sequencingRunDate', ascending=False)
            for index in group.index:
                self.logger.log("[{}]: {} - {} - {}".format(index, group.at[index, "sequencingRun"], group.at[index, "sequencingRunDate"], group.at[index, "qcCallFinal"]), verbosity=3)
            
            selected_index = group['sequencingRunDate'].first_valid_index()
            self.logger.log("Keeping index: {}".format(str(selected_index)), verbosity=3)

            # edge case where NO dates are available, just go with the first entry
            if selected_index == None:
                selected_index = group.index.tolist()[0]

            for index in group.index.tolist():
                if index != selected_index:
                    self.qc_dataframe.drop(index=index, inplace=True)

                    # drop entries from the AMR dataframe that are no longer represented in the QC dataframe as well
                    mrsnId = group.at[index, "mrsnId"]
                    run_id = group.at[index, "sequencingRun"]
                    self.amr_dataframe.drop(self.amr_dataframe[(self.amr_dataframe["mrsnId"] ==  mrsnId) & (self.amr_dataframe["sequencingRun"] != run_id)].index, inplace = True)
        
        # verify that we now have only one result per mrsnId
        grouped_by_mrsn_id = self.qc_dataframe.groupby(['mrsnId'])
        for name, group in grouped_by_mrsn_id:
            if len(group.index.to_list()) > 1:
                raise MultipleMrsnIdsError(self.qc_dataframe)
            
    def filter_amr_dataframe(self, amr_tier_filter="2"):
        """
        filter amr alleles by tier or expected phenotype
        """
        self.logger.log_section_header("Filtering the AMR DataFrame")
        self.logger.log_explanation("Removing AMR results that are less than tier {}".format(amr_tier_filter))

        if amr_tier_filter == "1":
            self.filtered_amr_dataframe = self.amr_dataframe.loc[self.amr_dataframe['tier1'] == True]
        elif amr_tier_filter == "2":
            self.filtered_amr_dataframe = self.amr_dataframe.loc[(self.amr_dataframe['tier1'] == True) | (self.amr_dataframe['tier2'] == True)]
        elif amr_tier_filter == "3":
            self.filtered_amr_dataframe = self.amr_dataframe.loc[(self.amr_dataframe['tier1'] == True) | (self.amr_dataframe['tier2'] == True) | (self.amr_dataframe['tier3'] == True)]
        else:
            self.filtered_amr_dataframe = self.amr_dataframe
        
    def reformat_amr_dataframe(self, amr_tier_filter="2"):
        """
        transform the amr dataframe to one line per sample format
        """
        self.logger.log_section_header("Reformatting the AMR DataFrame")
        self.logger.log_explanation("Transforming the AMR results to one line per sample format")
        
        amr_by_expected_phenotype = self.filtered_amr_dataframe.groupby(['expectedPhenotype'])

        amr_expected_phenotypes = amr_by_expected_phenotype.groups.keys()
        self.reformatted_amr_dataframe = pd.DataFrame(columns=amr_expected_phenotypes)

        amr_by_expected_phenotype = self.filtered_amr_dataframe.groupby(['mrsnId','expectedPhenotype'])
        for name, group in amr_by_expected_phenotype:
            families = " | ".join(sorted(list(set(group.mrsnSuperFamily.to_list())), reverse=True))
            self.reformatted_amr_dataframe.at[name[0], name[1]] = families


class MultipleMrsnIdsError(Exception):
    """Raise this if the qc dataframe contains duplicate MRSN ID results after filtering for most recent valid entry"""
    
    def __init__(self, qc_dataframe):

        self.header = "FATAL ERROR: The following MRSN ID's appear multiple time in the QC/AMR DataFrames after filtering for the most recent valid entry"
        self.qc_dataframe = qc_dataframe
    
    def __str__(self):

        # get a list of all duplicate mrsnId in the qc dataframe
        duplicates = self.qc_dataframe["mrsnId"].loc[self.qc_dataframe["mrsnId"].duplicated()].to_list()

        return log.bold_red('\n{}\nSample(s): {}\n'.format(self.header, '\n'.join(duplicates)))
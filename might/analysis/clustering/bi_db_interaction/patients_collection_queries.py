#!/usr/bin/env python
# encoding: utf-8
import sys

import pandas as pd
from might.common import log
from might.common.errors import InvalidInputError, SampleNotFoundError
from might.common.miscellaneous import (log_a_list, pick_from_a_list,
                                        query_yes_no)
from pandas.errors import MergeError
from pymongo import MongoClient
from pymongo import errors as pyerr


#################################################################################################################################################
#
#  Class PatientAstQuery
#
#   Class PatientAstQuery is responsible for querying the patients collection of the bi-db and parsing the results
#   to populate the patient_dataframe and ast_dataframe. This process may involve user interaction when ambiguous 
#   results are returned
#
#################################################################################################################################################
class PatientAstQuery:

    def __init__(self, db, context_dataframe, logger, pickle_jar, verbosity=2):
        
        self.db = db
        self.context_dataframe = context_dataframe
        self.logger = logger
        self.verbosity = verbosity  # allows for the logging in this section to be selectively printed to the terminal

        # primary output
        self.patient_dataframe = pd.DataFrame()
        self.ast_dataframe = pd.DataFrame()

        # the reformatted ast dataframe has been filtered and converted to one line per sample
        self.reformatted_ast_dataframe = None
    
        self.drug_classes_of_interest = [
            'AMINOGLYCOSIDES',
            'BETA-LACTAMS - B',
            'FLUOROQUINOLONES'
        ]

        self.pickle_jar = pickle_jar
    
    def validate_context_dataframe(self):
        """
        ensure that the context dataframe passed during init contains the required columns
        """
        req_cols = [
            'mrsnId', 
            'chcsAccession', 
            'genus', 
            'patientsCollectionId'
        ]

        if not all(col in self.context_dataframe.columns for col in req_cols):
            raise InvalidInputError(['context dataframe'], 'Missing at least one required column!')
    
    def query_patients_collection(self):
        """
        Primary method of this class, orchestrates collection of patient and ast data from the bi-dbs patient collection based
        on the patientsCollectionId and chcsAccession field values in the context DataFrame. In the case where the relationship
        between a sample and a sample from a given chcsAccession is ambiguous the user is called upon to make an adjudication
        """
        # first, gather patient/AST data for any entries that have a patientsCollectionId in bulk OR were already processed and stored in the picklejar
        located_samples = self.collect_patient_and_ast_for_patient_collection_ids()

        self.remaining_context_dataframe = self.context_dataframe.loc[~self.context_dataframe['mrsnId'].isin(located_samples)]

        # generate a list of remaining CHCS values to interrogate
        remaining_samples = [chcs for chcs in self.remaining_context_dataframe['chcsAccession'].loc[self.remaining_context_dataframe['chcsAccession'].notna()].to_list()]

        # query the patients collection for the remaining samples
        query = list(self.db.patients.find({"chcsAccessionNumber": {"$in": remaining_samples}}))

        # generate the list of samples that did/didn't return any results from the patients collection
        samples_located = [chcs for chcs in remaining_samples if chcs in [doc["chcsAccessionNumber"] for doc in query]]
        samples_not_located = [chcs for chcs in remaining_samples if chcs not in samples_located]

        # DEBUG 
        sample_set = set(samples_located)
        if len(samples_located) != len(sample_set):
            self.logger.log("WARNING: the sample set is {} entries shorter than the list, indicating the potential presence of deprecated samples!".format(str(len(samples_located) - len(sample_set))), verbosity=self.verbosity)
            for entry in sample_set:
                if samples_located.count(entry) > 1:
                    self.logger.log(entry)
                    log_a_list(self.context_dataframe['mrsnId'].loc[self.context_dataframe["chcsAccession"] == entry].to_list(), self.logger)

        self.logger.log("Found the following {} accession numbers in the patients collection:".format(str(len(samples_located))))
        log_a_list(sample_set, self.logger)

        # iterate through samples located attempting to get the correct document from query
        self.logger.log_section_header("Retrieving Patient and AST data for samples with matches in the patients collection", verbosity=self.verbosity)
        self.logger.log_explanation("Querying the patients collection for patient metadata and AST data for isolates that have one or more matches on the chcsAccession/chcsAccessionNumber field", verbosity=self.verbosity)

        for chcs_accession in sample_set:

            mrsn_id = self.context_dataframe['mrsnId'].loc[self.context_dataframe["chcsAccession"] == chcs_accession].to_list()

            self.chcs_match_utility(chcs_accession, mrsn_id, query)
            
        # now we move on to iterating through the non-matching chcs values, giving the user the option to try and correct the chcs value
        self.logger.log_section_header("Retrieving Patient and AST data for samples with NO matches in the patients collection", verbosity=self.verbosity)
        self.logger.log_explanation("The isolates in this section have no matches on the chcsAccession/chcsAccessionNumber field in the patients collection. The user will have the opportunity to make corrections to these values or abandon them", verbosity=self.verbosity)

        self.logger.log("Unable to locate the following {} accession numbers in the patients collection:".format(str(len(samples_not_located))))
        log_a_list(samples_not_located, self.logger)

        samples_not_located_set = set(samples_not_located)

        for chcs_accession in samples_not_located_set:
            continue
            mrsn_id = self.context_dataframe['mrsnId'].loc[self.context_dataframe["chcsAccession"] == chcs_accession].to_list()

            # Try and get the user to provide a valid chcs entry
            try:
                new_chcs_accession = self.check_patients_collection_for_chcs_accession(chcs_accession, mrsn_id)
            except SampleNotFoundError as err:
                self.logger.log(str(err))
                continue

            self.logger.log("Querying the patients collection for corrected chcsAccessionNumber: {}".format(new_chcs_accession), verbosity=self.verbosity)
            query = list(self.db.patients.find({"chcsAccessionNumber": new_chcs_accession}))

            self.chcs_match_utility(new_chcs_accession, mrsn_id, query)

        # move the chcsAccessionNumber and raw_organism_id column in ast_results to first and second position
        chcs_col = self.ast_dataframe.pop('patientsCollectionId')
        raw_organism_col = self.ast_dataframe.pop('rawOrganismId')
        self.ast_dataframe.insert(0, 'patientsCollectionId', chcs_col)
        self.ast_dataframe.insert(1, 'rawOrganismId', raw_organism_col)

    def collect_patient_and_ast_for_patient_collection_ids(self):
        """
        bulk query for all samples in the context dataframe where patientsCollectionIds field is populated
        Returns a list of chcsAccession's that correspond with samples that were successfully located
        """
        self.logger.log_section_header("Retrieving Patient and AST data for samples with patientsCollectionIds", verbosity=self.verbosity)
        self.logger.log_explanation("Querying the patients collection for patient metadata and AST data for isolates that have patientCollectionIds. These values are assigned and stored in the surveillance_clusters collection of the bi-db (for samples identified in previous analyses) OR in the picklejar (for samples identified in interrupted iterations of the current run)", verbosity=self.verbosity)

        patient_collection_ids = self.context_dataframe["patientsCollectionId"].loc[self.context_dataframe["patientsCollectionId"].notna()].to_list()

        log_a_list(patient_collection_ids, self.logger)

        query = list(self.db.patients.find({"_id": {"$in": patient_collection_ids}}))

        # pass a copy of the query to add_patient_query_return_to_patients_and_ast_dataframes() to collect the patient metadata and ast results
        for doc in query:
            self.add_patient_query_return_to_patients_and_ast_dataframes(doc)

        # return a list of chcsAccession values that correspond to the successfully located docs
        located_entries = [doc["_id"] for doc in list(query)]
        located_samples = self.context_dataframe['mrsnId'].loc[self.context_dataframe["patientsCollectionId"].isin(located_entries)]

        self.logger.log("Located the following {} samples based on a valid patientsCollectionId:".format(str(len(located_samples))))
        log_a_list(located_samples, self.logger)

        return located_samples
    
    def preview_patient_and_ast(self, patient_documents):
        """
        sub function that prints a preview of each document in patient_documents
        """
        counter = 0
        for doc in patient_documents:

            doc["patientId"] = str(int(doc["patientId"]))

            counter +=1
            self.logger.log("\nPreview of document: {}".format(log.bold(str(counter))))
            self.logger.log("CHCS Accession: {}".format(doc["chcsAccessionNumber"]))
            self.logger.log("Patient ID: {}".format(doc["patientId"]))
            self.logger.log("Facility: {}".format(doc["verifiedFacilityName"]))
            try:
                self.logger.log("Collection Date: {}".format(doc["dateOfCulture"].strftime("%Y-%m-%d")))
            except AttributeError:  # collection date is misformatted
                self.logger.log("Collection Date: {}".format("COULD NOT PARSE"))
            self.logger.log("Raw Organism ID: {}".format(doc["rawOrganismId"]))
            self.logger.log("AST results:")
            try:
                for test in doc['testResults']:
                    self.logger.log("{}:\t{}".format(test["antibioticName"], test["sensitivity"]))
            except KeyError:
                self.logger.log("No AST results detected")
    
    def chcs_match_utility(self, chcs_accession, mrsn_id, query):
        """
        handle all of the possible matching cases for a given chcs_accession. The end result of any option is either a) appending the matching/selected
        result to the patient and ast DataFrames OR adding nothing (no metadata will be available for such samples)
        """

        # identify the 1+ matching documents in query
        matching_documents = [doc.copy() for doc in query if doc["chcsAccessionNumber"] == chcs_accession]

        # determine how many of the matching documents matches on the genus
        if len(mrsn_id) == 1:
            mrsn_id = mrsn_id[0]
            genus = self.context_dataframe['genus'].loc[self.context_dataframe["mrsnId"] == mrsn_id].to_list()[0]  # we can only have one genus in a cgMLST tree
        else:
            genus = self.context_dataframe['genus'].loc[self.context_dataframe["mrsnId"] == mrsn_id[0]].to_list()[0]  # we can only have one genus in a cgMLST tree
            self.logger.log("WARNING: CHCS: {} is associated with more than one mrsnId:\n{}".format(chcs_accession, "\n".join(mrsn_id)))

        # if the length of matching documents is 1, use this doc. If more than one sample, log a warning about deprecated samples
        if len(matching_documents) == 1:
            self.single_chcs_match(chcs_accession, mrsn_id, matching_documents[0])
            return

        matching_raw_organism_ids = [doc.copy() for doc in matching_documents if genus.upper() in doc["rawOrganismId"]]

        # if the length of matching_raw_organism_ids == 1
        if len(matching_raw_organism_ids) == 1:
            if type(mrsn_id) == str:  # if only one MRSN#, use this doc
                self.single_raw_organism_id_match(chcs_accession, mrsn_id, genus, matching_raw_organism_ids[0])
                return
            else:  # in the case of multiple MRSN#, we can't rule out one of the samples being a non-match, so we'll use two_mrsn_ids_one_chcs
                self.two_mrsn_id_one_chcs(chcs_accession, mrsn_id, matching_documents)
                return
    
        # if the length of matching_raw_organism_ids > 1, use the pick from list method to try and resolve the ambiguity
        if len(matching_raw_organism_ids) > 1:
            self.mutliple_raw_organism_id_matches(mrsn_id, matching_raw_organism_ids)
            return
        
        # if the length of matching_raw_organisms is 0, use the pick from list method to try and resolve the ambiguity
        if len(matching_raw_organism_ids) == 0:
            self.no_raw_organism_id_matches(mrsn_id, matching_documents)
            return 

    def single_chcs_match(self, chcs_accession, mrsn_id, matching_document):
        """
        handle the case where a chcs_accession matches to only a single entry in the patients collection
        """
        self.logger.log(log.underline("Located a single entry for CHCS {}".format(chcs_accession)))
    
        # update the patientsCollectionId field
        if type(mrsn_id) == list:
            self.logger.log("WARNING: One entry located in the patients collection but {} distinct mrsnId's, likely deprecated samples".format(str(len(mrsn_id))))
            for mrsn in mrsn_id:
                self.pickle_jar.write_a_pickle(mrsn, matching_document["_id"])
                self.update_patients_collection_id(mrsn, matching_document["_id"])
        else:
            self.pickle_jar.write_a_pickle(mrsn_id, matching_document["_id"])
            self.update_patients_collection_id(mrsn_id, matching_document["_id"])

        
        self.add_patient_query_return_to_patients_and_ast_dataframes(matching_document)

    def single_raw_organism_id_match(self, chcs_accession, mrsn_id, genus, matching_document):
        """
        handles the case where a chcs_accession AND genus match only a single entry in the patients collection for a 1 to 1 chcsAccession to mrsnId relationship
        """
        self.logger.log(log.underline("Located a single entry for CHCS {} that matches the WGS designated genus {}".format(chcs_accession, genus)))
        self.update_patients_collection_id(mrsn_id, matching_document["_id"])
        self.pickle_jar.write_a_pickle(mrsn_id, matching_document["_id"])
        self.add_patient_query_return_to_patients_and_ast_dataframes(matching_document)

    def two_mrsn_id_one_chcs(self, chcs_accession, mrsn_id, matching_documents):
        """
        handles the case where a chcs_accession matches to 2+ entries in the patients collection BUT only one is a genus match
        """
        self.logger.log("WARNING: multiple entries located in the patients collection with a single genus match and {} distinct mrsnId's. We will tackle each one separately".format(str(len(mrsn_id))))
        for mrsn in mrsn_id:
            self.logger.log(log.underline("There is one match between the WGS genus and raw organism IDs for {}:".format(mrsn)))
            self.preview_patient_and_ast(matching_documents)
            choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
            if choice:
                self.update_patients_collection_id(mrsn, choice["_id"])
                self.pickle_jar.write_a_pickle(mrsn, choice["_id"])
                self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
            else:
                self.logger.log("No patient metadata or AST will be available for this sample in the reporting")

    def mutliple_raw_organism_id_matches(self, mrsn_id, matching_documents):
        """
        handles the case where a chcs_accession AND genus match 2+ entries in the patients collection
        """
        if type(mrsn_id) == str:  # a single mrsnId was passed
            self.logger.log(log.underline("There are multiple matches between the WGS genus and raw organism IDs for {}:".format(mrsn_id)))
            self.preview_patient_and_ast(matching_documents)
            choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
            if choice:
                self.update_patients_collection_id(mrsn_id, choice["_id"])
                self.pickle_jar.write_a_pickle(mrsn_id, choice["_id"])
                self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
            else:
                self.logger.log("No patient metadata or AST will be available for this sample in the reporting")
        
        elif type(mrsn_id) == list: # a list of 2+ mrsnIds were passed
            self.logger.log("WARNING: multiple entries located in the patients collection and {} distinct mrsnId's. We will tackle each one separately".format(str(len(mrsn_id))))
            for mrsn in mrsn_id:
                self.logger.log(log.underline("There are multiple matches between the WGS genus and raw organism IDs for {}:".format(mrsn)))
                self.preview_patient_and_ast(matching_documents)
                choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
                if choice:
                    self.update_patients_collection_id(mrsn, choice["_id"])
                    self.pickle_jar.write_a_pickle(mrsn, choice["_id"])
                    self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
                else:
                    self.logger.log("No patient metadata or AST will be available for this sample in the reporting")

    def no_raw_organism_id_matches(self, mrsn_id, matching_documents):
        """
        handles the case where a chcs_accession matches to 2+ entries in the patients collection BUT none of them are a genus match
        """
        if type(mrsn_id) == str:  # a single mrsnId was passed
            self.logger.log(log.underline("There are NO matches between the WGS genus and raw organism IDs for {}:".format(mrsn_id)))
            self.preview_patient_and_ast(matching_documents)
            choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
            if choice:
                self.update_patients_collection_id(mrsn_id,choice["_id"])
                self.pickle_jar.write_a_pickle(mrsn_id, choice["_id"])
                self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
            else:
                self.logger.log("No patient metadata or AST will be available for this sample in the reporting")
        
        elif type(mrsn_id) == list: # a list of 2+ mrsnIds were passed
            self.logger.log("WARNING: multiple entries located in the patients collection and {} distinct mrsnId's. We will tackle each one separately".format(str(len(mrsn_id))))
            for mrsn in mrsn_id:
                self.logger.log(log.underline("There are NO matches between the WGS genus and raw organism IDs for {}:".format(mrsn)))
                self.preview_patient_and_ast(matching_documents)
                choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
                if choice:
                    self.update_patients_collection_id(mrsn, choice["_id"])
                    self.pickle_jar.write_a_pickle(mrsn, choice["_id"])
                    self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
                else:
                    self.logger.log("No patient metadata or AST will be available for this sample in the reporting")
    
    def add_patient_query_return_to_patients_and_ast_dataframes(self, document):
        """
        unpack and add the singular patient query result passed to the patient and ast dataframes
        """
        patient_metadata = []
        ast_results = []

        # to avoid potential issues with multiple samples matching to a document, work on a copy
        working_document = document.copy()
        
        patients_collection_id = working_document['_id']
        raw_organism_id = working_document['rawOrganismId']

        working_document["patientId"] = str(int(working_document["patientId"]))

        # extract the array of AST results to a new dictionary, append the chcs_number to each test object, and add to ast_results
        try:
            for test in working_document['testResults']:
                test['patientsCollectionId'] = patients_collection_id
                test['rawOrganismId'] = raw_organism_id
                ast_results.append(test)
                working_document.pop('testResults', None)
        except KeyError:
            pass
        
        # change the key "_id" to "patientsCollectionId"
        # working_document["patientsCollectionId"] = working_document.pop("_id")
        # working_document["patientsCollectionId"] = working_document["_id"]
        working_document.pop("_id",None)
        working_document["patientsCollectionId"] = patients_collection_id
        
        # drop the tests from the working_document and then append the remaining information to patient_metadata
        patient_metadata.append(working_document)

        # free my memory
        del working_document

        # convert the ast_results and patient_metadata into pandas DataFrames
        single_patient_dataframe = pd.DataFrame(patient_metadata)
        single_ast_dataframe = pd.DataFrame(ast_results)
        
        # concat these to the object attribute dataframes
        self.patient_dataframe = pd.concat([self.patient_dataframe, single_patient_dataframe], sort=True) 
        self.ast_dataframe = pd.concat([self.ast_dataframe, single_ast_dataframe], sort=True) 

    def update_patients_collection_id(self, mrsn_id, patients_collection_id):
        """
        update the patientsCollectionId field for the provided mrsn_id in the context DataFrame
        """
        self.context_dataframe.loc[self.context_dataframe["mrsnId"] == mrsn_id, "patientsCollectionId"] = patients_collection_id

    def check_patients_collection_for_chcs_accession(self, chcs_accession, mrsn_id):
        """
        sub function that solicits user edits to a chcs accession until the user gives up or 1+ documents in the patients collection matches the chcs accession
        This method is only invoked if the original CHCS accession results in no hits in the patients collection (inefficient)

        Returns a chcs_accession that is present in the patients collection OR raises a SampleNotFoundError if the user gives up
        """
        while self.db.patients.count_documents({"chcsAccessionNumber": chcs_accession}) == 0:
            self.logger.log("The current chcs accession: {} for sample(s): {} does not appear in the patients collection".format(chcs_accession, ' | '.join(mrsn_id)))
            if query_yes_no("Do you wish to rety with an alternative chcs accession?"):
                self.logger.log("Please enter the alternative CHCS entry now")
                chcs_accession = input().upper()
            else:
                # if the user can't or chooses not to correct the chcs we will not be able to collect patient metadata or AST for this sample
                raise SampleNotFoundError(chcs_accession, "patients collection", "No patient metadata or AST will be available for this sample in the reporting")

        return chcs_accession

    def filter_and_reformat_ast(self):
        """
        apply filters and reformatting to the AST results in the ast_dataframe
        """
        self.logger.log_section_header("Filtering and Reformatting the AST results", verbosity=self.verbosity)
        self.logger.log_explanation("Applying drug class filters to the AST results to retain only drugs of interest, then reformatting the results to one line per sample", verbosity=self.verbosity)

        # filter and reformat the ast_dataframe
        self.logger.log("Filtering AST results to remove entries that do not match any of the drug classes of interest:\n{}".format('\n'.join(self.drug_classes_of_interest)), verbosity=self.verbosity)
        
        self.reformatted_ast_dataframe = pd.DataFrame(columns=self.drug_classes_of_interest)

        ast_by_antibiotic_class = self.ast_dataframe.groupby(['patientsCollectionId','antibioticClass'])
        for name, group in ast_by_antibiotic_class:
            class_sensitivity = "unknown"

            if name[1] not in self.drug_classes_of_interest:
                continue

            #self.logger.log("Analyzing AST for chcsAccession {} in drug class: {}".format(name[0], name[1]), verbosity=3)
            for index in group.index:
                try:
                    drug_name = group.at[index, "antibioticName"]
                    sensitivity = group.at[index, "sensitivity"]
                except ValueError:
                    print(drug_name)
                    print(name)
                    sys.exit()

                if sensitivity == 'R':
                    #self.logger.log("AST indicates that this sample is {} to {}".format(sensitivity, drug_name), verbosity=3)
                    class_sensitivity = "R"
                elif sensitivity == 'I':
                    #self.logger.log("AST indicates that this sample is {} to {}".format(sensitivity, drug_name), verbosity=3)
                    if not class_sensitivity == "R":
                        class_sensitivity = "I"
                elif sensitivity == 'S':
                    #self.logger.log("AST indicates that this sample is {} to {}".format(sensitivity, drug_name), verbosity=3)
                    if not class_sensitivity in ["R", "I"]:
                        class_sensitivity = "S"

            self.reformatted_ast_dataframe.at[name[0], name[1]] = class_sensitivity


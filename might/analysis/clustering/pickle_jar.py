#!/usr/bin/env python
# encoding: utf-8

import pickle

#################################################################################################################################################
#
#  Class PickleJar
#
#   Manages the storage and retrieval of mrsnId to patientCollectionId relationships that are determined during the course of a run
#
#################################################################################################################################################
class PickleJar:

    def __init__(self, output_directory):

        self.output_directory = output_directory

        self.patient_collection_ids = None  # dictionary of user-curated patients collection _id's for ambiguous samples. It is of form mrsn_id: [patients_collection_id, original chcs_accession from sequenced_isolates collection]

        # pickles in the jar
        self.patient_collection_ids_pickle  = self.output_directory / "user-curated-patient-collection-ids.pickle"  # pickled dictionary of user-curated patients collection _id's for ambiguous samples

    def write_a_pickle(self, key, value):
        """
        rewrite the pickle to include new information.
        """
        if not self.patient_collection_ids:
            self.patient_collection_ids = {}

        self.patient_collection_ids[key] = value

        with open(self.patient_collection_ids_pickle, 'wb') as f:
            pickle.dump(self.patient_collection_ids, f)

    def read_a_pickle(self):
        """
        try to read in an existing pickle if the file is present
        """
        if self.patient_collection_ids_pickle.exists():
            with open(self.patient_collection_ids_pickle, 'rb') as f:
                self.patient_collection_ids = pickle.load(f)

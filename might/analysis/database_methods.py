#!/usr/bin/env python
# encoding: utf-8
import shutil
import subprocess
from datetime import datetime
from pathlib import Path

from might.common import log
from might.common.miscellaneous import file_check, quit_with_error


def database_check(logger, update=False):
    """
    Check to see if we can see a) the AMRFinderPlus database and b) the ariba reference derived from (a)
    """

    logger.log_section_header('AMR Database Validation')

    if update:
        logger.log_explanation(
            'Since --update was selected, we will update the AMRFinderPlus database to the most recent'
            ' version, and update the ARIBA reference directory accordingly')
    else:
        logger.log_explanation('In order to perform AMR gene identification, MIGHT requires that a copy of the NCBI '
                               'AMRFinderPlus database is installed on your machine in an accessible location. '
                               'Additionally, a reference directory prepared using the AMR_CDS file by ARIBA is required '
                               'to perform AMR gene identification from read files.')

    logger.log('Checking for the required databases...')

    # by default we will NOT remake the ARIBA reference unless we have to
    refresh = False

    # find the path to the amrfinder executable, as it installs its database relative to this location
    proc = subprocess.Popen(['which', 'amrfinder'], stdout=subprocess.PIPE)
    amrfinder_path = Path(str(proc.stdout.read().strip())[2:-1])
    amrfinder_database = amrfinder_path.parent.parent / 'share' / 'amrfinderplus' / 'data' / 'latest'
    amrfinder_database = Path(amrfinder_database).resolve()
    ariba_database = amrfinder_path.parent.parent / 'share' / 'andale_ariba_amrfinder_reference'

    # check to see if the AMRFinderPlus database is present. If the AMRFinderPlus database directory can't be located,
    # call the get_amrfinderplus_database() method to download it

    if not amrfinder_database.is_dir() or update:
        get_amrfinderplus_database(logger)
        # if we are downloading a new AMRFinder database we will also want to remake the ariba reference if it already
        # exists
        if ariba_database.is_dir():
            refresh = True
    else:
        logger.log('AMRFinderPlus database directory located')

    # check to see if ariba prepareref has been run on the AMRFinderPlus database. If the ariba reference directory
    # can't be located or needs to be remade, call the prepare_ariba_reference() method to construct it
    if not ariba_database.is_dir() or refresh:
        prepare_ariba_reference(ariba_database,
                                amrfinder_database,
                                refresh,
                                logger)

    else:
        logger.log('ARIBA reference directory located')

    if update:
        update_pubmlst(logger)

    return amrfinder_database, ariba_database


def get_amrfinderplus_database(logger):
    """
    Download the latest version of the AMRFinderPlus database from the ncbi via rsync
    """
    logger.log('Downloading AMRFinderPlus database from NCBI...')
    # utilize the AMRFinderPlus built in utility to download/update the amrfinder database
    subprocess.run(['amrfinder', '-u'], stderr=subprocess.STDOUT, stdout=subprocess.DEVNULL)
    logger.log('Download complete!\n')
    return


def prepare_ariba_reference(ariba_database, amrfinder_database, refresh, logger):
    """
    Prepare the ariba reference directory using the AMRFinderPlus database
    """
    logger.log('Preparing the ARIBA reference using the AMRFinderPlus database...\n')

    # if refresh is true we first need to remove the existing ariba reference directory
    if refresh:
        subprocess.run(['rm', '-r', ariba_database])

    # we require the 'AMR_CDS' file from the database to construct the ariba reference
    amrfinder_CDS_file = amrfinder_database / 'AMR_CDS'
    ariba_prepareref_cmd = 'ariba prepareref --all_coding yes -f ' + str(amrfinder_CDS_file) + ' ' + str(ariba_database)

    logger.log('Command: ' + log.bold(ariba_prepareref_cmd) + '\n')

    subprocess.run(ariba_prepareref_cmd, shell=True, stderr=subprocess.STDOUT, stdout=subprocess.DEVNULL)

    logger.log('ARIBA reference is ready for use!\n')

    return


def update_pubmlst(logger):
    """
    Use the mlst builtin tools "mlst-download_pub_mlst" and "mlst-make_blast_db" tools to update the pubMLST MLST
    definitions

    NOTE: Beginning in MIGHT v.1.2.6 I am including modified versions of the mlst updater scripts in the 
    resources directory that are compatible with the newest version of the pubMLST xml
    """
    logger.log_section_header("Now updating the pubMLST schemes and definitions")
    logger.log_explanation("Downloading the most up-to-date definitions from pubMLST and preparing them for use with mlst")


    # verify the locations of the scripts that we need to run to update the definitions
    proc = subprocess.Popen(['which', 'mlst'], stdout=subprocess.PIPE)
    scripts_dir = Path(str(proc.stdout.read().strip())[2:-1]).resolve().parent.parent / 'scripts'
    mlst_download_script = scripts_dir / 'mlst-download_pub_mlst'
    mlst_blast_script = scripts_dir / 'mlst-make_blast_db'
    db_dir = scripts_dir.parent / 'db'
    
    # new in v1.2.6: using packaged mlst database update scripts
    resources_dir = Path(__file__).parent.parent / 'resources'
    local_mlst_download_script = resources_dir / 'mlst-download_pub_mlst'
    local_mlst_blast_script = resources_dir / 'mlst-make_blast_db'
    logger.log("Verifying the modified helper scripts are available")
    logger.log("Modified mlst download script located at: {}".format(str(local_mlst_download_script)))
    logger.log("Modified mlst blast script located at: {}".format(str(local_mlst_blast_script)))
    logger.log("DB directory: {}".format(str(db_dir)))
    logger.log('\nReplacing the following:\nDownload script:\nOriginal: {}\nMIGHT version: {}\nBLAST script:\nOriginal: {}\nMIGHT version: {}'.format(
        str(mlst_download_script),
        str(local_mlst_download_script),
        str(mlst_blast_script),
        str(local_mlst_blast_script)
    ))

    # the update scripts are full of relative paths. Rather than fix those we will just replace the ones that are
    # installed with mlst
    mlst_download_script_bak = mlst_download_script.parent / (mlst_download_script.name + '.bak')
    mlst_blast_script_bak = mlst_blast_script.parent / (mlst_blast_script.name + '.bak')

    shutil.move(mlst_download_script, mlst_download_script_bak)
    shutil.move(mlst_blast_script, mlst_blast_script_bak)
    shutil.copy(local_mlst_download_script, mlst_download_script)
    shutil.copy(local_mlst_blast_script, mlst_blast_script)

    if not file_check(file_path=mlst_download_script, file_or_directory='file'):
        quit_with_error(logger, 'unable to verify the mlst download script at: {}'.format(str(mlst_download_script)))

    if not file_check(file_path=mlst_blast_script, file_or_directory='file'):
        quit_with_error(logger, 'unable to verify the mlst blast script at: {}'.format(str(mlst_blast_script)))

    # verify the location of the exiting pubMLST definitions
    mlst_database_current = db_dir / 'pubmlst'
    mlst_database_old = mlst_database_current.parent / 'pubmlst.old.{}'.format(datetime.now().strftime("%Y-%m-%d_"
                                                                                                       "%H-%M"))

    if not file_check(file_path=mlst_database_current, file_or_directory='directory'):
        rollback = False
        logger.log('WARNING: unable to verify the existing mlst database at: {}'.format(str(mlst_database_current)))
    else:
        rollback = True
        logger.log('Moving existing pubmlst database to {}'.format(str(mlst_database_old)))
        shutil.move(str(mlst_database_current), str(mlst_database_old))

    # run the downloader script
    logger.log('Downloading new pubMLST definitions')
    download_step_1 = subprocess.Popen(
        [str(mlst_download_script)],
        stderr=subprocess.DEVNULL,
        stdout=subprocess.PIPE
    )

    download_step_2 = subprocess.run(
        ['bash'],
        stdin=download_step_1.stdout
    )

    # verify that the database was downloaded and move it to where it belongs
    if download_step_2.returncode != 0:
        # Roll back the changes to the scripts
        subprocess.run(['rm', str(mlst_download_script)])
        subprocess.run(['rm', str(mlst_blast_script)])
        shutil.move(mlst_download_script_bak, mlst_download_script)
        shutil.move(mlst_blast_script_bak, mlst_blast_script)
        if rollback:
            shutil.move(str(mlst_database_old), str(mlst_database_current))
        quit_with_error(logger, 'The download process returned an error code: {}! Rolling back update!'.format(str(download_step_2.returncode)))

    if not file_check(file_path=mlst_database_current, file_or_directory='directory'):
        if rollback:
            shutil.move(mlst_database_old, mlst_database_current)
        quit_with_error(logger, 'unable to verify that the download completed! Rolling back update!')

    logger.log('download successful')

    logger.log('making blast database')
    make_blast = subprocess.run(
        [str(mlst_blast_script)],
        stderr=subprocess.DEVNULL,
        stdout=subprocess.DEVNULL
    )

    if make_blast.returncode !=0:
        subprocess.run(['rm', '-r', str(mlst_database_current)])
        # Roll back the changes to the scripts
        subprocess.run(['rm', str(mlst_download_script)])
        subprocess.run(['rm', str(mlst_blast_script)])
        shutil.move(mlst_download_script_bak, mlst_download_script)
        shutil.move(mlst_blast_script_bak, mlst_blast_script)
        if rollback:
            shutil.move(mlst_database_old, mlst_database_current)
        quit_with_error(logger, 'unable to verify that the download completed! Rolling back update!')

    # Roll back the changes to the scripts
    subprocess.run(['rm', str(mlst_download_script)])
    subprocess.run(['rm', str(mlst_blast_script)])
    shutil.move(mlst_download_script_bak, mlst_download_script)
    shutil.move(mlst_blast_script_bak, mlst_blast_script)

    logger.log('pubMLST definitions updated!')


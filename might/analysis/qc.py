#!/usr/bin/env python
# encoding: utf-8
import os
import subprocess
from datetime import datetime
from pathlib import Path

import pandas as pd

from might.analysis.sample import Sample
from might.common import log
from might.common.miscellaneous import file_check, quit_with_error
from might.common.errors import BadPathError


class Qc:

    def __init__(self, sample_list_file, output_directory, verbosity=2):

        self.sample_list_file = sample_list_file
        self.output_directory = output_directory
        self.log_directory = None
        self.verbosity = verbosity

        self.qc_dataframe = None
        
        # define the columns of the QC dataframe that we are searching for
        self.qc_column_list = [
            'Sample',
            'QC Call - Final',
            'Submitted Organism ID',
            'Genus',
            'Species',
            'WGS ID',
            'top hits',  # added in 1.2.6
            'WGS ID - Method',
            'DB Build',
            'Contaminated?',
            'Manually entered fields?',
            'QC Call - ID',  # manually entered
            'CHCS',
            'Patient ID',
            'Project Code(s)',  # pipe-delimitted field of applicable project codes
            'Project Name(s)',
            'Project Description(s)',
            'Facility Code',
            'Facility Name',
            'Collection Date',
            'Library Plate',
            'Sequencing Run',
            'Number of Contigs - Raw',
            'Number of Contigs Filtered for Length',
            'Total Length of Contigs Filtered for Length',
            'Number of Contigs Filtered for Low Coverage',
            'Total Length of Contigs Filtered for Low Coverage',
            'Number of Good Contigs',
            'Average Read Depth of Good Contigs',
            'Total Length of Good Contigs',
            'Median Insert Size',
            'Assembler',
            'QC Call - Assembly',  # manually entered
            'scheme',
            'mlst',
            'gene 1',
            'gene 2',
            'gene 3',
            'gene 4',
            'gene 5',
            'gene 6',
            'gene 7',
            'gene 8',
            'QC Call - MLST'  # manually entered
        ]

        self.sample_list = []

    def check_paths(self):
        """
        validate the required paths exist
        """
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            print(log.bold_yellow('WARNING: The output directory path ({}) is new, so we will make it now'.format(str(self.output_directory))))
            subprocess.run(
                [
                    'mkdir', '-p', str(self.output_directory)
                ]
            )

        # if a sample_list_file was passed, validate it
        if self.sample_list_file:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list file')
        
        # ensure that the output directory at least has an individual_sample_analyses directory for all but bcl2fastq and cluster
        self.individual_sample_analyses = self.output_directory / 'individual_sample_analyses'
        if not file_check(file_path=self.individual_sample_analyses, file_or_directory='directory'):
            subprocess.run(
                [
                    'mkdir', '-p', str(self.individual_sample_analyses)
                ]
            )
            
        # prepare the log directory
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])
        
        self.qc_summary = self.output_directory / "qc-summary.csv"
  
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        try:
            self.logger = log.Log(log_filename=self.log_directory / ('{}.qc.might.log'.format(datetime.now().strftime("%Y-%m-%d_%H-%M"))), stdout_verbosity_level=self.verbosity)
        except: # need to figure out the logger error codes
            pass

    def generate_sample_list(self):
        """
        generate the list of samples to assemble, UNLESS the list was passed by the user
        """
        # clean, validate, and read in the sample list IF IT WAS PASSED
        if self.sample_list_file is not None:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list')

            with open(self.sample_list_file, 'r') as f:
                self.sample_list = [line.strip() for line in f.readlines()]
        else:
            
            for entry in os.scandir(self.individual_sample_analyses):
                if file_check(file_path=Path(entry.path).expanduser().resolve(), file_or_directory='directory'):
                    if entry.name == 'individual_sample_analyses':
                        continue
                    sample_name = entry.name
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)

    def generate_qc_sample_objects(self):
        """
        generate sample objects for all of the samples in sample list and store it in self.sample_objects
        """
        self.sample_objects = []
        for sample in self.sample_list:
            if sample != 'Undetermined':  # Don't grab the recycling bin
                self.sample_objects.append(
                    QcSample(
                        sample,
                        self.qc_column_list, 
                        self.output_directory,
                        verbosity=self.verbosity
                        )
                    )

    def summarize_qc(self):
        """
        use get_sample_qc() to gather QC for all samples, then write it to self.qc_dataframe and output it to
        self.qc_summary
        """

        # create the empty dataframe that will house all of the QC data that we are able to locate
        self.qc_dataframe = pd.DataFrame(columns=self.qc_column_list)

        for sample in self.sample_objects:

            sample.get_sample_qc()

            self.qc_dataframe = pd.concat([self.qc_dataframe, sample.qc_dataframe], ignore_index=True, sort=False)

        self.qc_dataframe.to_csv(str(self.qc_summary), index=False)


class QcSample(Sample):
    """
    Class QcSample extends the Sample class, adding the configuration required for taking a single sequencing sample through the QC report generation procee
    """

    def __init__(self, name, qc_column_list, output_directory, verbosity=2):

        super().__init__(name, output_directory, verbosity=verbosity)

        self.qc_column_list = qc_column_list

        # key output files from other analysis steps
        self.kraken2_directory = self.individual_analysis_directory / 'kraken2'
        self.species_call_file = self.kraken2_directory / (self.name + '_species_call.csv')

        self.assembly_directory = self.individual_analysis_directory / 'assembly'
        self.assembly_QC_report = self.assembly_directory / (self.name + '.assembly_QC.csv')

        self.metadata_directory = self.individual_analysis_directory / 'metadata'
        self.metadata_file = self.metadata_directory / (self.name + '_metadata.csv')

        self.individual_mlst_directory = self.individual_analysis_directory / 'mlst'
        self.mlst_report = self.individual_mlst_directory / (self.name + '_mlst_report.csv') 

    def get_sample_qc(self):
        """
        iterate through the possible output to gather the sample QC and collect it in a dataframe
        """
        # create a new sample.qc_dataframe to hold all of the available QC data for a single sample
        self.qc_dataframe = pd.DataFrame(columns=self.qc_column_list)

        # populate the sample name from the sample object
        self.qc_dataframe.loc[0, 'Sample'] = self.name

        # attempt to gather Genus, Species, Contaminated?, and Manually entered fields? from the species caller
        # output
        species_column_list = [
            'Genus',
            'Species',
            'WGS ID',
            'top hits',
            'WGS ID - Method',
            'DB Build',
            'Contaminated?',
            'Manually entered fields?'
        ]
        if file_check(file_path=self.species_call_file, file_or_directory='file'):
            try:
                species_dataframe = pd.read_csv(str(self.species_call_file), usecols=species_column_list)
            except pd.errors.EmptyDataError:  # Case of empty csv
                self.logger.log('The individual species call file for sample {} is empty!'.format(self.name))
                species_dataframe = None
            except ValueError:  # Incorrectly formatted csv
                self.logger.log('The species call file for sample {} is incorrectly formatted!'.format(self.name))
                species_dataframe = None

            if species_dataframe is not None:
                for column in species_column_list:
                    self.qc_dataframe[column] = species_dataframe[column]
        else:
            self.logger.log("Unable to locate the species call file for {}: {}".format(self.name, str(self.species_call_file)))

        # attempt to gather Number of Contigs - Raw, Number of Contigs Filtered for Length,
        # Total Length of Contigs Filtered for Length, Number of Contigs Filtered for Low Coverage,
        # Total Length of Contigs Filtered for Low Coverage, Number of Good Contigs, Average Read Depth of Filtered Contigs,
        # Total Length of Filtered Contigs, Median Insert Size (bp), and Assembler from the assembly output
        assembly_column_list = [
            'Number of Contigs - Raw',
            'Number of Contigs Filtered for Length',
            'Total Length of Contigs Filtered for Length',
            'Number of Contigs Filtered for Low Coverage',
            'Total Length of Contigs Filtered for Low Coverage',
            'Number of Good Contigs',
            'Average Read Depth of Good Contigs',
            'Total Length of Good Contigs',
            'Median Insert Size',
            'Assembler'
        ]
        if file_check(file_path=self.assembly_QC_report, file_or_directory='file'):
            try:
                assembly_dataframe = pd.read_csv(str(self.assembly_QC_report), usecols=assembly_column_list)
            except pd.errors.EmptyDataError:  # Case of empty csv
                self.logger.log('The assembly QC file for sample {} is empty!'.format(self.name))
                assembly_dataframe = None
            except ValueError:  # Incorrectly formatted csv
                self.logger.log('The assembly QC file for sample {} is incorrectly formatted!'.format(self.name))
                assembly_dataframe = None

            if assembly_dataframe is not None:
                for column in assembly_column_list:
                    self.qc_dataframe[column] = assembly_dataframe[column]
        else:
            self.logger.log("Unable to locate the assembly QC file for {}: {}".format(self.name, str(self.assembly_QC_report)))

        # attempt to gather CHCS, Patient ID, Facility Code, Facility Name, Collection Date, Submitted Organsim ID,
        # Library Plate, and Sequencing Run from metadata output
        metadata_column_list = [
            'CHCS',
            'Patient ID',
            'Project Code(s)',  # pipe-delimitted field of applicable project codes
            'Project Name(s)',
            'Project Description(s)',
            'Facility Code',
            'Facility Name',
            'Collection Date',
            'Submitted Organism ID',
            'Library Plate',
            'Sequencing Run'
        ]
        if file_check(file_path=self.metadata_file, file_or_directory='file'):
            try:
                metadata_dataframe = pd.read_csv(str(self.metadata_file))
            except pd.errors.EmptyDataError:  # Case of empty csv
                self.logger.log('The metadata file for sample {} is empty!'.format(self.name))
                metadata_dataframe = None
            except ValueError:  # Incorrectly formatted csv
                self.logger.log('The metadata file for sample {} is incorrectly formatted!'.format(self.name))
                metadata_dataframe = None

            if metadata_dataframe is not None:
                for column in metadata_column_list:
                    if column in list(metadata_dataframe.columns):
                        self.qc_dataframe[column] = metadata_dataframe[column]
        else:
            self.logger.log("Unable to locate the metadata file for {}: {}".format(self.name, str(self.metadata_file)))

        # attempt to gather mlst, scheme, gene_1, gene_2, gene_3, gene_4, gene_5, gene_6, gene_7, and gene_8 from the mlst
        # output
        mlst_column_list = [
            'scheme',
            'mlst',
            'gene 1',
            'gene 2',
            'gene 3',
            'gene 4',
            'gene 5',
            'gene 6',
            'gene 7',
            'gene 8'
        ]
        if file_check(file_path=self.mlst_report, file_or_directory='file'):
            try:
                mlst_dataframe = pd.read_csv(str(self.mlst_report), sep=',', header=None, names=['source'] + mlst_column_list)
            except pd.errors.EmptyDataError:  # Case of empty csv
                self.logger.log('The individual mlst file for sample {} is empty!'.format(self.name))
                mlst_dataframe = None
            except ValueError:  # Incorrectly formatted csv
                self.logger.log('The mlst file for sample {} is incorrectly formatted!'.format(self.name))
                mlst_dataframe = None

            if mlst_dataframe is not None:
                for column in mlst_column_list:
                    self.qc_dataframe[column] = mlst_dataframe[column]
        else:
            self.logger.log("Unable to locate the MLST file for {}: {}".format(self.name, str(self.mlst_report)))

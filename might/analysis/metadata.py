#!/usr/bin/env python
# encoding: utf-8
import subprocess
import sys
import os
from datetime import datetime
from pathlib import Path

import pandas as pd

from might.analysis.sample import Sample
from might.common import log
from might.common.miscellaneous import file_check, quit_with_error
from might.common.errors import BadPathError, InvalidInputError, SampleNotFoundError, DuplicateEntriesFoundError


class Metadata:

    def __init__(self, output_directory, metadata_file, sample_list_file=None, force=False, verbosity=2):

        self.output_directory = output_directory
        self.metadata_file = metadata_file
        self.sample_list_file = sample_list_file
        self.force = force
        self.verbosity = verbosity

        self.sample_list = []

        # establish the list of expected columns in a run sheet
        self.input_column_list = [
            'MRSN #',
            'CHCS',
            'Description',
            'Species',
            'Lib Plate',
            'Project'
        ]

        # establish the list of columns in the output file
        self.output_column_list = [
            'MRSN #',
            'CHCS',
            'Submitted Organism ID',
            'Library Plate',
            'Project Name(s)'
            'Project Description(s)',
        ]

    def check_paths(self):
        """
        validate the required paths exist
        """
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            print(log.bold_yellow('WARNING: The output directory path ({}) is new, so we will make it now'.format(str(self.output_directory))))
            subprocess.run(
                [
                    'mkdir', '-p', str(self.output_directory)
                ]
            )

        # if a sample_list_file was passed, validate it
        if self.sample_list_file:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list file')
        
        # validate the metadata file path
        if self.metadata_file:
            self.metadata_file = Path(self.metadata_file).expanduser().resolve()
            if not file_check(file_path=self.metadata_file, file_or_directory='file'):
                raise BadPathError(self.metadata_file, 'metadata file')
        
        # ensure that the output directory at least has an individual_sample_analyses directory for all but bcl2fastq and cluster
        self.individual_sample_analyses = self.output_directory / 'individual_sample_analyses'
        if not file_check(file_path=self.individual_sample_analyses, file_or_directory='directory'):
            subprocess.run(
                [
                    'mkdir', '-p', str(self.individual_sample_analyses)
                ]
            )
            
        # prepare the log directory
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])
        
        # set the output file path
        self.metadata_summary = self.output_directory / 'metadata_summary.might.csv'
  
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        try:
            self.logger = log.Log(log_filename=self.log_directory / ('{}.metadata.might.log'.format(datetime.now().strftime("%Y-%m-%d_%H-%M"))), stdout_verbosity_level=self.verbosity)
        except: # need to figure out the logger error codes
            pass

    def metadata_file_to_dataframe(self):
        """
        attempt to import the metadata file into a pandas dataframe
        """
        self.logger.log_section_header("Reading in the metadata source file")
        self.logger.log_explanation("Attempting to parse the metadata file that was provided by the user: {}".format(str(self.metadata_file)))

        try:
            self.metadata_dataframe = pd.read_excel(self.metadata_file, skiprows=10, header=0, usecols=self.input_column_list, engine='openpyxl')
        except pd.errors.EmptyDataError:  # Case of empty csv
            raise InvalidInputError(['metadata dataframe'],'The passed metadata file is empty!')
        except ValueError:  # Incorrectly formatted csv
            raise InvalidInputError(['metadata dataframe'],'The metadata file is incorrectly formatted!')
        
        # convert the import column names to the ones we will be ultimately writing to the QC spreadsheet
        # establish a list of metadata columns to capture
        column_conversion_key = {
            'Project': 'Project Name(s)',
            'Description': 'Project Description(s)',
            'Species': 'Submitted Organism ID',
            'Lib Plate': 'Library Plate'
        }
        self.metadata_dataframe["MRSN #"] = self.metadata_dataframe["MRSN #"].map(str)
        self.metadata_dataframe.rename(columns=column_conversion_key, inplace=True)

    def generate_sample_list(self):
        """
        generate the list of samples to assemble, UNLESS the list was passed by the user
        """
        # clean, validate, and read in the sample list IF IT WAS PASSED
        if self.sample_list_file is not None:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list')

            with open(self.sample_list_file, 'r') as f:
                self.sample_list = [line.strip() for line in f.readlines()]
        else:
            for entry in os.scandir(self.individual_sample_analyses):
                if file_check(file_path=Path(entry.path).expanduser().resolve(), file_or_directory='directory'):
                    if entry.name == 'individual_sample_analyses':
                        continue
                    sample_name = entry.name
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)

    def generate_metadata_sample_objects(self):
        """
        generate sample objects for all of the samples in sample list and store it in self.sample_objects
        """
        self.sample_objects = []
        for sample in self.sample_list:
            self.sample_objects.append(
                MetaDataSample(
                    sample,
                    self.metadata_dataframe, 
                    self.output_directory,
                    force=self.force,
                    verbosity=self.verbosity
                    )
                )
            
    def attempt_individual_metadata_import(self):
        """
        iterate through sample_objects to get the individual metadata reports
        """
        self.logger.log_section_header("Generating the individual metadata report files")
        self.logger.log_explanation("For each sample in the individual_analyses_directory MIGHT will now attempt to extract the corresponding metadata from the user provided file")

        for sample in self.sample_objects:
            try:
                sample.get_metadata()
                sample.import_successful = True
            except InvalidInputError as err:
                self.logger.log(str(err))
                continue
            except SampleNotFoundError as err:
                self.logger.log(str(err))
                continue
            except DuplicateEntriesFoundError as err:
                self.logger.log(str(err))
                continue
            except BadPathError as err:
                self.logger.log(str(err))
                continue
    
    def summarize_metadata(self):
        """
        Generate a summary of all of the metadata that was successfully imported
        """
        self.logger.log_section_header("Generating the summary metadata report file")
        self.logger.log_explanation("Compiling all of the individual metadata files from this run")

        # create the empty dataframe that will house all of the metadata that we are able to locate
        self.metadata_summary_dataframe = pd.DataFrame(columns=self.output_column_list)

        for sample in self.sample_objects:

            if sample.import_successful and sample.sample_metadata is not None:

                self.metadata_summary_dataframe = pd.concat([self.metadata_summary_dataframe, sample.sample_metadata], ignore_index=True, sort=False)

        self.metadata_summary_dataframe.to_csv(str(self.metadata_summary), index=False)


class MetaDataSample(Sample):
    """
    Class MetaDataSample extends the Sample class, adding the configuration required for importing metadata for a single sequencing sample
    """

    def __init__(self, name, metadata_dataframe, output_directory, force=False, verbosity=2):

        super().__init__(name, output_directory, verbosity=verbosity)

        self.metadata_directory = self.individual_analysis_directory / 'metadata'
        self.metadata_file = self.metadata_directory / ("{}_metadata.csv".format(self.name))
        self.metadata_dataframe = metadata_dataframe
        self.force = force

        self.import_successful = False
        self.sample_metadata = None

        self.log_filename=self.log_directory / ('{}.metadata.might.log'.format(self.name)) 

        # initialize the logger for this sample
        self.initialize_logger(self.log_filename)

        # if force is being invoked, remove any output that may exist
        if self.force:
            if file_check(file_path=self.metadata_directory, file_or_directory='directory'):
                subprocess.run(['rm', '-r', str(self.metadata_directory)])

    def prepare_metadata_directory(self):
        """

        """
        if not self.metadata_directory.exists():
            subprocess.run(["mkdir","-p",str(self.metadata_directory)])

    def get_metadata(self):
        """ 
        Attempt to extract the metadata from the metadata_dataframe
        """
        self.logger.log_section_header("Extracting metadata for {} from the metadata file".format(self.name))
        self.logger.log_explanation("Determining what metadata if any is available for this sample from the metadata file supplied")
        
        self.prepare_metadata_directory()

        # determine if this sample has an entry in the metadata_dataframe, either as is or without the MRSN prefix
        if not self.name in self.metadata_dataframe['MRSN #'].to_list():
            self.logger.log("Unable to locate metadata for sample {}, will retry without the MRSN prefix".format(self.name))
            sample_sans_prefix = self.name[4:]
            if not sample_sans_prefix in self.metadata_dataframe['MRSN #'].to_list():
                raise SampleNotFoundError(self.name, 'metadata dataframe', '')
            else:
                self.sample_metadata = self.metadata_dataframe.loc[self.metadata_dataframe['MRSN #'] == sample_sans_prefix].copy()
                self.sample_metadata.loc[:,'MRSN #'] = self.name
        else:
            self.sample_metadata = self.metadata_dataframe.loc[self.metadata_dataframe['MRSN #'] == self.name].copy()

        # make sure that there is only one line in sample_metadata, otherwise there is a duplicate entry in the metadata sheet
        # for this sample, which is bad
        if len(list(self.sample_metadata.index.tolist())) > 1:
            raise DuplicateEntriesFoundError(self.name, 'metadata dataframe', 'Only one metadata entry for sample is acceptable!')

        self.logger.log("Located a unique metadata entry for sample {}".format(self.name))
        
        # write the sample data to the individual metadata file
        self.sample_metadata.to_csv(self.metadata_file, index=False)

        # verify that the individual metadata file was generated
        if not file_check(file_path=self.metadata_file, file_or_directory='file'):
            raise BadPathError(self.metadata_file, 'individual metadata file', 'Pandas failed to generate the individual metadata file for sample {}'.format(self.name))        

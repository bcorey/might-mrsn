#!/usr/bin/env python
# encoding: utf-8
import os
import subprocess
import sys
import time
import math
from datetime import datetime
from pathlib import Path
from fnmatch import fnmatch

import pandas as pd
from Bio import SeqIO
import multiprocessing as mp

from might.analysis.database_methods import database_check
from might.common import log
from might.analysis.sample import Sample
from might.common.miscellaneous import file_check, quit_with_error, check_for_dependency, compression_handler, \
    parallel_compression_handler, round_down
from might.common.errors import BadPathError, MissingPathError, CheckpointFileInconsistentError


class Amr:

    def __init__(self, output_directory, run, sample_list_file=None, analysis_type=None, reads_directory=None, assembly_directory=None,
                 keep=2, force=False, cores=1, cores_per_sample=1, verbosity=2, no_clean=False, summary_only=False):

        self.sample_list_file = sample_list_file
        self.output_directory = output_directory
        self.run = run.upper()
        self.analysis_type = analysis_type
        self.reads_directory = reads_directory
        self.assembly_directory = assembly_directory
        self.keep = keep
        self.force = force
        self.cores = cores
        self.cores_per_sample = cores_per_sample
        self.verbosity = verbosity
        self.no_clean = no_clean
        self.summary_only = summary_only

        self.sample_list = []

        # resources
        self.amrfinder_database = None
        self.ariba_database = None

        self.tiers_and_families_file = Path(__file__).parent.parent / 'resources' / 'amr_tiers_and_families.xlsx'
        
    def check_paths(self):
        """
        validate the required paths exist
        """
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            print(log.bold_yellow('WARNING: The output directory path ({}) is new, so we will make it now'.format(str(self.output_directory))))
            subprocess.run(
                [
                    'mkdir', '-p', str(self.output_directory)
                ]
            )

        # ensure that the output directory at least has an individual_sample_analyses directory
        self.individual_sample_analyses = self.output_directory / 'individual_sample_analyses'
        if not file_check(file_path=self.individual_sample_analyses, file_or_directory='directory'):
            subprocess.run(
                [
                    'mkdir', '-p', str(self.individual_sample_analyses)
                ]
            )
        
        # clean and validate the reads and assembly directories IF THEY WERE PASSED
        if self.reads_directory:
            self.reads_directory = Path(self.reads_directory).expanduser().resolve()
            if not file_check(file_path=self.reads_directory, file_or_directory='directory'):
                raise BadPathError(self.reads_directory, 'reads directory')
        
        if self.assembly_directory:
            self.assembly_directory = Path(self.assembly_directory).expanduser().resolve()
            if not file_check(file_path=self.assembly_directory, file_or_directory='directory'):
                raise BadPathError(self.assembly_directory, 'assembly directory')

        # if a sample_list_file was passed, validate it
        if self.sample_list_file:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list file')
            
        # prepare the log directory
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', str(self.log_directory)])

        self.summary_file = self.output_directory / '{}.amr_full_summary.xlsx'.format(self.run)

    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        try:
            self.logger = log.Log(
                log_filename=self.log_directory / 'amr.might.log',
                stdout_verbosity_level=self.verbosity
            )
        except: # need to figure out the logger error codes
            pass

    def dependency_check(self):
        """
        Ensure that the primary dependencies for this analysis are available on the path
        """
        try:
            assert check_for_dependency('ariba')
        except AssertionError:
            quit_with_error(self.logger, 'ARIBA could NOT be located')

        try:
            assert check_for_dependency('amrfinder')
        except AssertionError:
            quit_with_error(self.logger, 'AMRFinder could NOT be located')

    def validate_amr_resources(self):
        """
        use the database_check method to validate that the amrfinder database is installed and a formatted version compatible with
        ARIBA has been created. Verify that the copy of tiers_and_families.xlsx that comes packaged with MIGHT is visible
        """
        self.logger.log_section_header("Validating the AMR prediction resources")
        self.logger.log_explanation("Making sure that the AMRfinder database (as well as the ARIBA-prepared version) and the tiers and families file are visible")
        
        self.amrfinder_database, self.ariba_database = database_check(self.logger)

        try:
            assert file_check(file_path=self.tiers_and_families_file, file_or_directory='file')
        except AssertionError:
            raise BadPathError(self.tiers_and_families_file, 'Tiers and Families file')
        
    def generate_sample_list(self):
        """
        generate the list of samples for which amr should be predicted, UNLESS the list was passed by the user
        """
        # clean, validate, and read in the sample list IF IT WAS PASSED
        if self.sample_list_file is not None:
            self.sample_list_file = Path(self.sample_list_file).expanduser().resolve()
            if not file_check(file_path=self.sample_list_file, file_or_directory='file'):
                raise BadPathError(self.sample_list_file, 'sample list')

            with open(self.sample_list_file, 'r') as f:
                self.sample_list = [line.strip() for line in f.readlines()]
        else:
            
            for entry in os.scandir(self.individual_sample_analyses):
                if file_check(file_path=Path(entry.path).expanduser().resolve(), file_or_directory='directory'):
                    if entry.name == 'individual_sample_analyses':
                        continue
                    sample_name = entry.name
                    if sample_name not in self.sample_list:
                        self.sample_list.append(sample_name)
            if self.reads_directory:
                for entry in os.scandir(self.reads_directory):
                    if fnmatch(entry.name, '*_R*.fastq*'):
                        sample_name = entry.name.split('_')[0]
                        if sample_name not in self.sample_list:
                            self.sample_list.append(sample_name)
                    if fnmatch(entry.name, '*.R*.fastq*'):
                        sample_name = entry.name.split('.')[0]
                        if sample_name not in self.sample_list:
                            self.sample_list.append(sample_name)
            if self.assembly_directory:
                for entry in os.scandir(self.assembly_directory):
                    if fnmatch(entry.name, '*_*.fasta*') or fnmatch(entry.name, '*_*.fna*'):
                        sample_name = entry.name.split('_')[0]
                        if sample_name not in self.sample_list:
                            self.sample_list.append(sample_name)
                    elif fnmatch(entry.name, '*.*fasta*') or fnmatch(entry.name, '*.*fna*'):
                        sample_name = entry.name.split('.')[0]
                        if sample_name not in self.sample_list:
                            self.sample_list.append(sample_name)

    def generate_amr_sample_objects(self):
        """
        generate sample objects for all of the samples in sample list and store it in self.sample_objects
        """
        self.sample_objects = []
        for sample in self.sample_list:
            if sample != 'Undetermined' and not sample.lower().startswith('blank'):  # No point in assembling the recycling bin of read files
                self.sample_objects.append(
                    AmrSample(
                        sample, 
                        self.output_directory,
                        self.run,
                        analysis_type=self.analysis_type,
                        amrfinder_database=self.amrfinder_database,
                        ariba_database=self.ariba_database,
                        force = self.force, 
                        reads_directory=self.reads_directory,
                        assembly_directory=self.assembly_directory,
                        verbosity=self.verbosity,
                        cores=self.cores_per_sample,
                        )
                    )

        # we will only attempt to validate whether or not samples can undergo analysis if analysis is being requested
        if not self.summary_only:

            # iterate throught the sample objects in self.sample_objects. Partition samples into one of two lists:
            # analysis_required and analysis_complete based on the return value of <sample>.configure_amr_analysis
            self.analysis_required = []
            self.analysis_complete = []
            self.analysis_impossible = []

            for sample in self.sample_objects:

                sample.check_amr_status()
                if sample.amr_status == 'complete':
                    self.analysis_complete.append(sample)
                    continue

                try:
                    sample.validate_amr_analysis()  # the sample is reporting that it is capable of undergoing amr analysis
                    self.analysis_required.append(sample)
                except MissingPathError as err:
                    self.logger.log(str(err))
                    self.analysis_impossible.append(sample)
                except BadPathError as err:
                    self.logger.log(str(err))
                    self.analysis_impossible.append(sample)

    def preview_of_slated_analyses(self):
        """
        log a summary of the analyses to be performed
        """
        self.logger.log_section_header('Summary of AMR analyses to be performed')
        self.logger.log("The selected analysis type is: {}".format(self.analysis_type))
        self.logger.log('\n {} samples were detected for analysis'.format(str(len(self.sample_objects))))
        self.logger.log('\n {} samples have already undergone the requested amr analysis type but will be summarized again'.format(str(len(self.analysis_complete))))
        self.logger.log('\n The following {} samples will undergo amr analysis:'.format(str(len(self.analysis_required))))
        for sample in self.analysis_required:
            self.logger.log(str(sample.name))
        self.logger.log('\n The following {} samples were flagged for missing a requisite file for this analysis type:'.format(str(len(self.analysis_impossible))))
        for sample in self.analysis_impossible:
            self.logger.log(str(sample.name))

    def threaded_amr_analyses(self):
        """
        run threaded_amr() for each sample in self.analysis_required using multiprocessing
        """
        self.logger.log_section_header('Now starting threaded amr analyses')
        self.logger.log_explanation("Running threaded AMR prediction using python multiprocessing")

        if len(self.analysis_required) + len(self.analysis_complete) == 0:
            self.logger.log('\nAMR threaded analyses are now complete!')
            return

        pool_size = math.floor(self.cores / self.cores_per_sample)
        self.logger.log("Running {} processes ({} cores at {} cores per sample)".format(str(pool_size), str(self.cores), str(self.cores_per_sample)))

        p = mp.Pool(pool_size)
        p.map(AmrSample.amr_threaded_process, self.analysis_required + self.analysis_complete)
        p.close()
        p.join()

        self.logger.log('\nAMR threaded analyses are now complete!')

    def print_summary_for_summary_only(self):
        """
        print a quick summary to the terminal of samples that will be included in the summary-only method
        """
        self.logger.log_section_header("AMR analyses to be summarized")
        self.logger.log("\n" + log.underline("Samples that have undergone analysis:\n"))
        failed_samples = []
        for sample in self.analysis_complete:
            if not file_check(file_path=sample.amr_output_filtered, file_or_directory='file'):
                failed_samples.append(sample)
                self.logger.log(log.red("[WARNING] Sample {} "))
    
    def run_pbs(self):
        """
        create and submit a pbs batch script that will perform this analysis. This gets around the network stability
        issues that can cause long runnning jobs to be terminated
        """

        self.logger.log_section_header('Submitting amr job(s) to pbs server')
        self.logger.log_explanation('Since the -pbs flag was invoked, we will submit the job(s) to the pbs batch server for'
                                    'processing. Please note that the current script will terminate once the job has '
                                    'been submitted, so you will need to monitor qstat to evaluate when the job has '
                                    'completed\n')

        self.logger.log('Generating the pbs batch script(s)\n')

        for sample in self.analysis_required:

            # set the pbs scipt file path
            sample.pbs_script_file = sample.individual_amr_directory / (sample.name + '.amr.pbs')

            sample.logger.log('Batch script file at: {}\n'.format(str(sample.pbs_script_file)))

            # verify that the pbs file does NOT already exist. If it does, delete it
            if file_check(file_path=sample.pbs_script_file, file_or_directory='file'):
                sample.logger.log('An existing pbs batch script file was located and will be overwritten\n')
                subprocess.run(['rm', str(sample.pbs_script_file)])

            # generate a tmp file that will act as a sample-list. This will allow us to run kraken2 for a single sample
            sample.sample_list = sample.individual_amr_directory / (sample.name + '.sample_list.tmp')

            # verify that the sample list tmp file does NOT already exist. If it does, delete it
            if file_check(file_path=sample.sample_list, file_or_directory='file'):
                sample.logger.log('An existing sample list tmp file was located and will be overwritten\n')
                subprocess.run(['rm', str(sample.sample_list)])

            # write the sample name to the tmp file
            with open(str(sample.sample_list), 'w') as sample_list:
                sample_list.write(sample.name)

            # prepare the pbs file
            with open(str(sample.pbs_script_file), 'w') as pbs:

                # add the shebang line
                pbs.write('#!/bin/bash\n')

                # write the nodes, cores, and walltime parameters
                pbs.write('#PBS -l nodes=1:ppn=1\n')
                pbs.write('#PBS -l walltime=01:00:00\n')

                output_path = sample.individual_amr_directory / (sample.name + '.amr.stdout')
                error_path = sample.individual_amr_directory / (sample.name + '.amr.stderr')

                # write the stdout and stderr paths
                pbs.write('#PBS -o {}\n'.format(str(output_path)))
                pbs.write('#PBS -e {}\n'.format(str(error_path)))

                # teach pbs server about conda
                pbs.write('source /data/MRSN_Research/mrsn/miniconda/etc/profile.d/conda.sh\n')

                # activate might
                pbs.write('conda activate might\n')

                # write the actual might command to be run as a batch job. The exact form will depend on the self.analysis_type value
                if self.analysis_type == 'combination':
                    if self.no_clean:
                        pbs.write('might amr {} --reads {} --assembly {} --sample-list {} --no-clean'.format(self.output_directory, self.reads_directory, self.assembly_directory, sample.sample_list))
                    else:
                        pbs.write('might amr {} --reads {} --assembly {} --sample-list {}'.format(self.output_directory, self.reads_directory, self.assembly_directory, sample.sample_list))
                elif self.analysis_type == 'reads':
                    if self.no_clean:
                        pbs.write('might amr {} --reads {} --analysis-type reads --sample-list {} --no-clean'.format(self.output_directory, self.reads_directory, sample.sample_list))
                    else:
                        pbs.write('might amr {} --reads {} --analysis-type reads --sample-list {}'.format(self.output_directory, self.reads_directory, sample.sample_list))
                elif self.analysis_type == 'assembly':
                    if self.no_clean:
                        pbs.write('might amr {} --assembly {} --analysis-type assembly --sample-list {} --no-clean'.format(self.output_directory, self.assembly_directory, sample.sample_list))
                    else:
                        pbs.write('might amr {} --assembly {} --analysis-type assembly --sample-list {}'.format(self.output_directory, self.assembly_directory, sample.sample_list))
                else:
                    quit_with_error(sample.logger, 'Something is wrong! No analysis type is available for writing pbs jobs!')

            # submit the pbs job to the batch server
            sample.logger.log('Submitting the batch script file to the pbs batch server for sample: {}\n'.format(sample.name))

            subprocess.run(
                [
                    'qsub',
                    '-q',
                    'batch',
                    str(sample.pbs_script_file)
                ]
            )

            time.sleep(1)

        self.logger.log('The job(s) have been submitted. Please monitor qstat for job completion!')
        sys.exit()

    def summarize_all_amr(self):
        """
        generate an amr summary file for all samples in the output directory
        """
        self.logger.log_section_header('Collecting the individual amr files for summary')
        files_for_summary = []
        missing_files = []
        for sample in self.sample_objects:
            if sample.amr_summary_file.exists():
                files_for_summary.append(sample.amr_summary_file)
            else:
                missing_files.append(sample)

        self.logger.log("Located following summary files:")
        for entry in files_for_summary:
            self.logger.log(str(entry))
        self.logger.log("\nThe following samples do not have an amr summary file:")
        for entry in missing_files:
            self.logger.log(entry.name)

        self.logger.log_section_header('Now summarizing AMR files')
        # the amr summary file is split across 8 sheets. We will read in each sheet and write it out to a summary sheet
        # of the same format in the output file

        sheet_dict = {
            "tier 1": [],
            "tier 2": [],
            "tier 3": [],
            "carbapenemases": [],
            "pan aminoglycosides": [],
            "mcr": [],
            "vancomycin": [],
            "full summary": [],
        }

        for input_file in files_for_summary:

            for sheet in sheet_dict.keys():
                # read the sheet into a DataFrame
                if file_check(file_path=input_file, file_or_directory='file'):
                    try:
                        single_dataframe = pd.read_excel(str(input_file), sheet_name=sheet, engine='openpyxl')
                    except pd.errors.EmptyDataError:  # Case of empty csv
                        self.logger.log('This amr summary file ({}) is empty!'.format(str(input_file)))
                        continue
                    except ValueError:  # Incorrectly formatted csv
                        self.logger.log('This amr file ({}) is incorrectly formatted!'.format(str(input_file)))
                        continue

                sheet_dict[sheet].append(single_dataframe)

        # concatenate all of the dataframes for each sheet
        for key in sheet_dict.keys():
            sheet_dict[key] = pd.concat(sheet_dict[key])

        # Set the output column order for all of the entries in the full amr summary as a list full_summary_column_order
        full_summary_column_order = [
            'Sample', 'Sequencing Run', 'Source', 'Curation level', 'Hit Qualifier', 'Fusion Number', 'Fusion Count', 'Allele', 'MRSN SuperFamily', 'Extended annotation', 'Expected Phenotype', 'Method',
            '% Coverage of reference sequence', '% Identity to reference sequence', 'Start', 'Stop', 'Strand', 'Contig id',
            'Contig length', 'Accession of closest sequence', 'Element type', 'Element subtype', 'Tier 1', 'Tier 2', 'Tier 3'
        ]

        with pd.ExcelWriter(str(self.summary_file), mode='w') as writer: # pylint: disable=abstract-class-instantiated
            for key in sheet_dict.keys():
                sheet_dict[key].to_excel(writer, sheet_name=key, index=False, columns=full_summary_column_order, engine='openpyxl')


class AmrSample(Sample):
    """
    Class AmrSample extends the Sample class, adding the configuration required for taking a single sequencing sample through AMR detection using ARIBA and AMRFinderPlus
    """

    def __init__(self, name, output_directory, run, analysis_type, amrfinder_database, ariba_database, reads_directory=None, assembly_directory=None, force=False, verbosity=2, cores=1):

        super().__init__(name, output_directory, reads_directory=reads_directory, assembly_directory=assembly_directory, verbosity=verbosity)

        self.run = run
        self.cores = cores
        
        # database paths
        self.amrfinder_database = amrfinder_database
        self.ariba_database = ariba_database
        
        # analysis type governs what source material to use for AMR prediction (reads, asembly, combination)
        self.analysis_type = analysis_type

        # set the key amr output paths
        self.individual_amr_directory = self.individual_analysis_directory / 'amr'
        self.ariba_output_directory = self.individual_amr_directory / 'ARIBA_output'
        self.ariba_assembled_genes = self.ariba_output_directory / 'assembled_genes.fa.gz'
        self.ariba_assemblies_compressed = self.ariba_output_directory / 'assemblies.fa.gz'
        self.ariba_assemblies = self.ariba_output_directory / 'assemblies.fa'
        self.amrfinder_output_from_ariba = self.individual_amr_directory / 'amrfinder_output_from_ariba.txt'
        self.amrfinder_output_from_contigs = self.individual_amr_directory / 'amrfinder_output_from_contigs.txt'
        self.amr_output_raw_file = self.individual_amr_directory / (self.name + '.amr_output_raw.csv')
        self.amr_output_filtered_file = self.individual_amr_directory / (self.name + '.amr_output_filtered.csv')
        self.amr_output_curated_file = self.individual_amr_directory / (self.name + '.amr_output_curated.csv')
        self.amr_summary_file = self.individual_amr_directory / '{}.{}.amr_full_summary.xlsx'.format(self.name, self.run)

        self.tiers_and_families_file = Path(__file__).parent.parent / 'resources' / 'amr_tiers_and_families.xlsx'
            
        self.force = force
        self.log_filename=self.log_directory / '{}.{}.amr.might.log'.format(self.name, self.analysis_type)

        self.amr_status = None

        # initialize the logger for this sample
        self.initialize_logger(self.log_filename)

        # initialize the CheckPoint for this sample
        self.initialize_sample_checkpoint_file('amr', self.force)

        # if force is being invoked, remove any output that may exist
        if self.force:
            if file_check(file_path=self.individual_amr_directory, file_or_directory='directory'):
                subprocess.run(['rm', '-r', str(self.individual_amr_directory)])
        
    def check_amr_status(self):
        """
        return the current status of the amr analysis for this sample ('new', 'incomplete', 'complete') based on the checkpoint file and the
        analysis type
        """
        self.checkpoint.read_checkpoint_file()

        if self.analysis_type == 'reads':
            if self.checkpoint.checkpoint_dict['amrfinder for reads'] == 'complete' and self.checkpoint.checkpoint_dict['filtered summary'] == 'complete':
                if file_check(file_path=self.amr_output_filtered_file, file_or_directory='file') and file_check(file_path=self.amrfinder_output_from_ariba, file_or_directory='file'):
                    self.logger.log('Sample {} is marked complete in its amr checkpoint file for reads-based analysis, and the key output was located'
                            ', so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                    self.amr_status = 'complete'
                else:
                    raise CheckpointFileInconsistentError(self.checkpoint.checkpoint_file, 'amrfinder for reads', [str(self.amr_output_filtered_file), str(self.amrfinder_output_from_ariba)])
        
        if self.analysis_type == 'assembly':
            if self.checkpoint.checkpoint_dict['amrfinder for assembly'] == 'complete' and self.checkpoint.checkpoint_dict['filtered summary'] == 'complete':
                if file_check(file_path=self.amr_output_filtered_file, file_or_directory='file') and file_check(file_path=self.amrfinder_output_from_contigs, file_or_directory='file'):
                    self.logger.log('Sample {} is marked complete in its amr checkpoint file for assembly-based analysis, and the key output was located'
                            ', so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                    self.amr_status = 'complete'
                else:
                    raise CheckpointFileInconsistentError(self.checkpoint.checkpoint_file, 'amrfinder for assembly', [str(self.amr_output_filtered_file), str(self.amrfinder_output_from_contigs)])
        

        if self.analysis_type == 'combination':
            if self.checkpoint.checkpoint_dict['amrfinder for reads'] == 'complete' and self.checkpoint.checkpoint_dict['amrfinder for assembly'] == 'complete':
                if file_check(file_path=self.amrfinder_output_from_contigs, file_or_directory='file') and file_check(file_path=self.amrfinder_output_from_ariba, file_or_directory='file'):
                    self.logger.log('Sample {} is marked complete in its amr checkpoint file for reads-based analysis, and the key output was located'
                            ', so individual analysis of this sample may be skipped'.format(self.name), verbosity=3)
                    self.amr_status = 'complete'
                else:
                    raise CheckpointFileInconsistentError(self.checkpoint.checkpoint_file, 'amrfinder for reads', [str(self.amr_output_filtered_file), str(self.amrfinder_output_from_ariba)])

    def validate_amr_analysis(self):
        """
        ensure that the requirements for a kraken2 analysis are met. returns 0 if valid. If invalid, log the error message and returns 1
        """
        if self.analysis_type in ['reads', 'combination']:
            try:
                self.scan_for_fastq(self.reads_directory)
            except MissingPathError as err:
                self.logger.log(str(err))
                raise
            except BadPathError as err:
                self.logger.log(str(err))
                raise
        if self.analysis_type in ['assembly', 'combination']:
            try:
                self.scan_for_assembly(self.assembly_directory)
            except MissingPathError as err:
                self.logger.log(str(err))
                raise
            except BadPathError as err:
                self.logger.log(str(err))
                raise

    def configure_amr_analysis(self):
        """
        perform any setup operations for this analysis
        """
        if not file_check(file_path=self.individual_amr_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', '-p', str(self.individual_amr_directory)])

    def amr_threaded_process(self):
        """
        manages analysis for threaded processes
        """
        self.configure_amr_analysis()

        # if analysis type involves reads, run ariba and then amrfinder on the ariba output
        if self.analysis_type in ['reads', 'combination'] and self.amr_status != 'complete':
            try:
                self.ariba_run()
            except AssertionError:
                return
            
            try:
                self.amrfinderplus_run('reads')
            except AssertionError:
                return
        
        # if analysis type involves assemblies, run amrfinder on the assembly file
        if self.analysis_type in ['assembly', 'combination'] and self.amr_status != 'complete':
            try:
                self.amrfinderplus_run('assembly')
            except AssertionError:
                return
            
        # summarize the output based on the analysis type
        try:
            self.summarize_individual_amr_finder_output()
        except AssertionError:
            return
        
        # add the tiers and families information
        try:
            self.tiers_and_families()
        except AssertionError:
            pass
        
        self.logger.log_section_header('Individual sample AMR prediction complete!', verbosity=3)
        return

    def ariba_run(self):
        """
        This method will handle the ARIBA run command for a single sample:
        1. Check/modify compression of read files
        2. Perform local assembly of the read files using ariba and the AMRFinderPlus-derived prepared reference database
        3. Compress the read files once ariba is done
        """

        self.logger.log_section_header('ARIBA - AMR prediction from read files', verbosity=3)
        self.logger.log_explanation(
            'ARIBA is used to assemble raw illumina reads to the AMRFinderPlus reference database prepared '
            'during the AMR Database Validation step. While ARIBA offers its own built in prediction method'
            ' (refer to /AMR/<sample name>/ARIBA_output/report.tsv), we will be using the assemblies.fa '
            'file as input for AMRFinderPlus protein-based prediction', verbosity=3)

        # check to see if this step is marked completed by the checkpoint file
        self.checkpoint.read_checkpoint_file()
        if self.checkpoint.checkpoint_dict['ariba status'] == 'complete':
            self.logger.log('Checkpoint file indicates that {} has already successfully completed ARIBA analysis'.format(self.name),
                    verbosity=3)
            if file_check(file_path=self.ariba_assemblies, file_or_directory='file'):
                self.logger.log('ARIBA output for sample {} located. Skipping this step'.format(self.name), verbosity=3)
                return
            else:
                self.logger.log('Unable to locate the ARIBA output file for sample {}. Updating checkpoint file'.format(self.name),
                        verbosity=3)
                self.checkpoint.update_checkpoint_file(analysis_step='ariba status', new_status=None)
                self.checkpoint.update_checkpoint_file(analysis_step='amrfinder for reads', new_status=None)
                self.checkpoint.update_checkpoint_file(analysis_step='filtered summary', new_status=None)

        # If the isolate has reached this step, analysis will need to be performed. First we will purge any existing output
        self.logger.log('Cleaning up any partially completed analysis output', verbosity=3)
        if file_check(file_path=self.ariba_output_directory, file_or_directory='directory'):
            subprocess.run(['rm', '-r', str(self.ariba_output_directory)])

        # make sure that the self.r1_fastq and self.r2_fastq files are decompressed
        self.r1_fastq = compression_handler(self.r1_fastq, self.logger, decompress=True)
        self.r2_fastq = compression_handler(self.r2_fastq, self.logger, decompress=True)
        time.sleep(1)

        """
        ariba is prone to erroring out when running mutliple threads. Failure of the process is indictated by the message: "Stopping!
        Signal received: 28". We suspect this is a terminal error relating to timeout. In an effort to get around this, we will
        loop ariba until it completes without returning the error code with a maximum of 3 attempts
        """

        # run ariba, directing output files to sample.ariba_output_directory and piping the stderr to ariba_process
        ariba_cmd = 'ariba run {} {} {} {} {}'.format(str(self.ariba_database), str(self.r1_fastq), str(self.r2_fastq), str(self.ariba_output_directory), str(self.cores))
        self.logger.log('Command: ' + log.bold(ariba_cmd) + '\n', verbosity=3)

        error_code_received = True
        attempt_count = 0
        formatted_attempt_count = ['0', '1st', '2nd', '3rd']
        while error_code_received and attempt_count < 4:
            attempt_count += 1

            self.logger.log('Running ARIBA...', verbosity=3)

            ariba_process = subprocess.run(['ariba', 'run',
                                            str(self.ariba_database),
                                            str(self.r1_fastq),
                                            str(self.r2_fastq),
                                            str(self.ariba_output_directory),
                                            '--threads', str(self.cores)],
                                        stdout=subprocess.DEVNULL, stderr=subprocess.PIPE)

            # read in the stderr captured in ariba_process
            ariba_error = str(ariba_process.stderr)

            # check to see if the aborted run signal is present in ARIBA's stderr. If so, delete the failed ariba output
            # and try again
            if "Signal received: 28" in ariba_error:
                self.logger.log('ariba runtime error encountered, resulting in run termination. Retrying for the ' +
                        formatted_attempt_count[attempt_count] + ' time', verbosity=3)
                subprocess.run(['rm', '-r', str(self.ariba_output_directory)])
            else:
                error_code_received = False
                self.logger.log('ARIBA run finished without thowing Error code 28', verbosity=3)

        # check the final return code from ARIBA
        try:
            assert ariba_process.returncode == 0
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='ariba status', new_status='failed')
            self.logger.log('ARIBA returned a non-zero returncode for sample {}: {}'.format(self.name, str(ariba_process.returncode)))
            self.logger.log(ariba_error)
            raise

        # validate that the output file was generated where we were expecting it
        try:
            assert file_check(file_path=self.ariba_assemblies_compressed, file_or_directory='file')
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step='ariba status', new_status='failed')
            self.logger.log('something went wrong during amr detection from reads. ARIBA failed to generate the assembly file for {} at the expected path {}'.format(self.name, str(self.ariba_assemblies)))
            raise

        # decompress the output file for use by amrfinderplus
        self.ariba_assemblies = compression_handler(self.ariba_assemblies_compressed, self.logger, decompress=True)

        # update the checkpoint to indicate successful completion of this step
        self.checkpoint.update_checkpoint_file(analysis_step='ariba status', new_status='complete')

        # make sure that the self.r1_fastq and self.r2_fastq files are compressed
        self.r1_fastq = compression_handler(self.r1_fastq, self.logger, compress=True, compression_type="bzip2")
        self.r2_fastq = compression_handler(self.r2_fastq, self.logger, compress=True, compression_type="bzip2")

    def amrfinderplus_run(self, analysis_type):
        """
        run AMRFinderPlus on the selected sample object for the attribute (file) selected (contig file/ariba assemblies file)
        list_of_attributes is a two item list [input file for AMRFinderPlus, output file from AMRFinderPlus]
        """

        self.logger.log_section_header('AMRFinderPlus - AMR prediction from assemblies for {}'.format(analysis_type), verbosity=3)
        self.logger.log_explanation(
            'AMRFinderPlus will be used to perform AMR prediction using the assemblies associated with {}.'.format(analysis_type), verbosity=3)

        # check to see if this step is marked completed by the checkpoint file based on the analysis_type
        if analysis_type == 'reads':
            checkpoint_step = 'amrfinder for reads'
            output_file = self.amrfinder_output_from_ariba
            assembly_file = self.ariba_assemblies
        elif analysis_type == 'assembly':
            checkpoint_step = 'amrfinder for assembly'
            output_file = self.amrfinder_output_from_contigs
            assembly_file = self.assembly

        self.checkpoint.read_checkpoint_file()
        if analysis_type == 'reads':
            if self.checkpoint.checkpoint_dict[checkpoint_step] == 'complete':
                self.logger.log(
                    'Checkpoint file indicates that {} has already successfully completed amrfinder analysis for {}'.format(
                        self.name, self.analysis_type), verbosity=3)
                if file_check(file_path=output_file, file_or_directory='file'):
                    self.logger.log(
                        'amrfinder output for sample {} and analysis type {} located. Skipping this step'.format(self.name, analysis_type), verbosity=3)
                    return
                else:
                    self.logger.log(
                        'Unable to locate the amrfinder output file for sample {} and analysis type {}. Updating checkpoint file'.format(
                            self.name, analysis_type), verbosity=3)
                    self.checkpoint.update_checkpoint_file(analysis_step=checkpoint_step, new_status=None)

        elif analysis_type == 'assembly':
            if self.checkpoint.checkpoint_dict[checkpoint_step] == 'complete':
                self.logger.log(
                    'Checkpoint file indicates that {} has already successfully completed amrfinder analysis for {}'.format(
                        self.name, self.analysis_type), verbosity=3)
                if file_check(file_path=output_file, file_or_directory='file'):
                    self.logger.log(
                        'amrfinder output for sample {} and analysis type {} located. Skipping this step'.format(self.name, analysis_type), verbosity=3)
                    return
                else:
                    self.logger.log(
                        'Unable to locate the amrfinder output file for sample {} and analysis type {}. Updating checkpoint file'.format(
                            self.name, analysis_type), verbosity=3)
                    self.checkpoint.update_checkpoint_file(analysis_step=checkpoint_step, new_status=None)

        # if analysis is required, remove existing output file (if it exists)
        if file_check(file_path=output_file, file_or_directory='file'):
            subprocess.run(['rm', str(output_file)])

        # update the checkpoint file to indicate that summarization will be needed
        self.checkpoint.update_checkpoint_file(analysis_step='filtered summary', new_status=None)

        # run amrfinder in nucleotide mode for the provided assembly file
        amrfinderplus_cmd = 'amrfinder --plus -d {} -o {} -n {} --threads {}'.format(str(self.amrfinder_database),
                                                                                    str(output_file), str(assembly_file), str(self.cores))

        self.logger.log('amrfinder command: ' + log.bold(amrfinderplus_cmd) + '\n', verbosity=3)
        self.logger.log('Now running AMRFinder for ' + analysis_type, verbosity=3)

        amrfinder_process = subprocess.run(['amrfinder',
                                            '--plus',
                                            '-d', str(self.amrfinder_database),
                                            '-o', str(output_file),
                                            '-n',
                                            str(assembly_file),
                                            '--threads', str(self.cores)],
                                        stderr=subprocess.STDOUT,
                                        stdout=subprocess.DEVNULL)

        # check the final return code from amrfinder
        try:
            assert amrfinder_process.returncode == 0
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step=checkpoint_step, new_status='failed')
            self.logger.log('AMRFinder returned a non-zero returncode for sample {}: {}\n{}'.format(self.name, str(amrfinder_process.returncode), str(amrfinder_process.stdout)))
            raise

        # validate that the output file was generated where we were expecting it
        try:
            assert file_check(file_path=output_file, file_or_directory='file')
        except AssertionError:
            self.checkpoint.update_checkpoint_file(analysis_step=checkpoint_step, new_status='failed')
            self.logger.log('something went wrong during amr detection from {}. amrfinder failed to generate the assembly '
                            'file for {} at the expected path: {}'.format(analysis_type, self.name, str(output_file)))
            raise

        # update the checkpoint to indicate successful completion of this step
        self.checkpoint.update_checkpoint_file(analysis_step=checkpoint_step, new_status='complete')

    def summarize_individual_amr_finder_output(self):
        """
        Merge all of the available AMRFinderPlus output derived from contigs, repeat the process for output based on ariba
        assemblies. Generate TWO integrated report files:
            1) <Sample name>_amr_output_raw.csv, which is merely a concatenation of all available output for every sample, with true
            duplicates removed
            2) <sample name>_amr__output_FILTERED.csv, applying more aggressive filtering to reduce the number of redundant hits
        """

        self.logger.log_section_header('Summarize Individual AMR Results', verbosity=3)
        self.logger.log_explanation('Generate summary tables of all AMRFinderPlus results for a single sample.', verbosity=3)

        # if we are in this method, some part of the analysis for this sample has changed (reads/assembly/combination) so we will
        # need to redo the summary to incorporate the latest information
        for file in [self.amr_output_raw_file, self.amr_output_filtered_file, self.amr_output_curated_file]:
            if file.exists():
                subprocess.run(['rm', str(file)])
        
        # Establish the column headers that will be present in the amr_raw_output DataFrame
        column_list = [
            'Sample', 
            'Source', 
            'Gene symbol', 
            'Sequence name', 
            'Method',
            '% Coverage of reference sequence', 
            '% Identity to reference sequence', 
            'Start',
            'Stop', 
            'Strand', 
            'Contig id', 
            'Contig length', 
            'Accession of closest sequence',
            'Name of closest sequence', 
            'Scope', 
            'Element type', 
            'Element subtype', 
            'Class',
            'Subclass'
            ]


        # Create DataFrame "amr_output_raw" which we will add all of the data generated during the analysis run to
        amr_output_raw = pd.DataFrame(
            columns=column_list)

        # amr_output_raw will be populated based on the analysis type parameter
        if self.analysis_type == 'reads':
            sources = ['ariba']
        elif self.analysis_type == 'assembly':
            sources = ['assembly']
        else:
            sources = ['ariba', 'assembly']

        for source in sources:

            if source == 'ariba':
                input_file = self.amrfinder_output_from_ariba

            if source == 'assembly':
                input_file = self.amrfinder_output_from_contigs
                try:
                    self.scan_for_assembly(self.assembly_directory)
                    contig_lengths = get_contig_lengths(self.assembly, self.logger)
                except MissingPathError as err:
                    self.logger.log(str(err))
                    contig_lengths = ""
                except BadPathError as err:
                    self.logger.log(str(err))
                    contig_lengths = ""

            # Parse the current input file and insert the results into amr_output_raw
            if input_file.exists():

                self.logger.log('Generating summary file for: {}'.format(log.bold(str(input_file))), verbosity=3)

                input_column_list = ['Gene symbol', 'Sequence name', 'Scope', 'Element type', 'Element subtype',
                                    'Class', 'Subclass', 'Method', '% Coverage of reference sequence',
                                    '% Identity to reference sequence', 'Start', 'Stop', 'Strand', 'Contig id',
                                    'Accession of closest sequence', 'Name of closest sequence']

                # Read in the current input file (either assembly or reads output) to the DataFrame "report_data"
                try:
                    report_data = pd.read_csv(input_file, sep='\t', usecols=input_column_list)
                except pd.errors.EmptyDataError:  # Case of no amrfinder results
                    self.logger.log('The following AMR report file was empty: {}'.format(log.bold(str(input_file))), verbosity=3)
                    continue
                except ValueError:  # Case of no ARIBA assemblies for analysis
                    self.logger.log('The following AMR report file was empty: {}'.format(log.bold(str(input_file))), verbosity=3)
                    continue
                
                # add in Sample and Source information
                report_data['Sample'] = self.name
                report_data['Source'] = source
                report_data['Sequencing Run'] = self.run

                # if contigs were used, add the total length of the contig the hit is located on to "report_data"
                if source == 'assembly':
                    report_data['Contig id'] = report_data['Contig id'].astype(str)
                    report_data = report_data.merge(contig_lengths, on='Contig id', how='left')
                else:
                    report_data['Contig Length'] = "N/A"

                # add the current "report_data" to "amr_output_raw"
                amr_output_raw = pd.concat([amr_output_raw, report_data], ignore_index=True, sort=False)

        # if no amr genes were detected, report to the log and skip all remaining processing steps
        if 'report_data' not in locals():
            self.logger.log('No AMR genes detected, so output will not be written', verbosity=3)
            return

        self.logger.log('Generating summary file: ' + log.bold(str(self.amr_output_raw_file)), verbosity=3)

        amr_output_raw.to_csv(self.amr_output_raw_file, sep=',', index=False, mode='a')

        # Results filtering: Since we are potentially calling genes from two sources (assembled contigs and ARIBA output) we
        # anticipate that there will be significant redundancy in the output. In an attempt not to drive folks crazy with
        # deduplicating the resulting .csv file, we will perform some "safe" deduplication operations on the output
        # dataframe. Note that you still have access to the complete dataset in the andale_output_raw.csv file

        # First, we can safely drop all true duplicate entries from the dataframe
        amr_output_raw.drop_duplicates(inplace=True)

        # Second, in the case where the result for a given sample/gene is identical in the method of call (either EXACTX or
        # ALLELEX) as well as the %coverage and %ID, we will selectively keep the result from the contigs as this will
        # provide the greatest context
        filtered_amr_output = amr_output_raw.groupby(['Sample', 'Gene symbol', 'Method',
                                                    '% Coverage of reference sequence',
                                                    '% Identity to reference sequence'])
        for name, group in filtered_amr_output:
            if len(group.index) > 1:
                if 'assembly' in group.Source.values:
                    filter_methods = ['EXACTX', 'ALLELEX']
                    filtered_group = group[((group['Method'].isin(filter_methods)) |
                                            ((group['Method'] == 'BLASTX') &
                                            (group['% Coverage of reference sequence'] >= 90) &
                                            (group['% Identity to reference sequence'] >= 90))) &
                                        (group['Source'] == 'assembly')]
                    for i in group.index:
                        if i not in filtered_group.index:
                            amr_output_raw.drop(i, inplace=True)

        # Third, in the case where an EXACTX or ALLELEX hit OR a BLASTX hit with >95/95 %coverage/%ID exists for a given
        # sample/gene combination, we will remove any PARTIAL_CONTIG_ENDX or PARTIAL_CONTIGX that are the result of ARIBA
        # assembling partial genes adjacent to the target gene
        filtered_amr_output = amr_output_raw.groupby(['Sample', 'Gene symbol'])
        for name, group in filtered_amr_output:
            if len(group.index) > 1:
                if ('PARTIAL_CONTIG_ENDX' in group.Method.values) or ('PARTIALX' in group.Method.values):
                    filter_methods = ['EXACTX', 'ALLELEX']
                    filtered_group = group[(group['Method'].isin(filter_methods)) |
                                        ((group['Method'] == 'BLASTX') &
                                            (group['% Coverage of reference sequence'] >= 90) &
                                            (group['% Identity to reference sequence'] >= 90))]
                    for i in group.index:
                        if i not in filtered_group.index:
                            amr_output_raw.drop(i, inplace=True)

        # Finally, generate a gene-like gene symbol name IF ID and coverage are >95%
        # iterate through all rows in the dataframe. For a given row, if the 'Method" is BLASTX AND
        # '% Coverage of reference sequence' > 95 AND '% Identity to reference sequence' > 95 THEN replace the 'Gene symbol'
        # field with the value in 'Name of closest sequence' + '-like'
        for index in list(amr_output_raw.index):
            if amr_output_raw.loc[index, 'Method'] == 'BLASTX' and float(amr_output_raw.loc[index, '% Coverage of reference sequence']) >= 95 and float(amr_output_raw.loc[index, '% Identity to reference sequence']) >= 95:
                amr_output_raw.loc[index, 'Hit Qualifier'] = 'LIKE'
            elif amr_output_raw.loc[index, 'Method'] == 'BLASTX' and (float(amr_output_raw.loc[index, '% Coverage of reference sequence']) < 95 or float(amr_output_raw.loc[index, '% Identity to reference sequence']) < 95):
                amr_output_raw.loc[index, 'Hit Qualifier'] = 'WARNING'
            else:
                amr_output_raw.loc[index, 'Hit Qualifier'] = 'EXACT'

        self.logger.log('Generating summary file: ' + log.bold(str(self.amr_output_filtered_file)), verbosity=3)
        
        amr_output_raw.to_csv(self.amr_output_filtered_file, sep=',', index=False, mode='a')

        self.checkpoint.update_checkpoint_file(analysis_step='filtered summary', new_status='complete')
    
    def tiers_and_families(self):
        """
        New in v.1.2.5. tiers_and_families() is used to decorate the amr summary table(s) with additional fields that add
        further classifications to the amr gene hits that were identified. Most significantly it contextualizes gene hits
        based on:
            1)  Gene family: In addition to the NCBI gene family we introduce a broader gene family based on the gene class
            2)  Gene Tiers: Each gene is ranked for significance/potential impact based on a 3-tier scheme, where inclusion
                in a lower tier number indicates a greater significance/potential impact
        The additional fields will be packaged as an .xlsx spreadsheet in the resources directory. The first step in the
        process will be to verify that the sheet is present and has the correct format. We will then import it into a
        DataFrame and merge this with amr_summary_dataframe on the accession_number, which will serve as the foreign key.
        Finally, we will export the decorated summary file as a multi-sheet .xlsx file, with one sheet for each tier level
        (i.e. sheet 1 == tier 1 only, sheet 2 == tiers 1 and 2, sheet 3 == tiers 1, 2, and 3, and sheet 4 is all data).
        """
        self.logger.log_section_header("Importing MRSN Tiers and Families Information", verbosity=3)
        self.logger.log_explanation("Using metadata from {} to decorate the AMR summary file".format(str(self.tiers_and_families_file)), verbosity=3)

        # read in the tiers and families file to a DataFrame. We will attempt to index on the accession number, which should
        # a unique value for each line in the file. If not, we want to exit the method with an error message letting the
        # user know that there is a problem with the input file
        try:
            tiers_and_families_dataframe = pd.read_excel(self.tiers_and_families_file, header=0, engine='openpyxl')
        except pd.errors.EmptyDataError:  # Case of empty tiers_and_families_file
            self.logger.log('The tiers and families file was empty: ' + log.bold(str(self.tiers_and_families_file)))
            raise AssertionError
        except ValueError:  # Incorrectly formatted tiers_and_families_file
            self.logger.log('The tiers and families file was incorrectly formatted: ' + log.bold(str(self.tiers_and_families_file)))
            raise AssertionError

        tiers_and_families_dataframe['Extended annotation'] = tiers_and_families_dataframe['Extended annotation'].replace('_', ' ', regex=True)

        # read the filtered summary file into a pandas DataFrame
        filtered_summary_dataframe = pd.read_csv(self.amr_output_filtered_file, sep=',')

        # we want to merge the information from the tiers_and_families_dataframe into the amr_summary_dataframe s.t. each
        # entry in the amr_summary_dataframe includes the corresponding information from the tiers_and_families_dataframe.
        # To accomplish this we will perform a merge using the 'Accession of closest sequence' as the left key and 'Acc No'
        # as the right key

        # NEW IN V1.3: merging will be performed based on the 2 keys (Acc no and Allele) since fusion proteins have the same "Acc No"
        self.logger.log("Attempting tiers and family merge")
        try:
            full_summary_dataframe = filtered_summary_dataframe.merge(
                tiers_and_families_dataframe,
                how='left',
                left_on=['Accession of closest sequence', 'Name of closest sequence'],
                right_on=['Acc No', 'Extended annotation']
            )
        except ValueError:
            self.logger.log('An error occurred while attempting to merge the amr summary DataFrame with the Tiers and Families information!')
            raise AssertionError
        full_summary_dataframe.head()
        # try:
        #     full_summary_dataframe = filtered_summary_dataframe.merge(
        #         tiers_and_families_dataframe,
        #         how='left',
        #         left_on='Accession of closest sequence',
        #         right_on='Acc No'
        #         )
        # except ValueError:
        #     self.logger.log('An error occurred while attempting to merge the amr summary DataFrame with the Tiers and Families information!')
        #     raise AssertionError

        # Drop all PARTIALX and PARTIAL_CONTIG_ENDX hits from the table
        drop_method_list = [
            'PARTIALX',
            'PARTIAL_CONTIG_ENDX'
        ]
        full_summary_dataframe.drop(full_summary_dataframe.loc[full_summary_dataframe['Method'].isin(drop_method_list)].index, inplace=True)

        # Set the curation level to raw
        full_summary_dataframe["Curation level"] = "filtered"

        # handling amr_tiers_and_families.xlsx discordance with the current AMRFinderPlus database. If genes are renamed/added to the database
        # that are not present in the amr_tiers_and_families.xlsx file we will need the entry to use Gene Symbol for Allele and Sequence Name
        # for Extended Annotation

        for index in list(full_summary_dataframe.index.to_list()):
            if pd.isna(full_summary_dataframe.loc[index, 'Allele']):
                self.logger.log(log.yellow('WARNING: {} is not present in the amr tiers and families resource file, so the default nomenclature will be reported in amr_full_summary\n'.format(full_summary_dataframe.loc[index, 'Gene symbol'])))
                full_summary_dataframe.loc[index, 'Allele'] = full_summary_dataframe.loc[index, 'Gene symbol']
                full_summary_dataframe.loc[index, 'Extended annotation'] = full_summary_dataframe.loc[index, 'Sequence name']
        
        # Reformat the "Allele" column entry to reflect the quality of the hit
        # for index in list(full_summary_dataframe.index):
        #     if full_summary_dataframe.loc[index, 'Method'] == 'BLASTX' and float(full_summary_dataframe.loc[index, '% Coverage of reference sequence']) >= 95 and float(full_summary_dataframe.loc[index, '% Identity to reference sequence']) >= 95:
        #         full_summary_dataframe.loc[index, 'Allele'] = str(full_summary_dataframe.loc[index, 'Allele']) + '-like'
        #     elif full_summary_dataframe.loc[index, 'Method'] == 'BLASTX' and (float(full_summary_dataframe.loc[index, '% Coverage of reference sequence']) < 95 or float(full_summary_dataframe.loc[index, '% Identity to reference sequence']) < 95):
        #         full_summary_dataframe.loc[index, 'Allele'] = str(full_summary_dataframe.loc[index, 'Allele']) + '-like [WARNING]'

        # Set the output column order for all of the entries in the full amr summary as a list full_summary_column_order
        full_summary_column_order = [
            'Sample', 'Sequencing Run', 'Source', 'Curation level', 'Hit Qualifier', 'Fusion Number', 'Fusion Count', 'Allele', 'MRSN SuperFamily', 'Extended annotation', 'Expected Phenotype', 'Method',
            '% Coverage of reference sequence', '% Identity to reference sequence', 'Start', 'Stop', 'Strand', 'Contig id',
            'Contig length', 'Accession of closest sequence', 'Element type', 'Element subtype', 'Tier 1', 'Tier 2', 'Tier 3'
        ]

        # We will subset our full_summary_dataframe in different
        # ways and write these to different sheets as follows:
        #   sheet 1:    "tier_1"                only hits that have a value of "True" in the tier 1 column
        #   sheet 2:    "tier_1"                only hits that have a value of "True" in the tier 2 column
        #   sheet 3:    "tier_3"                only hits that have a value of "True" in the tier 3 column
        #   sheet 4:    "carbapenemases"        only hits that have a carbapenem value in "Expected Phenotype"
        #   sheet 5:    "pan-aminoglycosides"   only hits that have a pan-aminoglycoside value in "Expected Phenotype"
        #   sheet 6:    "mcr"                   only hits that have a colistin resistant value in "Expected Phenotype"
        #   sheet 7:    "vancomycin"            only hits that have a vancomycin resistant value in "Expected Phenotype"
        #   sheet 8:    "all results"           all hits from amr_summary_dataframe

        # prepare subset dataframe for sheet 1 "tier_1"
        tier_1 = full_summary_dataframe.loc[full_summary_dataframe['Tier 1'] == 'Yes']

        # prepare subset dataframe for sheet 2 "tier_2"
        tier_2 = full_summary_dataframe.loc[full_summary_dataframe['Tier 2'] == 'Yes']

        # prepare subset dataframe for sheet 3 "tier_3"
        tier_3 = full_summary_dataframe.loc[full_summary_dataframe['Tier 3'] == 'Yes']

        # prepare subset dataframe for sheet 4 "carbapenemases"
        carbapenemase_values = [
            'Beta-Lactams: Carbapenems',
            'Beta-Lactam: Carbapenems',
            'Beta-Lactam: Carbapenem, Clavulanic acid',
            'Beta-Lactam: Carbapenem'
        ]
        carbapenemases = full_summary_dataframe.loc[full_summary_dataframe['Expected Phenotype'].isin(carbapenemase_values)]

        # prepare subset dataframe for sheet 5 "pan-aminoglycosides"
        pan_aminoglycoside_values = [
            'Aminoglycoside: Pan resistance'
        ]
        pan_aminoglycosides = full_summary_dataframe.loc[full_summary_dataframe['Expected Phenotype'].isin(pan_aminoglycoside_values)]

        # prepare subset dataframe for sheet 6 "mcr"
        mcr_values = [
            'Polymyxin: Colistin',
            'Polymyxin: Not determined'
        ]
        mcr = full_summary_dataframe.loc[full_summary_dataframe['Expected Phenotype'].isin(mcr_values)]

        # prepare subset dataframe for sheet 7 "vancomycin"
        vancomycin_values = [
            'Glycopeptide: Vancomycin'
        ]
        vancomycin = full_summary_dataframe.loc[full_summary_dataframe['Expected Phenotype'].isin(vancomycin_values)]

        # write all 8 sheets out to the full_summary_amr_file
        with pd.ExcelWriter(self.amr_summary_file, mode='w', engine='openpyxl') as writer: # pylint: disable=abstract-class-instantiated
            tier_1.to_excel(writer, sheet_name="tier 1", index=False, columns=full_summary_column_order)
            tier_2.to_excel(writer, sheet_name="tier 2", index=False, columns=full_summary_column_order)
            tier_3.to_excel(writer, sheet_name="tier 3", index=False, columns=full_summary_column_order)
            carbapenemases.to_excel(writer, sheet_name="carbapenemases", index=False, columns=full_summary_column_order)
            pan_aminoglycosides.to_excel(writer, sheet_name="pan aminoglycosides", index=False, columns=full_summary_column_order)
            mcr.to_excel(writer, sheet_name="mcr", index=False, columns=full_summary_column_order)
            vancomycin.to_excel(writer, sheet_name="vancomycin", index=False, columns=full_summary_column_order)
            full_summary_dataframe.to_excel(writer, sheet_name="full summary", index=False, columns=full_summary_column_order)
            


def get_contig_lengths(assembly_file, logger):
    """
    method takes a multifasta file and returns a pandas series where the index is the name of the contigs
    and the values are the lengths of those contigs
    """

    # Use SeqIO to read all of the contig names and lists into two lists, contig_names and contig_lengths
    contig_length_dict = {'Contig id': [], 'Contig length': []}
    for contig in SeqIO.parse(assembly_file, "fasta"):
        contig_length_dict['Contig id'].append((str(contig.id).split(' '))[0])
        contig_length_dict['Contig length'].append(int(len(contig.seq)))

    # convert the two lists into a series object
    contig_lengths = pd.DataFrame(contig_length_dict, columns=['Contig id', 'Contig length'])

    # assembly_file = might_compression_handler(compress=True, target_file=assembly_file)

    return contig_lengths


#!/usr/bin/env python
# encoding: utf-8
import csv
import os
import sys
import subprocess
import time
from pathlib import Path
import shutil
import pickle

import datetime

from coolname import generate_slug

import numpy as np
import pandas as pd
from pandas.errors import MergeError

from pymongo import MongoClient
from pymongo import errors as pyerr

from sklearn.cluster import DBSCAN
#from skbio import DistanceMatrix
#from skbio.tree import nj

#from might.analysis.mst_methods import Mst
from might.common import log
from might.common.miscellaneous import path_clean, file_check, file_extension_check, quit_with_error, query_yes_no
from might.common.errors import BadPathError, NothingToDoError, InvalidInputError, SampleNotFoundError

#################################################################################################################################################
#
#  Class ClusterManager
#
#   Class ClusterManager is responsible for initiating and running all of the individual clusters (one per input distance matrix file). It
#   is also responsible for the validation of user provided file paths, credentials, and availability of the bi-db
#
#################################################################################################################################################
class ClusterManager:
    """
    Class ClusterManager is responsible for instantiating and managing GenusClusters based on the distance matrix files present in the input
    directory.
    """

    def __init__(self, input_directory, output_directory, username, password, db_uri, auth_source, threshold=10, force=False, verbosity=2):

        self.input_directory = input_directory
        self.output_directory = output_directory

        self.username = username
        self.password = password
        self.db_uri = db_uri
        self.auth_source = auth_source

        self.uri_string = self.uri_string = "mongodb://{}:{}@{}?authSource={}".format(self.username, self.password, self.db_uri, self.auth_source)
        self.client = MongoClient(self.uri_string)
        self.db = self.client['bi-db']

        self.threshold = threshold

        self.force = force
        self.verbosity = verbosity
        if self.verbosity == 3:
            self.debug = True
        else:
            self.debug = False

        self.log_directory = None

        self.distance_matrix_file_paths = []
        self.genus_cluster_objects = []

    def validate_input_paths(self):
        """
        ensure that the input and output directory paths are valid
        """
        self.input_directory = Path(self.input_directory).expanduser().resolve()
        if not self.input_directory.is_dir():
            raise BadPathError(self.input_directory, 'input directory', 'The input directory path is invalid!')
        
        self.output_directory = Path(self.output_directory).expanduser().resolve()
        if not self.output_directory.exists():
            subprocess.run(['mkdir', '-p', str(self.output_directory)])
    
    def validate_connection_to_bi_database(self):
        """
        ensure that the passed bi-db parameters are valid
        """
        self.logger.log_section_header("Establishing connection to the bi-db")
        self.logger.log_explanation("Ensuring that the bi-db is available to MIGHT")
        try:
            self.client.server_info()
        except pyerr.ServerSelectionTimeoutError:
            raise

    def scan_input_directory_for_distance_matrix_files(self):
        """
        scan the input directory for files that match the formatting requirements to be used as input for GenusCluster analysis
        """
        acceptable_file_ending = 'distance_matrix.xlsx'

        self.logger.log_section_header("Scanning for Distance Matrix file(s)")
        self.logger.log_explanation("Scanning the provided input directory ({}) for distance matrix files (files ending with '{}'".format(str(self.input_directory), acceptable_file_ending))

        for entry in os.scandir(self.input_directory):
            if entry.name.endswith(acceptable_file_ending):
                if entry not in self.distance_matrix_file_paths:
                    self.distance_matrix_file_paths.append(Path(entry.path).expanduser().resolve())
        
        if len(self.distance_matrix_file_paths) == 0:
            raise NothingToDoError(str(self.input_directory), 'No properly formatted distance matrix files were detected (ending in distance_matrix.xlsx)')

        self.logger.log("Detected the following distance matrix files:")
        log_a_list(self.distance_matrix_file_paths, self.logger)
            
    def double_check_force(self):
        """
        if the user has requested force, give them a chance to rethink that
        """
        if self.force == True:
            if not query_yes_no('You have indicated that you want to force overwrite the content at {}. Are you sure about this?'.format(str(self.output_directory))):
                print('Good call. Pick a new output directory and try again!')
                sys.exit()
        
        # testing option
        if self.force == 'auto':
            subprocess.run(['rm', '-r', str(self.output_directory)])
    
    def initialize_logger(self):
        """
        initialize the logger for this sample
        """
        self.log_directory = self.output_directory / 'logs'
        if not self.log_directory.is_dir():
            subprocess.run(['mkdir', '-p', str(self.log_directory)])
        try:
            self.logger = log.Log(
                log_filename=self.log_directory / 'cluster.might.log',
                stdout_verbosity_level=self.verbosity
            )
        except: # need to figure out the logger error codes
            pass

    def run_individial_cluster_analyses(self):
        """
        for each file that was added to distance_matrix_file_paths, initialize and run through the GenusCluster workflow
        """
        self.logger.log_section_header('Now starting individual file cluster analyses')
        self.logger.log_explanation('For each file that was identified as a potential distance matrix based on the format of the name in the input directory we will perform cluster analysis')
        
        for distance_matrix_file in self.distance_matrix_file_paths:

            current_cluster = ClusterAnalysis(
                self.output_directory,
                distance_matrix_file,
                self.logger,
                self.db,
                threshold=self.threshold,
                verbosity=self.verbosity
            )

            # perform individual cluster analysis
            current_cluster.analyze_cluster()
            
            # append the current cluster object to clusters
            self.genus_cluster_objects.append(current_cluster)

    def summarize_all_genus_cluster_reports(self):
        """
        generate a summary file that contains all of the information contained in the individual GenusCluster report files
        """
        self.logger.log_section_header("Summarizing individual report files")
        self.logger.log_explanation("Concatenating all of the information present in each of the individual cluster analysis reports")

        self.summary_report_file = self.output_directory / "summary.clusters.might.xlsx"

        self.context_dataframe_new_only = pd.DataFrame()
        self.context_dataframe_multipatient = pd.DataFrame()
        self.context_dataframe = pd.DataFrame()
        self.qc_dataframe = pd.DataFrame()
        self.filtered_amr_dataframe = pd.DataFrame()
        self.patient_dataframe = pd.DataFrame()
        self.ast_dataframe = pd.DataFrame()

        for genus_cluster in self.genus_cluster_objects:
            self.context_dataframe_new_only = pd.concat([self.context_dataframe_new_only, genus_cluster.context_dataframe_new_only])
            self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, genus_cluster.context_dataframe_multipatient])
            self.context_dataframe = pd.concat([self.context_dataframe, genus_cluster.context_dataframe])
            self.qc_dataframe = pd.concat([self.qc_dataframe, genus_cluster.qc_dataframe])
            self.filtered_amr_dataframe = pd.concat([self.filtered_amr_dataframe, genus_cluster.filtered_amr_dataframe])
            self.patient_dataframe = pd.concat([self.patient_dataframe, genus_cluster.patient_dataframe])
            self.ast_dataframe = pd.concat([self.ast_dataframe, genus_cluster.ast_dataframe])

        with pd.ExcelWriter(self.summary_report_file, mode='w', engine='openpyxl') as writer:
            self.context_dataframe_new_only.to_excel(writer, sheet_name="Clusters - New Isolates", index=False)
            self.context_dataframe_multipatient.to_excel(writer, sheet_name="Clusters - Multiple Patients", index=False)
            self.context_dataframe.to_excel(writer, sheet_name="Clusters - All", index=False)
            self.qc_dataframe.to_excel(writer, sheet_name="QC Summary", index=False)
            self.filtered_amr_dataframe.to_excel(writer, sheet_name="AMR Summary", index=False)
            self.patient_dataframe.to_excel(writer, sheet_name="Patient Summary", index=False)
            self.ast_dataframe.to_excel(writer, sheet_name="AST Summary", index=False)


#################################################################################################################################################
#
#  Class ClusterAnalysis
#
#   Objects of class ClusterAnalysis represent the core cluster analyses processes originating from a single distance matrix dataframe. It 
#   contains the cluster detection and filtering methods, as well as housekeeping tasks like preparing the individual output directory, 
#   recovering progress from the picklejar, and writing the individual cluster analysis summary file (xlsx)
#
#################################################################################################################################################
class ClusterAnalysis:
    """
    Handles the cluster analysis tasks for a single Genus (i.e. based on a single cgMLST distance matrix file)
    """

    def __init__(self, output_directory, distance_matrix_file, logger, db, threshold=10, force=False, verbosity=2):

        # input files and parameters
        self.output_directory = output_directory  # directory where the output will be stored
        self.distance_matrix_file = distance_matrix_file  # path to the distance matrix file
        self.output_subdirectory = self.output_directory / (self.distance_matrix_file.name.split(".distance_matrix.xlsx")[0])
        self.logger = logger
        self.db = db
        self.clustering_threshold = threshold  # the threshold allelic distance for inclusion in outbreak cluster detection
        self.verbosity = verbosity  # determines the amount of information that will be logged to the terminal
        self.debug = False  # if True, all debugging messages and table previews will be logged to the terminal
        if self.verbosity == 3:
            self.debug = True

        # instances of non-class objects. Initialized in the methods under "External-class methods"
        self.distance_matrix_manipulation = None
        self.qc_amr_query = None
        self.surveillance_clusters = None
        self.patient_ast_query = None

        # distance matrix attributes
        self.sorted_distance_matrix_dataframe = None

        # outbreak clustering DataFrames
        self.outbreak_cluster_dataframe = None  # results of DBSCAN cluster analysis as a pandas DataFrame
        self.metadata_dataframe = None  # isolate metadata stored in a DataFrame
        self.decorated_outbreak_cluster_dataframe = None  # result of merging outbreak_cluster_dataframe with metadata
        self.existing_clusters_dataframe = None  # result of importing previous analysis results as a pandas DataFrame
        self.filtered_outbreak_dataframe = None  # result of filtering out same patient isolates from outbreak_cluster_dataframe
        self.removed_isolates = None  # list of isolates that were removed during same patient deduplication

        # database query results
        self.qc_dataframe = None
        self.amr_dataframe = None
        self.patient_dataframe = None
        self.ast_dataframe = None

        # reformatted DataFrames
        self.reformatted_amr_dataframe = None
        self.reformatted_ast_dataframe = None

        # merged DataFrames
        self.context_dataframe = None
        self.context_dataframe_multipatient = pd.DataFrame()
        self.context_dataframe_new_only = pd.DataFrame()

        # output file paths
        self.potential_transmission_pairs_file = self.output_subdirectory / 'potential_transmission_pairs.csv'  # path to the potential transmission pairs .csv file
        self.potential_transmission_itol_dataset_txt_file = self.output_subdirectory / 'potential_transmission_iTOL_dataset.txt'  # path to the potential transmission pairs iTOL dataset file
        self.outbreak_cluster_file = self.output_subdirectory / 'cluster_analysis.xlsx'  # path to the outbreak cluster summary file

        # the output directory should not exist at this point. We will generate it (as well as the parent directory) now
        if not file_check(file_path=self.output_directory, file_or_directory='directory'):
            subprocess.run(['mkdir', '-p', str(self.output_directory)])
        
        # initialize the picklejar for this run
        self.pickle_jar = PickleJar(self.output_subdirectory)

    def analyze_cluster(self):
        """
        run the full analysis for this cluster
        """
        self.logger.log_section_header("Now running the individual analysis for {}".format(str(self.distance_matrix_file.name)))
        self.logger.log_explanation("The distance matrix file will be imported and clustered. Context will be collected from the bi-db and used to decorate and filter the results")

        # prepare the current clusters output subdirectory
        self.prepare_output_subdirectory()

        # import and manipulate the distance matrix
        self.distance_matrix_import()
        
        # PERFORM OUTBREAK CLUSTER DETECTION
        self.outbreak_cluster_detection()

        # query for, filter, and reformat the qc and amr data
        self.query_qc_and_amr()

        # merge the outbreak_cluster_dataframe and 1) the qc dataframe and 2) the reformatted_amr_dataframe
        self.merge_qc_amr()

        if self.debug:
            print(self.context_dataframe)

        # query for existing outbreak cluster information from the surveillance_clusters collection
        self.query_existing_surveillance_clusters()
        
        # merge the existing clusters information into the context dataframe
        self.merge_existing_clusters()

        # add in any patientsCollectionIds from previous attempts from the picklejar
        self.add_patient_collection_ids_from_pickle_jar()

        if self.debug:
            print(self.context_dataframe)

        # query for, perform interactive filtering of patients collection records, and filter/reformat ast results
        self.patient_ast_query()

        # merge the patient metadata and ast results into the context dataframe
        self.merge_patient_ast()

        self.clean_up_dates()

        # establish whether samples are newly identified or previously described in clusters
        self.establish_new_vs_old_sample()

        if self.debug:
            print(self.context_dataframe.loc[:,["mrsnId", "isNew", "patientsCollectionId"]])

        # update the surveillance_clusters collection to reflect the most recent data
        self.update_surveillance_clusters()

        # Apply first filter: Remove clusters that involve only a single patient
        self.filter_same_patient_clusters()

        # Apply second filter: Remove clusters that don't include any newly identified isolates
        self.filter_old_clusters()

        # Write out the report
        self.generate_report_file()

    def prepare_output_subdirectory(self):
        if not self.output_subdirectory.is_dir():
            subprocess.run(['mkdir', '-p', str(self.output_subdirectory)])

    #################################################################################################################################################
    #
    #   External-class data manipulations
    #
    #       Methods that involve using other classes (DistanceMatrixManipulation, QcAmrQuery, PatientAstQuery, SurveillanceClusters) to gather and
    #       manipulate datasets 
    #
    #################################################################################################################################################
    def distance_matrix_import(self):
        """
        Initialize an object of class DistanceMatrixManipulation to manage import of distance matrix from input file
        """
        self.distance_matrix_manipulator = DistanceMatrixManipulation(self.output_subdirectory, self.distance_matrix_file, self.clustering_threshold, self.logger)
        try:
            self.distance_matrix_manipulator.distance_matrix_to_dataframe()
            self.sorted_distance_matrix_dataframe = self.distance_matrix_manipulator.sorted_distance_matrix_dataframe.copy()
            self.distance_matrix_manipulator.dataframe_to_pairwise_distance()
            self.distance_matrix_manipulator.threshold_filter()
            self.distance_matrix_manipulator.multi_index_series_to_dict()
            self.distance_matrix_manipulator.generate_potential_transmission_itol_dataset()
        except InvalidInputError as err:
            self.logger.log(str(err))
            self.logger.log("Can not continue analysis of this cluster, aborting and moving on")
        except BadPathError as err:
            self.logger.log(str(err))
            self.logger.log("WARNING: Due to the missing file, the iTOL transmission file will not be generated for this distance matrix")
    
    def query_qc_and_amr(self):
        """
        Initialize an object of class QcAmrQuery to collect and filter the QC and AMR data for isolates implicated in outbreak clusters
        """
        self.qc_amr_query = QcAmrQuery(self.db, self.outbreak_cluster_dataframe, self.logger, self.pickle_jar, verbosity=3)
        try:
            self.qc_amr_query.validate_outbreak_cluster_dataframe()
            self.qc_amr_query.gather_isolate_metadata()
            self.qc_amr_query.filter_qc_and_amr_dataframes_for_most_recent()
            self.qc_amr_query.filter_amr_dataframe()
            self.qc_amr_query.reformat_amr_dataframe()
            self.qc_dataframe = self.qc_amr_query.qc_dataframe.copy()
            self.amr_dataframe = self.qc_amr_query.amr_dataframe.copy()
            self.filtered_amr_dataframe = self.qc_amr_query.filtered_amr_dataframe.copy()
            self.reformatted_amr_dataframe = self.qc_amr_query.reformatted_amr_dataframe.copy()
        except InvalidInputError as err:
            self.logger.log(str(err))
            self.logger.log("Due to the uncaught error, no metadata will be collected for this run")
    
    def merge_qc_amr(self):
        """
        merge the results of query_qc_amr()) into the context dataframe
        """
        self.logger.log_section_header("Merging QC and AMR results")
        self.logger.log_explanation("Combining the outbreak cluster information with the QC, MLST, and reformatted AMR data collected from the bi-db")

        self.context_dataframe = self.outbreak_cluster_dataframe.copy()

        # first merge the QC results into the context dataframe. Validate 1:1 (we should have filtered for a single QC entry in qc_amr_query() with filter_qc_and_amr_dataframes_for_most_recent())
        try:
            self.context_dataframe = self.context_dataframe.merge(self.qc_dataframe, how='left', on='mrsnId', validate="1:1")
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: QC results were not 1:1 with the context dataframe"))
            sys.exit()

        # second, merge the reformatted amr results into the context dataframe. Validate 1:1 (otherwise this indicates that the AMR reformatting step didn't collapse AMR alleles correctly)
        try:
            self.context_dataframe = self.context_dataframe.merge(self.reformatted_amr_dataframe, how='left', left_on='mrsnId', right_on=self.reformatted_amr_dataframe.index, validate="1:1")
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: AMR results were not 1:1 with the context dataframe"))
            sys.exit()
        
    def query_existing_surveillance_clusters(self):
        """
        Initialize an object of class SurveillanceClusters to collect information about previously identified outbreak clusters
        """
        self.surveillance_clusters = SurveillanceClusters(self.db, self.logger, verbosity=self.verbosity)
        self.surveillance_clusters.gather_surveillance_cluster_data(self.context_dataframe)

    def merge_existing_clusters(self):
        """
        merge the results of query_existing_surveillance_clusters() into the context dataframe
        """
        self.logger.log_section_header("Merging existing outbreak cluster information")
        self.logger.log_explanation("Combining the outbreak cluster information with previously observed cluster information from the bi-db")

        try:
            self.context_dataframe = self.context_dataframe.merge(self.surveillance_clusters.surveillance_clusters_dataframe, how='left', on='mrsnId', validate="1:1")
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: Existing cluster results were not 1:1 with the context dataframe. Potentially duplicate entries in the BI-DB"))
            log_a_list(self.surveillance_clusters.surveillance_clusters_dataframe['mrsnId'].loc[self.surveillance_clusters.surveillance_clusters_dataframe['mrsnId'].duplicated(keep=False)].to_list()))
            sys.exit()
        
    def query_patient_and_ast(self):
        """
        Initialize an object of class PatientAstQuery to collect and filter patient metadata for isolates implicated in outbreak clusters
        """
        self.patient_ast_query = PatientAstQuery(self.db, self.context_dataframe, self.logger, self.pickle_jar, verbosity=3)
        try:
            self.patient_ast_query.validate_context_dataframe()
            self.patient_ast_query.query_patients_collection()
            self.patient_ast_query.filter_and_reformat_ast()
            self.context_dataframe = self.patient_ast_query.context_dataframe.copy()
            self.patient_dataframe = self.patient_ast_query.patient_dataframe.copy()
            self.ast_dataframe = self.patient_ast_query.ast_dataframe.copy()
            self.reformatted_ast_dataframe = self.patient_ast_query.reformatted_ast_dataframe.copy()
        except InvalidInputError as err:
            self.logger.log(str(err))
            self.logger.log("Due to the uncaught error, no patient or AST metadata will be collected for this run")
    
    def merge_patient_ast(self):
        """
        merge the results of query_patient_and_ast() into the context dataframe
        """
        self.logger.log_section_header("Merging patient metadata and AST results")
        self.logger.log_explanation("Combining the outbreak cluster information with the patient metadata and AST results data collected from the bi-db")

        # first merge the patient metadata. Since a given patientCollectionId can correspond to more than one mrsnId <shudder> this validation is many-to-1
        try:
            self.context_dataframe = self.context_dataframe.merge(self.patient_dataframe, how='left', on='patientsCollectionId', validate="m:1")
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: Patient metadata results were not 1:m with the context dataframe"))
            sys.exit()
        
        # second merge in the reformatted ast results. Like with the metadata this will also require a many-to-1 validation scheme
        try:
            self.context_dataframe = self.context_dataframe.merge(self.reformatted_ast_dataframe, how='left', left_on='patientsCollectionId', right_on=self.reformatted_ast_dataframe.index, validate='m:1')
        except MergeError as err:
            self.logger.log(str(err))
            self.logger.log(log.bold_red("FATAL ERROR: Reformatted AST results were not 1:m with the context dataframe"))
            sys.exit()
    
    def update_surveillance_clusters(self):
        """
        Use the instance of SurveillanceClusters initialized in existing_surveillance_clusters_query() to update the collection to reflect
        any new information from this run
        """
        self.surveillance_clusters.update_surveillance_clusters_collection(self.context_dataframe, self.distance_matrix_manipulator)


    #################################################################################################################################################
    #
    #   Outbreak Cluster Detection and Filtering
    #
    #################################################################################################################################################
    def outbreak_cluster_detection(self):
        """
        Perform potential outbreak cluster detection using DBSCAN
        """
        self.logger.log_section_header('Performing potential outbreak cluster detection')
        self.logger.log(log.dim('   a) Use DBSCAN to perform cluster assignments for all isolates'))
        self.logger.log(log.dim('   b) Import metadata (if available)'))
        self.logger.log(log.dim('      i) Remove sets containing ONLY isolates from a single patient'))
        self.logger.log(log.dim('   c) Remove sets containing ONLY isolates from a single patient'))
        self.logger.log(log.dim('   d) Reconstruct distance matrices for detected clusters'))

        self.logger.log('\nRunning DBSCAN with {} and {}'.format(log.bold_green('eps={}'.format(self.clustering_threshold)),log.bold_green('min_samples=2\n')))

        # a) Use DBSCAN to perform cluster assignments for all isolates
        db = DBSCAN(eps=self.clustering_threshold, min_samples=2, metric='precomputed').fit_predict(self.sorted_distance_matrix_dataframe)
        clusters = []

        if self.debug:
            print(db)

        for cluster in set(db):
            if cluster != -1:
                clusters.append(sorted([i[1] for i in set(zip(db, self.sorted_distance_matrix_dataframe.index)) if i[0] == cluster]))

        self.logger.log('\nNumber of clusters detected before filtering: ' + str(len(clusters)))
        for cluster in range(0, len(clusters)):
            self.logger.log('\n' + log.underline('Cluster ' + str(cluster + 1) + ' (n=' + str(
                len(clusters[cluster])) + ')' + ' contains the following isolates:'))
            for isolate in clusters[cluster]:
                self.logger.log('   ' + str(isolate))

        # Generate a dataframe of clustered isolates with metadata
        flattened_cluster_dict = {'mrsnId': [], 'cluster': []}
        for cluster in range(0, len(clusters)):
            for isolate in clusters[cluster]:
                flattened_cluster_dict['mrsnId'].append("MRSN" + isolate)
                flattened_cluster_dict['cluster'].append(cluster)

        if self.debug:
            print(flattened_cluster_dict)

        column_list = ['mrsnId', 'cluster']

        self.outbreak_cluster_dataframe = pd.DataFrame(flattened_cluster_dict, columns=column_list).astype({'mrsnId': 'str'})

        if self.debug:
            print(self.outbreak_cluster_dataframe)

    def clean_up_dates(self):
        """
        clean up date fields in context DataFrame
        """
        for index in self.context_dataframe.index:
            if not isinstance(self.context_dataframe.loc[index, "dateOfCulture"], datetime.date) and self.context_dataframe.loc[index, "dateOfCulture"] is not np.nan:
                try:
                    self.context_dataframe.loc[index, "dateOfCulture"] = datetime.datetime.strptime(self.context_dataframe.at[index, "dateOfCulture"], '%d-%m-%Y')
                except TypeError:
                    print("experienced a typeerror")
                    print(self.context_dataframe.at[index, "dateOfCulture"])

    def establish_new_vs_old_sample(self):
        """
        If a sample already belongs to a named outbreak cluster, the sample is old aka previously observed. Otherwise the sample is considered
        new aka not previously implicated in an outbreak cluster. Record this information as a boolean in the column "isNew"
        """
        self.logger.log_section_header("Annotating Previously Observed Isolates")
        self.logger.log_explanation("Adding a column to the context DataFrame, 'isNew', that is True if an isolate has not previsouly been implicated in a potential transmission cluster")

        print(self.context_dataframe.columns)

        self.context_dataframe['isNew'] = False
        mask = pd.isnull(self.context_dataframe['clusterName'])
        self.context_dataframe.loc[mask,'isNew'] = True

    def filter_same_patient_clusters(self):
        """
        Create a new subset of the context dataframe, context_dataframe_multipatient, that contains only information on clusters that contain at least 
        two distinct patients
        """
        self.logger.log_section_header("Removing Same Patient Clusters")
        self.logger.log_explanation("Removing potential outbreak clusters that contain isolates from only one patient (i.e. serial isolates)")

        grouped_by_cluster = self.context_dataframe.groupby("clusterName")

        for name, group in grouped_by_cluster:

            # add back the clusterName field
            group.loc[:,"clusterName"] = name

            self.logger.log(log.bold("\nNow analyzing cluster: {}".format(name)))
            
            if self.debug:
                print(group[["mrsnId", "patientId", "armordPatientId"]])

            # if all isolates have a patientId value, we can use this to assess the number of patients involved
            if group["patientId"].notna().all():  # returns True if all isolates have a patientId in the context dataframe
                if len(set(group["patientId"].to_list())) == 1:
                    self.logger.log("Cluster {} only contains isolates from one patient ({}), so it will be filtered".format(name, group["patientId"].to_list()[0]))
                    continue
                else:
                    self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                    continue

            # if all isolates have a armordPatientId value, we can use this to assess the number of patients involved
            if group["armordPatientId"].notna().all():  # returns True if all isolates have a armordPatientId in the context dataframe
                if len(set(group["armordPatientId"].to_list())) == 1 and len(set(group["patientId"].loc[group["patientId"].notna()])) <= 1:
                    self.logger.log("Cluster {} only contains isolates from one armordPatientId ({}), so it will be filtered".format(name, group["patientId"].to_list()[0]))
                    continue
                else:
                    self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                    continue

            # in all other cases we will have insufficient patient information to confidently filter out a cluster programmatically, so the remaining code only
            # determines the logging method prior to retaining the cluster

            # if some isolates have a patientId and the remainder have an M#, attempt to reconcile these values and see if multiple patients are involved
            if group["patientId"].notna().any() and group["armordPatientId"].notna().any():  # returns True if any isolates have a patientId or an M# in the context dataframe
                
                # first see if the isolates with patientId values represent 2+ patients, which will automatically qualify the cluster for inclusion
                if len(set(group["patientId"].loc[group["patientId"].notna()])) > 1:
                    self.logger.log("Cluster {} contains some isolates without patientIds, but 2+ patientIds are present, so it will be retained".format(name))
                    self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                    continue

                # if only one patientId is present, see if there are more than one M# present, which also qualifies the cluster to move forward
                if len(set(group["armordPatientId"].loc[group["armordPatientId"].notna()])) > 1:
                    self.logger.log("Cluster {} contains some isolates without patientIds, but 2+ armordPatientIds are present, so it will be retained".format(name))
                    self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                    continue

            # if any isolates don't have a patientId OR an M#, keep the cluster and log a warning that same patient filtering couldn't be performed reliably
            double_null = False
            for index in group.index.tolist():
                if pd.isna(group.loc[index, "patientId"]) and pd.isna(group.loc[index, "armordPatientId"]):
                    self.logger.log("WARNING: Isolate {} has neither a patientId or an M#. Out of an abundance of caution we will pass this cluster ({}) through".format(group.at[index, "mrsnId"], name))
                    double_null = True
            if double_null:
                self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
                continue
            
            # if we got this far, it appears that there are patientIds and M#'s for all samples but no overlap, so we keep the cluster
            self.logger.log("There is insufficient information to determine the number of patients in this cluster. Out of an abundance of caution we will pass this cluster ({}) through".format(name))
            self.context_dataframe_multipatient = pd.concat([self.context_dataframe_multipatient, group])
            continue

    def filter_old_clusters(self):
        """
        Create a new subset of the context dataframe, context_dataframe_new_only, from context_dataframe_multipatient, that contains only information
        on clusters that contain at least one sample that was identified as "new"
        """
        self.logger.log_section_header("Removing clusters containing only previously reported on isolates")
        self.logger.log_explanation("Removing potential outbreak clusters that contain isolates that are all located in the surveillance clusters collection")

        grouped_by_cluster = self.context_dataframe_multipatient.groupby("clusterName")

        for name, group in grouped_by_cluster:

            if (group["isNew"] == True).any():  # returns True if any isolate in the cluster is new
                self.context_dataframe_new_only = pd.concat([self.context_dataframe_new_only, group])
            else:
                self.logger.log("Cluster {} contained no new isolates, so it will be filtered".format(name))

    #################################################################################################################################################
    #
    #   Checkpointing: temporary storage of mrsnId to patientCollectionId relationships that were determined during the run. Allows the user
    #   to stop/abort/error out of the current run without losing the progress they already made
    #
    #   For more info, see class PickleJar below
    #
    ################################################################################################################################################# 
    def add_patient_collection_ids_from_pickle_jar(self):
        """
        use the contents of the pickle jar to catch up any progress that was made on assigning patientCollectionId's from the last run
        """
        # add in any patient_collection_ids that were found in the picklejar
        self.pickle_jar.read_a_pickle()
        
        if self.pickle_jar.patient_collection_ids:
            for k,v in self.pickle_jar.patient_collection_ids.items():
                
                self.context_dataframe.loc[self.context_dataframe["mrsnId"] == k, "patientsCollectionId"] = v
    
    #################################################################################################################################################
    #
    #  Output/Report Generation
    #
    #################################################################################################################################################
    def generate_report_file(self):
        """
        prepare the final report document, writing out the filtered and raw files to an Excel Spreadsheet
        """
        self.logger.log_section_header("Generating the report file(s) for this run")
        self.logger.log_explanation("Generating an Excel spreadsheet that contains detected clusters under various filtering conditions as well as raw data")
    
        with pd.ExcelWriter(self.outbreak_cluster_file, mode='w', engine='openpyxl') as writer:
            self.context_dataframe_new_only.to_excel(writer, sheet_name="Clusters - New Isolates", index=False)
            self.context_dataframe_multipatient.to_excel(writer, sheet_name="Clusters - Multiple Patients", index=False)
            self.context_dataframe.to_excel(writer, sheet_name="Clusters - All", index=False)
            self.qc_dataframe.to_excel(writer, sheet_name="QC Summary", index=False)
            self.filtered_amr_dataframe.to_excel(writer, sheet_name="AMR Summary", index=False)
            self.patient_dataframe.to_excel(writer, sheet_name="Patient Summary", index=False)
            self.ast_dataframe.to_excel(writer, sheet_name="AST Summary", index=False)


#################################################################################################################################################
#
#  Class DistanceMatrixManipulation
#
#   Class DistanceMatrixManipulation is responsible for any and all tasks related to importing and manipulating the distance matrix that informs
#   cluster detection
#
#################################################################################################################################################
class DistanceMatrixManipulation:

    def __init__(self, output_directory, distance_matrix_file, clustering_threshold, logger, verbosity=2):

        self.output_directory = output_directory
        self.distance_matrix_file = distance_matrix_file
        self.clustering_threshold = clustering_threshold
        self.logger = logger
        self.verbosity = verbosity
        self.debug = False
        if self.verbosity == 3:
            self.debug = True

        self.distance_matrix_dataframe = None  # used to store the distance matrix as a pandas DataFrame
        self.sorted_distance_matrix_dataframe = None
        self.pairwise_series = None  # reorganization of the distance matrix into a multi-index pairwise pandas Series object
        self.filtered_pairwise_series = None  # multi-index pairwise pandas Series object with only values <= the threshold retained
        self.series_as_dict = None  # information from the filtered_pairwise_series as a dict
        self.multi_index_dict = None # reorganization of the filtered_pairwise_series Object into a multi-index python dict

        self.potential_transmission_pairs_file = self.output_directory / 'potential_transmission_pairs.csv'  # path to the potential transmission pairs .csv file
        self.potential_transmission_itol_dataset_txt_file = self.output_directory / 'potential_transmission_iTOL_dataset.txt'  # path to the potential transmission pairs iTOL dataset file

    def distance_matrix_to_dataframe(self):
        """
        read the distance matrix file into a pandas DataFrame. Map the index and column names to strings, sort on both axes, and ensure that it is square
        """
        self.logger.log_section_header('Converting distance matrix in {} to pandas Dataframe object'.format(str(self.distance_matrix_file)))

        try:
            self.distance_matrix_dataframe = pd.read_excel(self.distance_matrix_file, index_col=0, header=0, engine='openpyxl')
        except ValueError:
            raise InvalidInputError(['distance matrix'], 'The distance matrix file is misformatted')

        if self.debug:
            print(self.distance_matrix_dataframe.head(20))

        self.distance_matrix_dataframe.index = self.distance_matrix_dataframe.index.map(str)
        self.distance_matrix_dataframe.columns = self.distance_matrix_dataframe.columns.map(str)
        self.sorted_distance_matrix_dataframe = self.distance_matrix_dataframe.sort_index(axis=0).sort_index(axis=1)
        
        if not self.sorted_distance_matrix_dataframe.shape[0] == self.sorted_distance_matrix_dataframe.shape[1]:
            raise InvalidInputError(['distance matrix dataframe'], 'The distance matrix must be square!')

        self.logger.log("The distance matrix has been imported and is {}x{}".format(str(self.sorted_distance_matrix_dataframe.shape[0]), str(self.sorted_distance_matrix_dataframe.shape[0])))

        if self.debug:
            print(self.sorted_distance_matrix_dataframe.head(20))

    def dataframe_to_pairwise_distance(self):
        """
        convert the dataframe to a pairwise distance multi-index Series object
        """
        self.logger.log_section_header('Converting Dataframe to multi-index Series object')
        self.logger.log(
            log.dim('   a) Create an upper triangle matrix from the distance matrix to remove redundant information'))
        self.logger.log(log.dim('   b) Generate multi-index Series object from the upper triangle matrix using stack()\n'))

        seqsphere_upper_tri_matrix = self.sorted_distance_matrix_dataframe.where(np.triu(np.ones(self.sorted_distance_matrix_dataframe.shape), 1).astype(bool))
        self.seqsphere_pairwise = seqsphere_upper_tri_matrix.stack()

        if self.debug:
            print(self.seqsphere_pairwise)

    def threshold_filter(self):
        """
        apply the threshold filter
        """
        self.logger.log_section_header('Applying threshold filter')
        self.logger.log(
            log.dim('   a) Filter and drop all series entries where the pairwise distance is greater than the threshold\n'))

        starting_len = self.seqsphere_pairwise.shape[0]

        self.filtered_pairwise_series = self.seqsphere_pairwise.mask(self.seqsphere_pairwise >= self.clustering_threshold).dropna()

        ending_len = self.filtered_pairwise_series.shape[0]

        if self.debug:
            print(self.filtered_pairwise_series)

        self.logger.log("Retained {} of {} pairwise distances that were below the threshold of {}".format(str(ending_len), str(starting_len), str(self.clustering_threshold)))

    def multi_index_series_to_dict(self):
        """
        Convert the multi-index series into a dictionary
        """
        self.logger.log_section_header('Converting multi-index Series object to dictionary')
        self.logger.log(log.dim('   a) Convert the Series object to a dictionary of form {(isolate 1, isolate2): distance}\n'))

        self.series_as_dict = self.filtered_pairwise_series.to_dict()

        if self.debug:
            self.logger.log('Printing result of multi_index_series_to_dict() for debugging')
            self.logger.log('Isolate 1\tIsolate 2\tDistance')
            for isolate_pair, distance in self.series_as_dict.items():
                self.logger.log(str(isolate_pair[0]) + '\t' + str(isolate_pair[1]) + '\t' + str(distance))

    def generate_potential_transmission_itol_dataset(self):
        self.logger.log_section_header('Generating potential transmission dataset for iTOL')
        self.logger.log(
            log.dim('   a) Write output to potential_transmission_pairs.csv as "isolate 1, isolate 2, allelic distance"'))
        self.logger.log(log.dim('   b) Check for the iTOL template file'))
        self.logger.log(log.dim('   c) Write potential transmission data to potential_transmission_iTOL_dataset.txt'))

        self.logger.log('\nNow generating potential transmission pair file: ' + log.green(str(self.potential_transmission_pairs_file)))

        with open(self.potential_transmission_pairs_file, 'w') as output_csv:
            csv_writer = csv.writer(output_csv, delimiter=',')
            for isolate_pair, allelic_distance in self.series_as_dict.items():
                csv_writer.writerow([isolate_pair[0], isolate_pair[1], allelic_distance])

        if self.debug:
            with open(self.potential_transmission_pairs_file, 'r') as output_csv:
                csv_reader = csv.reader(output_csv, delimiter=',')
                for line in csv_reader:
                    self.logger.log(line)

        itol_template_file = Path(__file__).parent.parent / 'resources' / 'itol_connection_template.txt'

        try:
            assert itol_template_file.exists()
        except AssertionError:
            raise BadPathError(itol_template_file, "itol template file")

        self.logger.log('\nThe itol connection template file is: ' + log.green(str(itol_template_file)))
        shutil.copyfile(itol_template_file, self.potential_transmission_itol_dataset_txt_file)

        try:
            assert self.potential_transmission_itol_dataset_txt_file.exists()
        except AssertionError:
            raise BadPathError(self.potential_transmission_itol_dataset_txt_file, "potential transmission itol dataset file")

        self.logger.log('Now generating itol connection dataset: ' + log.green(str(self.potential_transmission_itol_dataset_txt_file)))

        with open(self.potential_transmission_itol_dataset_txt_file, 'a') as dataset_file:
            connecting_line_features = '5\trgba(255, 15, 15, 0.5)\tnormal\n'
            for isolate_pair, allelic_distance in self.series_as_dict.items():
                dataset_file.write(
                    str(isolate_pair[0]) + '\t' + str(isolate_pair[1]) + '\t' + connecting_line_features)

        if self.debug:  # print the dataset file to the terminal for inspection
            with open(self.potential_transmission_itol_dataset_txt_file, 'r') as debug_file:
                for line in debug_file.readlines():
                    self.logger.log(line.strip())

    def generate_subset_distance_matrix_dataframe(self, cluster_name, context_dataframe):
        """
        return a subset of self.distance_matrix_dataframe that only contains the entries specific to cluster_name
        """
        self.logger.log_section_header("Creating the subset distance matrix DataFrame for cluster {}".format(cluster_name))
        self.logger.log_explanation("Filtering out all non-cluster implicated samples from the input distance matrix DataFrame")

        # get sample_list, the list of mrsnId implicated in the cluster cluster_name
        context_subset = context_dataframe.loc[context_dataframe["clusterName"] == cluster_name]
        sample_list = context_subset["mrsnId"].to_list()

        # create a subset distance matrix dataframe that only includes samples in the current cluster
        subset_distance_dataframe = self.sorted_distance_matrix_dataframe.drop(columns=[col for col in self.sorted_distance_matrix_dataframe.columns.to_list() if col not in sample_list])
        subset_distance_dataframe = subset_distance_dataframe.drop(index=[col for col in subset_distance_dataframe.index.to_list() if col not in sample_list])

        return subset_distance_dataframe


#################################################################################################################################################
#
#  Class QcAmrQuery
#
#   Class QcAmrQuery is responsible for querying the sequenced_isolates collection of the bi-db and parsing the results
#   to populate the qc_dataframe and amr_dataframe. It includes methods to performing filtering operations for cases where
#   multiple good run data is available for a sample. It can also filter AMR and reformat it to one line per sample
#
#################################################################################################################################################
class QcAmrQuery:

    def __init__(self, db, outbreak_cluster_dataframe, logger, pickle_jar, verbosity=2):
        
        self.db = db
        self.outbreak_cluster_dataframe = outbreak_cluster_dataframe
        self.logger = logger
        self.verbosity = verbosity  # allows for the logging in this section to be selectively printed to the terminal

        # primary output
        self.qc_dataframe = pd.DataFrame()
        self.amr_dataframe = pd.DataFrame()

        self.filtered_amr_dataframe = None

        # the reformatted ast dataframe has been filtered and converted to one line per sample
        self.reformatted_amr_dataframe = None

    def validate_outbreak_cluster_dataframe(self):
        """
        ensure that the context dataframe passed during init contains the required columns
        """
        req_cols = [
            'mrsnId', 
        ]

        if not all(col in self.outbreak_cluster_dataframe.columns for col in req_cols):
            raise InvalidInputError(['outbreak cluster dataframe'], 'Missing at least one required column!')

    def gather_isolate_metadata(self):
        """
        collect the isolate metadata from the bi-db's sequenced_isolates collection
        """
        self.logger.log_section_header("Gathering Isolate Data from BI-DB")
        self.logger.log_explanation("Collecting available species ID, MLST, and AMR information from the BI-DB")

        qc_dataframe_fields = [
            'mrsnId',
            'qcCallFinal',
            'chcsAccession',
            'genus',
            'wgsId',
            'sequencingRun',
            'sequencingRunDate',
            'mlst',
            'projects',
            'amr',
            'armordPatientId',
            'armordFacility',
            'armordCultureDate',
            'armordCultureType'
        ]

        acceptable_qc = [
            "PASS",
            "PASS - ID",
            "pass",
            "pass - ID"
        ]

        #samples = [''.join(["MRSN",entry]) for entry in self.outbreak_cluster_dataframe['mrsnId'].to_list()]
        samples = [entry for entry in self.outbreak_cluster_dataframe['mrsnId'].to_list()]

        query = self.db.sequenced_isolates.find({'mrsnId': { '$in': samples}, 'qcCallFinal': {'$in': acceptable_qc}}, projection=qc_dataframe_fields)

        amr_query_list = []
        qc_query_list = []  

        for document in query:

            mrsn_id = document['mrsnId']
            sequencing_run = document['sequencingRun']

            # convert the array of projects to a | separated string of projectCodes
            project_codes = []
            try:
                for project in document['projects']:
                    project_codes.append(project['projectCode'])
                project_codes_as_string = ' | '.join(project_codes)
            except TypeError:  # handle case where no projects are present
                project_codes_as_string = ""

            # replace the projects array entry with the string representation
            document['projects'] = project_codes_as_string

            query_entry = document

            # add the mrsn_id to each amr entry as mrsnId
            if 'amr' in document.keys():
                for amr in document['amr']:
                    amr['mrsnId'] = mrsn_id
                    amr['sequencingRun'] = sequencing_run
                    amr_query_list.append(amr)

                # save the qc results (document sans amr) to self.qc_query_list
                query_entry.pop('amr', None)

            qc_query_list.append(query_entry)

        # convert the qc_query_list and amr_query_list to pandas DataFrames
        self.qc_dataframe = pd.DataFrame(qc_query_list)
        self.amr_dataframe = pd.DataFrame(amr_query_list)

        # move the mrsnId column in amr_dataframe to first position
        last_col = self.amr_dataframe.pop('mrsnId')
        self.amr_dataframe.insert(0, 'mrsnId', last_col)
        last_col = self.amr_dataframe.pop('sequencingRun')
        self.amr_dataframe.insert(0, 'sequencingRun', last_col)

    def filter_qc_and_amr_dataframes_for_most_recent(self):
        """
        Some isolates have multiple records corresponding to sequencing runs that they passed QC on. In these cases we will
        default to the most recent run on which the isolate passed.
        """
        self.logger.log_section_header("Filtering Isolate QC Results for the Most Recent Passed Entry")
        self.logger.log_explanation("Some isolates have multiple records corresponding to sequencing runs that they passed QC on. In these cases we will default to the most recent run on which the isolate passed.")

        # Use pandas groupby() on mrsnId. If more than one entry is present, sort by sequencingRunDate and drop all but the most recent
        grouped_by_mrsn_id = self.qc_dataframe.groupby(['mrsnId'])
        for name, group in grouped_by_mrsn_id:
            if len(group.index) == 1:
                continue
            
            self.logger.log("{} has more than one PASS or PASS - ID qcCallFinal status in the sequenced_isolates collection".format(name), verbosity=3)
            group = group.sort_values('sequencingRunDate', ascending=False)
            for index in group.index:
                self.logger.log("[{}]: {} - {} - {}".format(index, group.at[index, "sequencingRun"], group.at[index, "sequencingRunDate"], group.at[index, "qcCallFinal"]), verbosity=3)
            
            selected_index = group['sequencingRunDate'].first_valid_index()
            self.logger.log("Keeping index: {}".format(str(selected_index)), verbosity=3)

            # edge case where NO dates are available, just go with the first entry
            if selected_index == None:
                selected_index = group.index.tolist()[0]

            for index in group.index.tolist():
                if index != selected_index:
                    self.qc_dataframe.drop(index=index, inplace=True)

                    # drop entries from the AMR dataframe that are no longer represented in the QC dataframe as well
                    mrsnId = group.at[index, "mrsnId"]
                    run_id = group.at[index, "sequencingRun"]
                    self.amr_dataframe.drop(self.amr_dataframe[(self.amr_dataframe["mrsnId"] ==  mrsnId) & (self.amr_dataframe["sequencingRun"] != run_id)].index, inplace = True)
        
        # verify that we now have only one result per mrsnId
        grouped_by_mrsn_id = self.qc_dataframe.groupby(['mrsnId'])
        for name, group in grouped_by_mrsn_id:
            if len(group.index.to_list()) > 1:
                self.logger.log("WARNING: {} still has more than one entry!".format(name), verbosity=3)
            
    def filter_amr_dataframe(self, amr_tier_filter="2"):
        """
        filter amr alleles by tier or expected phenotype
        """
        self.logger.log_section_header("Filtering the AMR DataFrame")
        self.logger.log_explanation("Removing AMR results that are less than tier {}".format(amr_tier_filter))

        if amr_tier_filter == "1":
            self.filtered_amr_dataframe = self.amr_dataframe.loc[self.amr_dataframe['tier1'] == True]
        elif amr_tier_filter == "2":
            self.filtered_amr_dataframe = self.amr_dataframe.loc[(self.amr_dataframe['tier1'] == True) | (self.amr_dataframe['tier2'] == True)]
        elif amr_tier_filter == "3":
            self.filtered_amr_dataframe = self.amr_dataframe.loc[(self.amr_dataframe['tier1'] == True) | (self.amr_dataframe['tier2'] == True) | (self.amr_dataframe['tier3'] == True)]
        else:
            self.filtered_amr_dataframe = self.amr_dataframe
        
    def reformat_amr_dataframe(self, amr_tier_filter="2"):
        """
        transform the amr dataframe to one line per sample format
        """
        self.logger.log_section_header("Reformatting the AMR DataFrame")
        self.logger.log_explanation("Transforming the AMR results to one line per sample format")
        
        amr_by_expected_phenotype = self.filtered_amr_dataframe.groupby(['expectedPhenotype'])

        amr_expected_phenotypes = amr_by_expected_phenotype.groups.keys()
        self.reformatted_amr_dataframe = pd.DataFrame(columns=amr_expected_phenotypes)

        amr_by_expected_phenotype = self.filtered_amr_dataframe.groupby(['mrsnId','expectedPhenotype'])
        for name, group in amr_by_expected_phenotype:
            families = " | ".join(sorted(list(set(group.mrsnSuperFamily.to_list())), reverse=True))
            self.reformatted_amr_dataframe.at[name[0], name[1]] = families


#################################################################################################################################################
#
#  Class PatientAstQuery
#
#   Class PatientAstQuery is responsible for querying the patients collection of the bi-db and parsing the results
#   to populate the patient_dataframe and ast_dataframe. This process may involve user interaction when ambiguous 
#   results are returned
#
#################################################################################################################################################
class PatientAstQuery:

    def __init__(self, db, context_dataframe, logger, pickle_jar, verbosity=2):
        
        self.db = db
        self.context_dataframe = context_dataframe
        self.logger = logger
        self.verbosity = verbosity  # allows for the logging in this section to be selectively printed to the terminal

        # primary output
        self.patient_dataframe = pd.DataFrame()
        self.ast_dataframe = pd.DataFrame()

        # the reformatted ast dataframe has been filtered and converted to one line per sample
        self.reformatted_ast_dataframe = None
    
        self.drug_classes_of_interest = [
            'AMINOGLYCOSIDES',
            'BETA-LACTAMS - B',
            'FLUOROQUINOLONES'
        ]

        self.pickle_jar = pickle_jar
    
    def validate_context_dataframe(self):
        """
        ensure that the context dataframe passed during init contains the required columns
        """
        req_cols = [
            'mrsnId', 
            'chcsAccession', 
            'genus', 
            'patientsCollectionId'
        ]

        if not all(col in self.context_dataframe.columns for col in req_cols):
            raise InvalidInputError(['context dataframe'], 'Missing at least one required column!')
    
    def query_patients_collection(self):
        """
        Primary method of this class, orchestrates collection of patient and ast data from the bi-dbs patient collection based
        on the patientsCollectionId and chcsAccession field values in the context DataFrame. In the case where the relationship
        between a sample and a sample from a given chcsAccession is ambiguous the user is called upon to make an adjudication
        """
        # first, gather patient/AST data for any entries that have a patientsCollectionId in bulk OR were already processed and stored in the picklejar
        located_samples = self.collect_patient_and_ast_for_patient_collection_ids()

        self.remaining_context_dataframe = self.context_dataframe.loc[~self.context_dataframe['mrsnId'].isin(located_samples)]

        # generate a list of remaining CHCS values to interrogate
        remaining_samples = [chcs for chcs in self.remaining_context_dataframe['chcsAccession'].loc[self.remaining_context_dataframe['chcsAccession'].notna()].to_list()]

        # query the patients collection for the remaining samples
        query = list(self.db.patients.find({"chcsAccessionNumber": {"$in": remaining_samples}}))

        # generate the list of samples that did/didn't return any results from the patients collection
        samples_located = [chcs for chcs in remaining_samples if chcs in [doc["chcsAccessionNumber"] for doc in query]]
        samples_not_located = [chcs for chcs in remaining_samples if chcs not in samples_located]

        # DEBUG 
        sample_set = set(samples_located)
        if len(samples_located) != len(sample_set):
            self.logger.log("WARNING: the sample set is {} entries shorter than the list, indicating the potential presence of deprecated samples!".format(str(len(samples_located) - len(sample_set))), verbosity=self.verbosity)
            for entry in sample_set:
                if samples_located.count(entry) > 1:
                    self.logger.log(entry)
                    log_a_list(self.context_dataframe['mrsnId'].loc[self.context_dataframe["chcsAccession"] == entry].to_list(), self.logger)

        self.logger.log("Found the following {} accession numbers in the patients collection:".format(str(len(samples_located))))
        log_a_list(sample_set, self.logger)

        # iterate through samples located attempting to get the correct document from query
        self.logger.log_section_header("Retrieving Patient and AST data for samples with matches in the patients collection", verbosity=self.verbosity)
        self.logger.log_explanation("Querying the patients collection for patient metadata and AST data for isolates that have one or more matches on the chcsAccession/chcsAccessionNumber field", verbosity=self.verbosity)

        for chcs_accession in sample_set:

            mrsn_id = self.context_dataframe['mrsnId'].loc[self.context_dataframe["chcsAccession"] == chcs_accession].to_list()

            self.chcs_match_utility(chcs_accession, mrsn_id, query)
            
        # now we move on to iterating through the non-matching chcs values, giving the user the option to try and correct the chcs value
        self.logger.log_section_header("Retrieving Patient and AST data for samples with NO matches in the patients collection", verbosity=self.verbosity)
        self.logger.log_explanation("The isolates in this section have no matches on the chcsAccession/chcsAccessionNumber field in the patients collection. The user will have the opportunity to make corrections to these values or abandon them", verbosity=self.verbosity)

        self.logger.log("Unable to locate the following {} accession numbers in the patients collection:".format(str(len(samples_not_located))))
        log_a_list(samples_not_located, self.logger)

        samples_not_located_set = set(samples_not_located)

        for chcs_accession in samples_not_located_set:
            
            mrsn_id = self.context_dataframe['mrsnId'].loc[self.context_dataframe["chcsAccession"] == chcs_accession].to_list()

            # Try and get the user to provide a valid chcs entry
            try:
                new_chcs_accession = self.check_patients_collection_for_chcs_accession(chcs_accession, mrsn_id)
            except SampleNotFoundError as err:
                self.logger.log(str(err))
                continue

            self.logger.log("Querying the patients collection for corrected chcsAccessionNumber: {}".format(new_chcs_accession), verbosity=self.verbosity)
            query = list(self.db.patients.find({"chcsAccessionNumber": new_chcs_accession}))

            self.chcs_match_utility(new_chcs_accession, mrsn_id, query)

        # move the chcsAccessionNumber and raw_organism_id column in ast_results to first and second position
        chcs_col = self.ast_dataframe.pop('patientsCollectionId')
        raw_organism_col = self.ast_dataframe.pop('rawOrganismId')
        self.ast_dataframe.insert(0, 'patientsCollectionId', chcs_col)
        self.ast_dataframe.insert(1, 'rawOrganismId', raw_organism_col)

    def collect_patient_and_ast_for_patient_collection_ids(self):
        """
        bulk query for all samples in the context dataframe where patientsCollectionIds field is populated
        Returns a list of chcsAccession's that correspond with samples that were successfully located
        """
        self.logger.log_section_header("Retrieving Patient and AST data for samples with patientsCollectionIds", verbosity=self.verbosity)
        self.logger.log_explanation("Querying the patients collection for patient metadata and AST data for isolates that have patientCollectionIds. These values are assigned and stored in the surveillance_clusters collection of the bi-db (for samples identified in previous analyses) OR in the picklejar (for samples identified in interrupted iterations of the current run)", verbosity=self.verbosity)

        patient_collection_ids = self.context_dataframe["patientsCollectionId"].loc[self.context_dataframe["patientsCollectionId"].notna()].to_list()

        log_a_list(patient_collection_ids, self.logger)

        query = list(self.db.patients.find({"_id": {"$in": patient_collection_ids}}))

        # pass a copy of the query to add_patient_query_return_to_patients_and_ast_dataframes() to collect the patient metadata and ast results
        for doc in query:
            self.add_patient_query_return_to_patients_and_ast_dataframes(doc)

        # return a list of chcsAccession values that correspond to the successfully located docs
        located_entries = [doc["_id"] for doc in list(query)]
        located_samples = self.context_dataframe['mrsnId'].loc[self.context_dataframe["patientsCollectionId"].isin(located_entries)]

        self.logger.log("Located the following {} samples based on a valid patientsCollectionId:".format(str(len(located_samples))))
        log_a_list(located_samples, self.logger)

        return located_samples
    
    def preview_patient_and_ast(self, patient_documents):
        """
        sub function that prints a preview of each document in patient_documents
        """
        counter = 0
        for doc in patient_documents:

            doc["patientId"] = str(int(doc["patientId"]))

            counter +=1
            self.logger.log("\nPreview of document: {}".format(log.bold(str(counter))))
            self.logger.log("CHCS Accession: {}".format(doc["chcsAccessionNumber"]))
            self.logger.log("Patient ID: {}".format(doc["patientId"]))
            self.logger.log("Facility: {}".format(doc["verifiedFacilityName"]))
            try:
                self.logger.log("Collection Date: {}".format(doc["dateOfCulture"].strftime("%Y-%m-%d")))
            except AttributeError:  # collection date is misformatted
                self.logger.log("Collection Date: {}".format("COULD NOT PARSE"))
            self.logger.log("Raw Organism ID: {}".format(doc["rawOrganismId"]))
            self.logger.log("AST results:")
            try:
                for test in doc['testResults']:
                    self.logger.log("{}:\t{}".format(test["antibioticName"], test["sensitivity"]))
            except KeyError:
                self.logger.log("No AST results detected")
    
    def chcs_match_utility(self, chcs_accession, mrsn_id, query):
        """
        handle all of the possible matching cases for a given chcs_accession. The end result of any option is either a) appending the matching/selected
        result to the patient and ast DataFrames OR adding nothing (no metadata will be available for such samples)
        """

        # identify the 1+ matching documents in query
        matching_documents = [doc.copy() for doc in query if doc["chcsAccessionNumber"] == chcs_accession]

        # determine how many of the matching documents matches on the genus
        if len(mrsn_id) == 1:
            mrsn_id = mrsn_id[0]
            genus = self.context_dataframe['genus'].loc[self.context_dataframe["mrsnId"] == mrsn_id].to_list()[0]  # we can only have one genus in a cgMLST tree
        else:
            genus = self.context_dataframe['genus'].loc[self.context_dataframe["mrsnId"] == mrsn_id[0]].to_list()[0]  # we can only have one genus in a cgMLST tree
            self.logger.log("WARNING: CHCS: {} is associated with more than one mrsnId:\n{}".format(chcs_accession, "\n".join(mrsn_id)))

        # if the length of matching documents is 1, use this doc. If more than one sample, log a warning about deprecated samples
        if len(matching_documents) == 1:
            self.single_chcs_match(chcs_accession, mrsn_id, matching_documents[0])
            return

        matching_raw_organism_ids = [doc.copy() for doc in matching_documents if genus.upper() in doc["rawOrganismId"]]

        # if the length of matching_raw_organism_ids == 1
        if len(matching_raw_organism_ids) == 1:
            if type(mrsn_id) == str:  # if only one MRSN#, use this doc
                self.single_raw_organism_id_match(chcs_accession, mrsn_id, genus, matching_raw_organism_ids[0])
                return
            else:  # in the case of multiple MRSN#, we can't rule out one of the samples being a non-match, so we'll use two_mrsn_ids_one_chcs
                self.two_mrsn_id_one_chcs(chcs_accession, mrsn_id, matching_documents)
                return
    
        # if the length of matching_raw_organism_ids > 1, use the pick from list method to try and resolve the ambiguity
        if len(matching_raw_organism_ids) > 1:
            self.mutliple_raw_organism_id_matches(mrsn_id, matching_raw_organism_ids)
            return
        
        # if the length of matching_raw_organisms is 0, use the pick from list method to try and resolve the ambiguity
        if len(matching_raw_organism_ids) == 0:
            self.no_raw_organism_id_matches(mrsn_id, matching_documents)
            return 

    def single_chcs_match(self, chcs_accession, mrsn_id, matching_document):
        """
        handle the case where a chcs_accession matches to only a single entry in the patients collection
        """
        self.logger.log(log.underline("Located a single entry for CHCS {}".format(chcs_accession)))
    
        # update the patientsCollectionId field
        if type(mrsn_id) == list:
            self.logger.log("WARNING: One entry located in the patients collection but {} distinct mrsnId's, likely deprecated samples".format(str(len(mrsn_id))))
            for mrsn in mrsn_id:
                self.pickle_jar.write_a_pickle(mrsn, matching_document["_id"])
                self.update_patients_collection_id(mrsn, matching_document["_id"])
        else:
            self.pickle_jar.write_a_pickle(mrsn_id, matching_document["_id"])
            self.update_patients_collection_id(mrsn_id, matching_document["_id"])

        
        self.add_patient_query_return_to_patients_and_ast_dataframes(matching_document)

    def single_raw_organism_id_match(self, chcs_accession, mrsn_id, genus, matching_document):
        """
        handles the case where a chcs_accession AND genus match only a single entry in the patients collection for a 1 to 1 chcsAccession to mrsnId relationship
        """
        self.logger.log(log.underline("Located a single entry for CHCS {} that matches the WGS designated genus {}".format(chcs_accession, genus)))
        self.update_patients_collection_id(mrsn_id, matching_document["_id"])
        self.pickle_jar.write_a_pickle(mrsn_id, matching_document["_id"])
        self.add_patient_query_return_to_patients_and_ast_dataframes(matching_document)

    def two_mrsn_id_one_chcs(self, chcs_accession, mrsn_id, matching_documents):
        """
        handles the case where a chcs_accession matches to 2+ entries in the patients collection BUT only one is a genus match
        """
        self.logger.log("WARNING: multiple entries located in the patients collection with a single genus match and {} distinct mrsnId's. We will tackle each one separately".format(str(len(mrsn_id))))
        for mrsn in mrsn_id:
            self.logger.log(log.underline("There is one match between the WGS genus and raw organism IDs for {}:".format(mrsn)))
            self.preview_patient_and_ast(matching_documents)
            choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
            if choice:
                self.update_patients_collection_id(mrsn, choice["_id"])
                self.pickle_jar.write_a_pickle(mrsn, choice["_id"])
                self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
            else:
                self.logger.log("No patient metadata or AST will be available for this sample in the reporting")

    def mutliple_raw_organism_id_matches(self, mrsn_id, matching_documents):
        """
        handles the case where a chcs_accession AND genus match 2+ entries in the patients collection
        """
        if type(mrsn_id) == str:  # a single mrsnId was passed
            self.logger.log(log.underline("There are multiple matches between the WGS genus and raw organism IDs for {}:".format(mrsn_id)))
            self.preview_patient_and_ast(matching_documents)
            choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
            if choice:
                self.update_patients_collection_id(mrsn_id, choice["_id"])
                self.pickle_jar.write_a_pickle(mrsn_id, choice["_id"])
                self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
            else:
                self.logger.log("No patient metadata or AST will be available for this sample in the reporting")
        
        elif type(mrsn_id) == list: # a list of 2+ mrsnIds were passed
            self.logger.log("WARNING: multiple entries located in the patients collection and {} distinct mrsnId's. We will tackle each one separately".format(str(len(mrsn_id))))
            for mrsn in mrsn_id:
                self.logger.log(log.underline("There are multiple matches between the WGS genus and raw organism IDs for {}:".format(mrsn)))
                self.preview_patient_and_ast(matching_documents)
                choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
                if choice:
                    self.update_patients_collection_id(mrsn, choice["_id"])
                    self.pickle_jar.write_a_pickle(mrsn, choice["_id"])
                    self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
                else:
                    self.logger.log("No patient metadata or AST will be available for this sample in the reporting")

    def no_raw_organism_id_matches(self, mrsn_id, matching_documents):
        """
        handles the case where a chcs_accession matches to 2+ entries in the patients collection BUT none of them are a genus match
        """
        if type(mrsn_id) == str:  # a single mrsnId was passed
            self.logger.log(log.underline("There are NO matches between the WGS genus and raw organism IDs for {}:".format(mrsn_id)))
            self.preview_patient_and_ast(matching_documents)
            choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
            if choice:
                self.update_patients_collection_id(mrsn_id,choice["_id"])
                self.pickle_jar.write_a_pickle(mrsn_id, choice["_id"])
                self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
            else:
                self.logger.log("No patient metadata or AST will be available for this sample in the reporting")
        
        elif type(mrsn_id) == list: # a list of 2+ mrsnIds were passed
            self.logger.log("WARNING: multiple entries located in the patients collection and {} distinct mrsnId's. We will tackle each one separately".format(str(len(mrsn_id))))
            for mrsn in mrsn_id:
                self.logger.log(log.underline("There are NO matches between the WGS genus and raw organism IDs for {}:".format(mrsn)))
                self.preview_patient_and_ast(matching_documents)
                choice = pick_from_a_list("Select the document that corresponds to this sample", matching_documents, self.logger)
                if choice:
                    self.update_patients_collection_id(mrsn, choice["_id"])
                    self.pickle_jar.write_a_pickle(mrsn, choice["_id"])
                    self.add_patient_query_return_to_patients_and_ast_dataframes(choice)
                else:
                    self.logger.log("No patient metadata or AST will be available for this sample in the reporting")
    
    def add_patient_query_return_to_patients_and_ast_dataframes(self, document):
        """
        unpack and add the singular patient query result passed to the patient and ast dataframes
        """
        patient_metadata = []
        ast_results = []

        # to avoid potential issues with multiple samples matching to a document, work on a copy
        working_document = document.copy()
        
        patients_collection_id = working_document['_id']
        raw_organism_id = working_document['rawOrganismId']

        working_document["patientId"] = str(int(working_document["patientId"]))

        # extract the array of AST results to a new dictionary, append the chcs_number to each test object, and add to ast_results
        try:
            for test in working_document['testResults']:
                test['patientsCollectionId'] = patients_collection_id
                test['rawOrganismId'] = raw_organism_id
                ast_results.append(test)
                working_document.pop('testResults', None)
        except KeyError:
            pass
        
        # change the key "_id" to "patientsCollectionId"
        # working_document["patientsCollectionId"] = working_document.pop("_id")
        # working_document["patientsCollectionId"] = working_document["_id"]
        working_document.pop("_id",None)
        working_document["patientsCollectionId"] = patients_collection_id
        
        # drop the tests from the working_document and then append the remaining information to patient_metadata
        patient_metadata.append(working_document)

        # free my memory
        del working_document

        # convert the ast_results and patient_metadata into pandas DataFrames
        single_patient_dataframe = pd.DataFrame(patient_metadata)
        single_ast_dataframe = pd.DataFrame(ast_results)
        
        # concat these to the object attribute dataframes
        self.patient_dataframe = pd.concat([self.patient_dataframe, single_patient_dataframe], sort=True) 
        self.ast_dataframe = pd.concat([self.ast_dataframe, single_ast_dataframe], sort=True) 

    def update_patients_collection_id(self, mrsn_id, patients_collection_id):
        """
        update the patientsCollectionId field for the provided mrsn_id in the context DataFrame
        """
        self.context_dataframe.loc[self.context_dataframe["mrsnId"] == mrsn_id, "patientsCollectionId"] = patients_collection_id

    def check_patients_collection_for_chcs_accession(self, chcs_accession, mrsn_id):
        """
        sub function that solicits user edits to a chcs accession until the user gives up or 1+ documents in the patients collection matches the chcs accession
        This method is only invoked if the original CHCS accession results in no hits in the patients collection (inefficient)

        Returns a chcs_accession that is present in the patients collection OR raises a SampleNotFoundError if the user gives up
        """
        while self.db.patients.count_documents({"chcsAccessionNumber": chcs_accession}) == 0:
            self.logger.log("The current chcs accession: {} for sample(s): {} does not appear in the patients collection".format(chcs_accession, ' | '.join(mrsn_id)))
            if query_yes_no("Do you wish to rety with an alternative chcs accession?"):
                self.logger.log("Please enter the alternative CHCS entry now")
                chcs_accession = input().upper()
            else:
                # if the user can't or chooses not to correct the chcs we will not be able to collect patient metadata or AST for this sample
                raise SampleNotFoundError(chcs_accession, "patients collection", "No patient metadata or AST will be available for this sample in the reporting")

        return chcs_accession

    def filter_and_reformat_ast(self):
        """
        apply filters and reformatting to the AST results in the ast_dataframe
        """
        self.logger.log_section_header("Filtering and Reformatting the AST results", verbosity=self.verbosity)
        self.logger.log_explanation("Applying drug class filters to the AST results to retain only drugs of interest, then reformatting the results to one line per sample", verbosity=self.verbosity)

        # filter and reformat the ast_dataframe
        self.logger.log("Filtering AST results to remove entries that do not match any of the drug classes of interest:\n{}".format('\n'.join(self.drug_classes_of_interest)), verbosity=self.verbosity)
        
        self.reformatted_ast_dataframe = pd.DataFrame(columns=self.drug_classes_of_interest)

        ast_by_antibiotic_class = self.ast_dataframe.groupby(['patientsCollectionId','antibioticClass'])
        for name, group in ast_by_antibiotic_class:
            class_sensitivity = "unknown"

            if name[1] not in self.drug_classes_of_interest:
                continue

            #self.logger.log("Analyzing AST for chcsAccession {} in drug class: {}".format(name[0], name[1]), verbosity=3)
            for index in group.index:
                try:
                    drug_name = group.at[index, "antibioticName"]
                    sensitivity = group.at[index, "sensitivity"]
                except ValueError:
                    print(drug_name)
                    print(name)
                    sys.exit()

                if sensitivity == 'R':
                    #self.logger.log("AST indicates that this sample is {} to {}".format(sensitivity, drug_name), verbosity=3)
                    class_sensitivity = "R"
                elif sensitivity == 'I':
                    #self.logger.log("AST indicates that this sample is {} to {}".format(sensitivity, drug_name), verbosity=3)
                    if not class_sensitivity == "R":
                        class_sensitivity = "I"
                elif sensitivity == 'S':
                    #self.logger.log("AST indicates that this sample is {} to {}".format(sensitivity, drug_name), verbosity=3)
                    if not class_sensitivity in ["R", "I"]:
                        class_sensitivity = "S"

            self.reformatted_ast_dataframe.at[name[0], name[1]] = class_sensitivity


#################################################################################################################################################
#
#  Class SurveillanceClusters
#
#   Class SurveillanceClusters is responsible for querying the surveillance_clusters collection of the bi-db and parsing the results
#   to provide context regarding existing clusters and take advantage of MRSN-to-patient linking done previously. It is also used to 
#   create, update, and merge surveillance cluster documents based on clustering performed during the current run
#
#################################################################################################################################################
class SurveillanceClusters:

    def __init__(self, db, logger, verbosity=2):
        
        self.db = db
        self.logger = logger
        self.verbosity = verbosity  # allows for the logging in this section to be selectively printed to the terminal
        self.debug=False
        if self.verbosity == 3:
            self.debug = True

        self.surveillance_clusters_dataframe = None
    
    def gather_surveillance_cluster_data(self, context_dataframe):
        """
        Query the surveillance_clusters collection of the bi-db to get relevant information
        """
        self.context_dataframe = context_dataframe

        self.logger.log_section_header("Gathering Potential Transmission Cluster Data from BI-DB")
        self.logger.log_explanation("Collecting available information on previously identified potential transmission clusters from the BI-DB's surveillance_clusters collection")

        samples = self.context_dataframe["mrsnId"].to_list()
        
        query = self.db.surveillance_clusters.find( { "isolates.mrsnId": { "$in": samples } } )  # gather all active clusters that have a sample in samples

        column_list = ["mrsnId", "clusterName", "clusterNickName", "patientsCollectionId", "dateAdded", "priorClusters"]

        clusters_query_list = []
        for doc in query:

            if not doc["isActive"]:
                continue

            cluster_name = doc["_id"]
            cluster_nick_name = doc["clusterNickName"]
            
            # unpack the isolates embedded object list
            for isolate in doc['isolates']:
                isolate["clusterName"] = cluster_name
                isolate["clusterNickName"] = cluster_nick_name
                clusters_query_list.append(isolate)
        
        self.surveillance_clusters_dataframe = pd.DataFrame(clusters_query_list, columns=column_list)

        if self.debug:
            print(self.surveillance_clusters_dataframe)  

    def update_surveillance_clusters_collection(self, context_dataframe, distance_matrix_manipulation_object):
        """
        main method of SurveillanceClusters. Manages updating the surveillance_clusters collection of the bi-db to reflect new samples/data
        """
        self.context_dataframe = context_dataframe
        self.distance_matrix_manipulation_object = distance_matrix_manipulation_object

        self.logger.log_section_header("Updating the surveillance clusters collection in the bi-db")
        self.logger.log_explanation("Creating, modifying, or merging detected potential transmission clusters as necessary based on the latest data")

        # group the context dataframe by cluster
        context_dataframe_by_cluster = self.context_dataframe.groupby(["cluster"])
        for name, group in context_dataframe_by_cluster:

            self.logger.log("Processing cluster: {}".format(log.bold(str(name))))
            self.logger.log("This cluster contains {} entries".format(log.bold(str(len(group.index.to_list())))))

            # sort by date of culture since this will be the basis of naming/renaming clusters
            group = group.sort_values('dateOfCulture')

            cluster_names = group["clusterName"].loc[group["clusterName"].notna()].to_list()
            cluster_has_new = any(group["isNew"] == True)

            self.logger.log("This cluster contains {} entries with existing cluster names".format(log.bold(str(len(cluster_names)))))
            for cluster_name in cluster_names:
                self.logger.log("\t{}".format(cluster_name))
            
            # case 1: all samples are previously described and have the same clusterName (no action required)
            if len(cluster_names) == 1 and not cluster_has_new:
                self.logger.log("No changes detected for cluster {}".format(cluster_names[0]), verbosity=3)
                continue

            # case 2: 1+ new samples, 1+ samples have a clusterName, only one clusterName (update surveillance document)
            if len(cluster_names) == 1 and cluster_has_new:
                cluster_name = cluster_names[0]
                self.update_surveillance_cluster_document(cluster_name, group)
                continue

            # case 3: 1+ new samples, 1+ samples have a clusterName, 2+ clusterNames (merge surveillance documents)
            if len(cluster_names) > 1 and cluster_has_new:
                self.merge_surveillance_cluster_documents(group)
                continue

            # case 4: ALL new samples (create surveillance document)
            if len(cluster_names) == 0 and cluster_has_new:
                self.create_surveillance_cluster_document(name, group)
                continue
            
            # case 5: all samples are previously described, 2+ clusterNames (WARNING + merge surveillance documents)
            if len(cluster_names) > 1 and not cluster_has_new:
                pass

    def summarize_surveillance_cluster_document(self, doc_id):
        """
        log a summary of the surveillance cluster document with _id doc_id
        """
        query = self.db.surveillance_clusters.find_one({"_id": doc_id})

        if query["isActive"]:
            cluster_status = "Active"
        else:
            cluster_status = "Deactivated"

        # collect all values for fields of interest into a dictionary
        isolate_info_dict = {"verifiedFacilityName": [], "dateOfCulture": [], "specificPatientLocation": [], "cultureType": []}
        patients_collection_ids = [isolate["patientsCollectionId"] for isolate in query["isolates"]]
        patients_collection_query = self.db.patients.find({"_id": { "$in": patients_collection_ids } }, projection=isolate_info_dict.keys())
        for doc in patients_collection_query:
            for key in isolate_info_dict.keys():
                isolate_info_dict[key].append(doc[key])

        list_of_facilities = sorted(list(set(isolate_info_dict["verifiedFacilityName"])))
        list_of_culture_types = sorted(list(set(isolate_info_dict["cultureType"])))
        

        # log out the significant information
        self.logger.log("Cluster Name: {}".format(log.bold(query["_id"])))
        self.logger.log("Cluster Nick Name: {}".format(log.bold(query["clusterNickName"])))
        self.logger.log("Cluster Status: {}".format(log.bold(cluster_status)))

    def create_surveillance_cluster_document(self, cluster, subset_dataframe):
        """
        For entirely NEW clusters, create a new document in the bi-db's surveillance_clusters collection.
        """
        self.logger.log_section_header("Creating a new entry for cluster {} in BI-DB's surveillance_clusters collection".format(cluster))
        self.logger.log_explanation("Documents in the surveillance_clusters collection track isolates implicated in potential transmission clusters through time")

        # get the subset of the context dataframe that pertains to the cluster_name
        cluster_subset_dataframe = self.distance_matrix_manipulation_object.generate_subset_distance_matrix_dataframe(cluster, self.context_dataframe)

        # get all of the _ids (aka cluster names) in the surveillance_clusters collection
        current_cluster_names = self.db.surveillance_clusters.distinct("_id")

        print(subset_dataframe)

        # cluster names are of the form verified_facility-wgsId-ST-#. If there are matches for the first three, we need to index the # field by one
        try:
            cluster_facility = str(subset_dataframe["verifiedFacilityName"].loc[subset_dataframe["verifiedFacilityName"].first_valid_index()])
        except KeyError:  # no patient data was available for this cluster, so we will need to get the facility name from somewhere else
            self.logger.log("None of the isolates in this cluster contain a valid verifiedFacilityName field value")
            if query_yes_no("Would you like to add one manually?"):
                cluster_facility = input("Please enter the facility name to use: ").upper()
            else:
                cluster_facility = "unknown"
        cluster_wgs_id = str(subset_dataframe["wgsId"].loc[subset_dataframe["wgsId"].first_valid_index()])

        # for cluster st, we want to replace "-" with "unknown|novel for clarity"
        cluster_st = str(subset_dataframe["mlst"].loc[subset_dataframe["mlst"].first_valid_index()])
        if cluster_st == "-":
            cluster_st = "UNKNOWN|NOVEL"

        # contruct the cluster name string prefix
        cluster_name_prefix = "-".join([cluster_facility, cluster_wgs_id, cluster_st]).replace(" ", "_")
        self.logger.log("The cluster name prefix for this cluster: {}".format(log.bold(cluster_name_prefix)))

        # compare the cluster name prefix to the current cluster names. If it appears, we set the # to the len of the existing list +1
        matching_current_cluster_names = [cluster_name for cluster_name in current_cluster_names if cluster_name_prefix in cluster_name]
        cluster_name = "-".join([cluster_name_prefix, str(len(matching_current_cluster_names)+1)])
        
        # pick a cluster nickname
        cluster_nick_name = None
        while not cluster_nick_name:
            potential_nick_name = generate_slug(3)
            while potential_nick_name in self.db.surveillance_clusters.distinct("clusterNickNames"):
                potential_nick_name = generate_slug(3)
            cluster_nick_name = potential_nick_name
            #if query_yes_no("Accept {} as the cluster nick name?".format(log.bold(potential_nick_name)), default="yes"):
             #   cluster_nick_name = potential_nick_name

        # create the new surveillance_clusters collection document
        date_added = datetime.datetime.now()
        isolates = []
        isolate_entries = subset_dataframe.loc[:,["mrsnId", "patientsCollectionId"]].to_dict(orient="index")
        for v in isolate_entries.values():
            v["dateAdded"] = date_added
            isolates.append(v)
        
        new_surveillance_cluster = {
            "_id": cluster_name,
            "clusterNickName": cluster_nick_name,
            "isActive": True,
            "dateCreated": date_added,
            "isolates": isolates,
            "cgmlstDistanceMatrix": cluster_subset_dataframe.to_dict(orient="records")
        }

        self.db.surveillance_clusters.insert_one(new_surveillance_cluster)

        # update the context dataframe with the new values of cluster_name and cluster_nick_name
        for sample in subset_dataframe["mrsnId"].to_list():
            self.context_dataframe["clusterName"].loc[self.context_dataframe["mrsnId"] == sample] = cluster_name
            self.context_dataframe["clusterNickName"].loc[self.context_dataframe["mrsnId"] == sample] = cluster_nick_name

        # TODO log a summary of date range, facilities, and previous reports for the new cluster

    def update_surveillance_cluster_document(self, cluster_name, subset_dataframe):
        """
        For clusters with an existing document in the bi-db's surveillance_clusters collection update the isolates list and cgmlstDistanceMatrix fields
        """
        self.logger.log_section_header("Updating the entry for {} in BI-DB's surveillance_clusters collection".format(cluster_name))
        self.logger.log_explanation("Documents in the seqeuenced_isolates collection will be updated to reflect the current outbreakClusterName value")

        # get the subset of the context dataframe that pertains to the cluster_name
        cluster_subset_dataframe = self.distance_matrix_manipulation_object.generate_subset_distance_matrix_dataframe(cluster_name, self.context_dataframe)

        # add isolate information for each isolate in the cluster that is not already present
        new_isolate_dataframe = subset_dataframe.loc[subset_dataframe["isNew"] == True, ["mrsnId", "patientsCollectionId"]]
        date_added = datetime.datetime.now()

        for index in new_isolate_dataframe.index:
            new_isolate = {
                "mrsnId": new_isolate_dataframe.at[index, "mrsnId"],
                "patientsCollectionId": new_isolate_dataframe.at[index, "patientsCollectionId"],
                "dateAdded": date_added,
            }
            self.db.surveillance_clusters.update_one({"_id": cluster_name}, {"$addToSet": {"isolates": new_isolate}})

            # update the value for clusterName/clusterNickName for this isolate in the context dataframe
            cluster_nick_name = str(subset_dataframe["clusterNickName"].loc[subset_dataframe["clusterNickName"].first_valid_index()])

            self.context_dataframe.at[index, "clusterName"] = cluster_name
            self.context_dataframe.at[index, "clusterNickName"] = cluster_nick_name
        
        # set the cgmlstDistanceMatrix field in the surveillance_clusters collection document to the new subset dataframe
        self.db.surveillance_clusters.update_one({"_id": cluster_name}, {"$set": {"cgmlstDistanceMatrix": cluster_subset_dataframe.to_dict(orient="records")}})
        
        # TODO log a summary of date range, facilities, and previous reports for the updated cluster

    def merge_surveillance_cluster_documents(self, subset_dataframe):
        """
        When a cgMLST cluster is associated with 2+ entries in the bi-db's surveillance_clusters collection this method is used to merge the relevant fields from the cluster
        being retired into the one that is being expanded. It then sets the isActive field in the retired cluster to False
        """
        self.logger.log_section_header("Merging entries in BI-DB's surveillance_clusters collection")
        self.logger.log_explanation("MIGHT cluster has detected that a cluster contains isolates that were previously being tracked by 2+ documents in the surveillance_clusters collection. The isolates will be merged into the earliest cluster and the other clusters will be set to inactive")

        # determine the earliest cluster based on the collection date of the earliest isolate in the current cluster
        earliest_index = subset_dataframe.loc[subset_dataframe["mrsnId"].first_valid_index()]
        earliest_mrsnid = str(earliest_index["mrsnId"])
        earliest_cluster = str(earliest_index["clusterName"])
        earliest_cluster_nick_name = str(earliest_index["clusterNickName"])

        self.logger.log("Isolate {} from cluster {} has the earliest collection date, so all isolates will be merged into this cluster".format(earliest_mrsnid, earliest_cluster))
        
        clusters_to_deactivate = [cluster for cluster in subset_dataframe["clusterName"].to_list() if cluster != earliest_cluster]
        self.logger.log("The following clusters will be merged. The associated bi-db surveillance_clusters documents will be set to inactive")
        for cluster in clusters_to_deactivate:
            self.logger.log("\t{}".format(cluster))

        # gather all of the information from the isolates embedded documents field for each cluster in clusters_to_deactivate
        for cluster in clusters_to_deactivate:

            # get the info from the isolates field, and add the cluster to be inactivated to the priorClusters field
            deactivated_isolates = subset_dataframe.loc[subset_dataframe["clusterName"] == cluster].to_dict(orient="index")
            for v in deactivated_isolates.values():
                if v["priorClusters"] != np.nan:
                    v["priorClusters"] = "|".join([v["priorClusters"], cluster])
                else:
                    v["priorClusters"] = cluster

                #insert the entry into the active clusters isolates list
                self.db.surveillance_clusters.update_one({"_id": earliest_cluster}, {"$addToSet": {"isolates": v}})

                # update the clusterName, clusterNickname, and priorClusters fields for this sample in the context dataframe
                self.context_dataframe["clusterName"].loc[self.context_dataframe["mrsnId"] == v["mrsnId"]] = earliest_cluster
                self.context_dataframe["clusterNickName"].loc[self.context_dataframe["mrsnId"] == v["mrsnId"]] = earliest_cluster_nick_name
                self.context_dataframe["priorClusters"].loc[self.context_dataframe["mrsnId"] == v["mrsnId"]] = v["priorClusters"]

            # set the clusters isActive field to False
            self.db.surveillance_clusters.update_one({"_id": cluster}, {"$set": {"isActive": False}})

        # use the update_surveillance_cluster_document() method to handle any new isolates
        self.update_surveillance_cluster_document(earliest_cluster, subset_dataframe)


#################################################################################################################################################
#
#  Class PickleJar
#
#   Manages the storage and retrieval of mrsnId to patientCollectionId relationships that are determined during the course of a run
#
#################################################################################################################################################
class PickleJar:

    def __init__(self, output_directory):

        self.output_directory = output_directory

        self.patient_collection_ids = None  # dictionary of user-curated patients collection _id's for ambiguous samples. It is of form mrsn_id: [patients_collection_id, original chcs_accession from sequenced_isolates collection]

        # pickles in the jar
        self.patient_collection_ids_pickle  = self.output_directory / "user-curated-patient-collection-ids.pickle"  # pickled dictionary of user-curated patients collection _id's for ambiguous samples

    def write_a_pickle(self, key, value):
        """
        rewrite the pickle to include new information.
        """
        if not self.patient_collection_ids:
            self.patient_collection_ids = {}

        self.patient_collection_ids[key] = value

        with open(self.patient_collection_ids_pickle, 'wb') as f:
            pickle.dump(self.patient_collection_ids, f)

    def read_a_pickle(self):
        """
        try to read in an existing pickle if the file is present
        """
        if self.patient_collection_ids_pickle.exists():
            with open(self.patient_collection_ids_pickle, 'rb') as f:
                self.patient_collection_ids = pickle.load(f)


def pick_from_a_list(prompt, choices, logger):
    """
    present the user with a list of choices and ask them to choose one

    input is a list of choices and a prompt. Returns one of the choices as selected by the user
    """

    logger.log("\nUser intervention required!")

    enumerated_choices = [(str(choice[0]),choice[1]) for choice in enumerate(choices, start=1)]
    enumerated_choices.append(("n", None))

    user_choice = None
    while user_choice not in [choice[0] for choice in enumerated_choices]:
        logger.log(prompt + " or select n to select none of these")
        for option in enumerated_choices:
            if option[0] == 'n':
                logger.log("n - None of these documents match")
            else:
                logger.log("{} - Select Document #{}".format(str(option[0]), str(option[0])))
                
        
        user_choice = input().lower()

        if user_choice not in [choice[0] for choice in enumerated_choices]:
            logger.log("Not an acceptable answer, try again!")
    
    if user_choice != "n":  # user made a valid choice from the list that was offered
        user_choice = int(user_choice)
        return enumerated_choices[user_choice - 1][1]
    else:
        return None


def log_a_list(a_list, a_logger):
    """
    log out a list
    """
    for element in a_list:
        try:
            a_logger.log(str(element))
        except TypeError:
            print(str(element))
            raise



#######################################################################################################################
#
# generate_patient_aware_potential_transmission_itol_dataset():
#   Write all entries from a multi-index dictionary to output
#       a) Write output to potential_transmission_pairs.csv as "isolate 1, isolate 2, allelic distance"
#       b) Check for the iTOL template file
#       c) Write potential transmission data to potential_transmission_iTOL_dataset.txt
#       d) NEW IN 1.2.7: color code the potential_transmission_iTOL_dataset.txt entries based on whether or not the two
#          isolates came from the same patient 
#
#######################################################################################################################
def generate_patient_aware_potential_transmission_itol_dataset(potential_transmission_dict, outbreak_cluster_dataframe,
                                                 patient_aware_potential_transmission_itol_dataset_txt_file, logger=None,
                                                 debug=False):
    logger.log_section_header('Generating Patient Aware Potential Transmission Dataset for iTOL')
    logger.log(
        log.dim('   a) Check for the iTOL template file'))
    logger.log(log.dim('   b) Establish color code for links based on whether or not the two isolates came from the same or distinct patients'))
    logger.log(log.dim('   c) Write potential transmission data to patient_aware_potential_transmission_iTOL_dataset.txt'))

    try:
        assert potential_transmission_dict is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg potential_transimission_dict is required'))
        raise

    itol_template_file = Path(__file__).parent.parent / 'resources' / 'itol_connection_template.txt'

    try:
        assert itol_template_file.exists()
    except AssertionError:
        logger.log(log.bold_red('ERROR: Failed to locate template file: ' + str(itol_template_file)))
        raise

    logger.log('\nThe itol connection template file is: ' + log.green(str(itol_template_file)))
    shutil.copyfile(itol_template_file, patient_aware_potential_transmission_itol_dataset_txt_file)

    try:
        assert patient_aware_potential_transmission_itol_dataset_txt_file.exists()
    except AssertionError:
        logger.log(
            log.bold_red('ERROR: Failed to locate template file: ' + str(patient_aware_potential_transmission_itol_dataset_txt_file)))
        raise

    logger.log(
        'Now generating itol connection dataset: ' + log.green(str(patient_aware_potential_transmission_itol_dataset_txt_file)))
    
    outbreak_cluster_dataframe.set_index('Sample ID', inplace=True)

    if debug:
        print(outbreak_cluster_dataframe)

    # compare the patient IDs for each isolate in the potential_transmission_dict, set the connection type
    # with the appropriate connecting line feature (grey for same patient, red for different patient)
    same_patient_connection = '5\trgba(175, 175, 175, 0.5)\tnormal'
    first_different_patient_connection = '5\trgba(255, 15, 15, 0.8)\tnormal'
    second_different_patient_connection = '5\trgba(255, 244, 89, 0.2)\tnormal'

    patient_aware_potential_transmission_list = []
    same_patient_connection_count = 0
    different_patient_connection_count = 0
    patient_to_patient_connections = {}  # dict where keys are two patient ID's as a set, and values are lists of form (isolate 1, isolate 2, alleic distance)
    isolate_keep_list = []  # list of isolates that will be retained in the deduplicated tree
    isolate_remove_list = []  # list of isolates that should NOT be retained in the deduplicated tree

    for isolate_pair, allelic_distance in potential_transmission_dict.items():
        patient_id_1 = outbreak_cluster_dataframe.loc[isolate_pair[0], 'Patient ID']  # patient associated with the first isolate in the isolate pair
        patient_id_2 = outbreak_cluster_dataframe.loc[isolate_pair[1], 'Patient ID']  # patient associated with the second isolate in the isolate pair
        cluster_1 = outbreak_cluster_dataframe.loc[isolate_pair[0], 'cluster']  # cluster associated with the first isolate in the isolate pair
        cluster_2 = outbreak_cluster_dataframe.loc[isolate_pair[1], 'cluster']  # cluster associated with the second isolate in the isolate pair
        
        if  patient_id_1 == patient_id_2:  # case where both isolates in the pair are from the same patient (serial isolates)
            patient_aware_potential_transmission_list.append([isolate_pair[0], isolate_pair[1], same_patient_connection])
            same_patient_connection_count +=1
            for isolate in [isolate_pair[0], isolate_pair[1]]:  # we tentatively add serial isolates to the isolate_remove_list. They can be recovered later if they are found on the isolate_keep_list
                isolate_remove_list.append(isolate)
        else:  # case where the isolates in the pair come from different patients
            patient_set = frozenset([patient_id_1, patient_id_2])
            isolate_distance_list = [isolate_pair[0], isolate_pair[1], int(allelic_distance)]
            if cluster_1 not in patient_to_patient_connections.keys():
                patient_to_patient_connections[cluster_1] = {patient_set: [isolate_distance_list]} 
            else:
                if not patient_set in patient_to_patient_connections[cluster_1].keys():
                    patient_to_patient_connections[cluster_1][patient_set] = [isolate_distance_list]
                else:
                    patient_to_patient_connections[cluster_1][patient_set].append(isolate_distance_list)

            #patient_aware_potential_transmission_list.append((isolate_pair[0], isolate_pair[1], different_patient_connection))
            different_patient_connection_count +=1


    # parse the patient_to_patient_connections dict to find the smallest connection. We retain the shortest connection isolates for each patient in isolate_keep_list
    for cluster, patient_set_dict in patient_to_patient_connections.items():
        for patient_set, occurences in patient_set_dict.items():
            if len(occurences) >1:  # case where more than one set of isolates are present in the cluster for two patients, so only the most highly related will be kept
                occurences.sort(key=lambda x: x[2]) # sort the occurences in ascending order of allelic distance
                print("The first example of {} in cluster {} is {} | {} | {} ".format(str(cluster), str(patient_set), occurences[0][0], occurences[0][1], str(occurences[0][2])))
                occurences[0][2] = first_different_patient_connection
                for isolate in [occurences[0][0], occurences[0][1]]:
                        isolate_keep_list.append(isolate)
                for i in occurences[1:]:
                    #print("The NOT FIRST example of {} in cluster {} is {} | {} | {} ".format( str(patient_set), str(cluster), i[0], i[1], str(i[2])))
                    i[2] = second_different_patient_connection
                    for isolate in [i[0], i[1]]:
                        isolate_remove_list.append(isolate)
                for occurence in occurences:
                    patient_aware_potential_transmission_list.append(occurence)
                patient_aware_potential_transmission_list.append(occurences[0])
            else:  # case where a the interface between two patients in a cluster is a single pair of isolates, so both will be kept
                print("The SINGLE example of {} in cluster {} is {} | {} | {} ".format(str(patient_set), str(cluster), occurences[0][0], occurences[0][1], str(occurences[0][2])))
                occurences[0][2] = first_different_patient_connection
                for isolate in [occurences[0][0], occurences[0][1]]:
                        isolate_keep_list.append(isolate)
                patient_aware_potential_transmission_list.append(occurences[0])

    # finalize the isolate remove list by ensuring nothing in the isolate_keep_list is in it
    isolate_remove_list = list(set([isolate for isolate in isolate_remove_list if isolate not in isolate_keep_list]))

    
    
    #print(patient_to_patient_connections)
    # for k,v in patient_to_patient_connections.items():
    #     print(k)
    #     for key, value in v.items():
    #         print(key)
    #         print(value)
    
    # print('\n'.join([' | '.join(list) for list in patient_aware_potential_transmission_list]))

    logger.log('Detected a total of {} same patient connections'.format(str(same_patient_connection_count)))
    logger.log('Detected a total of {} different patient connections'.format(str(different_patient_connection_count)))

    with open(patient_aware_potential_transmission_itol_dataset_txt_file, 'a') as dataset_file:
        for entry in reversed(patient_aware_potential_transmission_list):
            dataset_file.write('{}\t{}\t{}\n'.format(str(entry[0]), str(entry[1]), str(entry[2])))

    if debug:  # print the dataset file to the terminal for inspection
        with open(patient_aware_potential_transmission_itol_dataset_txt_file, 'r') as debug_file:
            for line in debug_file.readlines():
                print(line.strip())
    
    return isolate_remove_list


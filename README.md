# MIGHT

MIGHT: MRSN Integrated Genome Handling Tool for bacterial clinical isolates

## Contents
* [Introduction](#introduction)
* [Installation](#installation)
  * [Conda Installation](#conda-installation)
* [Usage](#usage)

## Introduction

MIGHT was developed as a way to automate many of the standard bioinformatics tasks that the MRSN
performs as part of its surveillance mission.

Brief summary of the available workflows:

1. Run [bcl2fastq](https://support.illumina.com/downloads/bcl2fastq-conversion-software-v2-20.html) to demultiplex Illumina paired-end read data from MiSeq/Nextseq data
2. Run [Kraken2](https://github.com/DerrickWood/kraken2) to get species ID and identify possible sample contamination using the guided kraken report analysis tool call-id
3. Trim reads using [bbduk](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/bbduk-guide/) and generate short read draft assemblies with [Newbler](https://en.wikipedia.org/wiki/Newbler)
4. Run the MIGHT AMR gene detection tool, a hybrid read/assembly AMR gene identification tool (uses [ARIBA](https://github.com/sanger-pathogens/ariba) and [AMRFinderPlus](https://www.ncbi.nlm.nih.gov/pathogens/antimicrobial-resistance/AMRFinder/))
5. Perform MLST assignment using [mlst](https://github.com/tseemann/mlst)
6. Import relevant metadata from an Excel spreadsheet
7. Summarize ID, assembly, metadata, and MLST results

## Installation

This script was designed to be installed using conda

### Conda Installation

MIGHT is designed to be installed using the conda package manager. To install:

```
conda install -c b.corey mrsn-might
```

Given the number of dependencies associated with MIGHT, I highly recommend that you install it in a conda env to avoid
conflicts

```
conda create --name might -c b.corey mrsn-might
```

### Docker Installation

In development... stay tuned!

## Usage

might
```
            .___  ___.  __    _______  __    __  .__________.
            |   \/   | |  |  /  _____||  |  |  | |          |
            |  \  /  | |  | |  |  __  |  |__|  | `---|  |---`
            |  |\/|  | |  | |  | |_ | |   __   |     |  |     
            |  |  |  | |  | |  |__| | |  |  |  |     |  |     
            |__|  |__| |__|  \______| |__|  |__|     |__|     
     
		MRSN Intergrated Genome Handling Tool
			Brendan Corey
				v1.1.1


usage: might <command> <input file> [options]

MRSN Integrated Genome Handling Tool 

optional arguments:
  -h, --help      show this help message and exit

Available commands:
  
    bcl2fastq     Perform bcl2fastq to generate sample read files
    kraken2       Perform kraken2 analysis for sample read files
    species-caller
                  Perform species-caller analysis for sample read files
    amr           Perform amr analysis for samples using reads/assemblies/both
    mlst          Perform mlst analysis for samples using assembly files
    version       Get versions and exit
```

###bcl2fastq

```
            .___  ___.  __    _______  __    __  .__________.
            |   \/   | |  |  /  _____||  |  |  | |          |
            |  \  /  | |  | |  |  __  |  |__|  | `---|  |---`
            |  |\/|  | |  | |  | |_ | |   __   |     |  |     
            |  |  |  | |  | |  |__| | |  |  |  |     |  |     
            |__|  |__| |__|  \______| |__|  |__|     |__|     
     
		MRSN Intergrated Genome Handling Tool
			Brendan Corey
				v1.1.1


usage: might bcl2fastq [options] <output_directory> <run_directory> <sample_sheet>

command "bcl2fastq" uses bcl2fastq to generate sample read files

positional arguments:
  output               path to the directory where output is/will be stored
  run_directory        path to illumina run output directory
  sample_sheet         path to the SampleSheet.csv used for sample
                       demultiplexing

optional arguments:
  -h, --help           show this help message and exit

Optional Arguments:
  --cores CORES        number of cores to use during sample processing
  --force              force overwrite of existing output
  --verbosity {0,1,2}  the level of reporting done to the terminal window [1]

```

###kraken2

```
            .___  ___.  __    _______  __    __  .__________.
            |   \/   | |  |  /  _____||  |  |  | |          |
            |  \  /  | |  | |  |  __  |  |__|  | `---|  |---`
            |  |\/|  | |  | |  | |_ | |   __   |     |  |     
            |  |  |  | |  | |  |__| | |  |  |  |     |  |     
            |__|  |__| |__|  \______| |__|  |__|     |__|     
     
		MRSN Intergrated Genome Handling Tool
			Brendan Corey
				v1.1.1


usage: might kraken2 [options] <output_directory> <reads_directory> <kraken2_database_directory

command "kraken2" performs kraken2 analysis for samples from read
files

positional arguments:
  output                path to the directory where output is/will be stored
  reads                 path to the directory where the reads files are stored
  kraken2_database      path to the directory housing the kraken2 database
                        files

optional arguments:
  -h, --help            show this help message and exit

Optional Arguments:
  --ramdisk RAMDISK     path to the ramdisk where the kraken2 ramdisk is/will
                        be mounted
  --sample-list SAMPLE_LIST
                        path to the file containing the list of samples to be
                        analyzed [autodetect]
  --cores CORES         number of cores to use during sample processing
  --force               force overwrite of existing output
  --verbosity {0,1,2}   the level of reporting done to the terminal window [1]
```

###species-caller

```
            .___  ___.  __    _______  __    __  .__________.
            |   \/   | |  |  /  _____||  |  |  | |          |
            |  \  /  | |  | |  |  __  |  |__|  | `---|  |---`
            |  |\/|  | |  | |  | |_ | |   __   |     |  |     
            |  |  |  | |  | |  |__| | |  |  |  |     |  |     
            |__|  |__| |__|  \______| |__|  |__|     |__|     
     
		MRSN Intergrated Genome Handling Tool
			Brendan Corey
				v1.1.1


usage: might species-caller [options] <output_directory>

command "species-caller" performs interactive species
calling/contamination detection analysis for samples using the results from
kraken2 analysis

positional arguments:
  output                path to the directory where output is/will be stored

optional arguments:
  -h, --help            show this help message and exit

Optional Arguments:
  --sample-list SAMPLE_LIST
                        path to the file containing the list of samples to be
                        analyzed [autodetect]
  --force               force overwrite of existing output
  --verbosity {0,1,2}   the level of reporting done to the terminal window [1]
```

###amr

```
            .___  ___.  __    _______  __    __  .__________.
            |   \/   | |  |  /  _____||  |  |  | |          |
            |  \  /  | |  | |  |  __  |  |__|  | `---|  |---`
            |  |\/|  | |  | |  | |_ | |   __   |     |  |     
            |  |  |  | |  | |  |__| | |  |  |  |     |  |     
            |__|  |__| |__|  \______| |__|  |__|     |__|     
     
		MRSN Intergrated Genome Handling Tool
			Brendan Corey
				v1.1.1


usage: might amr [options] <output_directory>

command "amr" performs amr analysis for samples from
reads/assemblies/both

positional arguments:
  output                path to the directory where output is/will be stored

optional arguments:
  -h, --help            show this help message and exit

Optional Arguments:
  --reads READS         path to the directory where the read files are stored
  --assembly ASSEMBLY   path to the directory where the assembly files are
                        stored
  --analysis-type {reads,assembly,combination}
                        requested analysis type [combination]
  --sample-list SAMPLE_LIST
                        path to the file containing the list of samples to be
                        analyzed [autodetect]
  --cores CORES         number of cores to use during sample processing
  --force               force overwrite of existing output
  --keep {1,2}          Level of intermediate files to be retained after the
                        amr completes, 0 is the least and 2 is the most [2]
  --verbosity {0,1,2}   the level of reporting done to the terminal window [1]
```

###mlst

```
            .___  ___.  __    _______  __    __  .__________.
            |   \/   | |  |  /  _____||  |  |  | |          |
            |  \  /  | |  | |  |  __  |  |__|  | `---|  |---`
            |  |\/|  | |  | |  | |_ | |   __   |     |  |     
            |  |  |  | |  | |  |__| | |  |  |  |     |  |     
            |__|  |__| |__|  \______| |__|  |__|     |__|     
     
		MRSN Intergrated Genome Handling Tool
			Brendan Corey
				v1.1.1


usage: might mlst [options] <output_directory> <assembly_directory

command "mlst" performs mlst analysis for samples from assembly
files

positional arguments:
  output                path to the directory where output is/will be stored
  assembly              path to the directory where sample assembly files are
                        stored

optional arguments:
  -h, --help            show this help message and exit

Optional Arguments:
  --sample-list SAMPLE_LIST
                        path to the file containing the list of samples to be
                        analyzed [autodetect]
  --cores CORES         number of cores to use during sample processing
  --force               force overwrite of existing output
  --keep {1,2}          Level of intermediate files to be retained after the
                        mlst completes, 0 is the least and 2 is the most [2]
  --verbosity {0,1,2}   the level of reporting done to the terminal window [1]

```

#!/usr/bin/env python
# encoding: utf-8

import setuptools

with open("README.md", "r") as readme:
    long_description = readme.read()

setuptools.setup(
    name="mrsn-might",
    version="1.2.9",
    author="Brendan Corey",
    author_email="brendan.w.corey.ctr@mail.mil",
    description="MIGHT: MRSN Integrated Genome Handling Tool for bacterial clinical isolates",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bcorey/might-mrsn/",
    license="GPLv3",
    packages=setuptools.find_packages(),
    package_data={
        "might": [
            "resources/*.fa", 
            "resources/*.txt", 
            "resources/amr_tiers_and_families.xlsx", 
            "resources/mlst-download_pub_mlst", 
            "resources/mlst-make_blast_db"
            ]
    },
    entry_points={'console_scripts': ['might = might.run:might']
                  },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    python_requires='>=3.6',
    zip_safe=False
)